SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `refine_budget_additional` (IN `p_budget_additional_id` INT)  BEGIN
	DECLARE p_source_amount DECIMAL(17,2);
	DECLARE p_source_amount_new DECIMAL(17,2);
	DECLARE p_additional_amount DECIMAL(17,2);
	DECLARE p_is_approved_by_fa TINYINT;
	DECLARE p_budget_proposal_id INT;
	DECLARE p_budget_final_id INT;

	SELECT is_approved_by_fa, additional_amount, budget_final_id
	INTO p_is_approved_by_fa, p_additional_amount, p_budget_final_id
	FROM budget_additional
	WHERE id=p_budget_additional_id;
	
	IF p_is_approved_by_fa=1 THEN
		SELECT budget_proposal_id
		INTO p_budget_proposal_id
		FROM budget_final
		WHERE id=p_budget_final_id;
	
		SELECT amount
		INTO p_source_amount
		FROM budget_proposal
		WHERE id=p_budget_proposal_id;
		
		SET p_source_amount_new = p_source_amount + p_additional_amount;
		
		UPDATE budget_proposal
		SET amount=p_source_amount_new, is_additional=1
		WHERE id=p_budget_proposal_id;
		
	END IF;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `refine_proposal_transfer` (IN `p_budget_transfer_id` INT)  BEGIN
	DECLARE p_source_amount DECIMAL(17,2);
	DECLARE p_source_amount_new DECIMAL(17,2);
	DECLARE p_amount_destination DECIMAL(17,2);
	DECLARE p_amount_destination_new DECIMAL(17,2);
	DECLARE p_transfer_amount DECIMAL(17,2);
	DECLARE p_is_approved_by_fa TINYINT;
	DECLARE p_source_id INT;
	DECLARE p_destination_id INT;
	DECLARE p_source_proposal_id INT;
	DECLARE p_destination_proposal_id INT;

	SELECT is_approved_by_fa, amount, source, destination
	INTO p_is_approved_by_fa, p_transfer_amount, p_source_id, p_destination_id
	FROM budget_transfer
	WHERE id=p_budget_transfer_id;
	
	IF p_is_approved_by_fa=1 THEN
		SELECT amount, budget_proposal.id
		INTO p_source_amount, p_source_proposal_id
		FROM budget_proposal
		JOIN budget_final ON budget_proposal.id=budget_proposal_id
		WHERE budget_final.id=p_source_id;
		
		SELECT amount, budget_proposal.id
		INTO p_amount_destination, p_destination_proposal_id
		FROM budget_proposal
		JOIN budget_final ON budget_proposal.id=budget_proposal_id
		WHERE budget_final.id=p_destination_id;
		
		SET p_source_amount_new = p_source_amount - p_transfer_amount;
		SET p_amount_destination_new = p_amount_destination + p_transfer_amount;
		
		UPDATE budget_proposal
		SET amount=p_source_amount_new, is_transfer=1
		WHERE id=p_source_proposal_id;
		
		UPDATE budget_proposal
		SET amount=p_amount_destination_new, is_transfer=1
		WHERE id=p_destination_proposal_id;
		
	END IF;
 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `truncate_transaction_data` ()  BEGIN
	DELETE FROM po_revision_detail;
	DELETE FROM po_revision;
	DELETE FROM po_realization_actual;
	DELETE FROM po_payment;
	DELETE FROM po_realization;
	DELETE FROM po_pr_detail;
	DELETE FROM purchase_order;
	
	DELETE FROM dp_realization_plan;
	DELETE FROM dp_realization_actual;
	DELETE FROM dp_payment;
	DELETE FROM dp_realization;
	DELETE FROM dp_pr_detail;
	DELETE FROM direct_purchase;
	
	DELETE FROM pr_revision_detail;
	DELETE FROM pr_revision;
	DELETE FROM purchase_requisition_detail;
	DELETE FROM purchase_requisition;
	
	DELETE FROM budget_additional;
	DELETE FROM budget_reschedule;
	DELETE FROM budget_transfer;
	DELETE FROM budget_final;
	
	DELETE FROM budget_proposal_detail;
	DELETE FROM budget_proposal;
	
	DELETE FROM notification;
END$$

DELIMITER ;

CREATE TABLE `acl_access` (
  `id` int(11) NOT NULL,
  `acl_list_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `is_access` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `acl_list` (
  `id` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `module_item` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `bank_detail` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `account_number` varchar(20) NOT NULL,
  `holder_name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget` (
  `id` int(11) NOT NULL,
  `budget_sub_group_id` int(11) NOT NULL,
  `number` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `is_packaging_component` tinyint(4) DEFAULT '0',
  `comment` text,
  `amount` decimal(17,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='import excel bersama budget_detail\n\nsebuah budget dapat terdiri dari beberapa budget detail, misalnya pengadaan satu set alat tulis dapat terdiri dari beberapa barang, antara lain pensil, ballpoin, penghapus, penggaris, dsb';

CREATE TABLE `budget_additional` (
  `id` int(11) NOT NULL,
  `budget_final_id` int(11) NOT NULL,
  `additional_amount` decimal(17,2) NOT NULL,
  `is_approved_by_manager` tinyint(4) NOT NULL,
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_approved_by_fa` tinyint(4) NOT NULL,
  `approved_by_fa_time` datetime DEFAULT NULL,
  `time` datetime NOT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DELIMITER $$
CREATE TRIGGER `refine additional budget` AFTER UPDATE ON `budget_additional` FOR EACH ROW CALL refine_budget_additional(old.id)
$$
DELIMITER ;

CREATE TABLE `budget_category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_category_period` (
  `id` int(11) NOT NULL,
  `budget_category_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `active_period` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_final` (
  `id` int(11) NOT NULL,
  `budget_proposal_id` int(11) NOT NULL,
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_group` (
  `id` int(11) NOT NULL,
  `budget_category_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='data master';

CREATE TABLE `budget_limit` (
  `id` int(11) NOT NULL,
  `budget_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `amount` decimal(17,2) NOT NULL,
  `currency_id` int(11) DEFAULT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_period` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `period_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_proposal` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `budget_period_id` int(11) NOT NULL,
  `budget_id` int(11) NOT NULL,
  `amount` decimal(17,2) DEFAULT NULL,
  `amount_used` decimal(17,2) NOT NULL,
  `prev_amount_actual` decimal(17,2) NOT NULL DEFAULT '0.00',
  `comment` text,
  `is_approved_by_manager` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_approved_by_fa` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_fa_time` datetime DEFAULT NULL,
  `is_reschedule` tinyint(4) NOT NULL DEFAULT '0',
  `is_additional` tinyint(4) DEFAULT '0',
  `is_rejected` tinyint(4) DEFAULT '0',
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `is_final` tinyint(4) DEFAULT '0',
  `is_transfer` tinyint(4) NOT NULL DEFAULT '0',
  `is_expired` tinyint(4) NOT NULL DEFAULT '0',
  `active_until` datetime NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='mungkin diperlukan agar hanya benar2 budget yang disetujui yang ada pada budget_detail';

CREATE TABLE `budget_proposal_detail` (
  `id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` varchar(45) DEFAULT NULL,
  `price_estimation` decimal(17,2) DEFAULT NULL,
  `carline_id` int(11) DEFAULT NULL,
  `comment` text,
  `is_approved_by_manager` tinyint(4) DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_approved_by_fa` tinyint(4) DEFAULT '0',
  `approved_by_fa_time` datetime DEFAULT NULL,
  `budget_proposal_id` int(11) NOT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_reschedule` (
  `id` int(11) NOT NULL,
  `budget_final_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `budget_sub_group` (
  `id` int(11) NOT NULL,
  `budget_group_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='data master';

CREATE TABLE `budget_transfer` (
  `id` int(11) NOT NULL,
  `source` int(11) NOT NULL,
  `destination` int(11) NOT NULL,
  `amount` decimal(17,2) NOT NULL,
  `time` datetime NOT NULL,
  `is_approved_by_source_manager` tinyint(4) NOT NULL,
  `approved_by_source_manager_time` datetime DEFAULT NULL,
  `is_approved_by_destination_manager` tinyint(4) NOT NULL,
  `approved_by_destination_manager_time` datetime DEFAULT NULL,
  `is_approved_by_fa` tinyint(4) NOT NULL,
  `approved_by_fa_time` datetime DEFAULT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DELIMITER $$
CREATE TRIGGER `refine proposal transfer` AFTER UPDATE ON `budget_transfer` FOR EACH ROW CALL refine_proposal_transfer(old.id)
$$
DELIMITER ;

CREATE TABLE `carline` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `is_base_currency` tinyint(4) DEFAULT '0',
  `current_rate` decimal(21,6) DEFAULT NULL,
  `last_updated_rate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `currency_detail` (
  `id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `current_rate` decimal(21,6) NOT NULL,
  `is_base_currency` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `is_fa` tinyint(4) DEFAULT '0',
  `is_ga_gs` tinyint(4) DEFAULT '0',
  `is_under_dfm` tinyint(4) DEFAULT '1',
  `is_under_fm` tinyint(4) DEFAULT '1',
  `is_under_presdir` tinyint(4) DEFAULT '1',
  `is_non_department` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='data master';

CREATE TABLE `direct_purchase` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `is_approved_by_lp_manager` tinyint(4) DEFAULT '0',
  `approved_by_lp_manager_time` datetime DEFAULT NULL,
  `comment` text NOT NULL,
  `submission_time` datetime DEFAULT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `is_received` tinyint(4) NOT NULL DEFAULT '0',
  `is_delivered` tinyint(4) NOT NULL DEFAULT '0',
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `amount_plan` decimal(17,2) NOT NULL,
  `amount_actual` decimal(17,2) NOT NULL,
  `amount_estimation` decimal(17,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dp_payment` (
  `id` int(11) NOT NULL,
  `dp_realization_id` int(11) NOT NULL,
  `bank_detail_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `payment_document` varchar(75) NOT NULL,
  `time` datetime NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dp_pr_detail` (
  `id` int(11) NOT NULL,
  `direct_purchase_id` int(11) NOT NULL,
  `purchase_requisition_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dp_realization` (
  `id` int(11) NOT NULL,
  `direct_purchase_id` int(11) NOT NULL,
  `document` varchar(45) DEFAULT NULL,
  `paid_to` varchar(75) NOT NULL,
  `amount` decimal(17,2) NOT NULL,
  `is_approved_by_manager` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `is_rejected` tinyint(4) NOT NULL DEFAULT '0',
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `is_paid` tinyint(4) NOT NULL DEFAULT '0',
  `is_additional` tinyint(4) NOT NULL DEFAULT '0',
  `is_voucher_receiving` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dp_realization_actual` (
  `id` int(11) NOT NULL,
  `dp_pr_detail_id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `received` int(11) NOT NULL DEFAULT '0',
  `delivered` int(11) NOT NULL DEFAULT '0',
  `price` decimal(17,2) DEFAULT NULL,
  `is_fixed` tinyint(4) NOT NULL DEFAULT '0',
  `carline_id` int(11) NOT NULL,
  `rev_tax_name` varchar(255) NOT NULL,
  `rev_tax_rate` float NOT NULL,
  `rev_tax_amount` decimal(20,2) NOT NULL,
  `vat_name` varchar(255) NOT NULL,
  `vat_rate` float NOT NULL,
  `vat_amount` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `dp_realization_plan` (
  `id` int(11) NOT NULL,
  `dp_pr_detail_id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` decimal(17,2) DEFAULT NULL,
  `carline_id` int(11) NOT NULL,
  `rev_tax_name` varchar(255) NOT NULL,
  `rev_tax_rate` float NOT NULL,
  `rev_tax_amount` decimal(20,2) NOT NULL,
  `vat_name` varchar(255) NOT NULL,
  `vat_rate` float NOT NULL,
  `vat_amount` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `franco` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `goods_receipt_dp` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `direct_purchase_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `goods_receipt_dp_detail` (
  `id` int(11) NOT NULL,
  `goods_receipt_dp_id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `goods_receipt_po` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `comment` varchar(45) DEFAULT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `goods_receipt_po_detail` (
  `id` int(11) NOT NULL,
  `goods_receipt_po_id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `goods_service` (
  `id` int(11) NOT NULL,
  `budget_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `specification` text,
  `currency_id` int(11) DEFAULT NULL,
  `is_po` tinyint(4) NOT NULL DEFAULT '0',
  `is_dp` tinyint(4) NOT NULL DEFAULT '0',
  `type` enum('goods','service') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category` varchar(45) NOT NULL,
  `type` varchar(20) NOT NULL COMMENT 'success, warning, danger',
  `title` varchar(200) NOT NULL,
  `target_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `is_read` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `notification_message` (
  `id` int(11) NOT NULL,
  `key` varchar(75) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `is_non_bank` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `period` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL,
  `is_future` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `po_payment` (
  `id` int(11) NOT NULL,
  `po_realization_id` int(11) NOT NULL,
  `bank_detail_id` int(11) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `payment_document` varchar(75) NOT NULL,
  `comment` text NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DELIMITER $$
CREATE TRIGGER `update realization paid to 0` AFTER DELETE ON `po_payment` FOR EACH ROW BEGIN

UPDATE po_realization
SET is_paid=0
WHERE id = old.po_realization_id;

END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update realization paid to 1` AFTER INSERT ON `po_payment` FOR EACH ROW BEGIN

UPDATE po_realization
SET is_paid=1
WHERE id = new.po_realization_id;

END
$$
DELIMITER ;

CREATE TABLE `po_pr_detail` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `purchase_requisition_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `po_realization` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `supplier_invoice_number` varchar(45) DEFAULT NULL,
  `invoice` varchar(75) NOT NULL,
  `is_final_payment` tinyint(4) NOT NULL DEFAULT '0',
  `is_approved_by_manager` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `prepared_by_user_id` int(11) DEFAULT NULL,
  `approved_by_user_id` int(11) DEFAULT NULL,
  `verified_by_user_id` int(11) DEFAULT NULL,
  `is_rejected` tinyint(4) NOT NULL DEFAULT '0',
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `is_paid` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `po_realization_actual` (
  `id` int(11) NOT NULL,
  `po_pr_detail_id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `received` int(11) NOT NULL DEFAULT '0',
  `delivered` int(11) NOT NULL DEFAULT '0',
  `price` decimal(17,2) DEFAULT NULL,
  `carline_id` int(11) NOT NULL,
  `rev_tax_name` varchar(255) NOT NULL,
  `rev_tax_rate` float NOT NULL,
  `rev_tax_amount` decimal(20,2) NOT NULL,
  `vat_name` varchar(255) NOT NULL,
  `vat_rate` float NOT NULL,
  `vat_amount` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `po_revision` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `is_approved_by_manager` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `is_rejected` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `po_revision_detail` (
  `id` int(11) NOT NULL,
  `po_revision_id` int(11) NOT NULL,
  `po_realization_actual_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` decimal(20,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pricelist` (
  `id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT '1',
  `market_price` decimal(17,2) DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `is_package` tinyint(4) DEFAULT '0',
  `add_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `revenue_tax_id` int(11) DEFAULT NULL,
  `value_added_tax_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pr_revision` (
  `id` int(11) NOT NULL,
  `purchase_requisition_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `is_approved_by_manager` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `is_rejected` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `pr_revision_detail` (
  `id` int(11) NOT NULL,
  `pr_revision_id` int(11) NOT NULL,
  `purchase_requisition_detail_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `purchase_order` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `date_submit` date DEFAULT NULL,
  `is_approved_by_lp_manager` tinyint(4) DEFAULT '0',
  `approved_by_lp_manager_time` datetime DEFAULT NULL,
  `refax_path` varchar(45) DEFAULT NULL,
  `term_id` int(11) DEFAULT NULL,
  `franco_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `estimated_time_arival` datetime DEFAULT NULL,
  `actual_time_arival` datetime DEFAULT NULL,
  `is_with_down_payment` tinyint(4) DEFAULT '0',
  `down_payment_amount` decimal(17,2) DEFAULT NULL,
  `amount_estimation` decimal(17,2) NOT NULL,
  `amount_realization` decimal(17,2) NOT NULL,
  `is_payment_complete` tinyint(4) NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `is_closed` tinyint(4) DEFAULT '0',
  `is_rejected` tinyint(4) DEFAULT '0',
  `is_confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `is_received` tinyint(4) NOT NULL DEFAULT '0',
  `is_delivered` tinyint(4) NOT NULL DEFAULT '0',
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0',
  `is_ontime` tinyint(4) NOT NULL DEFAULT '0',
  `is_revision` tinyint(4) NOT NULL DEFAULT '0',
  `is_checked` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='enak name atau code?';

CREATE TABLE `purchase_requisition` (
  `id` int(11) NOT NULL,
  `no` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `budget_final_id` int(11) NOT NULL,
  `price_estimation` decimal(17,2) NOT NULL,
  `date` date DEFAULT NULL,
  `is_approved_by_manager` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_approved_by_dfm` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_dfm_time` datetime DEFAULT NULL,
  `is_approved_by_fm` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_fm_time` datetime DEFAULT NULL,
  `is_approved_by_presdir` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_presdir_time` datetime DEFAULT NULL,
  `is_approved_by_fa` tinyint(4) NOT NULL DEFAULT '0',
  `approved_by_fa_time` datetime DEFAULT NULL,
  `comment` text,
  `is_closed` tinyint(4) DEFAULT '0',
  `is_direct_purchase` tinyint(4) DEFAULT '0',
  `is_purchase_order` tinyint(4) DEFAULT '0',
  `is_rejected` tinyint(4) NOT NULL DEFAULT '0',
  `is_canceled` tinyint(4) DEFAULT '0',
  `is_revision` tinyint(4) NOT NULL DEFAULT '0',
  `is_checked` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `purchase_requisition_detail` (
  `id` int(11) NOT NULL,
  `purchase_requisition_id` int(11) NOT NULL,
  `goods_service_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `used_quantity` int(11) NOT NULL,
  `available_quantity` int(11) NOT NULL,
  `price_estimation` decimal(17,2) DEFAULT NULL,
  `carline_id` int(11) DEFAULT NULL,
  `required_date` date NOT NULL,
  `comment` text,
  `is_approved_by_manager` tinyint(4) DEFAULT '0',
  `approved_by_manager_time` datetime DEFAULT NULL,
  `is_approved_by_dfm` tinyint(4) DEFAULT '0',
  `approved_by_dfm_time` datetime DEFAULT NULL,
  `is_approved_by_fm` tinyint(4) DEFAULT '0',
  `approved_by_fm_time` datetime DEFAULT NULL,
  `is_approved_by_presdir` tinyint(4) DEFAULT '0',
  `approved_by_presdir_time` datetime DEFAULT NULL,
  `is_approved_by_fa` tinyint(4) NOT NULL,
  `approved_by_fa_time` datetime DEFAULT NULL,
  `is_rejected` tinyint(4) NOT NULL,
  `is_canceled` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `receiver` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `revenue_tax` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `rate` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `alias` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `is_exim` tinyint(4) NOT NULL DEFAULT '0',
  `is_ga_gs` tinyint(4) NOT NULL DEFAULT '0',
  `is_lp` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='data master';

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL,
  `supplier_category_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `address` text,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `phone3` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `authorized_person` varchar(45) DEFAULT NULL,
  `authorized_person_cellphone` varchar(45) DEFAULT NULL,
  `siup` varchar(45) DEFAULT NULL,
  `tdp` varchar(45) DEFAULT NULL,
  `npwp` varchar(45) DEFAULT NULL,
  `sppkp` varchar(45) DEFAULT NULL,
  `is_pkp` tinyint(4) DEFAULT '0',
  `siup_path` varchar(45) DEFAULT NULL,
  `tdp_path` varchar(45) DEFAULT NULL,
  `npwp_path` varchar(45) DEFAULT NULL,
  `sppkp_path` varchar(45) DEFAULT NULL,
  `skt_badan_path` varchar(45) DEFAULT NULL,
  `skt_pribadi_path` varchar(45) DEFAULT NULL,
  `non_pkp_path` varchar(45) DEFAULT NULL,
  `deadline_day` int(11) NOT NULL DEFAULT '7',
  `website` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `supplier_category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `term` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `unit` (
  `id` int(11) NOT NULL,
  `unit_category_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `unit_category` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(32) NOT NULL,
  `role` int(11) NOT NULL,
  `registration_date` datetime NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `is_administration` tinyint(4) DEFAULT NULL,
  `is_supervisor` tinyint(4) DEFAULT NULL,
  `profile_picture` varchar(255) DEFAULT NULL,
  `signature` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user_meta` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `section_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `is_administration` tinyint(4) NOT NULL DEFAULT '0',
  `is_supervisor` tinyint(4) NOT NULL DEFAULT '0',
  `profile_picture` varchar(75) NOT NULL,
  `signature` varchar(75) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `value_added_tax` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `rate` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `voucher_payment` (
  `id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `v_budget_additional` (
`id` int(11)
,`budget_final_id` int(11)
,`budget_name` varchar(45)
,`budget_number` varchar(45)
,`additional_amount` decimal(17,2)
,`is_approved_by_manager` tinyint(4)
,`approved_by_manager_time` datetime
,`is_approved_by_fa` tinyint(4)
,`approved_by_fa_time` datetime
,`time` datetime
,`is_rejected` tinyint(4)
,`is_canceled` tinyint(4)
);
CREATE TABLE `v_budget_final` (
`id` int(11)
,`budget_proposal_id` int(11)
,`section_id` int(11)
,`section` varchar(45)
,`budget_period` varchar(45)
,`budget_name` varchar(45)
,`budget_number` varchar(45)
,`amount` decimal(17,2)
,`amount_used` decimal(17,2)
,`comment` text
,`is_approved_by_manager` tinyint(4)
,`approved_by_manager_time` datetime
,`is_approved_by_fa` tinyint(4)
,`approved_by_fa_time` datetime
,`is_additional` tinyint(4)
,`is_rejected` tinyint(4)
,`is_final` tinyint(4)
,`is_expired` tinyint(4)
,`date` datetime
,`active_until` datetime
);
CREATE TABLE `v_budget_transfer` (
`id` int(11)
,`source_id` int(11)
,`source` varchar(45)
,`source_number` varchar(45)
,`destination_id` int(11)
,`destination` varchar(45)
,`destination_number` varchar(45)
,`amount` decimal(17,2)
,`time` datetime
,`is_approved_by_source_manager` tinyint(4)
,`approved_by_source_manager_time` datetime
,`is_approved_by_destination_manager` tinyint(4)
,`approved_by_destination_manager_time` datetime
,`is_approved_by_fa` tinyint(4)
,`approved_by_fa_time` datetime
,`is_rejected` tinyint(4)
,`is_canceled` tinyint(4)
);
DROP TABLE IF EXISTS `v_budget_additional`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_budget_additional`  AS  select `budget_additional`.`id` AS `id`,`budget_additional`.`budget_final_id` AS `budget_final_id`,`budget`.`name` AS `budget_name`,`budget`.`number` AS `budget_number`,`budget_additional`.`additional_amount` AS `additional_amount`,`budget_additional`.`is_approved_by_manager` AS `is_approved_by_manager`,`budget_additional`.`approved_by_manager_time` AS `approved_by_manager_time`,`budget_additional`.`is_approved_by_fa` AS `is_approved_by_fa`,`budget_additional`.`approved_by_fa_time` AS `approved_by_fa_time`,`budget_additional`.`time` AS `time`,`budget_additional`.`is_rejected` AS `is_rejected`,`budget_additional`.`is_canceled` AS `is_canceled` from (((`budget_additional` join `budget_final` on((`budget_final`.`id` = `budget_additional`.`budget_final_id`))) join `budget_proposal` on((`budget_proposal`.`id` = `budget_final`.`budget_proposal_id`))) join `budget` on((`budget`.`id` = `budget_proposal`.`budget_id`))) ;
DROP TABLE IF EXISTS `v_budget_final`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_budget_final`  AS  select `budget_final`.`id` AS `id`,`budget_final`.`budget_proposal_id` AS `budget_proposal_id`,`budget_proposal`.`section_id` AS `section_id`,`section`.`name` AS `section`,`budget_period`.`name` AS `budget_period`,`budget`.`name` AS `budget_name`,`budget`.`number` AS `budget_number`,`budget_proposal`.`amount` AS `amount`,`budget_proposal`.`amount_used` AS `amount_used`,`budget_proposal`.`comment` AS `comment`,`budget_proposal`.`is_approved_by_manager` AS `is_approved_by_manager`,`budget_proposal`.`approved_by_manager_time` AS `approved_by_manager_time`,`budget_proposal`.`is_approved_by_fa` AS `is_approved_by_fa`,`budget_proposal`.`approved_by_fa_time` AS `approved_by_fa_time`,`budget_proposal`.`is_additional` AS `is_additional`,`budget_proposal`.`is_rejected` AS `is_rejected`,`budget_proposal`.`is_final` AS `is_final`,`budget_proposal`.`is_expired` AS `is_expired`,`budget_proposal`.`date` AS `date`,`budget_proposal`.`active_until` AS `active_until` from ((((`budget_final` join `budget_proposal` on((`budget_proposal`.`id` = `budget_final`.`budget_proposal_id`))) join `section` on((`section`.`id` = `budget_proposal`.`section_id`))) join `budget_period` on((`budget_period`.`id` = `budget_proposal`.`budget_period_id`))) join `budget` on((`budget`.`id` = `budget_proposal`.`budget_id`))) ;
DROP TABLE IF EXISTS `v_budget_transfer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_budget_transfer`  AS  select `budget_transfer`.`id` AS `id`,`budget_transfer`.`source` AS `source_id`,`b01`.`name` AS `source`,`b01`.`number` AS `source_number`,`budget_transfer`.`destination` AS `destination_id`,`b02`.`name` AS `destination`,`b02`.`number` AS `destination_number`,`budget_transfer`.`amount` AS `amount`,`budget_transfer`.`time` AS `time`,`budget_transfer`.`is_approved_by_source_manager` AS `is_approved_by_source_manager`,`budget_transfer`.`approved_by_source_manager_time` AS `approved_by_source_manager_time`,`budget_transfer`.`is_approved_by_destination_manager` AS `is_approved_by_destination_manager`,`budget_transfer`.`approved_by_destination_manager_time` AS `approved_by_destination_manager_time`,`budget_transfer`.`is_approved_by_fa` AS `is_approved_by_fa`,`budget_transfer`.`approved_by_fa_time` AS `approved_by_fa_time`,`budget_transfer`.`is_rejected` AS `is_rejected`,`budget_transfer`.`is_canceled` AS `is_canceled` from ((((((`budget_transfer` join `budget_final` `fin01` on((`budget_transfer`.`source` = `fin01`.`id`))) join `budget_proposal` `bp01` on((`fin01`.`budget_proposal_id` = `bp01`.`id`))) join `budget` `b01` on((`bp01`.`budget_id` = `b01`.`id`))) join `budget_final` `fin02` on((`budget_transfer`.`destination` = `fin02`.`id`))) join `budget_proposal` `bp02` on((`fin02`.`budget_proposal_id` = `bp02`.`id`))) join `budget` `b02` on((`bp02`.`budget_id` = `b02`.`id`))) ;


ALTER TABLE `acl_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acl_list_id` (`acl_list_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `section_id` (`section_id`);

ALTER TABLE `acl_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module` (`module`),
  ADD KEY `module_item` (`module_item`);

ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `bank_detail`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_number` (`account_number`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `bank_id_2` (`bank_id`);

ALTER TABLE `budget`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_budget_budget_sub_groub1_idx` (`budget_sub_group_id`);

ALTER TABLE `budget_additional`
  ADD PRIMARY KEY (`id`),
  ADD KEY `budget_proposal_id` (`budget_final_id`);

ALTER TABLE `budget_category`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `budget_category_period`
  ADD PRIMARY KEY (`id`),
  ADD KEY `budget_category_id` (`budget_category_id`),
  ADD KEY `period_id` (`period_id`);

ALTER TABLE `budget_final`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_budget_final_budget_proposal1_idx` (`budget_proposal_id`);

ALTER TABLE `budget_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_budget_group_budget_category1_idx` (`budget_category_id`);

ALTER TABLE `budget_limit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `budget_id` (`budget_id`),
  ADD KEY `budget_period_id` (`period_id`),
  ADD KEY `currency_id` (`currency_id`);

ALTER TABLE `budget_period`
  ADD PRIMARY KEY (`id`),
  ADD KEY `period_id` (`period_id`);

ALTER TABLE `budget_proposal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_budget_proposal_budget1_idx` (`budget_id`),
  ADD KEY `fk_budget_proposal_section1_idx` (`section_id`),
  ADD KEY `fk_budget_proposal_budget_period1_idx` (`budget_period_id`);

ALTER TABLE `budget_proposal_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_budet_proposal_detail_goods_service1_idx` (`goods_service_id`),
  ADD KEY `fk_budet_proposal_detail_budget_proposal1_idx` (`budget_proposal_id`),
  ADD KEY `carline_id` (`carline_id`);

ALTER TABLE `budget_reschedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `budget_final_id` (`budget_final_id`);

ALTER TABLE `budget_sub_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_budget_sub_groub_budget_group1_idx` (`budget_group_id`);

ALTER TABLE `budget_transfer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `source` (`source`),
  ADD KEY `destination` (`destination`);

ALTER TABLE `carline`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `currency_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currency_id` (`currency_id`),
  ADD KEY `period_id` (`period_id`);

ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

ALTER TABLE `direct_purchase`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `dp_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_invoice_id` (`dp_realization_id`),
  ADD KEY `payment_method_id` (`payment_method_id`),
  ADD KEY `bank_detail_id` (`bank_detail_id`);

ALTER TABLE `dp_pr_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_order_id` (`direct_purchase_id`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`);

ALTER TABLE `dp_realization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `direct_purchase_id` (`direct_purchase_id`);

ALTER TABLE `dp_realization_actual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_direct_purchase_actual_direct_purchase1_idx` (`dp_pr_detail_id`),
  ADD KEY `fk_direct_purchase_actual_goods_service1_idx` (`goods_service_id`),
  ADD KEY `carline_id` (`carline_id`);

ALTER TABLE `dp_realization_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_direct_purchase_actual_direct_purchase1_idx` (`dp_pr_detail_id`),
  ADD KEY `fk_direct_purchase_actual_goods_service1_idx` (`goods_service_id`),
  ADD KEY `carline_id` (`carline_id`);

ALTER TABLE `franco`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `goods_receipt_dp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_goods_received_dp_direct_purchase1_idx` (`direct_purchase_id`),
  ADD KEY `fk_goods_receipt_dp_receiver1_idx` (`receiver_id`);

ALTER TABLE `goods_receipt_dp_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_goods_receipt_detail_goods_receipt_dp1_idx` (`goods_receipt_dp_id`),
  ADD KEY `fk_goods_receipt_detail_goods_service1_idx` (`goods_service_id`);

ALTER TABLE `goods_receipt_po`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_goods_received_po_purchase_order1_idx` (`purchase_order_id`),
  ADD KEY `fk_goods_received_po_receiver1_idx` (`receiver_id`),
  ADD KEY `fk_goods_receipt_po_supplier1_idx` (`supplier_id`);

ALTER TABLE `goods_receipt_po_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_goods_receipt_po_detail_goods_receipt_po1_idx` (`goods_receipt_po_id`),
  ADD KEY `fk_goods_receipt_po_detail_goods_service1_idx` (`goods_service_id`);

ALTER TABLE `goods_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_goods_service_unit1_idx` (`unit_id`),
  ADD KEY `fk_goods_service_budget1_idx` (`budget_id`),
  ADD KEY `currency_id` (`currency_id`);

ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `notification_message`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `period`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

ALTER TABLE `po_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_invoice_id` (`po_realization_id`),
  ADD KEY `bank_detail_id` (`bank_detail_id`),
  ADD KEY `payment_method_id` (`payment_method_id`);

ALTER TABLE `po_pr_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`);

ALTER TABLE `po_realization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_order_id` (`purchase_order_id`),
  ADD KEY `prepared_by_user_id` (`prepared_by_user_id`),
  ADD KEY `approved_by_user_id` (`approved_by_user_id`),
  ADD KEY `verified_by_user_id` (`verified_by_user_id`);

ALTER TABLE `po_realization_actual`
  ADD PRIMARY KEY (`id`),
  ADD KEY `goods_service_id` (`goods_service_id`),
  ADD KEY `po_pr_detail_id` (`po_pr_detail_id`),
  ADD KEY `carline_id` (`carline_id`);

ALTER TABLE `po_revision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_requisition_id` (`purchase_order_id`);

ALTER TABLE `po_revision_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `po_realization_actual_id` (`po_realization_actual_id`),
  ADD KEY `po_revision_id` (`po_revision_id`);

ALTER TABLE `pricelist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pricelist_goods_service1_idx` (`goods_service_id`),
  ADD KEY `fk_pricelist_supplier1_idx` (`supplier_id`),
  ADD KEY `revenue_tax_id` (`revenue_tax_id`),
  ADD KEY `value_added_tax_id` (`value_added_tax_id`);

ALTER TABLE `pr_revision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_requisition_id` (`purchase_requisition_id`);

ALTER TABLE `pr_revision_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pr_revision_id` (`pr_revision_id`),
  ADD KEY `purchase_requisition_detail_id` (`purchase_requisition_detail_id`);

ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchase_order_term1_idx` (`term_id`),
  ADD KEY `fk_purchase_order_franco1_idx` (`franco_id`),
  ADD KEY `fk_purchase_order_supplier1_idx` (`supplier_id`);

ALTER TABLE `purchase_requisition`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchase_requisition_section1_idx` (`section_id`),
  ADD KEY `fk_purchase_requisition_budget_final1_idx` (`budget_final_id`);

ALTER TABLE `purchase_requisition_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_purchase_requisition_detail_purchase_requisition1_idx` (`purchase_requisition_id`),
  ADD KEY `fk_purchase_requisition_detail_goods_service1_idx` (`goods_service_id`),
  ADD KEY `carline_id` (`carline_id`);

ALTER TABLE `receiver`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_receiver_section1_idx` (`section_id`);

ALTER TABLE `revenue_tax`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`rate`);

ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`),
  ADD UNIQUE KEY `nama` (`name`);

ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `fk_section_department1_idx` (`department_id`);

ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_supplier_supplier_category1_idx` (`supplier_category_id`);

ALTER TABLE `supplier_category`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `term`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_unit_unit_category1_idx` (`unit_category_id`);

ALTER TABLE `unit_category`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `role` (`role`);

ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `department_id` (`department_id`);

ALTER TABLE `value_added_tax`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`rate`);

ALTER TABLE `voucher_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_voucher_payment_purchase_order1_idx` (`purchase_order_id`);


ALTER TABLE `acl_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `acl_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `bank_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_additional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_category_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_final`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_limit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_proposal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_proposal_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_reschedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_sub_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `budget_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `carline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `currency_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `direct_purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `dp_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `dp_pr_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `dp_realization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `dp_realization_actual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `dp_realization_plan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `franco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `goods_receipt_dp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `goods_receipt_dp_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `goods_receipt_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `goods_receipt_po_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `goods_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `notification_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `po_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `po_pr_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `po_realization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `po_realization_actual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `po_revision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `po_revision_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pricelist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pr_revision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `pr_revision_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `purchase_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `purchase_requisition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `purchase_requisition_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `receiver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `revenue_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `supplier_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `term`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `unit_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `user_meta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `value_added_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `voucher_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `acl_access`
  ADD CONSTRAINT `acl_access_ibfk_1` FOREIGN KEY (`acl_list_id`) REFERENCES `acl_list` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_access_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_access_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `acl_access_ibfk_4` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `bank_detail`
  ADD CONSTRAINT `bank_detail_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`);

ALTER TABLE `budget`
  ADD CONSTRAINT `fk_budget_budget_sub_groub1` FOREIGN KEY (`budget_sub_group_id`) REFERENCES `budget_sub_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `budget_additional`
  ADD CONSTRAINT `budget_additional_ibfk_1` FOREIGN KEY (`budget_final_id`) REFERENCES `budget_final` (`id`);

ALTER TABLE `budget_category_period`
  ADD CONSTRAINT `budget_category_period_ibfk_1` FOREIGN KEY (`budget_category_id`) REFERENCES `budget_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `budget_category_period_ibfk_2` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `budget_final`
  ADD CONSTRAINT `fk_budget_final_budget_proposal1` FOREIGN KEY (`budget_proposal_id`) REFERENCES `budget_proposal` (`id`);

ALTER TABLE `budget_group`
  ADD CONSTRAINT `fk_budget_group_budget_category1` FOREIGN KEY (`budget_category_id`) REFERENCES `budget_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `budget_limit`
  ADD CONSTRAINT `budget_limit_ibfk_1` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `budget_limit_ibfk_2` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `budget_limit_ibfk_3` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`);

ALTER TABLE `budget_period`
  ADD CONSTRAINT `budget_period_ibfk_1` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `budget_proposal`
  ADD CONSTRAINT `fk_budget_proposal_budget1` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_budget_proposal_budget_period1` FOREIGN KEY (`budget_period_id`) REFERENCES `budget_period` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_budget_proposal_section1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `budget_proposal_detail`
  ADD CONSTRAINT `budget_proposal_detail_ibfk_2` FOREIGN KEY (`carline_id`) REFERENCES `carline` (`id`),
  ADD CONSTRAINT `fk_budet_proposal_detail_budget_proposal1` FOREIGN KEY (`budget_proposal_id`) REFERENCES `budget_proposal` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_budet_proposal_detail_goods_service1` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `budget_reschedule`
  ADD CONSTRAINT `budget_reschedule_ibfk_1` FOREIGN KEY (`budget_final_id`) REFERENCES `budget_final` (`id`);

ALTER TABLE `budget_sub_group`
  ADD CONSTRAINT `fk_budget_sub_groub_budget_group1` FOREIGN KEY (`budget_group_id`) REFERENCES `budget_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `budget_transfer`
  ADD CONSTRAINT `budget_transfer_ibfk_1` FOREIGN KEY (`source`) REFERENCES `budget_final` (`id`),
  ADD CONSTRAINT `budget_transfer_ibfk_2` FOREIGN KEY (`destination`) REFERENCES `budget_final` (`id`);

ALTER TABLE `currency_detail`
  ADD CONSTRAINT `currency_detail_ibfk_1` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `currency_detail_ibfk_2` FOREIGN KEY (`period_id`) REFERENCES `period` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `dp_payment`
  ADD CONSTRAINT `dp_payment_ibfk_1` FOREIGN KEY (`dp_realization_id`) REFERENCES `dp_realization` (`id`);

ALTER TABLE `dp_pr_detail`
  ADD CONSTRAINT `dp_pr_detail_ibfk_1` FOREIGN KEY (`direct_purchase_id`) REFERENCES `direct_purchase` (`id`),
  ADD CONSTRAINT `dp_pr_detail_ibfk_2` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`);

ALTER TABLE `dp_realization`
  ADD CONSTRAINT `dp_realization_ibfk_1` FOREIGN KEY (`direct_purchase_id`) REFERENCES `direct_purchase` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `dp_realization_actual`
  ADD CONSTRAINT `dp_realization_actual_ibfk_1` FOREIGN KEY (`dp_pr_detail_id`) REFERENCES `dp_pr_detail` (`id`),
  ADD CONSTRAINT `dp_realization_actual_ibfk_2` FOREIGN KEY (`carline_id`) REFERENCES `carline` (`id`),
  ADD CONSTRAINT `fk_direct_purchase_actual_goods_service1` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `dp_realization_plan`
  ADD CONSTRAINT `dp_realization_plan_ibfk_2` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`),
  ADD CONSTRAINT `dp_realization_plan_ibfk_3` FOREIGN KEY (`dp_pr_detail_id`) REFERENCES `dp_pr_detail` (`id`),
  ADD CONSTRAINT `dp_realization_plan_ibfk_4` FOREIGN KEY (`carline_id`) REFERENCES `carline` (`id`);

ALTER TABLE `goods_receipt_dp`
  ADD CONSTRAINT `fk_goods_receipt_dp_receiver1` FOREIGN KEY (`receiver_id`) REFERENCES `receiver` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_goods_received_dp_direct_purchase1` FOREIGN KEY (`direct_purchase_id`) REFERENCES `direct_purchase` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `goods_receipt_dp_detail`
  ADD CONSTRAINT `fk_goods_receipt_detail_goods_receipt_dp1` FOREIGN KEY (`goods_receipt_dp_id`) REFERENCES `goods_receipt_dp` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_goods_receipt_detail_goods_service1` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `goods_receipt_po`
  ADD CONSTRAINT `fk_goods_receipt_po_supplier1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_goods_received_po_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_goods_received_po_receiver1` FOREIGN KEY (`receiver_id`) REFERENCES `receiver` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `goods_receipt_po_detail`
  ADD CONSTRAINT `fk_goods_receipt_po_detail_goods_receipt_po1` FOREIGN KEY (`goods_receipt_po_id`) REFERENCES `goods_receipt_po` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_goods_receipt_po_detail_goods_service1` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `goods_service`
  ADD CONSTRAINT `fk_goods_service_budget1` FOREIGN KEY (`budget_id`) REFERENCES `budget` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_goods_service_unit1` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `goods_service_ibfk_1` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`);

ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `po_payment`
  ADD CONSTRAINT `po_payment_ibfk_1` FOREIGN KEY (`po_realization_id`) REFERENCES `po_realization` (`id`),
  ADD CONSTRAINT `po_payment_ibfk_2` FOREIGN KEY (`bank_detail_id`) REFERENCES `bank_detail` (`id`),
  ADD CONSTRAINT `po_payment_ibfk_3` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_method` (`id`);

ALTER TABLE `po_pr_detail`
  ADD CONSTRAINT `po_pr_detail_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `po_pr_detail_ibfk_2` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`);

ALTER TABLE `po_realization`
  ADD CONSTRAINT `po_realization_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `po_realization_ibfk_2` FOREIGN KEY (`prepared_by_user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `po_realization_ibfk_3` FOREIGN KEY (`approved_by_user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `po_realization_ibfk_4` FOREIGN KEY (`verified_by_user_id`) REFERENCES `user` (`id`);

ALTER TABLE `po_realization_actual`
  ADD CONSTRAINT `po_realization_actual_ibfk_2` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`),
  ADD CONSTRAINT `po_realization_actual_ibfk_3` FOREIGN KEY (`po_pr_detail_id`) REFERENCES `po_pr_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `po_realization_actual_ibfk_4` FOREIGN KEY (`carline_id`) REFERENCES `carline` (`id`);

ALTER TABLE `po_revision`
  ADD CONSTRAINT `po_revision_ibfk_1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`);

ALTER TABLE `po_revision_detail`
  ADD CONSTRAINT `po_revision_detail_ibfk_1` FOREIGN KEY (`po_revision_id`) REFERENCES `po_revision` (`id`),
  ADD CONSTRAINT `po_revision_detail_ibfk_2` FOREIGN KEY (`po_realization_actual_id`) REFERENCES `po_realization_actual` (`id`);

ALTER TABLE `pricelist`
  ADD CONSTRAINT `fk_pricelist_goods_service1` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pricelist_supplier1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pricelist_ibfk_1` FOREIGN KEY (`revenue_tax_id`) REFERENCES `revenue_tax` (`id`),
  ADD CONSTRAINT `pricelist_ibfk_2` FOREIGN KEY (`value_added_tax_id`) REFERENCES `value_added_tax` (`id`);

ALTER TABLE `pr_revision`
  ADD CONSTRAINT `pr_revision_ibfk_1` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`);

ALTER TABLE `pr_revision_detail`
  ADD CONSTRAINT `pr_revision_detail_ibfk_1` FOREIGN KEY (`pr_revision_id`) REFERENCES `pr_revision` (`id`),
  ADD CONSTRAINT `pr_revision_detail_ibfk_2` FOREIGN KEY (`purchase_requisition_detail_id`) REFERENCES `purchase_requisition_detail` (`id`);

ALTER TABLE `purchase_order`
  ADD CONSTRAINT `fk_purchase_order_franco1` FOREIGN KEY (`franco_id`) REFERENCES `franco` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_supplier1` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_order_term1` FOREIGN KEY (`term_id`) REFERENCES `term` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `purchase_requisition`
  ADD CONSTRAINT `fk_purchase_requisition_budget_final1` FOREIGN KEY (`budget_final_id`) REFERENCES `budget_final` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_purchase_requisition_section1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `purchase_requisition_detail`
  ADD CONSTRAINT `fk_purchase_requisition_detail_goods_service1` FOREIGN KEY (`goods_service_id`) REFERENCES `goods_service` (`id`),
  ADD CONSTRAINT `fk_purchase_requisition_detail_purchase_requisition1` FOREIGN KEY (`purchase_requisition_id`) REFERENCES `purchase_requisition` (`id`),
  ADD CONSTRAINT `purchase_requisition_detail_ibfk_1` FOREIGN KEY (`carline_id`) REFERENCES `carline` (`id`);

ALTER TABLE `receiver`
  ADD CONSTRAINT `fk_receiver_section1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `section`
  ADD CONSTRAINT `fk_section_department1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `supplier`
  ADD CONSTRAINT `fk_supplier_supplier_category1` FOREIGN KEY (`supplier_category_id`) REFERENCES `supplier_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `unit`
  ADD CONSTRAINT `fk_unit_unit_category1` FOREIGN KEY (`unit_category_id`) REFERENCES `unit_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `user_ibfk_3` FOREIGN KEY (`role`) REFERENCES `role` (`id`);

ALTER TABLE `user_meta`
  ADD CONSTRAINT `user_meta_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_meta_ibfk_2` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_meta_ibfk_3` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `voucher_payment`
  ADD CONSTRAINT `fk_voucher_payment_purchase_order1` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
