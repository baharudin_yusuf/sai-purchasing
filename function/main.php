<?php

function url($params = '')
{
    $baseURL = \yii\helpers\Url::home(true);
    if (strlen($params) > 0) {
        $baseURL .= $params;
    }
    return $baseURL;
}

function current_url($params = '')
{
    $baseURL = \Yii::$app->request->getAbsoluteUrl();
    if (strlen($params) > 0) {
        $baseURL .= $params;
    }
    return $baseURL;
}

function redirect($url)
{
    header("Location: $url");
    exit();
}

function encrypt($string)
{
    $remove = array('a', 'i', 'u', 'e', 'o');
    return md5(strrev(str_replace($remove, '', md5($string))));
}

function post_data($key, $default = null)
{
    return \Yii::$app->request->post($key, $default);
}

function get_data($key, $default = null)
{
    return \Yii::$app->request->get($key, $default);
}

function get_session($key, $default = null)
{
    if (isset($_SESSION[$key])) {
        return $_SESSION[$key];
    } else {
        return $default;
    }
}

function set_session($key, $data)
{
    $_SESSION[$key] = $data;
}

function remove_session($key)
{
    if (isset($_SESSION[$key])) {
        unset($_SESSION[$key]);
    }
}

function alert_danger($message)
{
    return "<div class='alert alert-danger'>$message</div>";
}

function alert_warning($message)
{
    return "<div class='alert alert-warning'>$message</div>";
}

function alert_success($message)
{
    return "<div class='alert alert-success'>$message</div>";
}

function terbilang($num)
{
    $num = intval($num);
    $bil = ['', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas'];

    if ($num < 12)
        return ' ' . $bil[$num];
    else if ($num < 20)
        return terbilang($num - 10) . 'belas';
    else if ($num < 100)
        return terbilang($num / 10) . ' puluh' . terbilang($num % 10);
    else if ($num < 200)
        return ' seratus' . terbilang($num - 100);
    else if ($num < 1000)
        return terbilang($num / 100) . ' ratus' . terbilang($num % 100);
    else if ($num < 2000)
        return ' seribu' . terbilang($num - 1000);
    else if ($num < 1000000)
        return terbilang($num / 1000) . ' ribu' . terbilang($num % 1000);
    else if ($num < 1000000000)
        return terbilang($num / 1000000) . ' juta' . terbilang($num % 1000000);
    else if ($num < 1000000000000)
        return terbilang($num / 1000000000) . ' miliar' . terbilang($num % 1000000000);
    else if ($num < 1000000000000000)
        return terbilang($num / 1000000000000) . ' triliun' . terbilang($num % 1000000000000);
}

function currency_format($number)
{
    return number_format($number, 2, ',', '.');
}

function check_times_cirle($opt, $title = '')
{
    if (intval($opt) == 1) {
        return "<i title='$title' class='opt-check-times fa fa-lg fa-check-circle green'><span>$opt</span></i>";
    } else {
        return "<i title='$title' class='opt-check-times fa fa-lg fa-times-circle red'><span>$opt</span></i>";
    }
}

function random_string($length = 32)
{
    $word = str_split('abcdefghijklmnopqrstuvwxyz1234567890');
    for ($i = 0; $i < 5; $i++) {
        shuffle($word);
        $word = array_merge($word, $word);
        shuffle($word);
    }
    $word = implode('', $word);
    return substr($word, 0, $length);
}

function dealias($string)
{
    return str_replace('-', ' ', $string);
}

function upload($key, $uploadPath = null, $filename = '', $filter = [])
{
    $blocked = ['php', 'html', 'exe', 'py', 'pl', 'rb'];
    if (is_null($uploadPath)) {
        $uploadPath = UPLOAD_PATH;
    }

    if (isset($_FILES[$key])) {
        if (strlen($_FILES[$key]['name']) == 0) {
            return ['error' => false, 'msg' => 'no file choosen'];
        }

        $split = explode('.', strtolower($_FILES[$key]['name']));
        $ext = end($split);
        if (isset($blocked[$ext])) {
            return ['error' => true, 'msg' => 'extension not allowed'];
        }

        if (!empty($filter)) {
            if (!in_array($ext, $filter)) {
                return ['error' => true, 'msg' => 'Extension not allowed. Allowed extension : ' . implode(', ', $filter)];
            }
        }

        if (strlen($filename) == 0) {
            $newName = random_string() . '.' . $ext;
        } else {
            $newName = $filename . '.' . $ext;
        }

        if (@move_uploaded_file($_FILES[$key]['tmp_name'], $uploadPath . '/' . $newName)) {
            $data = array(
                'error' => false,
                'name' => $_FILES[$key]['name'],
                'size' => $_FILES[$key]['size'],
                'new_name' => $newName,
                'tmp_name' => $_FILES[$key]['tmp_name'],
                'ext' => $ext
            );
            return $data;
        } else {
            return array('error' => true, 'msg' => 'error moving files');
        }
    } else {
        return array('error' => true, 'msg' => 'error initializing files');
    }
}

function is_cli()
{
    return (php_sapi_name() === 'cli');
}

function log_error($exception)
{
    $file = method_exists($exception, 'getFile') ? $exception->getFile() : '';
    $line = method_exists($exception, 'getLine') ? $exception->getLine() : '';
    $msg = method_exists($exception, 'getMessage') ? $exception->getMessage() : '';
    $msg = addslashes($msg);

    if (!is_cli()) {
        echo "
        <script>
            console.log({
                error: true,
                file: '$file',
                line: '$line',
                message: '$msg'
            });
        </script>";
    }
}

function is_with_rejected()
{

}

function is_with_canceled()
{

}

?>