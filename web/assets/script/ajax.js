function ajax(aURL, aData, aOutput){
	if(baseURL == undefined) baseURL = '';
	aURL = baseURL + aURL;
	show_loading();

	$.ajax({
		type	: 'POST',
		url		: aURL,
		data	: aData,
		success	: function(result){
			$(aOutput).html(result);
			hide_loading();
		},
		error	: function(jqXHR, textStatus, errorThrown){
			console.log('Ajax Error!');
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			hide_loading();
		}
	});
}

var active_modal = "";

function show_loading() {
	$('#ajax-loading').removeClass('hidden');
}

function hide_loading() {
	$('#ajax-loading').addClass('hidden');
}

function load_modal(t){
	t.setAttribute('href', '#modal-target');
	t.setAttribute('data-toggle', 'modal');
	
	var title = t.getAttribute('title');
	var modal_id = 'modal-target';
	var modal_box = "";
	active_modal = modal_id;
	
	if(title == null) title = t.innerHTML;
	$('.modal-backdrop, #'+modal_id+'.modal').remove();
	
	modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog' tabindex='-1' id='" + modal_id + "' class='modal fade' style='display: none;'>";
	modal_box += "<div class='modal-dialog'>";
	modal_box += "<div class='modal-content'>";
	modal_box += "<div class='modal-header'>";
	modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='remove_modal(this)' rel='" + modal_id + "' type='button'>&#215;</button>";
	modal_box += "<h4 class='modal-title'>" + title + "</h4>";
	modal_box += "</div>";
	modal_box += "<div id='modal-output' class='modal-body'></div>";
	modal_box += "</div>";
	modal_box += "</div>";
	modal_box += "</div>";
	$('html').append(modal_box);
	
	var target 	= t.getAttribute('target');
	var data 	= t.getAttribute('data').split(';');
	var aData 	= {};
	var aURL	= target;
	
	for(var i=0; i<data.length; i++){
		if(data[i].length == 0) continue;
		else{
			var temp  = data[i].split('=');
			var key   = temp[0];
			var value = temp[1];
			
			aData[key] = value;
		}
	}
	
	ajax(aURL, aData, '#modal-output');
}

function remove_modal(t){
	var modal_id = t.getAttribute('rel');
	setTimeout(function(){
		$('#'+modal_id+'.modal').remove();
		$('body').removeClass('modal-open');
	}, 400);
}

function close_modal(timeout){
	if(timeout == undefined) $('#'+active_modal+' .close').click();
	else{
		timeout = parseInt(timeout);
		setTimeout(function(){
			$('#'+active_modal+' .close').click();
		}, timeout);
	}
}

function get_form_data(formID){
	var input = $('#'+formID+' input, #'+formID+' textarea, #'+formID+' select');
	var dataTemp = {};
	
	for(var i=0; i<input.length; i++){
		var inputType = input[i].getAttribute('type');
		var inputName = input[i].getAttribute('name');
		var inputValue = input[i].value;
		
		if(inputType == 'submit' || inputType == 'reset') continue;
		else if(inputType == 'checkbox' || inputType == 'radio'){
			dataTemp[inputName] = $('input[name=\"'+inputName+'\"]:checked').val();
		}
		else{
			dataTemp[inputName] = inputValue;
		}
	}
	
	return dataTemp;
}

$(document).ready(function(){
	$('#ajax-temp').remove();
	$('body').append("<div id='ajax-temp'></div>");
});