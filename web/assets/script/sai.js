function convert_datatables(table_id){
	var columns = [], footer_filter = [];
	
	var t_columns = [];
	var temp_collumns = $('#'+table_id+' th');
	for(var i=0; i<temp_collumns.length; i++){
		var temp_collumn_name = $(temp_collumns[i]).html();
		temp_collumn_name = temp_collumn_name.toLowerCase();
		temp_collumn_name = temp_collumn_name.replace(/ /g, '-');
		
		t_columns.push(temp_collumn_name);
	}
	
	$('table#'+table_id).append('<tfoot></tfoot>');
	$('table#'+table_id+' thead tr').clone().appendTo('#'+table_id+' tfoot');
	$('table#'+table_id+' thead tr').before("<tr id='head-filter'></tr>");
	
	for(var i=0; i< t_columns.length; i++){
		columns.push({'data': t_columns[i]});
		footer_filter.push({type: 'text', sSelector: '.filter-'+t_columns[i]});
		$('#'+table_id+' #head-filter').append("<th class='head-filter filter-"+t_columns[i]+"' ></th>");
	}
	
	table = $('#'+table_id).dataTable({
		'bProcessing'	: true,
		'pagingType'	: 'full_numbers',
		'columns'		: columns
	}).columnFilter({
		aoColumns: footer_filter
	});
	
	$('#'+table_id+' #head-filter').appendTo('#'+table_id+' thead');
	
	var display_length = $('#'+table_id+' .dataTables_length select');
	display_length.appendTo('#'+table_id+' .dataTables_length label');
	$('#'+table_id+' .head-filter input').addClass('input-sm');
	
	$('.dataTables_paginate').addClass('bootstrap');
}

function reload(t){
	if(t == undefined) t = 0;
	
	setTimeout(function(){
		location.reload();
	}, t);
}

function view_budget_detail(){
	var budget_id = $('select[name="budget_id"]').val();
	var new_element = "<a id='btn-budget-detail' onclick='load_modal(this)' title='Detail Budget' data='id="+budget_id+"' target='master/budget/budget-detail'></a>";
	
	$('body').append(new_element);
	$('#btn-budget-detail').click();
	$('#btn-budget-detail').remove();
}

function include_reject(){
	ajaxTransfer('misc/include-rejected', {}, '#global-temp');
}

function only_reject(){
    ajaxTransfer('misc/only-rejected', {}, '#global-temp');
}

function include_cancel(){
    ajaxTransfer('misc/include-canceled', {}, '#global-temp');
}

function only_cancel(){
    ajaxTransfer('misc/only-canceled', {}, '#global-temp');
}
