var table;

function master_convert_datatables(table_id){
	var id = 1;
	var total_element = $('#' + table_id + ' tfoot th');
	total_element = total_element.length;
	
	$('#' + table_id + ' tfoot th').each(function(){
		if(id != total_element){
			var title = $('#' + table_id + ' thead th').eq($(this).index() ).text();
			var id_table = 'search-'+id;
			var temp_eid = title.split(' ');
			var eid = temp_eid[0];
			var eid = 'search-' + eid.toLowerCase();
			$(this).html('<input class="form-control input-filter ' + eid + '" id="' + id_table + '" type="text" placeholder="'+title+'" />');
		}
		
		id++;
	});
 
	var table = $('#' + table_id).DataTable();
	table.columns().eq(0).each(function(colIdx){
		$('input:not([type="checkbox"])', table.column(colIdx).footer()).on('keyup change', function(){
			table
				.column( colIdx )
				.search( this.value )
				.draw();
		});
	});
	
	$('.input-filter, input[type="search"]').addClass('form-control');
}

function get_kabupaten(){
	var data = {
		as_option	: true,
		idprovinsi 	: $('select[name="provinsi"]').val()
	};
	ajax('backend/lokasi/get-kabupaten', data, 'select[name="kabupaten"]');
}

function get_kecamatan(){
	var data = {
		as_option	: true,
		idprovinsi 	: $('select[name="provinsi"]').val(),
		idkabupaten	: $('select[name="kabupaten"]').val()
	};
	ajax('backend/lokasi/get-kecamatan', data, 'select[name="kecamatan"]');
}

function get_sub_kategori_institusi(){
	var data = {
		as_option	: true,
		idkategori_institusi : $('select[name="kategori_institusi"]').val()
	};
	ajax('backend/master/kategori-institusi/get-sub-kategori', data, 'select[name="sub_kategori_institusi"]');
}

function activate_menu(active_url){
	$(document).ready(function(){
		$('.sidebar-wrapper li').removeClass('current');
		$('.sidebar-wrapper li:has(a[href="' + active_url + '"])').addClass('current');
		
		$('.sub-sidebar-wrapper li').removeClass('current');
		$('.sub-sidebar-wrapper li:has(a[href="' + active_url + '"])').addClass('current');
		$('.sub-sidebar-wrapper li:has(li[class="current"])').addClass('open');
		$('.sub-sidebar-wrapper li:has(li[class="current"]) > a').addClass('current');
		$('.sub-sidebar-wrapper li.open > ul').css({display: 'block'});
		
		var selectedMenu = $('.sidebar-menu').attr('rel');
		$('#'+selectedMenu).addClass('current');
	});
}

var a_active = null;
function collapsible_close(c_active){
	if(c_active != null){
		var a_element  = $(c_active);
		var i_element  = $(a_element.context.children[0]);
		var ul_element = $(a_element.context.nextElementSibling);
		
		if(c_active != null){
			setTimeout(function(){
				i_element.removeClass('fa-caret-down');
				i_element.addClass('fa-caret-right');
				a_element.removeAttr('onclick');
				ul_element.removeAttr('style');
			}, 10);
		}
	}
}

function date_input(selector, type){
	if(type == 'date'){
		$(selector).datetimepicker({ timepicker:false, format:'Y-m-d'});
	} else if(type == 'datetime'){
		$(selector).datetimepicker({ format:'Y-m-d H:i'});
    }
}

function auto_complete(selector, json_url){
	var autocomplete_data = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		limit	: 10,
		prefetch: {
			url	: baseURL+json_url,
			filter: function(list) {
				return $.map(list, function(result){
					return { name: result };
				});
			}
		}
	});
	autocomplete_data.initialize();

	$(selector).typeahead(null, {
		hint		: true,
		highlight	: true,
		minLength	: 1,
		name		: 'result',
		displayKey	: 'name',
		source		: autocomplete_data.ttAdapter()
	});
}

var substringMatcher = function(strs) {
	return function findMatches(q, cb) {
		var matches, substrRegex;

		matches = [];
		substrRegex = new RegExp(q, 'i');

		$.each(strs, function(i, str) {
			if (substrRegex.test(str)) {
				matches.push({ value: str });
			}
		});

		cb(matches);
	};
};

function auto_complete2(selector, source_data){
	$(selector).typeahead({
		hint		: true,
		highlight	: true,
		minLength	: 1
	},
	{
		name		: 'states',
		displayKey	: 'value',
		source		: substringMatcher(source_data)
	});
}

var is_selected_all = false;
function select_unselect_all(t){
	var element = $(t);
	var child = element.context.children[1];
	
	if(is_selected_all){
		$(child).html('Select All');
		$('.select-data').prop('checked', false);
		is_selected_all = false;
	}
	else{
		$(child).html('Unselect All');
		$('.select-data').prop('checked', true);
		is_selected_all = true;
	}
}

function bulk_action(action){
	var selected_data = $('.select-data');
	var list_id = [];
	
	for(var i=0; i<selected_data.length; i++){
		if($(selected_data[i]).is(':checked')) list_id.push($(selected_data[i]).val());
	}
	
	if(list_id.length == 0){
		alert('Error! Harus ada data yang dipilih!');
    } else{
		var data = {
			action  : action,
			list_id : list_id.join(',')
		};
		
		if(action == 'approve'){
			msg = 'Apakah anda yakin akan menyetujui pengajuan data yang dipilih?';
        } else{
			msg = 'Apakah anda yakin akan menolak pengajuan data yang dipilih?';
        }
		
		if(confirm(msg)){
			bulkActionCallback(data);
        }
	}
}

function remove_warning(t){
	var elem = $(t).parent().parent().parent();
	elem.fadeOut(500);
	setTimeout(function(){
		elem.remove();
	}, 510);
}

$(document).ready(function(){
	/* collapsible */
	$('.collapsible ul li').has('ul').click(function(){
		var a_child  = $(this).context.children[0];
		var i_child  = $(this).context.children[0].children[0];
		var ul_child = $(this).context.children[1];
		var h_child  = ul_child.scrollHeight;

		if(a_active == a_child) a_active = null;
		collapsible_close(a_active);
		a_active = a_child;
		
		$(this).addClass('open');
		$(ul_child).css({height: h_child+'px'});
		$(a_child).attr('onclick', 'collapsible_close(this)');
		$(i_child).removeClass('fa-caret-right');
		$(i_child).addClass('fa-caret-down');
	});
	
	/* remove ckeditor useless tools */
	$('#cke_34').remove();
	
	$('#select-data').change(function(){
		if($(this).prop('checked')){
			$('.select-data').prop('checked', true);
		}
		else{
			$('.select-data').prop('checked', false);
		}
	});
	
	$('.select-data').change(function(){
		var select_data = $('.select-data');
		var checked = 0;
		
		for(var i=0; i<select_data.length; i++){
			if($(select_data[i]).is(':checked')) checked++;
		}

		if(checked == 0) $('#select-data').prop('checked', false);
		else $('#select-data').prop('checked', true);
	});
});