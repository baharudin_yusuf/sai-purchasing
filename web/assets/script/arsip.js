function convert_datatables(table_id, t_columns, ajaxURL){
	var columns = [], footer_filter = [];
	
	$('table').append('<tfoot></tfoot>');
	$('thead tr').clone().appendTo('tfoot');
	$('thead tr').before("<tr id='head-filter'></tr>");
	
	for(var i=0; i< t_columns.length; i++){
		columns.push({'data': t_columns[i]});
		footer_filter.push({type: 'date', sSelector: '.filter-'+t_columns[i]});
		$('#head-filter').append("<th class='head-filter filter-"+t_columns[i]+"' ></th>");
	}
	
	if(ajaxURL == undefined) ajaxURL = baseURL + table_id + '/ajax-source';
	else ajaxURL = baseURL + ajaxURL;
	
    table = $('#'+table_id).dataTable({
        'order'			: [[0, "desc" ]],
		'iDisplayLength': 50,
		'bProcessing'	: true,
		'pagingType'	: 'full_numbers',
		'ajax'			: ajaxURL,
        'columns'		: columns
    }).columnFilter({
		aoColumns: footer_filter
	});
	
	$('#head-filter').appendTo('thead');
	
	var display_length = $('.dataTables_length select');
	display_length.appendTo('.dataTables_length label');
	$("#tahun-entri").appendTo('.dataTables_length');
	$('.head-filter input').addClass('input-sm');
	
	var tahun_selected = $("#tahun-entri").val();
	$('.dataTables_length label select').change(function(){
		$("#tahun-entri option[value='"+tahun_selected+"']").attr('selected', 'selected');
	});
	
	/* ubah style */
	$('#tahun-entri, select[name="'+table_id+'_length"], #'+table_id+'_filter input').addClass('form-control input-sm');
	$('#tahun-entri, select[name="'+table_id+'_length"], #'+table_id+'_filter input').css({width: 'auto', display: 'inline', margin: '0 0 0 5px'});
	
	/* datepicker pada filter datatable */
	myDatePicker('.filter-tanggal_surat input');
	myDatePicker('.filter-tanggal_entri input');
	
	for(var i=0; i<t_columns.length; i++){
		if(t_columns[i] == 'tanggal_surat' || t_columns[i] == 'tanggal_entri'){	
			myDatePicker('.filter-'+t_columns[i]+' input');
			$('.filter-'+t_columns[i]+' input').attr('onchange', 'filter_tanggal(this, '+i+')');
		}
	}
}

function convert_datatables_normal(table_id, t_columns){
	var columns = [], footer_filter = [];
	
	$('table').append('<tfoot></tfoot>');
	$('thead tr').clone().appendTo('tfoot');
	$('thead tr').before("<tr id='head-filter'></tr>");
	
	for(var i=0; i< t_columns.length; i++){
		columns.push({'data': t_columns[i]});
		footer_filter.push({type: 'text', sSelector: '.filter-'+t_columns[i]});
		$('#head-filter').append("<th class='head-filter filter-"+t_columns[i]+"' ></th>");
	}
	
    table = $('#'+table_id).dataTable({
        'order'			: [[0, "desc" ]],
		'iDisplayLength': 50,
		'bProcessing'	: true,
		'pagingType'	: 'full_numbers',
        'columns'		: columns
    }).columnFilter({
		aoColumns: footer_filter
	});
	
	$('#head-filter').appendTo('thead');
	
	var display_length = $('.dataTables_length select');
	display_length.appendTo('.dataTables_length label');
	$("#tahun-entri").appendTo('.dataTables_length');
	$('.head-filter input').addClass('input-sm');
	
	var tahun_selected = $("#tahun-entri").val();
	$('.dataTables_length label select').change(function(){
		$("#tahun-entri option[value='"+tahun_selected+"']").attr('selected', 'selected');
	});
	
	/* datepicker pada filter datatable */
	for(var i=0; i<t_columns.length; i++){
		if(t_columns[i] == 'tanggal_surat' || t_columns[i] == 'tanggal_entri'){	
			myDatePicker('.filter-'+t_columns[i]+' input');
			$('.filter-'+t_columns[i]+' input').attr('onchange', 'filter_tanggal(this, '+i+')');
		}
	}
}

function filter_tanggal(t, i){
	table.fnFilter(t.value, i);
}

function myDatePicker(selector){
	$(selector).datetimepicker({ timepicker:false, format:'d-m-Y', scrollInput:false, validateOnBlur:false});
}

function date_input(selector, type){
	if(type == 'date'){
		$(selector).datetimepicker({ timepicker:false, format:'Y/m/d'});
	}
	else if(type == 'datetime') $(selector).datetimepicker({ format:'Y/m/d H:i'});
}

$(document).ready(function(){
	$('.datetime').datetimepicker({ format:'Y/m/d H:i'});
	$('.date').datetimepicker({ timepicker:false, format:'Y/m/d'});
});