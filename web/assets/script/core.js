var active_modal = "";
var modal_delay = 300;
var baseURL = "";
var fnPositiveButton, fnNegativeButton;
var loadingStack = {};

function getBaseURL() {
	var baseURL = $('input[name=base_url]').val();
	return baseURL;
}

function ajaxTransfer(url, data, callback, asJson){
	asJson = typeof asJson == 'undefined' ? false : true;

	if(asJson){
	    ajaxAsJson(url, data, callback);
	} else{
	    ajaxAsXhr(url, data, callback);
	}
}

function ajaxAsJson(url, data, callback){
    var response, CSRFToken, CSRFParam;
	var loadingId = generateRandomString();
	url = getBaseURL() + url;
	if(url.substring(0,2) == '//'){
	    url = url.substring(1, url.length);
	}

	if((data instanceof FormData)){
		data = Object.toFormData(data);
		data = data.serializeArray();
	}

    CSRFParam = $('meta[name=csrf-param]').attr('content');
    CSRFToken = $('meta[name=csrf-token]').attr('content');
    data = {
        json_data: JSON.stringify(data)
    };
    data[CSRFParam] = CSRFToken;
	showLoading(loadingId);

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(response){
            if(typeof callback == 'string'){
				$(callback).html(response);
			} else{
				callback(response);
			}

			setInputPlaceholder();
			validateRequiredInput();
		    hideLoading(loadingId);
			$('.title-tip').tooltip();
        },
        error: function(e){
            if(typeof callback == 'string'){
				$(callback).html(response);
			}

            console.log(e);
		    hideLoading(loadingId);
        }
    })
}

function ajaxAsXhr(url, data, callback){
    var response, CSRFToken, CSRFParam;
	var loadingId = generateRandomString();

	url = getBaseURL() + url;
	if(url.substring(0,2) == '//'){
	    url = url.substring(1, url.length);
	}

	if(!(data instanceof FormData)){
		data = Object.toFormData(data);
	}

    CSRFParam = $('meta[name=csrf-param]').attr('content');
    CSRFToken = $('meta[name=csrf-token]').attr('content');
	data.append(CSRFParam, CSRFToken);

	showLoading(loadingId);
	var xhr = new XMLHttpRequest();
	xhr.open('POST', url, true);
	xhr.onload = function () {
		response = xhr.responseText;
		if (xhr.status === 200) {
			if(typeof callback === 'function'){
                callback(response);
            } else{
                $(callback).html(response);
			}

			setInputPlaceholder();
			validateRequiredInput();
			$('.title-tip').tooltip();
		} else {
		    if(typeof callback == 'string'){
				$(callback).html(response);
			}

			console.log('ajax error! status : ' + xhr.status);
			console.log(xhr);
		}

		hideLoading(loadingId);
    };
	xhr.send(data);
}

function modalAlert(title, message){
	$('.modal-backdrop, #modal-target.modal').remove();

    var modal_box = "";
	modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog' tabindex='-1' id='modal-target' class='modal fade' data-backdrop='static' data-keyboard='false' style='display: none;'>";
	modal_box += "<div class='modal-dialog'>";
	modal_box += "<div class='modal-content'>";
	modal_box += "<div class='modal-header'>";
	modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'>&#215;</button>";
	modal_box += "<h4 class='modal-title'>" + title + "</h4>";
	modal_box += "</div>";
	modal_box += "<div id='modal-output' class='modal-body'>"+message+"</div>";
	modal_box += "</div>";
	modal_box += "</div>";
	modal_box += "</div>";

	$('html').append(modal_box);
	$('#modal-target').modal('show');
}

function modalConfirm(title, message, positiveButton, negativeButton){
	if(typeof(positiveButton) == 'function'){
		fnPositiveButton = positiveButton;
	}
	if(typeof(negativeButton) == 'function'){
		fnNegativeButton = negativeButton;
	}

	var modal_box = "";
	var modalOpen = $('#modal-target').html();
	if(modalOpen == undefined){
		$('.modal-backdrop, #modal-target.modal').remove();

		modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog' tabindex='-1' id='modal-target' class='modal fade' data-backdrop='static' data-keyboard='false' style='display: none;'>";
		modal_box += "<div class='modal-dialog'>";
		modal_box += "<div class='modal-content'>";
		modal_box += "<div class='modal-header'>";
		modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'>&#215;</button>";
		modal_box += "<h4 class='modal-title'>" + title + "</h4>";
		modal_box += "</div>";
		modal_box += "<div id='modal-output' class='modal-body'>"+message+"<div id='confirm-button-action'><a onclick='negativeButtonClick()' class='btn btn-danger btn-negative-response'>Cancel</a><a onclick='positiveButtonClick()' class='btn btn-primary btn-positive-response'>Ok</a></div></div>";
		modal_box += "</div>";
		modal_box += "</div>";
		modal_box += "</div>";

		$('html').append(modal_box);
		$('#modal-target').modal('show');
	} else{
		$('.modal-title').html(title);
		$('#modal-output').html(message+"<div id='confirm-button-action'><a onclick='negativeButtonClick()' class='btn btn-danger btn-negative-response'>Cancel</a><a onclick='positiveButtonClick()' class='btn btn-primary btn-positive-response'>Ok</a></div></div>");
		$('#modal-target').modal('show');
	}
}

function positiveButtonClick(){
	if(typeof(fnPositiveButton) != 'function'){
		return false;
	} else{
		fnPositiveButton();
	}
}

function negativeButtonClick(){
	if(typeof(fnNegativeButton) != 'function'){
		closeModal();
	} else{
		fnNegativeButton();
	}
}

function loadModal(t){
	t.setAttribute('href', '#modal-target');
	t.setAttribute('data-toggle', 'modal');

	var title = t.getAttribute('title');

	if(title == null || title.length == 0){
		title = $(t).attr('data-original-title');
	}
	if(title == null || title.length == 0){
		title = $(t).html();
	}
	$('.modal-backdrop, #modal-target.modal').remove();

	var modal_box = "";
	modal_box += "<div aria-hidden='true' aria-labelledby='myModalLabel' role='dialog' tabindex='-1' id='modal-target' class='modal fade' data-backdrop='static' data-keyboard='false' style='display: none;'>";
	modal_box += "<div class='modal-dialog'>";
	modal_box += "<div class='modal-content'>";
	modal_box += "<div class='modal-header'>";
	modal_box += "<button aria-hidden='true' data-dismiss='modal' class='close' onclick='removeModal(this)' rel='modal-target' type='button'>&#215;</button>";
	modal_box += "<h4 class='modal-title'>" + title + "</h4>";
	modal_box += "</div>";
	modal_box += "<div id='modal-output' class='modal-body'></div>";
	modal_box += "</div>";
	modal_box += "</div>";
	modal_box += "</div>";

	$('html').append(modal_box);

	var data = t.getAttribute('data');
	var ajaxData = new FormData();
	var ajaxUrl = t.getAttribute('target');

	if(data == null){
		data = [];
	} else{
		data = data.split(';');
	}

	for(var i=0; i<data.length; i++){
		if(data[i].length == 0) continue;
		else{
			var temp  = data[i].split('=');
			var key   = temp[0];
			var value = temp[1];

			ajaxData.append(key, value);
		}
	}
	ajaxTransfer(ajaxUrl, ajaxData, '#modal-output');
}

function removeModal(t){
	var modal_id = t.getAttribute('rel');
	$('.modal, .modal-overlay, .modal-backdrop').animate({opacity: 0}, modal_delay);
	setTimeout(function(){
		$('#modal-target.modal, .modal-overlay, .modal-backdrop').remove();
		$('body').removeClass('modal-open');
	}, modal_delay);
}

function closeModal(timeout){
	if(timeout == undefined) $('.modal-header .close').click();
	else{
		timeout = parseInt(timeout);
		setTimeout(function(){
			$('.modal-header .close').click();
		}, timeout);
	}
}

function showLoading(id){
	if(Object.keys(loadingStack).length == 0){
		$('#loading-overlay').css({display: 'block'}).animate({opacity: '1'}, 100);
	}
	loadingStack[id] = id;

	var loadingScreen = generateLoadingScreen(id);
	$('body').append(loadingScreen);
	$('#'+id).animate({opacity: '1'}, 100);
}

function hideLoading(id){
	$('#'+id).animate({opacity: '0'}, 100);
	setTimeout(function(){
		$('#'+id).remove();
	}, 100);

	delete loadingStack[id];
	if(Object.keys(loadingStack).length == 0){
		$('#loading-overlay').animate({opacity: '0'}, 100);
		setTimeout(function () {
			$('#loading-overlay').css({display: 'none'});
		}, 100);
	}
}

function reload(timeout){
	if(timeout == undefined) location.reload();
	else{
		timeout = parseInt(timeout);
		setTimeout(function(){
			location.reload();
		}, timeout);
	}
}

function generateLoadingScreen(id) {
	var loadingScreen = "";
	loadingScreen += "<div id='"+id+"' class='loading-screen'>";
	loadingScreen += "<div id='loading-box'>";
	loadingScreen += "<span>Sedang mengolah data, mohon tunggu...</span>";
	loadingScreen += "</div>";
	loadingScreen += "</div>";
	return loadingScreen;
}

function getFormData(formId, asObject) {
    if (typeof asObject == 'boolean' && asObject) {
        var $form = $("#" + formId);
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        $.map(unindexed_array, function (n, i) {
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

    var formData = new FormData();
    var input = $('#' + formId + ' input');
    var select = $('#' + formId + ' select');
    var textarea = $('#' + formId + ' textarea');
    var ignored = ['submit', 'button', 'reset'];
    var i, j, inputType, inputName, inputValue, file, files;

    for (i = 0; i < input.length; i++) {
        inputType = input[i].getAttribute('type');
        inputName = input[i].getAttribute('name');
        inputValue = input[i].value;

        if (ignored.indexOf(inputType) != -1) {
            continue;
        } else if (inputType == 'checkbox') {
            if (!input[i].checked) {
                inputValue = null;
            }
            formData.append(inputName, inputValue);
        } else if (inputType == 'radio') {
            inputValue = $('input[name="' + inputName + '"]:checked').val();
            formData.append(inputName, inputValue);
        } else if (inputType == 'file') {
            files = input[i].files;
            for (j = 0; j < files.length; j++) {
                file = files[j];
                formData.append(inputName, file, file.name);
            }
        } else {
            formData.append(inputName, inputValue);
        }
    }

    for (i = 0; i < select.length; i++) {
        inputName = select[i].getAttribute('name');
        inputValue = select[i].value;
        formData.append(inputName, inputValue);
    }

    for (i = 0; i < textarea.length; i++) {
        inputName = textarea[i].getAttribute('name');
        inputValue = textarea[i].value;
        formData.append(inputName, inputValue);
    }

    return formData;
}

function setInputPlaceholder(){
	var labels = $('label');
	var label, placeholder;

	for(var i=0; i<labels.length; i++){
		label = $(labels[i]).attr('for');
		placeholder = $(labels[i]).html();
		$(labels[i]).parents('form').find('*[name='+label + ']').attr('placeholder', placeholder);
	}
}

function pipelineDatatable(tableId, dataUrl, excludeFilter){
	$.fn.dataTable.pipeline = function ( opts ) {
        var conf = $.extend( {
            pages   : 5,
            url     : '',
            data    : null,
            method  : 'POST'
        }, opts );

        var cacheLower = -1;
        var cacheUpper = null;
        var cacheLastRequest = null;
        var cacheLastJson = null;

        return function(request, drawCallback, settings){
            var ajax          = false;
            var requestStart  = request.start;
            var drawStart     = request.start;
            var requestLength = request.length;
            var requestEnd    = requestStart + requestLength;

            if(settings.clearCache){
                ajax = true;
                settings.clearCache = false;
            } else if(cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper){
                ajax = true;
            } else if(JSON.stringify(request.order) !== JSON.stringify(cacheLastRequest.order) || JSON.stringify(request.columns) !== JSON.stringify(cacheLastRequest.columns) || JSON.stringify(request.search) !== JSON.stringify(cacheLastRequest.search)){
                ajax = true;
            }

            cacheLastRequest = $.extend(true, {}, request);
            if(ajax){
                if (requestStart < cacheLower) {
                    requestStart = requestStart - (requestLength*(conf.pages-1));
                    if(requestStart < 0 ){
                        requestStart = 0;
                    }
                }

                cacheLower = requestStart;
                cacheUpper = requestStart + (requestLength * conf.pages);

                request.start = requestStart;
                request.length = requestLength*conf.pages;

                if($.isFunction (conf.data)){
                    var d = conf.data(request);
                    if (d) {
                        $.extend(request, d);
                    }
                } else if ($.isPlainObject(conf.data)){
                    $.extend( request, conf.data );
                }

                settings.jqXHR = $.ajax( {
                    type      : conf.method,
                    url       : conf.url,
                    data      : request,
                    dataType  : 'json',
                    cache     : false,
                    success   : function(json){
                        cacheLastJson = $.extend(true, {}, json);
                        if(cacheLower != drawStart){
                            json.data.splice( 0, drawStart-cacheLower );
                        }
                        json.data.splice( requestLength, json.data.length );
                        drawCallback(json);
                    }
                });
            } else {
                json = $.extend(true, {}, cacheLastJson);
                json.draw = request.draw;
                json.data.splice( 0, requestStart-cacheLower );
                json.data.splice( requestLength, json.data.length );
                drawCallback(json);
            }
        }
    };

    $.fn.dataTable.Api.register('clearPipeline()', function(){
        return this.iterator( 'table', function(settings){
            settings.clearCache = true;
        });
    });

    $(tableId).append('<tfoot></tfoot>');
	$(tableId + ' thead tr').clone().appendTo(tableId + ' tfoot');
	$(tableId + ' thead tr').attr('id', 'head-order');
	//$(tableId + ' thead tr').before("<tr id='head-filter'></tr>");

    $(tableId + ' thead tr#head-order th').each(function(){
        $(this).clone().appendTo(tableId + ' thead tr#head-filter');
    });

    if(typeof(excludeFilter) == "undefined"){
    	excludeFilter = [];
    }

    var index = 0;
    $(tableId + ' thead tr#head-filter th').each(function(){
        $(this).addClass('th-filter');
        if(excludeFilter.indexOf(index) == -1){
            var title = $(this).text();
            $(this).html('<input type="text" class="form-control input-sm" placeholder="Search '+title+'" />');
        } else{
            $(this).html('');
        }
        index++;
    });

    var idx = 0;
    $(tableId).DataTable( {
        processing  : true,
        serverSide  : true,
        iDisplayLength: 25,
        ajax: $.fn.dataTable.pipeline( {
            url: getBaseURL() + dataUrl,
            pages: 5 // number of pages to cache
        } )
    }).columns().every(function(){
        var that = this;
        var elem = $(this.header()).parent().parent().find('#head-filter th');
        var input = $(elem[idx++]).find('input');
        $(input).on('keyup change', function(){
            if(that.search() !== this.value){
                that.search(this.value).draw();
            }
        });
    });

    $(tableId + ' thead tr#head-filter').appendTo('thead');
    $('.dataTables_processing').html('');
}

function dataTable(selector, length){
	$(document).ready(function(){
		$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings ) {
			return {
				"iStart":         oSettings._iDisplayStart,
				"iEnd":           oSettings.fnDisplayEnd(),
				"iLength":        oSettings._iDisplayLength,
				"iTotal":         oSettings.fnRecordsTotal(),
				"iFilteredTotal": oSettings.fnRecordsDisplay(),
				"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
				"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
			};
		}

        if(typeof(length) == 'undefined'){
            length = 25;
        }

		$(selector).DataTable({
			'iDisplayLength': length,
			'fnDrawCallback': function(x) {
				var currentPage = this.fnPagingInfo().iPage;
				var displayLength = this.fnPagingInfo().iLength;
				var page = (currentPage * displayLength) + 1;
				rearrangeDataTableNumbering(selector, page);
			}
		});
	});
}

function rearrangeDataTableNumbering(selector, page){
	var firstField = $(selector).find('th:first-child').html().toLowerCase();
	if(firstField == 'no'){
		var trList = $(selector).find('tbody tr');
		for(var i=0; i<trList.length; i++){
			$(trList[i]).find('td:first-child').html(page+i);
		}
	}
}

function dataTables(selector){
    $(document).ready(function(){
        $(selector).DataTable({
            'iDisplayLength': 25,
            "order": []
        });
    });
}

function setActiveMenu(url){
	if(typeof(url) == 'undefined'){
		url = document.URL;
	}
	$('li').has('a[href="'+url+'"]').addClass('active');
}

function validateRequiredInput(){
	var required = $(':required');
	for(var i=0; i<required.length; i++){
		$(required[i]).blur(function(t){
			var element = t.currentTarget;
			var value = $(element).val();
			var parent = $(element).parent();

			if(value.length == 0){
				$(parent).addClass('has-error');
			} else{
				$(parent).removeClass('has-error');
			}
		});
	}
}

function scrollToTop(){
    $('html, body').animate({
        scrollTop: 0
    }, 'slow');
 }

function chevronActive(classname){
    $('.chevron-shapes li').removeClass('active');
    $('.chevron-shapes li.'+classname).addClass('active');
}

function isValidDate(dateString){
    // First check for the pattern
    if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Check the ranges of month and year
    if(year < 1000 || year > 3000 || month == 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
        monthLength[1] = 29;

    // Check the range of the day
    return day > 0 && day <= monthLength[month - 1];
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function dateTimePicker(selector) {
	$(selector).datetimepicker({
		format: 'Y-m-d H:i'
	});
}

function datePicker(selector) {
	$(selector).datetimepicker({
		timepicker: false,
		format: 'Y-m-d',
		scrollMonth: false,
		scrollTime: false,
		scrollInput: false,
		yearStart: 1930
	});
}

function freezeScreen(timeout) {
	$('body').append("<div id='freeze-layer'></div>");
	if(!isNaN(timeout)){
		setTimeout(function () {
			$('#freeze-layer').remove();
		}, timeout);
	}
}

function unfreezeScreen() {
	$('#freeze-layer').remove();
}

function ajaxDataTable(selector, url, collumn, defaultOrder){
	$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings ) {
		return {
			"iStart":         oSettings._iDisplayStart,
			"iEnd":           oSettings.fnDisplayEnd(),
			"iLength":        oSettings._iDisplayLength,
			"iTotal":         oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage":          Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
			"iTotalPages":    Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
		};
	};

	if(typeof defaultOrder == 'undefined'){
		defaultOrder = [ 1, "asc" ];
	}

	$(selector).DataTable({
		processing: true,
		serverSide: true,
		iDisplayLength: 25,
		ajax: getBaseURL() + url,
		columns: collumn,
		order: [defaultOrder],
		fnDrawCallback: function(x) {
			var currentPage = this.fnPagingInfo().iPage;
			var displayLength = this.fnPagingInfo().iLength;
			var page = (currentPage * displayLength) + 1;
			rearrangeDataTableNumbering(selector, page);
		}
	});
}

function getCurrentDate() {
	return moment().format("YYYY-MM-DD");
}

function currencyFormat(value, curr) {
	value = isNaN(value) ? 0 : value;
	if(typeof curr == 'undefined'){
		curr = '';
	} else{
		curr = curr.trim().toUpperCase();
	}

	if(value < 0){
		value *= -1;
		return '(' + accounting.formatMoney(value, curr+' ', 2, ".", ",") + ')';
	} else{
		return accounting.formatMoney(value, curr+' ', 2, ".", ",");
	}
}

function setCleanPreview(source, destination){
	$(destination).html('').addClass('hidden');
	var sourceComponents = $(source).find('input, textarea, select');
	var i, j, value, type, activeComponent, parent, sourceClass, child, temp;

	for(i=0; i<sourceComponents.length; i++){
		activeComponent = $(sourceComponents[i]);
		if (activeComponent.is("select")) {
			value = $(sourceComponents[i]).val();
			value = $(sourceComponents[i]).find('option[value='+value+']').html();
			$(sourceComponents[i]).attr('preview-value', value);
		} else if (activeComponent.is("input")) {
			type = $(sourceComponents[i]).attr('type');
			value = null;

			if(type == 'text' || type == 'email' || type == 'number'){
				value = $(sourceComponents[i]).val();
			} else if(type == 'radio'){
				if($(sourceComponents[i]).prop('checked')) {
					value = $(sourceComponents[i]).attr('preview-value');
				} else{
					value = null;
				}
			} else if(type == 'checkbox'){
				if($(sourceComponents[i]).is(':checked')){
					value = '&#10003;';
				} else{
					value = '&times;';
				}
			} else{
				value = $(sourceComponents[i]).val();
			}

			$(sourceComponents[i]).attr('preview-value', value);
		} else if (activeComponent.is("textarea")) {
			value = $(sourceComponents[i]).val();
			$(sourceComponents[i]).attr('preview-value', value);
		}
	}

	var sourceHtml = $(source).html();
	$(destination).html(sourceHtml);
	var inputComponents = $(destination).find('input, textarea, select');

	for(i=0; i<inputComponents.length; i++){
		activeComponent = $(inputComponents[i]);
		parent = $(inputComponents[i]).parent().attr('id');
		if(parent == destination){
			continue;
		}

		if (activeComponent.is("input")) {
			type = $(inputComponents[i]).attr('type');
			parent = $(inputComponents[i]).parent().attr('class');

			if(type == 'hidden'){
				$(inputComponents[i]).remove();
				continue;
			} else if(typeof type == 'undefined'){
				type = 'text';
			}

			value = $(inputComponents[i]).attr('preview-value');

			if(typeof parent != 'undefined' && parent.indexOf('input-group') != -1){
				value = [];
				child = $(inputComponents[i]).parent().children();
				for(j=0; j<child.length; j++){
					temp = $(child[j]).attr('class');

					if ($(child[j]).is("input")) {
						value.push($(child[j]).attr('preview-value'));
					} else if(typeof temp != 'undefined' && temp.indexOf('input-group-addon') != -1){
						value.push($(child[j]).html());
					}
				}

				value = value.join(' ');
				$(inputComponents[i]).parent().parent().html(value);
				continue;
			} else if(type == 'radio'){
				value = $(inputComponents[i]).attr('preview-value');
			} else if(type == 'button' || type == 'submit'){
				$(inputComponents[i]).remove();
				continue;
			}
		} else if (activeComponent.is("select")) {
			value = $(inputComponents[i]).attr('preview-value');
		} else if (activeComponent.is("textarea")) {
			value = $(inputComponents[i]).attr('preview-value');
		}

		$(inputComponents[i]).parent().html(value);
		$(inputComponents[i]).remove();
	}

	var formGroupList = $(destination).find('.form-group');
	for(i=0; i<formGroupList.length; i++){
		$(formGroupList[i]).find('label').siblings('div').addClass('form-control-static');
	}

	sourceClass = $(source).attr('class');
	$(destination).find('.btn').remove();
	$(destination).find('.remove-on-preview').remove();
	$(destination).find('.hidden').remove();
	$(destination).find('.table').removeClass('tabel-small-padding');
	$(destination).removeClass('hidden').attr('class', sourceClass);
}

function generateRandomString()
{
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var unix = Math.round(+new Date()/1000);

	for( var i=0; i < 10; i++ )
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return unix + '-' + text;
}

function alertWarning(msg) {
	return "<div class='alert alert-warning'>"+msg+"</div>";
}

function alertDanger(msg) {
	return "<div class='alert alert-danger'>"+msg+"</div>";
}

function alertSuccess(msg) {
	return "<div class='alert alert-success'>"+msg+"</div>";
}

function titleCase(str){
	return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}

function redirectTo(url, timeout) {
	if(typeof timeout == 'undefined'){
		timeout = 0;
	}

	setTimeout(function () {
		location.href = url;
	}, timeout);
}

function chosenConvert(selector) {
	setTimeout(function () {
		$(selector).chosen();
    }, 100);
}

$(document).ready(function(){
	setActiveMenu();
	setInputPlaceholder();
	validateRequiredInput();
	$('.title-tip').tooltip();
});