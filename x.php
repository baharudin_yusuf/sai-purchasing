<?php
$data = [];
$data[] = ['PROPOSAL_REQUEST_APPROVAL', 'Pengajuan Budget Proposal oleh section {section_name} dengan id {budget_proposal_id} memerlukan konfirmasi'];
$data[] = ['PROPOSAL_APPROVED', 'Pengajuan Budget Proposal {budget_proposal_id} telah diterima oleh manager {department_name}'];
$data[] = ['PROPOSAL_REJECTED', 'Pengajuan Budget Proposal {budget_proposal_id} telah ditolak oleh manager {department_name}'];
$data[] = ['PROPOSAL_ITEM_REJECTED', 'Pengajuan item {good_service} pada Budget Proposal oleh dengan id {budget_proposal_id} telah ditolak oleh manager {department_name}'];
$data[] = ['TRANSFER_REQUEST_APPROVAL', 'Pengajuan Budget Transfer {budget_transfer_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['TRANSFER_APPROVED', 'Pengajuan Budget Transfer {budget_transfer_id} telah diterima oleh manager {department_name}'];
$data[] = ['TRANSFER_APPROVED_BY_DEST', 'Anda menerima Budget Transfer {budget_transfer_id} dari section {section_name}'];
$data[] = ['TRANSFER_REJECTED', 'Pengajuan Budget Transfer {budget_transfer_id} telah ditolak oleh manager {department_name}'];
$data[] = ['ADDITIONAL_REQUEST_APPROVAL', 'Pengajuan Additional Budget {budget_additional_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['ADDITIONAL_APPROVED', 'Pengajuan Additional Budget {budget_additional_id} telah diterima oleh manager {department_name}'];
$data[] = ['ADDITIONAL_REJECTED', 'Pengajuan Additional Budget {budget_additional_id} telah ditolak oleh manager {department_name}'];
$data[] = ['PR_REQUEST_APPROVAL', 'Pengajuan Purchase Requisition {purchase_requisition_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['PR_APPROVED', 'Pengajuan Purchase Requisition {purchase_requisition_id} telah diterima oleh manager {department_name}'];
$data[] = ['PR_REJECTED', 'Pengajuan Purchase Requisition {purchase_requisition_id} telah ditolak oleh manager {department_name}'];
$data[] = ['PR_ITEM_REJECTED', 'Pengajuan item {good_service} pada Purchase Requisition {purchase_requisition_id} telah ditolak oleh manager {department_name}'];
$data[] = ['PR_REVISION_REQUEST_APPROVAL', 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['PR_REVISION_APPROVED', 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} telah diterima oleh departemen {department_name}'];
$data[] = ['PR_REVISION_REJECTED', 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} telah ditolak oleh departemen {department_name}'];
$data[] = ['PR_REVISION_CANCELED', 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} telah dibatalkan oleh section {section_name}'];
$data[] = ['PO_REQUEST_APPROVAL', 'Pengajuan Purchase Order {purchase_order_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['PO_APPROVED', 'Pengajuan Purchase Order {purchase_order_id} telah diterima oleh manager {department_name}'];
$data[] = ['PO_REJECTED', 'Pengajuan Purchase Order {purchase_order_id} telah ditolak oleh manager {department_name}'];
$data[] = ['PO_INVOICE_APPROVAL', 'Pengajuan Invoice {invoice_id} pada Purchase Order {purchase_order_id} memerlukan konfirmasi'];
$data[] = ['PO_INVOICE_APPROVED', 'Pengajuan Invoice {invoice_id} pada Purchase Order {purchase_order_id} telah diterima oleh manager {department_name}'];
$data[] = ['PO_INVOICE_REJECTED', 'Pengajuan Invoice {invoice_id} pada Purchase Order {purchase_order_id} telah ditolak oleh manager {department_name}'];
$data[] = ['PO_REVISION_REQUEST_APPROVAL', 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['PO_REVISION_APPROVED', 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} telah diterima oleh departemen {department_name}'];
$data[] = ['PO_REVISION_REJECTED', 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} telah ditolak oleh departemen {department_name}'];
$data[] = ['PO_REVISION_CANCELED', 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} telah dibatalkan oleh section {section_name}'];
$data[] = ['DP_REQUEST_APPROVAL', 'Pengajuan Direct Purchase {direct_purchase_id} oleh section {section_name} memerlukan konfirmasi'];
$data[] = ['DP_APPROVED', 'Pengajuan Direct Purchase {direct_purchase_id} telah diterima oleh manager {department_name}'];
$data[] = ['DP_REJECTED', 'Pengajuan Direct Purchase {direct_purchase_id} telah ditolak oleh manager {department_name}'];
$data[] = ['DP_VP_APPROVAL', 'Pengajuan Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} memerlukan konfirmasi'];
$data[] = ['DP_VP_APPROVED', 'Pengajuan Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah diterima oleh manager {department_name}'];
$data[] = ['DP_VP_REJECTED', 'Pengajuan Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah ditolak oleh manager {department_name}'];
$data[] = ['DP_VP_READY_FOR_FA', 'Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah disetujui oleh {department_name}. Silahkan melakukan pembayaran'];
$data[] = ['DP_READY_FOR_ACTUAL', 'Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah dibayar oleh FA. Silahkan mengupdate harga aktual'];
$data[] = ['DP_VR_READY_FOR_FA', 'Voucher Receiving {vr_id} pada Direct Purchase {direct_purchase_id} telah dikirimkan oleh section {section_name}'];

mysql_connect('localhost', 'root', '12345');
mysql_select_db('sai_payroll2');

foreach($data as $list){
	$key = $list[0];
	$value = $list[1];
	
	$sql = "INSERT INTO `notification_message`(`key`, `message`) VALUES ('$key','$value')";
	//$sql = addslashes($sql);
	mysql_query($sql) or die(mysql_error());
}
?>