<?php
namespace app\controllers\dp;

use app\models\DirectPurchase;

class ReportController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionItem()
    {
        $data = self::getReceivingData();
        return $this->render('list-item', $data);
    }

    private function getReceivingData()
    {
        $cond = [
            'is_approved_by_lp_manager' => 1,
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        $params = [];
        if (post_data('only-completely-received')) {
            set_session('dp_only_completely_received', true);
            $cond['is_received'] = 1;
            $params['only_completely_received'] = 1;
        } else {
            set_session('dp_only_completely_received', false);
            $params['only_completely_received'] = 0;
        }

        if (post_data('only-completely-delivered')) {
            set_session('dp_only_completely_delivered', true);
            $cond['is_delivered'] = 1;
            $params['only_completely_delivered'] = 1;
        } else {
            set_session('dp_only_completely_delivered', false);
            $params['only_completely_delivered'] = 0;
        }

        $params['data'] = DirectPurchase::find()->where($cond)->all();
        return $params;
    }

    public function actionDownloadReceiving()
    {
        $objPHPExcel = new \PHPExcel();

        $style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );

        /* header file */
        $header[] = ['DP ID', 'PR ID', 'GOODS SERVICE', 'QUANTITY', 'RECEIVED', 'DELIVERED'];
        for ($i = 0; $i < count($header); $i++) {
            $row = $i + 1;
            $coll = 'A';

            for ($j = 0; $j < count($header[$i]); $j++) {
                $cell = $coll . $row;
                $objPHPExcel->getActiveSheet()->SetCellValue($cell, $header[$i][$j]);
                $coll++;
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);

        $data = self::getReceivingData();
        $row = count($header) + 1;
        foreach ($data['data'] as $list) {
            foreach ($list->purchaseRequisitionList as $prList) {
                foreach ($prList->realizationActual as $actual) {
                    $col = 'A';
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->code);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->directPurchase->code);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->goodsService->name);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->quantity);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->received);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col) . "$row", $actual->delivered);

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:$col$row")->applyFromArray($style);
                    $row++;
                }
            }
        }

        $filename = 'DP-REPORT-RECEIVING-' . date('dmY') . '.xlsx';
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(TEMP_DIR . $filename);

        return \Yii::$app->response->sendFile(TEMP_DIR . $filename);
    }
}

?>
