<?php
namespace app\controllers\dp;

use app\controllers\NotificationController;
use app\helpers\Auth;
use app\models\DirectPurchase;
use app\models\master\Section;
use Yii;

class ReceivingController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $params = [
            'data' => DirectPurchase::find()->where([
                'is_canceled' => 0,
                'is_rejected' => 0,
            ])->all(),
        ];
        return $this->render('index', $params);
    }

    public function actionDetail()
    {
        $id = get_data('id');
        $dp = DirectPurchase::findOne($id);
        if (is_null($dp)) {
            parent::throw404Error();
        }

        $params = [
            'data' => $dp,
            'prList' => $dp->purchaseRequisitionList
        ];
        return $this->render('detail', $params);
    }

    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $dpId = $request->post('dp_id');
        $dp = DirectPurchase::findOne($dpId);
        if (is_null($dp)) {
            parent::throw404Error();
        }

        $nDataUpdated = 0;
        $sectionNotified = [];

        foreach ($dp->purchaseRequisitionList as $dpPr) {
            foreach ($dpPr->realizationActual as $actual) {
                $receivedKey = "received_" . $actual->id;
                $received = intval($request->post($receivedKey, 0));
                if ($received > 0 && $actual->isAllowReceive()) {
                    $actual->received = $received;
                    $actual->save();
                    $nDataUpdated++;

                    // catat notif ke pembuat PR
                    $pr = $actual->purchaseRequisition;
                    $prSectionId = $pr->section_id;
                    if (!in_array($prSectionId, $sectionNotified)) {
                        $sectionNotified[] = $prSectionId;
                    }
                }

                $deliveredKey = "delivered_" . $actual->id;
                $delivered = intval($request->post($deliveredKey, 0));
                if ($delivered > 0 && $actual->isAllowDeliver()) {
                    $actual->delivered = $delivered;
                    $actual->save();
                    $nDataUpdated++;
                }
            }
        }

        // notif ke LP apabila ada perbaruan data
        if ($nDataUpdated > 0) {
            $lpCode = parent::getSectionCode('LP');
            $lpSection = Section::findOne(['code' => $lpCode]);
            if (!in_array($lpSection->id, $sectionNotified)) {
                $sectionNotified[] = $lpSection->id;
            }
        }

        // kirim pemberitahuan
        if ($nDataUpdated > 0 && count($sectionNotified) > 0) {
            $dpCode = $dp->getCode();
            $userdata = Auth::user();
            $sectionName = $userdata->section->name;
            $notifMsg = parent::getNotifMessage('DP_DELIVERING_READY');
            $notifMsg = str_replace('{direct_purchase_id}', $dpCode, $notifMsg);
            $notifMsg = str_replace('{section_name}', $sectionName, $notifMsg);
            $notifData = [
                'category' => 'dp',
                'type' => 'delivering-ready',
                'target_id' => $dpId,
                'title' => $notifMsg,
                'section_list' => $sectionNotified
            ];
            NotificationController::sendNotification($notifData);
        }

        if ($nDataUpdated > 0) {
            return "
            <div class='alert alert-success center'>$nDataUpdated data berhasil diperbarui</div>
            <script> setTimeout(function() { location.reload(); }, 1500); </script>";
        } else {
            return "<div class='alert alert-warning'>Perhatian! Tidak ada data diperbarui</div>";
        }
    }

}

?>
