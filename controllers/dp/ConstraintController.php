<?php
namespace app\controllers\dp;

use app\controllers\pr\MainController as PRMain;
use app\helpers\Auth;
use app\helpers\PriceConverter;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\DirectPurchase;
use app\models\DpPrDetail;
use app\models\DpRealization;
use app\models\DpRealizationActual;
use app\models\master\Budget;
use app\models\master\Department;
use app\models\master\Section;
use app\models\PurchaseRequisition;

class ConstraintController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public static function allowCreate()
    {
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');
        if ($activeUser->roleAs('section') && $activeUser->section_id == $lpSection->id) {
            return true;
        } else {
            return false;
        }
    }

    public static function allowUploadPayment(DpRealization $dpRealization)
    {
        if ($dpRealization->is_paid) {
            return false;
        }

        $activeUser = Auth::user();
        $faDept = Department::findByCode('FA');
        if ($activeUser->roleAs('manager') && $activeUser->department_id == $faDept->id) {
            return true;
        }
        return false;
    }

    public static function allowApproveDP(DirectPurchase $directPurchase)
    {
        $manager = $directPurchase->is_approved_by_lp_manager;
        $rejected = $directPurchase->is_rejected;
        $canceled = $directPurchase->is_canceled;
        $activeUser = Auth::user();

        if ($manager || $rejected || $canceled) {
            return false;
        } else if ($activeUser->roleAs('manager')) {
            $deptHRGA = Department::findByCode('HRGA');
            $deptFA = Department::findByCode('FA');
            if (in_array($activeUser->department_id, [$deptHRGA->id, $deptFA->id])) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowCreateVP(DirectPurchase $directPurchase)
    {
        // jika bukan section dan bukan LP
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');

        if (!$activeUser->roleAs('section') || $activeUser->section_id != $lpSection->id) {
            return [
                'allow' => false,
                'msg' => 'Anda tidak memiliki hak akses untuk membuat VP'
            ];
        }

        // unable to create vp when dp has not been approved by manager
        if (!$directPurchase->is_approved_by_lp_manager) {
            return [
                'allow' => false,
                'msg' => 'Error! DP belum disetujui oleh LP Manager!'
            ];
        }

        // find realization price inserted
        $realization = $directPurchase->getRealization()
            ->where(['is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        if (count($realization) == 0) {
            return ['allow' => true];
        } else {
            // check if there's vp that hasn't been approved
            foreach ($realization as $list) {
                if (!$list->is_approved_by_manager) {
                    return [
                        'allow' => false,
                        'msg' => 'Error! Anda sudah mengajukan VP, silahkan menunggu konfirmasi dari manager'
                    ];
                } else if (!$list->is_paid) {
                    return [
                        'allow' => false,
                        'msg' => 'Error! Voucher Paying sedang menunggu proses entri oleh FA'
                    ];
                }
            }

            // find actual price updated
            $actualPrice = 0;
            foreach ($directPurchase->purchaseRequisitionList as $prList) {
                foreach ($prList->realizationActual as $actual) {
                    if ($actual->price == 0) {
                        return [
                            'allow' => false,
                            'msg' => 'Error! Anda harus melengkapi data pada tab DP Actual terlebih dahulu'
                        ];
                    }

                    $price = $actual->price;
                    $cPrice = PriceConverter::convert($price, $actual->goodsService->currency_id);
                    $actualPrice += ($cPrice * $actual->quantity);
                }
            }

            $amountRealization = 0;
            foreach ($realization as $list) {
                $amountRealization += $list->amount;
            }

            if ($actualPrice > $amountRealization) {
                return ['allow' => true];
            } else {
                return [
                    'allow' => false,
                    'msg' => 'Error! Tidak ada kekurangan pembayaran tersedia!'
                ];
            }
        }
    }

    public static function allowCreateVR(DirectPurchase $directPurchase)
    {
        // jika bukan section dan bukan LP
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');
        if (!$activeUser->roleAs('section') || $activeUser->section_id != $lpSection->id) {
            return [
                'allow' => false,
                'msg' => 'Anda tidak memiliki hak akses untuk membuat VR'
            ];
        }

        // unable to create vp when dp has not been approved by manager
        if (!$directPurchase->is_approved_by_lp_manager) {
            return [
                'allow' => false,
                'msg' => 'Error! DP belum disetujui oleh LP Manager!'
            ];
        }

        // check if vr already exists
        $vrCount = $directPurchase->getRealization()
            ->where(['is_voucher_receiving' => 1])
            ->count();
        if ($vrCount > 0) {
            return [
                'allow' => false,
                'msg' => 'Error! Voucher Receiving telah dikirimkan pada FA!'
            ];
        }

        // find number of realization actual that already fixed
        $dpList = [];
        foreach ($directPurchase->purchaseRequisitionList as $list) {
            $dpList[] = $list->id;
        }

        $unfixedActual = DpRealizationActual::find()
            ->where(['is_fixed' => 0])
            ->andWhere(['IN', 'dp_pr_detail_id', $dpList])
            ->count();

        if ($unfixedActual > 0) {
            return [
                'allow' => false,
                'msg' => 'Anda harus melengkapi DP Actual terlebih dahulu!'
            ];
        } else if ($directPurchase->amount_plan <= $directPurchase->amount_actual) {
            return [
                'allow' => false,
                'msg' => 'Plan tidak lebih besar dari actual!'
            ];
        } else {
            return ['allow' => true];
        }
    }

    public static function allowApproveVoucherPaying()
    {
        $activeUser = Auth::user();
        if ($activeUser->roleAs('manager')) {
            $section = Section::findByCode('LP');
            if ($activeUser->department_id == $section->department_id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowCancelVoucherPaying()
    {
        $activeUser = Auth::user();
        if ($activeUser->roleAs('section')) {
            $section = Section::findByCode('LP');
            if ($activeUser->section_id == $section->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowUpdateActualPrice(DirectPurchase $directPurchase)
    {
        $activeUser = Auth::user();
        $realization = $directPurchase
            ->getRealization()
            ->where(['is_paid' => 1])
            ->count();

        $section = Section::findByCode('LP');
        if ($activeUser->roleAs('section') && $activeUser->section_id == $section->id) {
            if ($realization > 0) {
                $dpPrList = [];
                foreach ($directPurchase->purchaseRequisitionList as $list) {
                    $dpPrList[] = $list->id;
                }

                $actual = DpRealizationActual::find()
                    ->where(['IN', 'dp_pr_detail_id', $dpPrList])
                    ->andWhere(['is_fixed' => 0])
                    ->all();

                if (count($actual) > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowCancel(DirectPurchase $directPurchase)
    {
        $activeUser = Auth::user();
        $section = Section::findByCode('LP');
        if (!$activeUser->roleAs('section') || $activeUser->section_id != $section->id) {
            return false;
        } else {
            if ($directPurchase->is_approved_by_lp_manager) {
                return false;
            } else if ($directPurchase->is_canceled || $directPurchase->is_rejected) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static function allowReceiveItem($item_id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $DR = DpRealizationActual::findOne($item_id);
            $dpDetail = DpPrDetail::findOne($DR->dp_pr_detail_id);
            $prId = $dpDetail->purchase_requisition_id;
            $dpId = $dpDetail->direct_purchase_id;

            $dp = DirectPurchase::findOne($dpId);

            if ($dp->is_rejected) {
                return false;
            } else if ($dp->is_canceled) {
                return false;
            } else if ($dp->is_received) {
                return false;
            } else if ($dp->is_delivered) {
                return false;
            } else if ($dp->is_approved_by_lp_manager) {
                $section = $activeUser->section;

                if ($section->is_exim || $section->is_ga_gs) {
                    $PR = PurchaseRequisition::findOne($prId);
                    $finalId = $PR->budget_final_id;

                    $final = BudgetFinal::findOne($finalId);
                    $proposalId = $final->budget_proposal_id;

                    $Proposal = BudgetProposal::findOne($proposalId);
                    $budgetId = $Proposal->budget_id;

                    /* cek apakah packaging component */
                    $Budget = Budget::findOne($budgetId);
                    $packaging_component = $Budget->is_packaging_component;

                    if ($packaging_component) {
                        if ($section->is_exim) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        if ($section->is_ga_gs) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static function allowDeliverItem($item_id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $DR = DpRealizationActual::findOne($item_id);
            $dpDetail = DpPrDetail::findOne($DR->dp_pr_detail_id);
            $prId = $dpDetail->purchase_requisition_id;
            $dpId = $dpDetail->direct_purchase_id;

            $dp = DirectPurchase::findOne($dpId);

            if ($dp->is_rejected) {
                return false;
            } else if ($dp->is_canceled) {
                return false;
            } else if ($dp->is_delivered) {
                return false;
            } else if ($dp->is_approved_by_lp_manager) {
                $PR = PurchaseRequisition::findOne($prId);
                $finalId = $PR->budget_final_id;

                $final = BudgetFinal::findOne($finalId);
                $proposalId = $final->budget_proposal_id;

                $Proposal = BudgetProposal::findOne($proposalId);

                if ($activeUser->section_id == $Proposal->section_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static function CheckPRClosed()
    {
        $added_pr = get_session('added_pr');
        $msg = "";
        $open_all = true;

        foreach ($added_pr as $prId => $data) {
            $PR = PurchaseRequisition::findOne($prId);
            if ($PR->is_closed) {
                $pr_code = $PR->code;

                $msg = "<strong>Error!</strong> Purchase Requisition $pr_code telah ditutup!";
                $msg = alert_danger($msg);
                $open_all = false;
                break;
            }
        }

        return ['is_open_all' => $open_all, 'msg' => $msg];
    }

    public static function LimitCheck()
    {
        $added_pr = get_session('added_pr');
        $msg = "";
        $under_limit = true;

        foreach ($added_pr as $prId => $data) {
            $PR = PurchaseRequisition::findOne($prId);
            $price_estimation = $PR->price_estimation;

            /* CONVERT PRICE ESTIMATION TO CURRENT CURRENCY */
            $pr_currency = PRMain::getPRCurrencyUsed($prId);
            $currency_id = $pr_currency['id'];
            $price_estimation = parent::getConvertedPrice($price_estimation, $currency_id);

            /* COUNT ACTUAL PRICE FOR EACH PR */
            $actual = 0;
            foreach ($data as $list) {
                $actual += ($list['c_price_estimation'] * $list['quantity']);
            }

            if ($actual > $price_estimation) {
                $pr_code = $PR->code;

                $msg = "
				<strong>Error!</strong> Pengajuan detail aktual anda melebihi batas price estimation dari $pr_code.<br>
				Pengajuan anda : " . BASE_CURRENCY_NAME . ' ' . number_format($actual, 2, ',', '.') . ", batas $pr_code : " . BASE_CURRENCY_NAME . ' ' . number_format($price_estimation, 2, ',', '.') . "
				";
                $msg = alert_danger($msg);
                $under_limit = false;
                break;
            }
        }

        return ['is_under_limit' => $under_limit, 'msg' => $msg];
    }

}

?>
