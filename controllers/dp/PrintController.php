<?php
namespace app\controllers\dp;

use app\models\DirectPurchase;

class PrintController extends MainController
{
    public static $textLimit = 40;
    public static $maxLine = 26;
    private static $nextExists = false;

    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionFormDp()
    {
        $this->layout = 'print';
        $id = intval(get_data('id'));
        $data = DirectPurchase::firstOrFail($id);
        $page = intval(get_data('p', 1));
        $detail = self::getFormattedData($data);

        if (self::$nextExists) {
            $nextPage = $page + 1;
            $nextURL = url("dp/print/form-dp/?id=$id&p=$nextPage");
        } else {
            $nextURL = null;
        }

        if ($page > 1) {
            $prevPage = $page - 1;
            $prevURL = url("dp/print/form-dp/?id=$id&p=$prevPage");
        } else {
            $prevURL = null;
        }

        $params = [
            'data' => $data,
            'page' => $page,
            'detail' => $detail,
            'nextURL' => $nextURL,
            'prevURL' => $prevURL,
        ];
        return $this->render('form-dp', $params);
    }

    private static function getFormattedData(DirectPurchase $directPurchase)
    {
        $detail = [];
        foreach ($directPurchase->purchaseRequisitionList as $dpPrList) {
            foreach ($dpPrList->realizationActual as $actual) {
                $detail[] = $actual;
            }
        }

        $nData = count($detail);
        $page = intval(get_data('p', 1));
        $start = ($page - 1) * self::$maxLine;
        $end = $start + self::$maxLine;
        $end = ($end > $nData) ? $nData : $end;
        $blankText = '&nbsp;';

        $num = 0;
        $result = [];
        foreach ($detail as $actual) {
            $itemLine = [''];
            $lineCount = 0;

            $goodsService = $actual->goodsService;
            $gsNameSplit = explode(' ', $goodsService->name);
            foreach ($gsNameSplit as $gsWord) {
                $gsTemp = trim($itemLine[$lineCount] . ' ' . $gsWord);
                if (strlen($gsTemp) <= self::$textLimit) {
                    $itemLine[$lineCount] = $gsTemp;
                } else {
                    $lineCount++;
                    $itemLine[$lineCount] = $gsWord;
                }
            }

            $modLine = self::$maxLine - (count($result) % self::$maxLine);
            if (count($itemLine) > $modLine) {
                for ($i = 0; $i < $modLine; $i++) {
                    $result[] = [
                        'num' => $blankText,
                        'rowspan' => 1,
                        'code' => $blankText,
                        'goods_service' => $blankText,
                        'unit' => $blankText,
                        'plan_quantity' => $blankText,
                        'plan_price' => $blankText,
                        'plan_amount' => $blankText,
                        'actual_quantity' => $blankText,
                        'actual_price' => $blankText,
                        'actual_amount' => $blankText,
                    ];
                }
            }

            // get plan
            $plan = $actual->dpPrDetail->getRealizationPlan()
                ->where(['goods_service_id' => $actual->goods_service_id])
                ->one();

            foreach ($itemLine as $lineNum => $line) {
                $planPrice = $plan->display_price;
                if (strlen($plan->display_converted_price) > 0) {
                    $planPrice = $plan->display_converted_price;
                }

                $planAmount = $plan->display_subtotal;

                $actualPrice = $actual->display_price;
                if (strlen($actual->display_converted_price) > 0) {
                    $actualPrice = $actual->display_converted_price;
                }

                $actualAmount = $actual->display_subtotal;

                $result[] = [
                    'num' => $lineNum == 0 ? $num + 1 : '',
                    'rowspan' => $lineNum == 0 ? count($itemLine) : '',
                    'code' => $lineNum == 0 ? $actual->purchaseRequisition->code : '',
                    'goods_service' => $line,
                    'unit' => $lineNum == 0 ? $actual->goodsService->unit->name : '',
                    'plan_quantity' => $lineNum == 0 ? $plan->quantity : '',
                    'plan_price' => $lineNum == 0 ? $planPrice : '',
                    'plan_amount' => $lineNum == 0 ? $planAmount : '',
                    'actual_quantity' => $lineNum == 0 ? $actual->quantity : '',
                    'actual_price' => $lineNum == 0 ? $actualPrice : '',
                    'actual_amount' => $lineNum == 0 ? $actualAmount : '',
                ];
            }
            $num++;
        }

        // note
        // code goes here

        if ($num > $end) {
            self::$nextExists = true;
        }

        $newResult = [];
        for ($num = $start; $num < $start + self::$maxLine; $num++) {
            if (isset($result[$num])) {
                $newResult[] = $result[$num];
            }
        }
        $result = $newResult;

        $currentLine = count($result);
        for ($i = $currentLine; $i < self::$maxLine; $i++) {
            $result[] = [
                'num' => $blankText,
                'rowspan' => 1,
                'code' => $blankText,
                'goods_service' => $blankText,
                'unit' => $blankText,
                'plan_quantity' => $blankText,
                'plan_price' => $blankText,
                'plan_amount' => $blankText,
                'actual_quantity' => $blankText,
                'actual_price' => $blankText,
                'actual_amount' => $blankText,
            ];
        }

        return $result;
    }
}

?>
