<?php
namespace app\controllers\dp;

use app\controllers\BaseController;
use app\controllers\dp\ConstraintController as DPConstraint;
use app\controllers\NotificationController as Notification;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\DirectPurchase;
use app\models\master\Section;
use Yii;

class MainController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();
        parent::requirementCheck();

        $menu = ['Direct Purchase' => url('dp/main')];

        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');
        if ($activeUser->roleAs('section') && $activeUser->section_id == $lpSection->id) {
            $menu['Create New'] = url('dp/create');
        }

        if ($activeUser->roleAs('manager|admin')) {
            $menu['Pending Submission'] = url('dp/main/pending');
        }

        $menu['Report'] = [
            'Item Receiving' => url('dp/report/item'),
        ];
        $menu['Item Receiving'] = url('dp/receiving');

        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'direct-purchase-panel');

        return [];
    }

    public function actionLoadData()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        if ($this->include_rejected) {
            unset($cond['is_rejected']);
        }
        if ($this->include_canceled) {
            unset($cond['is_canceled']);
        }
        if ($this->only_rejected) {
            $cond['is_rejected'] = 1;
        }
        if ($this->only_canceled) {
            $cond['is_canceled'] = 1;
        }

        $data = DirectPurchase::find()
            ->where($cond)
            ->all();

        return $data;
    }

    public function actionIndex()
    {
        // redirect ke receiving jika section adalah ga/gs atau exim
        $activeUser = Auth::user();
        $section = $activeUser->section;
        if ($activeUser->roleAs('section')) {
            if ($section->is_exim || $section->is_ga_gs) {
                redirect(url('dp/receiving'));
            }
        }

        $data = [
            'data' => self::actionLoadData(),
            'activeUser' => $activeUser,
            'allowCreate' => ConstraintController::allowCreate()
        ];
        return $this->render('index', $data);
    }

    public function actionDetail()
    {
        $dpId = intval(get_data('id'));
        $data = DirectPurchase::firstOrFail($dpId);
        $data->save();

        $vpConstraint = DPConstraint::allowCreateVP($data);
        $vrConstraint = DPConstraint::allowCreateVR($data);

        $isReceiving = false;
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');
        if ($activeUser->roleAs('section') && $activeUser->section_id != $lpSection->id) {
            $isReceiving = true;
        }

        $isApproved = true;
        if ($data->is_canceled || $data->is_rejected || !$data->is_approved_by_lp_manager) {
            $isApproved = false;
        }

        $realization = $data->getRealization()->where(['is_paid' => 1])->count();
        $showDPActual = $realization > 0 ? true : false;

        $params = [
            'data' => $data,
            'activeUser' => $activeUser,
            'isReceiving' => $isReceiving,
            'isApproved' => $isApproved,
            'showDPActual' => $showDPActual,
            'allowCreateVP' => $vpConstraint['allow'],
            'allowCreateVR' => $vrConstraint['allow'],
            'allowCancelDP' => DPConstraint::allowCancel($data),
            'allowApproveDP' => DPConstraint::allowApproveDP($data),
            'allowApproveVP' => DPConstraint::allowApproveVoucherPaying(),
            'allowCancelVP' => DPConstraint::allowCancelVoucherPaying(),
            'allowUpdateActualPrice' => DPConstraint::allowUpdateActualPrice($data),
            'DPConstraint' => DPConstraint::class,
        ];
        return $this->render('detail', $params);
    }

    public function actionApproveDp($dpId = null)
    {
        $dpId = is_null($dpId) ? intval(post_data('id')) : $dpId;
        $dp = DirectPurchase::firstOrFail($dpId);
        if (DPConstraint::allowApproveDP($dp)) {
            $dp->is_approved_by_lp_manager = 1;
            $dp->approved_by_lp_manager_time = date('Y-m-d H:i:s');

            if ($dp->save()) {
                /* RECALCULATE PR QUANTITY */
                $dp->recalculatePrQuantity();

                // SEND NOTIF TO SECTION
                $notifMsg = Notificatixn::render('DP_APPROVED', [
                    'direct_purchase_id' => $dp->code,
                    'department_name' => Auth::user()->department->name
                ]);
                Notification::sendNotification([
                    'category' => 'dp',
                    'type' => 'success',
                    'target_id' => $dpId,
                    'title' => $notifMsg
                ]);

                // SEND NOTIF TO GA/GS ATAU EXIM
                $notifMsg = Notificatixn::render('DP_RECEIVING_READY', [
                    'direct_purchase_id' => $dp->code,
                ]);
                Notification::sendNotification([
                    'category' => 'dp',
                    'type' => 'receiving-ready',
                    'target_id' => $dpId,
                    'title' => $notifMsg
                ]);

                return alert_success('Direct purchase berhasil diterima!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Direct purchase gagal diterima!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menerima DP!');
        }
    }

    public function actionRejectDp($dpId = null)
    {
        $dpId = is_null($dpId) ? intval(post_data('id')) : $dpId;
        $dp = DirectPurchase::firstOrFail($dpId);
        if (DPConstraint::allowApproveDP($dp)) {
            $dp->is_rejected = 1;

            if ($dp->save()) {
                // kirim notif ke section
                $notifMsg = Notificatixn::render('DP_REJECTED', [
                    'direct_purchase_id' => $dp->code,
                    'department_name' => Auth::user()->department->name
                ]);
                Notification::sendNotification([
                    'category' => 'dp',
                    'type' => 'danger',
                    'target_id' => $dpId,
                    'title' => $notifMsg
                ]);

                return alert_success('Direct purchase berhasil ditolak!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Direct purchase gagal ditolak!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menolak DP!');
        }
    }

    public function actionCancelDp()
    {
        $dpId = intval(post_data('id'));
        if (DPConstraint::allowCancel($dpId)) {
            $dp = DirectPurchase::findOne($dpId);
            $dp->is_canceled = 1;

            if ($dp->save()) {
                /* SEND NOTIFICATION TO MANAGER */
                $userdata = parent::getUserData();
                $section_name = $userdata['section_name'];
                $dpCode = $dp->code;
                $notifMsg = parent::getNotifMessage('DP_CANCELED');
                $notifMsg = str_replace('{direct_purchase_id}', $dpCode, $notifMsg);
                $notifMsg = str_replace('{section_name}', $section_name, $notifMsg);

                Notification::sendNotification([
                    'category' => 'dp',
                    'type' => 'warning',
                    'target_id' => $dpId,
                    'title' => $notifMsg
                ]);

                $msg = "alert('Pengajuan direct purchase berhasil dibatalkan!'); reload();";
            } else {
                $msg = "alert('Error! Pengajuan direct purchase gagal dibatalkan!');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan DP');";
        }

        return "<script> $msg </script>";
    }

    public function actionPending()
    {
        $data['data'] = DirectPurchase::find()
            ->where(['is_approved_by_lp_manager' => 0, 'is_rejected' => 0, 'is_canceled' => 0])
            ->all();

        return $this->render('pending', $data);
    }

    public function actionUpdateActualPrice()
    {
        $dpId = intval(post_data('id'));
        $data = DirectPurchase::firstOrFail($dpId);
        if (!DPConstraint::allowUpdateActualPrice($data)) {
            parent::throw404Error();
        }

        $updated = 0;
        foreach ($data->purchaseRequisitionList as $dpPr) {
            foreach ($dpPr->realizationActual as $actual) {
                $id = $actual->id;
                $isUpdate = intval(post_data("checkbox-$id"));
                if (!$isUpdate) {
                    continue;
                }

                $price = floatval(post_data("price-$id"));
                $maxPrice = $actual->plan_price + (0.1 * $actual->plan_price);
                if ($price > $maxPrice) {
                    continue;
                }

                $actual->price = $price;
                $actual->is_fixed = 1;
                if ($actual->save()) {
                    $updated++;
                }
            }
        }

        if ($updated == 0) {
            return alert_warning('Tidak ada data DP Actual Payment diupdate!');
        } else {
            return alert_success("$updated data DP Actual Payment berhasil diupdate!") .
                "<script> reload(1500); </script>";
        }
    }

    public function actionBulkAction()
    {
        $action = post_data('action');
        $nApproved = 0;
        $nRejected = 0;

        $listId = explode(',', post_data('list_id'));
        foreach ($listId as $id) {
            if ($action == 'approve') {
                $this->actionApproveDp($id);
                $nApproved++;
            } else {
                $this->actionRejectDp($id);
                $nRejected++;
            }
        }

        if ($nApproved > 0) {
            $msg = "$nApproved pengajuan data berhasil diterima";
        } else if ($nRejected > 0) {
            $msg = "$nRejected pengajuan data berhasil ditolak";
        } else {
            $msg = 'Terjadi kesalahan! Data gagal diproses';
        }

        return "<script> alert('$msg'); reload(); </script>";
    }
}

?>
