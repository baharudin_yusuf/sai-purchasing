<?php
namespace app\controllers\dp;

use app\controllers\NotificationController as Notification;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\helpers\PriceConverter;
use app\models\DirectPurchase;
use app\models\DpPrDetail;
use app\models\DpRealizationActual;
use app\models\DpRealizationPlan;
use app\models\master\Department;
use app\models\master\RevenueTax;
use app\models\master\Section;
use app\models\master\ValueAddedTax;
use app\models\PurchaseRequisition;

class CreateController extends MainController
{
    private static $DP_THRESHOLD = 10;

    public function behaviors()
    {
        parent::behaviors();

        $userdata = $this::getUserData();
        if ($userdata['section_code'] != parent::getSectionCode('LP')) {
            redirect(url());
        }

        return [];
    }

    public function actionIndex()
    {
        $activeUser = Auth::user();
        $prReadyCount = PurchaseRequisition::find()
            ->where(['is_closed' => 0, 'is_approved_by_fa' => 1, 'is_direct_purchase' => 1])
            ->count();

        $params = [
            'activeUser' => $activeUser,
            'prReadyCount' => $prReadyCount,
            'sessionKey' => random_string(),
        ];
        return $this->render('index', $params);
    }

    public function actionGetSection()
    {
        $result = '';
        $deptId = intval(post_data('department_id'));
        foreach (Section::getOption($deptId) as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        return $result;
    }

    public function actionGetPrOpen()
    {
        $sectionId = intval(post_data('section_id'));

        $sessionKey = post_data('session_key');
        $addedPR = get_session($sessionKey, []);
        $addedPR = is_array($addedPR) ? $addedPR : [];

        $prList = [];
        foreach ($addedPR as $prId => $prData) {
            $prList[] = $prId;
        }

        $pr = PurchaseRequisition::find()->where([
            'section_id' => $sectionId,
            'is_closed' => 0,
            'is_direct_purchase' => 1,
            'is_approved_by_fa' => 1
        ])->andWhere([
            'not in', 'id', $prList
        ])->all();

        $result = '';
        foreach ($pr as $list) {
            $key = $list->id;
            $value = "[" . $list->code . '] ' . $list->budget->name;
            $result .= "<option value='$key'>$value</option>";
        }
        return $result;
    }

    public function actionAddPr()
    {
        $departmentOption = Department::getOption();

        $sectionOption = [];
        foreach ($departmentOption as $key => $value) {
            $sectionOption = Section::getOption($key);
            break;
        }

        $params = [
            'departmentOption' => $departmentOption,
            'sectionOption' => $sectionOption,
            'sessionKey' => post_data('session_key')
        ];
        return $this->renderPartial('add-pr', $params);
    }

    public function actionAddNewPr()
    {
        $prId = intval(post_data('purchase_requisition_id'));
        $purchaseRequsition = PurchaseRequisition::firstOrFail($prId);

        $sessionKey = post_data('session_key');
        $addedPR = get_session($sessionKey, []);
        $addedPR = is_array($addedPR) ? $addedPR : [];

        // tax
        $vatId = null;
        foreach (ValueAddedTax::getOption() as $key => $value) {
            $vatId = $key;
            break;
        }

        $revTaxId = null;
        foreach (RevenueTax::getOption() as $key => $value) {
            $revTaxId = $key;
            break;
        }

        $prDetail = $purchaseRequsition->getPurchaseRequisitionDetail()
            ->where(['>', 'available_quantity', 0])
            ->all();

        $newPR = [];
        foreach ($prDetail as $detail) {
            $gsCurrency = $detail->goodsService->currency;
            $baseCurrency = PriceConverter::getActiveBaseCurrency();
            $convertedPrice = PriceConverter::convert($detail->price_estimation, $gsCurrency->id);
            $convertedPrice *= $detail->available_quantity;

            // vat
            $vat = ValueAddedTax::findOne($vatId);
            if (!is_null($vat)) {
                $vatAmount = $vat->rate / 100 * $convertedPrice;
                $convertedPrice += $vatAmount;
            }

            // rev tax
            $revTax = RevenueTax::findOne($revTaxId);
            if (!is_null($revTax)) {
                $revTaxAmount = $revTax->rate / 100 * $convertedPrice;
                $convertedPrice += $revTaxAmount;
            }

            $newPR[$detail->id] = [
                'quantity' => $detail->available_quantity,
                'rev_tax_id' => $revTaxId,
                'vat_id' => $vatId,
                'price_estimation' => $detail->price_estimation,
                'converted_price' => $convertedPrice,
                'display_converted_price' => $baseCurrency->name . ' ' . currency_format($convertedPrice)
            ];
        }

        if (empty($newPR)) {
            $prName = '[' . $purchaseRequsition->code . '] ' . $purchaseRequsition->budget->name;
            return alert_danger("Tidak ada item tersedia pada PR $prName untuk ditambahkan");
        }

        $addedPR[$prId] = $newPR;
        set_session($sessionKey, $addedPR);

        return alert_success('Data PR berhasil ditambahkan') .
            "<script> closeModal(1000); loadAddedPR(); </script>";
    }

    public function actionLoadAddedPr()
    {
        $sessionKey = post_data('session_key');
        $addedPR = get_session($sessionKey, []);
        $addedPR = is_array($addedPR) ? $addedPR : [];

        $prList = [];
        $total = 0;
        foreach ($addedPR as $prId => $prData) {
            $prList[] = $prId;
            foreach ($prData as $prDatum) {
                $total += $prDatum['converted_price'];
            }
        }

        $baseCurrency = PriceConverter::getActiveBaseCurrency();

        $params = [
            'addedPR' => $addedPR,
            'revTaxOption' => RevenueTax::getOption(),
            'vatOption' => ValueAddedTax::getOption(),
            'data' => PurchaseRequisition::find()->where(['IN', 'id', $prList])->all(),
            'displayTotal' => $baseCurrency->name . ' ' . currency_format($total),
            'sessionKey' => $sessionKey,
            'threshold' => self::$DP_THRESHOLD
        ];
        return $this->renderPartial('data', $params);
    }

    public function actionUpdateAddedPr()
    {
        $sessionKey = post_data('session_key');
        $addedPR = get_session($sessionKey, []);
        $addedPR = is_array($addedPR) ? $addedPR : [];

        $prList = [];
        foreach ($addedPR as $prId => $prData) {
            $prList[] = $prId;
        }

        foreach ($addedPR as $prId => $prData) {
            $purchaseRequisition = PurchaseRequisition::firstOrFail($prId);
            foreach ($purchaseRequisition->getNonCancelReject() as $detail) {
                if (!isset($addedPR[$prId][$detail->id])) {
                    continue;
                }

                $inputKey = $prId . '_' . $detail->id;
                $price = floatval(post_data("price_estimation_$inputKey"));
                $treshold = (self::$DP_THRESHOLD / 100) * $detail->price_estimation;
                $maxPrice = $detail->price_estimation + $treshold;
                if ($price > $maxPrice) {
                    $price = $maxPrice;
                }

                $currency = $detail->goodsService->currency;
                $baseCurrency = PriceConverter::getActiveBaseCurrency();
                $convertedPrice = PriceConverter::convert($price, $currency->id);

                $quantity = intval(post_data("quantity_$inputKey"));
                if ($quantity > $detail->available_quantity) {
                    $quantity = $detail->available_quantity;
                }
                $convertedPrice *= $quantity;

                // vat
                $vatId = post_data("vat_$inputKey");
                $vat = ValueAddedTax::findOne($vatId);
                if (!is_null($vat)) {
                    $vatAmount = $vat->rate / 100 * $convertedPrice;
                    $convertedPrice += $vatAmount;
                }

                // rev tax
                $revTaxId = post_data("rev_tax_$inputKey");
                $revTax = RevenueTax::findOne($revTaxId);
                if (!is_null($revTax)) {
                    $revTaxAmount = $revTax->rate / 100 * $convertedPrice;
                    $convertedPrice += $revTaxAmount;
                }

                $addedPR[$prId][$detail->id] = [
                    'quantity' => $quantity,
                    'rev_tax_id' => $revTaxId,
                    'vat_id' => $vatId,
                    'price_estimation' => $price,
                    'converted_price' => $convertedPrice,
                    'display_converted_price' => $baseCurrency->name . ' ' . currency_format($convertedPrice)
                ];

            }
        }
        set_session($sessionKey, $addedPR);

        return "<script> loadAddedPR(); </script>";
    }

    public function actionSaveData()
    {
        $sessionKey = post_data('session_key');
        $addedPR = get_session($sessionKey, []);
        $addedPR = is_array($addedPR) ? $addedPR : [];

        if (empty($addedPR)) {
            return alert_danger('Terjadi kesalahan! Harus ada PR yang ditambahkan terlebih dahulu!');
        }

        // cek status pr dan batas pr
        foreach ($addedPR as $aprId => $aprData) {
            $pr = PurchaseRequisition::firstOrFail($aprId);
            $prCode = $pr->code;

            // cek status pr
            if ($pr->is_closed) {
                return alert_danger("Terjadi kesalahan! Purchase Requisition $prCode telah ditutup!");
            }

            // cek batas pr
            $actualPrice = 0;
            foreach ($aprData as $list) {
                $actualPrice += $list['converted_price'];
            }

            $maxTreshold = (self::$DP_THRESHOLD / 100) * $pr->converted_price_estimation;
            $maxPriceLimit = $pr->converted_price_estimation + $maxTreshold;
            if ($actualPrice > $maxPriceLimit) {
                $baseCurrency = PriceConverter::getActiveBaseCurrency();
                $fmtActualPrice = $baseCurrency->name . ' ' . currency_format($actualPrice);
                $fmtPriceEstimation = $baseCurrency->name . ' ' . currency_format($pr->converted_price_estimation);

                return alert_danger("Terjadi kesalahan! Pengajuan detail aktual anda melebihi batas price estimation dari $prCode.<br>Pengajuan anda : $fmtActualPrice, batas $prCode : $fmtPriceEstimation");
            }
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $directPurchase = new DirectPurchase();
            $directPurchase->submission_time = date('Y-m-d H:i:s');
            $directPurchase->comment = post_data('comment');

            if ($directPurchase->save()) {
                // save actual
                foreach ($addedPR as $aprId => $aprData) {
                    $purchaseRequisition = PurchaseRequisition::firstOrFail($aprId);

                    $dpPr = new DpPrDetail();
                    $dpPr->direct_purchase_id = $directPurchase->id;
                    $dpPr->purchase_requisition_id = $purchaseRequisition->id;
                    $dpPr->save();

                    foreach ($purchaseRequisition->getNonCancelReject() as $prDetail) {
                        if (!isset($addedPR[$purchaseRequisition->id][$prDetail->id])) {
                            continue;
                        }

                        $actualData = $addedPR[$purchaseRequisition->id][$prDetail->id];
                        $revTax = RevenueTax::firstOrFail($actualData['rev_tax_id']);
                        $vat = ValueAddedTax::firstOrFail($actualData['vat_id']);

                        $vat->name = '';
                        $vat->rate = 0;

                        $plan = new DpRealizationPlan();
                        $plan->dp_pr_detail_id = $dpPr->id;
                        $plan->goods_service_id = $prDetail->goods_service_id;
                        $plan->quantity = $actualData['quantity'];
                        $plan->price = $actualData['price_estimation'];
                        $plan->carline_id = $prDetail->carline_id;
                        $plan->rev_tax_name = $revTax->name;
                        $plan->rev_tax_rate = $revTax->rate;
                        $plan->vat_name = $vat->name;
                        $plan->vat_rate = $vat->rate;
                        $plan->save();

                        $actual = new DpRealizationActual();
                        $actual->dp_pr_detail_id = $dpPr->id;
                        $actual->goods_service_id = $prDetail->goods_service_id;
                        $actual->quantity = $actualData['quantity'];
                        $actual->price = 0;
                        $actual->received = 0;
                        $actual->delivered = 0;
                        $actual->carline_id = $prDetail->carline_id;
                        $actual->rev_tax_name = $revTax->name;
                        $actual->rev_tax_rate = $revTax->rate;
                        $actual->vat_name = $vat->name;
                        $actual->vat_rate = $vat->rate;
                        $actual->save();
                    }
                }

                $transaction->commit();

                $directPurchase->refresh();
                $directPurchase->save();

                // send notification
                $notifMsg = Notificatixn::render('DP_REQUEST_APPROVAL', [
                    'direct_purchase_id' => $directPurchase->code,
                    'section_name' => Auth::user()->section->name
                ]);
                Notification::sendNotification([
                    'category' => 'dp',
                    'type' => 'warning',
                    'target_id' => $directPurchase->id,
                    'title' => $notifMsg
                ]);

                remove_session($sessionKey);
                $redirectUrl = url("dp/main/detail/?id=" . $directPurchase->id);

                return alert_success("Pengajuan direct purchase berhasil disimpan!") .
                    "<script> redirectTo('$redirectUrl', 1500); </script>";
            } else {
                $transaction->rollBack();
                return alert_danger('Terjadi kesalahan! Pengajuan direct purchase gagal disimpan!');
            }
        } catch (\Exception $exception) {
            log_error($exception);
            $transaction->rollBack();
            return alert_danger('Terjadi kesalahan! Pengajuan direct purchase gagal disimpan!');
        }
    }

    public function actionDeleteAddedPrItem()
    {
        $prId = intval(post_data('purchase_requisition_id'));
        $detailId = intval(post_data('detail_id'));

        $sessionKey = post_data('session_key');
        $addedPR = get_session($sessionKey, []);
        $addedPR = is_array($addedPR) ? $addedPR : [];

        if (isset($addedPR[$prId][$detailId])) {
            unset($addedPR[$prId][$detailId]);
        }

        if (empty($addedPR[$prId])) {
            unset($addedPR[$prId]);
        }

        set_session($sessionKey, $addedPR);
        return alert_success('Item berhasil dihapus!') .
            "<script> loadAddedPR(); closeModal(1500); </script>";
    }
}

?>
