<?php
namespace app\controllers\dp;

use app\controllers\NotificationController as Notification;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\DpRealization;
use app\models\master\Department;

class PaymentController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        $activeUser = Auth::user();
        $faDept = Department::findByCode('FA');
        if ($activeUser->department_id != $faDept->id && $activeUser->roleAs('manager')) {
            redirect(url());
        }

        return [];
    }

    public function actionUploadForm()
    {
        $id = intval(post_data('id'));
        $data = DpRealization::firstOrFail($id);
        if (!ConstraintController::allowUploadPayment($data)) {
            parent::throw404Error();
        }

        $params = [
            'data' => $data
        ];
        return $this->renderPartial('form-dokumen', $params);
    }

    public function actionUploadData()
    {
        $id = intval(post_data('id'));
        $data = DpRealization::firstOrFail($id);
        if (!ConstraintController::allowUploadPayment($data)) {
            parent::throw404Error();
        }

        $document = md5('dp-document-' . $data->id);
        $upload = upload('document', DP_DOCUMENT_PATH, $document, ['pdf', 'jpg', 'png']);
        if ($upload['error']) {
            return alert_danger('Error! Document gagal disimpan!<br>Keterangan : ' . $upload['msg']);
        } else {
            $data->document = $upload['new_name'];
            $data->is_paid = 1;

            if ($data->save()) {
                $dp = $data->directPurchase;

                // SEND NOTIFICATION TO SECTION THAT VP IS CONFIRMED
                $notifMsg = Notificatixn::render('DP_READY_FOR_ACTUAL', [
                    'direct_purchase_id' => $dp->code,
                    'vp_id' => $data->code
                ]);
                $notif_data = [
                    'category' => 'dp',
                    'type' => 'success',
                    'target_id' => $dp->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notif_data);

                return alert_success('Document berhasil disimpan!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Document gagal disimpan!');
            }
        }
    }
}

?>
