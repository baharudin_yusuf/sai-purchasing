<?php
namespace app\controllers\dp;

use app\controllers\dp\ConstraintController as DPConstraint;
use app\controllers\NotificationController as Notification;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\helpers\PriceConverter;
use app\models\DirectPurchase;
use app\models\DpRealization;
use app\models\master\Department;

class VoucherPayingController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionCreateNewVp()
    {
        $dpId = intval(post_data('dp_id'));
        $directPurchase = DirectPurchase::firstOrFail($dpId);
        $constraint = DPConstraint::allowCreateVP($directPurchase);
        if (!$constraint['allow']) {
            return alert_danger($constraint['msg']);
        }

        // additional status
        $nRealization = $directPurchase->getRealization()
            ->where(['is_paid' => 1])
            ->count();

        $params = [
            'data' => $directPurchase,
            'amount' => self::getCurrentAmountVP($directPurchase),
            'isAdditional' => $nRealization == 0 ? 0 : 1,
            'currency' => PriceConverter::getActiveBaseCurrency()
        ];
        return $this->renderPartial('create-new-vp', $params);
    }

    private function getCurrentAmountVP(DirectPurchase $directPurchase)
    {
        $realization = $directPurchase->getRealization()
            ->where(['is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        if (count($realization) == 0) {
            $amountVP = $directPurchase->amount_plan;
        } else {
            // count amount in realization
            $amountRealization = 0;
            foreach ($realization as $list) {
                $amountRealization += $list->amount;
            }
            $amountVP = $directPurchase->amount_actual - $amountRealization;
        }

        return $amountVP;
    }

    public function actionCreateNewVr()
    {
        $dpId = intval(post_data('dp_id'));
        $directPurchase = DirectPurchase::firstOrFail($dpId);
        $constraint = DPConstraint::allowCreateVR($directPurchase);
        if (!$constraint['allow']) {
            return alert_danger($constraint['msg']);
        }

        // get payment for
        $dept = Department::findByCode('FA');
        $params = [
            'data' => $directPurchase,
            'paymentFor' => $dept->name,
            'amount' => $directPurchase->amount_plan - $directPurchase->amount_actual,
            'currency' => PriceConverter::getActiveBaseCurrency(),
        ];
        return $this->renderPartial('create-new-vr', $params);
    }

    public function actionDetail()
    {
        $id = intval(post_data('id'));
        $params = [
            'data' => DpRealization::firstOrFail($id),
            'currency' => PriceConverter::getActiveBaseCurrency()
        ];
        return $this->renderPartial('detail', $params);
    }

    public function actionSaveData()
    {
        $dpId = intval(post_data('direct_purchase_id'));
        $directPurchase = DirectPurchase::firstOrFail($dpId);
        $isVR = intval(post_data('is_voucher_receiving'));
        if (!$isVR) {
            $constraint = DPConstraint::allowCreateVP($directPurchase);
            if (!$constraint['allow']) {
                return alert_danger($constraint['msg']);
            }

            // additional status
            $nRealization = $directPurchase->getRealization()
                ->where(['is_paid' => 1])
                ->count();
            $isAdditional = $nRealization == 0 ? 0 : 1;
        } else {
            $constraint = DPConstraint::allowCreateVR($directPurchase);
            if (!$constraint['allow']) {
                return alert_danger($constraint['msg']);
            }

            $isAdditional = 0;
        }

        $realization = new DpRealization();
        $realization->paid_to = post_data('paid_to');
        $realization->time = date('Y-m-d H:i:s');
        $realization->is_additional = $isAdditional;
        $realization->direct_purchase_id = $directPurchase->id;
        $realization->is_voucher_receiving = $isVR;

        // calculate amount
        if ($realization->is_voucher_receiving) {
            $realization->amount = $directPurchase->amount_plan - $directPurchase->amount_actual;
        } else {
            $realization->amount = self::getCurrentAmountVP($directPurchase);
        }

        if ($realization->save()) {
            if ($realization->is_voucher_receiving) {
                $realization->is_approved_by_manager = 1;
                $realization->approved_by_manager_time = null;
                $realization->save();

                // send notification to fa
                $notifMsg = Notificatixn::render('DP_VR_READY_FOR_FA', [
                    'direct_purchase_id' => $directPurchase->code,
                    'vr_id' => $realization->code,
                    'section_name' => Auth::user()->section->name
                ]);
                $notifData = [
                    'category' => 'dp',
                    'type' => 'warning-to-fa',
                    'target_id' => $directPurchase->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);

                return alert_success('Voucher Receiving berhasil disimpan!') .
                    "<script> reload(1000); </script>";
            } else {
                // send notification to manager
                $notifMsg = Notificatixn::render('DP_VP_APPROVAL', [
                    'direct_purchase_id' => $directPurchase->code,
                    'vp_id' => $realization->code,
                ]);
                $notifData = [
                    'category' => 'dp',
                    'type' => 'warning',
                    'target_id' => $directPurchase->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);

                return alert_success('Voucher Paying berhasil disimpan!') .
                    "<script> reload(1000); </script>";
            }
        } else {
            return alert_danger('Error! Voucher Paying gagal disimpan!');
        }
    }

    public function actionCancelData()
    {
        $id = intval(post_data('id'));
        $dpRealization = DpRealization::firstOrFail($id);

        if (DPConstraint::allowCancelVoucherPaying()) {
            $dpRealization->is_canceled = 1;

            if ($dpRealization->save()) {
                return alert_success('Voucher paying berhasil dibatalkan!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Voucher paying gagal dibatalkan!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!');
        }
    }

    public function actionApproveData()
    {
        $vpId = intval(post_data('id'));
        $dpRealization = DpRealization::firstOrFail($vpId);
        $directPurchase = $dpRealization->directPurchase;

        if (DPConstraint::allowApproveVoucherPaying()) {
            $dpRealization->is_approved_by_manager = 1;
            $dpRealization->approved_by_manager_time = date('Y-m-d H:i:s');

            if ($dpRealization->save()) {
                // send notification to section that vp is confirmed
                $notifMsg = Notificatixn::render('DP_VP_APPROVED', [
                    'direct_purchase_id' => $directPurchase->code,
                    'vp_id' => $dpRealization->code,
                    'department_name' => Auth::user()->department->name
                ]);
                $notifData = [
                    'category' => 'dp',
                    'type' => 'success',
                    'target_id' => $directPurchase->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);

                // send notification to fa that vp is ready to paid
                $notifMsg = Notificatixn::render('DP_VP_READY_FOR_FA', [
                    'direct_purchase_id' => $directPurchase->code,
                    'vp_id' => $dpRealization->code,
                    'department_name' => Auth::user()->department->name
                ]);
                $notifData = [
                    'category' => 'dp',
                    'type' => 'warning-to-fa',
                    'target_id' => $directPurchase->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);

                return alert_success('Voucher paying berhasil diterima!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Voucher paying gagal diterima!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!');
        }
    }

    public function actionRejectData()
    {
        $vpId = intval(post_data('id'));
        $dpRealization = DpRealization::firstOrFail($vpId);
        $directPurchase = $dpRealization->directPurchase;

        if (DPConstraint::allowApproveVoucherPaying()) {
            $dpRealization->is_rejected = 1;

            if ($dpRealization->save()) {
                // kirim notif ke section
                $notifMsg = Notificatixn::render('DP_VP_REJECTED', [
                    'direct_purchase_id' => $directPurchase->code,
                    'vp_id' => $dpRealization->code,
                    'department_name' => Auth::user()->department->name
                ]);
                $notifData = [
                    'category' => 'dp',
                    'type' => 'danger',
                    'target_id' => $directPurchase->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);

                return alert_success('Voucher paying berhasil ditolak!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Voucher paying gagal ditolak!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!');
        }
    }

    public function actionPrintData()
    {
        $this->layout = 'print';

        $id = intval(get_data('id'));
        $data = DpRealization::firstOrFail($id);

        $params = [
            'data' => $data,
            'directPurchase' => $data->directPurchase,
            'activeUser' => Auth::user()
        ];
        return $this->render('print', $params);
    }
}

?>
