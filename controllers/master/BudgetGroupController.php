<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\BudgetCategory;
use app\models\master\BudgetGroup;

class BudgetGroupController extends AdminController
{
    public static $moduleName = 'data-master-budget-group';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'budget-group';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = BudgetGroup::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? BudgetGroup::firstOrFail($id) : new BudgetGroup();
        $params = [
            'data' => $data,
            'budgetCategoryOption' => BudgetCategory::getOption(),
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? BudgetGroup::firstOrFail($id) : new BudgetGroup();
        $data->budget_category_id = post_data('budget_category_id');
        $data->name = post_data('name');

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = BudgetGroup::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['BUDGET GROUP', 'BUDGET CATEGORY'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            $budgetCategory = BudgetCategory::findOne(['name' => $list['BUDGET CATEGORY']]);
            if (is_null($budgetCategory)) {
                $failed[] = $list;
                continue;
            }

            $cond = [
                'name' => $list['BUDGET GROUP'],
            ];

            $data = BudgetGroup::findOne($cond);
            if (is_null($data)) {
                $data = new BudgetGroup();
            }

            $data->name = $list['BUDGET GROUP'];
            $data->budget_category_id = $budgetCategory->id;

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
