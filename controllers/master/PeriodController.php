<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\BudgetPeriod;
use app\models\master\Period;
use app\models\Month;

class PeriodController extends AdminController
{
    public static $moduleName = 'data-master-period';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'period';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = Period::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Period::firstOrFail($id) : new Period();
        $params = [
            'data' => $data,
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Period::firstOrFail($id) : new Period();
        $data->name = post_data('name');
        $data->date_start = post_data('date_start');
        $data->date_end = post_data('date_end');
        $data->is_active = intval(post_data('is_active'));
        $data->is_future = intval(post_data('is_future'));

        // validasi periode
        if (strtotime($data->date_end) < strtotime($data->date_start)) {
            return alert_danger('Terjadi kesalahan! Waktu awal dan akhir tidak valid!');
        }

        if ($data->save()) {
            if (!$id) {
                $month = Month::getOption();
                $yearStart = intval(date('Y', strtotime($data->date_start)));
                $monthStart = intval(date('n', strtotime($data->date_start)));
                $dateStop = date('Y-n', strtotime($data->date_end));

                $loop = 0;
                for ($i = $monthStart; $i <= 100; $i++) {
                    $monthLoop = $i + $loop;
                    $monthIndex = $monthLoop % 12;
                    $monthIndex = $monthIndex == 0 ? 12 : $monthIndex;

                    $budgetPeriod = new BudgetPeriod();
                    $budgetPeriod->name = $month[$monthIndex] . " [" . $data->name . "]";
                    $budgetPeriod->year = $yearStart;
                    $budgetPeriod->month = $monthIndex;
                    $budgetPeriod->period_id = $data->id;
                    $budgetPeriod->save();

                    if ("$yearStart-$monthIndex" == $dateStop) {
                        break;
                    }
                    if ($monthIndex == 12) {
                        $yearStart++;
                    }
                }
            }

            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = Period::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }
}

?>
