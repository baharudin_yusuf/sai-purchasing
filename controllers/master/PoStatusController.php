<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\models\master\PoStatus;

class PoStatusController extends AdminController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = PoStatus::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $object = new PoStatus();
        $id = $object->id;

        $data = $_POST;
        if (isset($data[$id])) {
            $id = $data[$id];
            $data = $object->getData($id);
        } else $data = $object->newEmpty;

        return $this->renderPartial('form-input', $data);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $_POST;
        $object = new PoStatus();
        $id = $object->id;

        if (isset($data[$id]) && intval($data[$id]) > 0) $model = PoStatus::findOne($data[$id]);
        else $model = new PoStatus;

        foreach ($model->schema as $key) {
            if (isset($data[$key])) $model->$key = $data[$key];
        }

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = PoStatus::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }
}

?>
