<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Supplier;
use app\models\master\SupplierCategory;

class SupplierController extends AdminController
{
    public static $moduleName = 'data-master-supplier';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'supplier';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = Supplier::find()->all();
        return $this->renderPartial('data', $data);
    }

    private function getUploadField()
    {
        return [
            'siup_path' => 'SIUP File',
            'tdp_path' => 'TDP File',
            'npwp_path' => 'NPWP File',
            'sppkp_path' => 'SPPKP File',
            'skt_badan_path' => 'SKT Badan File',
            'skt_pribadi_path' => 'SKT Pribadi File',
            'non_pkp_path' => 'Non-PKP File',
        ];
    }

    public function actionEntriData()
    {
        $id = intval(get_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Supplier::findOne($id) : new Supplier();
        $params = [
            'data' => $data,
            'categoryOption' => SupplierCategory::getOption(),
            'uploadField' => self::getUploadField()
        ];
        return $this->render('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Supplier::findOne($id) : new Supplier();
        $data->name = post_data('name');
        $data->address = post_data('address');
        $data->phone1 = post_data('phone1');
        $data->phone2 = post_data('phone2');
        $data->phone3 = post_data('phone3');
        $data->fax = post_data('fax');
        $data->email = post_data('email');
        $data->website = post_data('website');
        $data->siup = post_data('siup');
        $data->tdp = post_data('tdp');
        $data->npwp = post_data('npwp');
        $data->sppkp = post_data('sppkp');
        $data->is_pkp = intval(post_data('is_pkp'));
        $data->supplier_category_id = post_data('supplier_category_id');
        $data->authorized_person = post_data('authorized_person');
        $data->authorized_person_cellphone = post_data('authorized_person_cellphone');
        $data->deadline_day = intval(post_data('deadline_day'));

        if ($data->save()) {
            $id = $data->id;

            foreach (self::getUploadField() as $field => $label) {
                $filename = md5($id . $field);
                $path = strtoupper($field);
                $path = defined($path) ? constant($path) : 'uploads';
                $upload = upload($field, $path, $filename, ['pdf']);
                var_dump($upload);
                if (!$upload['error'] && isset($upload['new_name'])) {
                    $data->setAttribute($field, $upload['new_name']);
                }
            }

            $data->save();

            $redirectURL = url("master/supplier/entri-data/?id=$id");
            $msg = "<script> alert('Data berhasil disimpan!'); redirectTo('$redirectURL', 0); </script>";
        } else {
            $msg = "<script> alert('Error! Data gagal disimpan!'); </script>";
        }

        return $msg;
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = Supplier::firstOrFail($id);

        try {
            $files = ['siup_path', 'tdp_path', 'npwp_path', 'sppkp_path', 'skt_badan_path', 'skt_pribadi_path', 'non_pkp_path'];

            $fileList = [];
            foreach ($files as $file) {
                if (defined(strtoupper($file))) {
                    $filepath = constant(strtoupper($file)) . '/' . $model->getAttribute($file);
                    if (is_file($filepath)) {
                        $fileList[] = $filepath;
                    }
                }
            }

            if ($model->delete()) {
                foreach ($fileList as $filepath) {
                    @unlink($filepath);
                }

                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['SUPPLIER CATEGORY', 'SUPPLIER NAME', 'ADDRESS', 'PHONE 1', 'PHONE 2', 'PHONE 3', 'FAX', 'EMAIL', 'WEBSITE', 'AUTHORIZED PERSON', 'AP CELLPHONE', 'SIUP', 'TDP', 'NPWP', 'SPPKP', 'PKP', 'PAYMENT DUE DATE'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            /* GET SUPPLIER CATEGORY DATA */
            $supplierCategory = SupplierCategory::findOne(['name' => $list['SUPPLIER CATEGORY']]);
            if (is_null($supplierCategory)) {
                $failed[] = $list;
                continue;
            }

            /* GET SUPPLIER DATA */
            $data = Supplier::findOne(['name' => $list['SUPPLIER NAME']]);
            if (is_null($data)) {
                $data = new Supplier();
            }

            $data->supplier_category_id = $supplierCategory->id;
            $data->name = $list['SUPPLIER NAME'];
            $data->address = $list['ADDRESS'];
            $data->phone1 = $list['PHONE 1'];
            $data->phone2 = $list['PHONE 2'];
            $data->phone3 = $list['PHONE 3'];
            $data->fax = $list['FAX'];
            $data->email = $list['EMAIL'];
            $data->website = $list['WEBSITE'];
            $data->authorized_person = $list['AUTHORIZED PERSON'];
            $data->authorized_person_cellphone = $list['AP CELLPHONE'];
            $data->siup = $list['SIUP'];
            $data->tdp = $list['TDP'];
            $data->npwp = $list['NPWP'];
            $data->sppkp = $list['SPPKP'];
            $data->is_pkp = $list['PKP'];
            $data->deadline_day = $list['PAYMENT DUE DATE'];

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
