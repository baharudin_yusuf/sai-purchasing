<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Currency;
use app\models\master\GoodsService;
use app\models\master\Pricelist;
use app\models\master\RevenueTax;
use app\models\master\Supplier;
use app\models\master\ValueAddedTax;

class PricelistController extends AdminController
{
    public static $moduleName = 'data-master-pricelist';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'pricelist';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = Pricelist::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionLoadCurrency()
    {
        $gsId = intval(post_data('goods_service_id'));
        $goodsService = GoodsService::findOne($gsId);

        if (is_null($goodsService)) {
            return '';
        } else {
            $currencyId = $goodsService->currency_id;
            $currency = Currency::findOne($currencyId);
            return $currency->name;
        }
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Pricelist::firstOrFail($id) : new Pricelist();

        $params = [
            'data' => $data,
            'goodsServiceOption' => GoodsService::getOption(),
            'currencyOption' => Currency::getOption(),
            'supplierOption' => Supplier::getOption(),
            'revenueTaxOption' => RevenueTax::getOption(),
            'valueAddedTaxOption' => ValueAddedTax::getOption(),
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Pricelist::firstOrFail($id) : new Pricelist();
        $data->add_time = $id ? $data->add_time : date('Y-m-d H:i:s');
        $data->goods_service_id = post_data('goods_service_id');
        $data->quantity = post_data('quantity');
        $data->market_price = post_data('market_price');
        $data->supplier_id = post_data('supplier_id');
        $data->is_package = intval(post_data('is_package'));
        $data->update_time = date('Y-m-d H:i:s');
        $data->revenue_tax_id = post_data('revenue_tax_id');
        $data->value_added_tax_id = post_data('value_added_tax_id');

        // cek duplikasi
        $dups = Pricelist::findOne([
            'goods_service_id' => $data->goods_service_id,
            'supplier_id' => $data->supplier_id
        ]);
        if (!is_null($dups) && $dups->id != $data->id) {
            return alert_danger('Terjadi kesalahan! Item goods/service dengan supplier terpilih sudah tersedia pada data anda');
        }

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = Pricelist::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['GOODS SERVICE', 'SUPPLIER', 'REVENUE TAX', 'VALUE ADDED TAX', 'QUANTITY', 'MARKET PRICE', 'PACKAGE'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            $goodsService = GoodsService::findOne(['name' => $list['GOODS SERVICE']]);
            if (is_null($goodsService)) {
                continue;
            }

            $supplier = Supplier::findOne(['name' => $list['SUPPLIER']]);
            if (is_null($supplier)) {
                continue;
            }

            $revenueTax = RevenueTax::findOne(['name' => $list['REVENUE TAX']]);
            if (is_null($revenueTax)) {
                continue;
            }

            $valueAddedTax = ValueAddedTax::findOne(['name' => $list['VALUE ADDED TAX']]);
            if (is_null($valueAddedTax)) {
                continue;
            }

            $cond = [
                'goods_service_id' => $goodsService->id,
                'supplier_id' => $supplier->id,
            ];

            $data = Pricelist::findOne($cond);
            if (is_null($data)) {
                $data = new Pricelist();
            }

            $data->goods_service_id = $goodsService->id;
            $data->quantity = $list['QUANTITY'];
            $data->market_price = $list['MARKET PRICE'];
            $data->supplier_id = $supplier->id;
            $data->is_package = $list['PACKAGE'];
            $data->add_time = date('Y-m-d H:i:s');
            $data->revenue_tax_id = $revenueTax->id;
            $data->value_added_tax_id = $valueAddedTax->id;

            // cek duplikasi
            $dups = Pricelist::findOne([
                'goods_service_id' => $data->goods_service_id,
                'supplier_id' => $data->supplier_id
            ]);
            if (!is_null($dups) && $dups->id != $data->id) {
                $failed[] = $list;
                continue;
            }

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
