<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Department;

class DepartmentController extends AdminController
{
    public static $moduleName = 'data-master-department';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'department';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = Department::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Department::firstOrFail($id) : new Department();
        $params = [
            'data' => $data,
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Department::firstOrFail($id) : new Department();
        $data->code = post_data('code');
        $data->name = post_data('name');
        $data->is_fa = intval(post_data('is_fa'));
        $data->is_ga_gs = intval(post_data('is_ga_gs'));
        $data->is_under_dfm = intval(post_data('is_under_dfm'));
        $data->is_under_fm = intval(post_data('is_under_fm'));
        $data->is_under_presdir = intval(post_data('is_under_presdir'));

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = Department::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['CODE', 'DEPARTMENT', 'FA', 'GA GS', 'UNDER DFM', 'UNDER FM', 'UNDER PRESDIR'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            $cond = [
                'code' => $list['CODE'],
            ];

            $data = Department::findOne($cond);
            if (is_null($data)) {
                $data = new Department();
            }

            $data->code = $list['CODE'];
            $data->name = $list['DEPARTMENT'];
            $data->is_fa = intval($list['FA']);
            $data->is_ga_gs = intval($list['GA GS']);
            $data->is_under_dfm = intval($list['UNDER DFM']);
            $data->is_under_fm = intval($list['UNDER FM']);
            $data->is_under_presdir = intval($list['UNDER PRESDIR']);

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
