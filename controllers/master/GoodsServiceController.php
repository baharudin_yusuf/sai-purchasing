<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Budget;
use app\models\master\Currency;
use app\models\master\GoodsService;
use app\models\master\Unit;
use app\models\master\UnitCategory;

class GoodsServiceController extends AdminController
{
    public static $moduleName = 'data-master-goods-service';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'goods-service';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = GoodsService::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? GoodsService::firstOrFail($id) : new GoodsService();
        $data->is_po = $id ? $data->is_po : 1;

        $unitCategoryOption = UnitCategory::getOption();
        $unitOption = [];

        $unit = $data->unit;
        if (!is_null($unit)) {
            $unitOption = Unit::getOption($unit->id);
            $data->unit_category_id = $unit->unit_category_id;
        }

        if (empty($unitOption)) {
            foreach ($unitCategoryOption as $key => $value) {
                $unitOption = Unit::getOption($key);
                break;
            }
        }

        $params = [
            'data' => $data,
            'budgetOption' => Budget::getOption(),
            'currencyOption' => Currency::getOption(),
            'typeOption' => GoodsService::generateTypeOption(),
            'unitCategoryOption' => $unitCategoryOption,
            'unitOption' => $unitOption,
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionGetUnit()
    {
        $result = '';
        $unitCategoryId = intval(post_data('unit_category_id'));
        foreach (Unit::getOption($unitCategoryId) as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        return $result;
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? GoodsService::firstOrFail($id) : new GoodsService();
        $data->budget_id = post_data('budget_id');
        $data->name = post_data('name');
        $data->unit_id = post_data('unit_id');
        $data->specification = post_data('specification');
        $data->currency_id = post_data('currency_id');
        $data->type = post_data('type');

        if (post_data('purchase_method') == 'po') {
            $data->is_po = 1;
            $data->is_dp = 0;
        } else {
            $data->is_po = 0;
            $data->is_dp = 1;
        }

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = GoodsService::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['BUDGET', 'UNIT', 'CURRENCY', 'GOODS SERVICE', 'TYPE', 'SPESIFICATIONS', 'PO', 'DP'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            $budget = Budget::findOne(['name' => $list['BUDGET']]);
            if (is_null($budget)) {
                continue;
            }

            $unit = Unit::findOne(['name' => $list['UNIT']]);
            if (is_null($unit)) {
                continue;
            }

            $currency = Currency::findOne(['name' => $list['CURRENCY']]);
            if (is_null($currency)) {
                continue;
            }

            $data = GoodsService::find()
                ->where(['budget_id' => $budget->id])
                ->andWhere(['like', 'name', $list['GOODS SERVICE']])
                ->one();
            if (is_null($data)) {
                $data = new GoodsService();
            }

            $data->budget_id = $budget->id;
            $data->name = $list['GOODS SERVICE'];
            $data->type = $list['TYPE'];
            $data->unit_id = $unit->id;
            $data->specification = $list['SPESIFICATIONS'];
            $data->currency_id = $currency->id;
            $data->is_po = $list['PO'];
            $data->is_dp = $list['DP'];

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
