<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Budget;
use app\models\master\BudgetLimit;
use app\models\master\Currency;
use app\models\master\Period;

class BudgetLimitController extends AdminController
{
    public static $moduleName = 'data-master-budget-limit';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'budget-limit';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = BudgetLimit::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? BudgetLimit::firstOrFail($id) : new BudgetLimit();
        $params = [
            'data' => $data,
            'budgetOption' => Budget::getOption(),
            'periodOption' => Period::getOption(),
            'currencyOption' => Currency::getOption(),
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? BudgetLimit::firstOrFail($id) : new BudgetLimit();
        $data->budget_id = post_data('budget_id');
        $data->period_id = post_data('period_id');
        $data->amount = post_data('amount');
        $data->comment = post_data('comment');
        $data->currency_id = post_data('currency_id');

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = BudgetLimit::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['BUDGET', 'PERIOD', 'CURRENCY', 'AMOUNT', 'COMMENT'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            $budget = Budget::findOne(['name' => $list['BUDGET']]);
            if (is_null($budget)) {
                $failed[] = $list;
                continue;
            }

            $period = Period::findOne(['name' => $list['PERIOD']]);
            if (is_null($period)) {
                $failed[] = $list;
                continue;
            }

            $currency = Currency::findOne(['name' => $list['CURRENCY']]);
            if (is_null($currency)) {
                $failed[] = $list;
                continue;
            }

            $cond = [
                'budget_id' => $budget->id,
                'period_id' => $period->id
            ];

            $data = BudgetLimit::findOne($cond);
            if (is_null($data)) {
                $data = new BudgetLimit();
            }

            $data->budget_id = $budget->id;
            $data->period_id = $period->id;
            $data->currency_id = $currency->id;
            $data->amount = $list['AMOUNT'];
            $data->comment = $list['COMMENT'];

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
