<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Currency;
use app\models\master\CurrencyDetail;
use app\models\master\Period;

class CurrencyDetailController extends AdminController
{
    public static $moduleName = 'data-master-currency-detail';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'currency-detail';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $periodShown = [];
        $periodUsed = CurrencyDetail::find()->groupBy('period_id')->all();
        foreach ($periodUsed as $item) {
            $periodShown[] = $item->period_id;
        }

        $params = [
            'periodAll' => Period::find()->where(['in', 'id', $periodShown])->all(),
            'currencyOption' => Currency::getOption()
        ];
        return $this->renderPartial('data', $params);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $periodId = intval(post_data('period_id'));
        $period = $periodId ? Period::firstOrFail($periodId) : new Period();

        $periodExcept = [];
        $periodUsed = CurrencyDetail::find()->groupBy('period_id')->all();
        foreach ($periodUsed as $item) {
            $periodExcept[] = $item->period_id;
        }

        $periodOption = [];
        foreach (Period::getOption() as $key => $value) {
            if (!in_array($key, $periodExcept)) {
                $periodOption[$key] = $value;
            }
        }

        if (!$periodId && empty($periodOption)) {
            return alert_danger('Tidak ada periode tersedia untuk diatur currencynya');
        }

        $params = [
            'period' => $period,
            'periodOption' => $periodOption,
            'currencyOption' => Currency::getOption(),
            'currencyBase' => $period->getCurrencyDetailBase()
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $periodId = intval(post_data('period_id'));
        $baseCurrency = intval(post_data('base_currency_id'));
        CurrencyDetail::deleteAll(['period_id' => $periodId]);

        foreach (Currency::find()->all() as $curr) {
            $isBaseCurrency = ($curr->id == $baseCurrency) ? true : false;

            $data = new CurrencyDetail();
            $data->currency_id = $curr->id;
            $data->period_id = $periodId;
            $data->current_rate = $isBaseCurrency ? 1 : post_data('rate_' . $curr->id);
            $data->is_base_currency = $isBaseCurrency ? 1 : 0;

            if (!$data->save()) {
                $transaction->rollBack();
                return alert_danger('Terjadi kesalahan! Data gagal disimpan!');
            }
        }

        $transaction->commit();

        return alert_success('Data berhasil disimpan!') .
            "<script> closeModal(1500); reloadData(); </script>";
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $periodId = post_data('period_id');
        if (CurrencyDetail::deleteAll(['period_id' => $periodId])) {
            return alert_success('Data berhasil dihapus!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal dihapus!');
        }
    }
}

?>
