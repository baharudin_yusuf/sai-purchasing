<?php
namespace app\controllers\master;

use app\controllers\AdminController;
use app\helpers\Auth;
use app\models\master\Budget;
use app\models\master\BudgetCategory;
use app\models\master\BudgetGroup;
use app\models\master\BudgetSubGroup;

class BudgetController extends AdminController
{
    public static $moduleName = 'data-master-budget';
    public static $subModule = ['entri-data', 'ubah-data', 'hapus-data'];

    public function behaviors()
    {
        parent::behaviors();
        parent::$type = 'budget';

        $activeUser = Auth::user();
        $allowCount = 0;
        foreach (self::$subModule as $item) {
            if ($activeUser->allowTo(self::$moduleName, $item)) {
                $allowCount++;
            }
        }
        if ($allowCount == 0) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $data['data'] = Budget::find()->all();
        return $this->renderPartial('data', $data);
    }

    public function actionBudgetDetail()
    {
        $id = intval(post_data('id'));
        $params['data'] = Budget::firstOrFail($id);
        return $this->renderPartial('budget-detail', $params);
    }

    public function actionEntriData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Budget::firstOrFail($id) : new Budget();

        $budgetCategoryOption = BudgetCategory::getOption();
        $budgetGroupOption = [];
        $budgetSubGroupOption = [];

        $subGroup = $data->subGroup;
        if (!is_null($subGroup)) {
            $group = $subGroup->group;
            if (!is_null($group)) {
                $category = $group->category;
                if (!is_null($category)) {
                    $budgetGroupOption = BudgetGroup::getOption($category->id);
                    $data->budget_category_id = $category->id;
                }

                $budgetSubGroupOption = BudgetSubGroup::getOption($group->id);
                $data->budget_group_id = $group->id;
            }
        }

        if (empty($budgetGroupOption)) {
            foreach ($budgetCategoryOption as $key => $value) {
                $budgetGroupOption = BudgetGroup::getOption($key);
                break;
            }
        }

        if (empty($budgetSubGroupOption)) {
            foreach ($budgetGroupOption as $key => $value) {
                $budgetSubGroupOption = BudgetSubGroup::getOption($key);
                break;
            }
        }

        $params = [
            'data' => $data,
            'monthOption' => $this::getMonthOption(),
            'budgetCategoryOption' => $budgetCategoryOption,
            'budgetGroupOption' => $budgetGroupOption,
            'budgetSubGroupOption' => $budgetSubGroupOption,
        ];
        return $this->renderPartial('form-input', $params);
    }

    public function actionSimpanData()
    {
        $id = intval(post_data('id'));
        if (!Auth::user()->allowTo(self::$moduleName, $id ? 'ubah-data' : 'entri-data')) {
            parent::throw404Error();
        }

        $data = $id ? Budget::firstOrFail($id) : new Budget();
        $data->budget_sub_group_id = post_data('budget_sub_group_id');
        $data->number = post_data('number');
        $data->name = post_data('name');
        $data->amount = $id ? $data->amount : 0;
        $data->is_packaging_component = intval(post_data('is_packaging_component'));
        $data->comment = post_data('comment');

        if ($data->save()) {
            return alert_success('Data berhasil disimpan!') .
                "<script> closeModal(1500); reloadData(); </script>";
        } else {
            return alert_danger('Data gagal disimpan!');
        }
    }

    public function actionHapusData()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'hapus-data')) {
            parent::throw404Error();
        }

        $id = post_data('id');
        $model = Budget::firstOrFail($id);

        try {
            if ($model->delete()) {
                return alert_success('Data berhasil dihapus!') .
                    "<script> closeModal(1500); reloadData(); </script>";
            } else {
                return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
            }
        } catch (\Exception $exception) {
            return alert_danger('Terjadi kesalahan! Data gagal dihapus!');
        }
    }

    public function actionGetBudgetGroup()
    {
        $result = '';
        $categoryId = intval(post_data('budget_category_id'));
        foreach (BudgetGroup::getOption($categoryId) as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        return $result;
    }

    public function actionGetBudgetSubGroup()
    {
        $result = '';
        $groupId = intval(post_data('budget_group_id'));
        foreach (BudgetSubGroup::getOption($groupId) as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        return $result;
    }

    public function actionSaveImport()
    {
        if (!Auth::user()->allowTo(self::$moduleName, 'entri-data')) {
            parent::throw404Error();
        }

        $required = ['BUDGET SUB GROUP', 'NUMBER', 'NAME', 'PKG COMPONENT', 'COMMENT'];
        $data = parent::validateImport($required);
        if (!is_array($data)) {
            return strval($data);
        }

        $failed = [];
        $imported = 0;
        foreach ($data as $list) {
            $budgetSubGroup = BudgetSubGroup::findOne(['name' => $list['BUDGET SUB GROUP']]);
            if (is_null($budgetSubGroup)) {
                $failed[] = $list;
                continue;
            }

            $cond = [
                'number' => $list['NUMBER'],
            ];

            $data = Budget::findOne($cond);
            if (is_null($data)) {
                $data = new Budget();
            }

            $data->number = $list['NUMBER'];
            $data->name = $list['NAME'];
            $data->is_packaging_component = $list['PKG COMPONENT'];
            $data->comment = $list['COMMENT'];
            $data->budget_sub_group_id = $budgetSubGroup->id;

            if ($data->save()) {
                $imported++;
            } else {
                $failed[] = $list;
            }
        }

        return parent::finalizeImport($imported, $failed);
    }
}

?>
