<?php
namespace app\controllers;

use app\helpers\Auth;
use app\models\User;
use Yii;

class ProfileController extends UserController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = ['Manage Profile' => url('profile')];

        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'profile-panel');

        return [];
    }

    public function actionIndex()
    {
        $data['user'] = Auth::user();
        return $this->render('index', $data);
    }

    public function actionUpdate()
    {
        $user = Auth::user();

        // validasi email
        $email = post_data('email');
        $check = User::findOne(['email' => $email]);
        if (!is_null($check) && $check->id != $user->id) {
            return alert_danger('Terjadi kesalahan! Email telah digunakan!');
        }

        $user->name = post_data('name');
        $user->email = post_data('email');
        $user->save();

        if (isset($_FILES['profile_picture'])) {
            $filename = encrypt($user->id);
            $upload = upload('profile_picture', PROFILE_PICTURE_PATH, $filename, ['png', 'jpg']);
            if (!$upload['error']) {
                $user->profile_picture = $upload['new_name'];
            } else {
                return alert_danger($upload['msg']);
            }
        }

        if (isset($_FILES['signature'])) {
            $filename = encrypt($user->id);
            $upload = upload('signature', SIGNATURE_PATH, $filename, ['png', 'jpg']);
            if (!$upload['error']) {
                $user->signature = $upload['new_name'];
            } else {
                return alert_danger($upload['msg']);
            }
        }

        $user->save();

        return alert_success('Data profil berhasil diubah') .
            "<script> reload(1500); </script>";
    }

    public function actionChangePassword()
    {
        return $this->renderPartial('change-password');
    }

    public function actionUpdatePassword()
    {
        $password = post_data('password1');
        $confirm = post_data('password2');
        if ($password != $confirm) {
            return alert_danger('Terjadi kesalahan! Konfirmasi password tidak sesuai!');
        }

        $user = Auth::user();
        $user->password = encrypt($password);
        $user->save();

        return alert_success('Password berhasil diubah!') .
            "<script> closeModal(1500); </script>";
    }
}

?>
