<?php
namespace app\controllers;

use app\models\DirectPurchase;
use app\models\master\Currency;
use app\models\master\CurrencyDetail;
use app\models\master\Period;
use app\models\Notification;
use app\models\PurchaseOrder;
use app\models\PurchaseRequisition;
use Yii;

class SiteController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Dashboard' => url(),
        ];
        $menu['Requirement'] = url('site/requirement');
        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'home-panel');

        return [];
    }

    public function actionRefineNumber()
    {
        foreach (PurchaseRequisition::find()->all() as $item) {
            $item->save();
        }

        foreach (PurchaseOrder::find()->all() as $item) {
            $item->save();
        }

        foreach (DirectPurchase::find()->all() as $item) {
            $item->save();
        }
    }

    public function actionIndex()
    {
        $userdata = $this::getUserData();
        $user_id = $userdata['id'];

        $Notification = Notification::find()
            ->asArray()
            ->where(['user_id' => $user_id])
            ->orderBy(['date' => SORT_DESC])
            ->all();

        $data['notification'] = $Notification;

        return $this->render('index', $data);
    }

    public function actionRequirement()
    {
        $complete = true;

        /* cek base currency */
        $currency = Currency::find()->where(['is_base_currency' => 1])->one();
        $data['currency'] = [
            'status' => false,
            'title' => 'Pengaturan Base Currency',
            'description' => 'Base Currency harus didefinisikan terlebih dahulu',
        ];
        if (!is_null($currency)) {
            $data['currency']['status'] = true;
        } else {
            $complete = false;
        }

        /* cek base periode */
        $activePeriod = Period::find()->where(['is_active' => 1])->one();
        $data['period_active'] = [
            'status' => false,
            'title' => 'Pengaturan Periode Aktif',
            'description' => 'Periode Aktif harus didefinisikan terlebih dahulu',
        ];
        if (!is_null($activePeriod)) {
            $data['period_active']['status'] = true;
        } else {
            $activePeriod = new Period();
            $complete = false;
        }

        $futurePeriod = Period::find()->where(['is_future' => 1])->one();
        $data['period_future'] = [
            'status' => false,
            'title' => 'Pengaturan Periode Kedepan',
            'description' => 'Periode Kedepan harus didefinisikan terlebih dahulu',
        ];
        if (!is_null($futurePeriod)) {
            $data['period_future']['status'] = true;
        } else {
            $futurePeriod = new Period();
            $complete = false;
        }

        /* cek currency detail */
        $activeCurrencyValue = CurrencyDetail::find()->where(['period_id' => $activePeriod->id])->all();
        $data['currency_active'] = [
            'status' => false,
            'title' => 'Pengaturan Detail Currency Periode Aktif',
            'description' => 'Pengaturan Currency Periode Aktif harus didefinisikan terlebih dahulu',
        ];
        if (count($activeCurrencyValue) > 0) {
            $data['currency_active']['status'] = true;
        } else {
            $complete = false;
        }

        $futureCurrencyValue = CurrencyDetail::find()->where(['period_id' => $futurePeriod->id])->all();
        $data['currency_future'] = [
            'status' => false,
            'title' => 'Pengaturan Detail Currency Periode Kedepan',
            'description' => 'Pengaturan Currency Periode Kedepan harus didefinisikan terlebih dahulu',
        ];
        if (count($futureCurrencyValue) > 0) {
            $data['currency_future']['status'] = true;
        } else {
            $complete = false;
        }

        if ($complete) {
            $msg = "
			<div class='alert alert-success'>Data berhasil dilengkapi! Sistem siap digunakan!</div>
			";
        } else {
            $msg = "
			<div class='alert alert-danger'>Data belum dilengkapi! Silahkan melengkapi data terlebih dahulu!!</div>
			";
        }

        $params['data'] = $data;
        $params['msg'] = $msg;
        return $this->render('requirement', $params);
    }

    public function actionRefineData()
    {
        foreach (DirectPurchase::find()->all() as $item) {
            $item->save();
        }

        foreach (PurchaseOrder::find()->all() as $item) {
            $item->save();
        }

        redirect(url());
    }
}

?>
