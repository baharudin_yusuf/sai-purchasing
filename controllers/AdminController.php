<?php
namespace app\controllers;

use app\controllers\master\BankController;
use app\controllers\master\BankDetailController;
use app\controllers\master\BudgetCategoryController;
use app\controllers\master\BudgetCategoryPeriodController;
use app\controllers\master\BudgetController;
use app\controllers\master\BudgetGroupController;
use app\controllers\master\BudgetLimitController;
use app\controllers\master\BudgetPeriodController;
use app\controllers\master\BudgetSubGroupController;
use app\controllers\master\CarlineController;
use app\controllers\master\CurrencyController;
use app\controllers\master\CurrencyDetailController;
use app\controllers\master\DepartmentController;
use app\controllers\master\FrancoController;
use app\controllers\master\GoodsServiceController;
use app\controllers\master\PaymentMethodController;
use app\controllers\master\PeriodController;
use app\controllers\master\PricelistController;
use app\controllers\master\RevenueTaxController;
use app\controllers\master\SectionController;
use app\controllers\master\SupplierCategoryController;
use app\controllers\master\SupplierController;
use app\controllers\master\TermController;
use app\controllers\master\UnitCategoryController;
use app\controllers\master\UnitController;
use app\controllers\master\ValueAddedTaxController;
use app\helpers\Auth;
use app\helpers\DataImporter;
use app\models\User;
use Yii;

class AdminController extends BaseController
{
    protected static $type;

    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Data Master' => self::getMasterMenu(),
            'List User' => url('admin/user-list'),
            'Access Control List' => url('acl/index'),
        ];
        ksort($menu['Data Master']);

        /* check if is admin */
        if (!Auth::user()->roleAs('admin')) {
            redirect(url());
        }

        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'admin-panel');

        return [];
    }

    private function getMasterMenu()
    {
        $data = [
            'Budget' => [
                'controller' => BudgetController::class,
                'url' => url('master/budget'),
            ],
            'Budget Category' => [
                'controller' => BudgetCategoryController::class,
                'url' => url('master/budget-category')
            ],
            'Budget Category Period' => [
                'controller' => BudgetCategoryPeriodController::class,
                'url' => url('master/budget-category-period')
            ],
            'Budget Group' => [
                'controller' => BudgetGroupController::class,
                'url' => url('master/budget-group')
            ],
            'Budget Limit' => [
                'controller' => BudgetLimitController::class,
                'url' => url('master/budget-limit')
            ],
            'Budget Period' => [
                'controller' => BudgetPeriodController::class,
                'url' => url('master/budget-period')
            ],
            'Budget Sub Group' => [
                'controller' => BudgetSubGroupController::class,
                'url' => url('master/budget-sub-group')
            ],
            'Currency' => [
                'controller' => CurrencyController::class,
                'url' => url('master/currency')
            ],
            'Currency Detail' => [
                'controller' => CurrencyDetailController::class,
                'url' => url('master/currency-detail')
            ],
            'Franco' => [
                'controller' => FrancoController::class,
                'url' => url('master/franco')
            ],
            'Goods Service' => [
                'controller' => GoodsServiceController::class,
                'url' => url('master/goods-service')
            ],
            'Term' => [
                'controller' => TermController::class,
                'url' => url('master/term')
            ],
            'Unit Category' => [
                'controller' => UnitCategoryController::class,
                'url' => url('master/unit-category')
            ],
            'Unit' => [
                'controller' => UnitController::class,
                'url' => url('master/unit')
            ],
            'Carline' => [
                'controller' => CarlineController::class,
                'url' => url('master/carline')
            ],
            'Department' => [
                'controller' => DepartmentController::class,
                'url' => url('master/department')
            ],
            'Period' => [
                'controller' => PeriodController::class,
                'url' => url('master/period')
            ],
            'Pricelist' => [
                'controller' => PricelistController::class,
                'url' => url('master/pricelist')
            ],
            'Section' => [
                'controller' => SectionController::class,
                'url' => url('master/section')
            ],
            'Supplier' => [
                'controller' => SupplierController::class,
                'url' => url('master/supplier')
            ],
            'Supplier Category' => [
                'controller' => SupplierCategoryController::class,
                'url' => url('master/supplier-category')
            ],
            'Bank' => [
                'controller' => BankController::class,
                'url' => url('master/bank')
            ],
            'Bank Detail' => [
                'controller' => BankDetailController::class,
                'url' => url('master/bank-detail')
            ],
            'Payment Method' => [
                'controller' => PaymentMethodController::class,
                'url' => url('master/payment-method')
            ],
            'Revenue Tax' => [
                'controller' => RevenueTaxController::class,
                'url' => url('master/revenue-tax')
            ],
            'Value Added Tax' => [
                'controller' => ValueAddedTaxController::class,
                'url' => url('master/value-added-tax')
            ],
        ];

        $result = [];
        $activeUser = Auth::user();
        foreach ($data as $label => $param) {
            $class = $param['controller'];
            if (!property_exists($class, 'moduleName') || !property_exists($class, 'subModule')) {
                continue;
            }

            $allowCount = 0;
            $moduleName = $class::$moduleName;
            $subModule = $class::$subModule;

            foreach ($subModule as $item) {
                if ($activeUser->allowTo($moduleName, $item)) {
                    $allowCount++;
                }
            }

            if ($allowCount == 0) {
                continue;
            }

            $result[$label] = $param['url'];
        }

        return $result;
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionImport()
    {
        $params = [
            'importKey' => random_string(),
            'type' => self::$type
        ];
        return $this->render('//master/import', $params);
    }

    public function actionPreviewImport()
    {
        $data = DataImporter::loadExcel('file', 100);
        if (!is_array($data)) {
            return strval($data);
        }

        if (empty($data) || count($data) == 0) {
            return alert_danger('Terjadi kesalahan! Tidak ada data tersedia untuk di import');
        }

        $importKey = post_data('import_key');
        set_session($importKey, $data);

        $params = [
            'data' => $data,
            'type' => self::$type,
            'importKey' => $importKey
        ];
        return $this->renderPartial('//master/result-import', $params);
    }

    protected function validateImport($required)
    {
        $importKey = post_data('import_key');
        $data = get_session($importKey);
        if (!is_array($data) || count($data) == 0) {
            return alert_danger('Terjadi kesalahan! Tidak ada data tersedia untuk diimport');
        }

        foreach ($required as $key) {
            if (!isset($data[0][$key])) {
                return alert_danger('Terjadi kesalahan! Format import tidak sesuai');
            }
        }

        return $data;
    }

    protected function finalizeImport($imported, $failed)
    {
        if (!empty($failed)) {
            $params = [
                'imported' => $imported,
                'data' => $failed,
                'type' => self::$type,
            ];
            return $this->renderPartial('//master/import-error', $params);
        }

        $importKey = post_data('import_key');
        remove_session($importKey);
        $redirectURL = url('master/' . self::$type);

        return alert_success("$imported data berhasil di import") .
            "<script> redirectTo('$redirectURL', 1500); </script>";
    }

    public function actionUserList()
    {
        $data = [
            'data' => User::find()->all()
        ];
        return $this->render('user-list', $data);
    }
}

?>
