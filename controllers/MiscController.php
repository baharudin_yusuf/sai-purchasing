<?php
namespace app\controllers;

use Yii;

class MiscController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Registration' => url('user/register'),
            'Login' => url('user/login'),
        ];
        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'unsorted');

        return [];
    }

    public function actionIncludeRejected()
    {
        $include_rejected = get_session('include_rejected');
        if (is_null($include_rejected)) {
            set_session('include_rejected', true);
        } else {
            if ($include_rejected) set_session('include_rejected', false);
            else set_session('include_rejected', true);
        }

        return "<script> location.reload(); </script>";
    }

    public function actionOnlyRejected()
    {
        $only_rejected = get_session('only_rejected');
        if (is_null($only_rejected)) {
            set_session('only_rejected', true);
        } else {
            if ($only_rejected) set_session('only_rejected', false);
            else set_session('only_rejected', true);
        }

        return "<script> location.reload(); </script>";
    }

    public function actionIncludeCanceled()
    {
        $include_canceled = get_session('include_canceled');
        if (is_null($include_canceled)) {
            set_session('include_canceled', true);
        } else {
            if ($include_canceled) set_session('include_canceled', false);
            else set_session('include_canceled', true);
        }

        return "<script> location.reload(); </script>";
    }

    public function actionOnlyCanceled()
    {
        $only_canceled = get_session('only_canceled');
        if (is_null($only_canceled)) {
            set_session('only_canceled', true);
        } else {
            if ($only_canceled) set_session('only_canceled', false);
            else set_session('only_canceled', true);
        }

        return "<script> location.reload(); </script>";
    }
}

?>
