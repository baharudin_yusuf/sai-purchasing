<?php

namespace app\controllers;

use app\helpers\AccessControl;
use PHPMailer\PHPMailer\PHPMailer;

class MailerController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();
        if (!AccessControl::allow('ae|dekanat')) die();

        return [];
    }

    public static function sendMail($data)
    {
        date_default_timezone_set('Asia/Jakarta');

        try {
            $mail = new PHPMailer();
            $mail->isSMTP();
            $mail->SMTPDebug = 0;
            $mail->Debugoutput = 'html';
            $mail->Host = SMTP_HOST;
            $mail->Port = SMTP_PORT;
            $mail->SMTPAuth = true;
            $mail->Username = SMTP_USERNAME;
            $mail->Password = SMTP_PASSWORD;
            $mail->SMTPSecure = SMTP_ENCRYPTION;
            $mail->Subject = $data['subject'];
            $mail->AltBody = 'This is a plain-text message body';
            $mail->setFrom($data['pengirim'], 'SAI System');
            $mail->addReplyTo($data['pengirim'], 'SAI System');
            $mail->msgHTML($data['pesan']);

            $kirim = false;
            $tujuan = explode(',', $data['tujuan']);
            foreach ($tujuan as $email) {
                $email = trim($email);
                if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $mail->addAddress($email, $email);
                }
            }

            return $mail->send();
        } catch (\Exception $exception) {
            return false;
        }
    }
}
