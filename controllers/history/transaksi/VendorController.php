<?php
namespace app\controllers\history\transaksi;

use app\controllers\HistoryTransaksiController;
use app\models\master\Supplier;
use app\models\PurchaseOrder;

class VendorController extends HistoryTransaksiController
{
    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public function actionIndex()
    {
        /* cari supplier yang pernah transaksi */
        $supplierList = [];
        $po = PurchaseOrder::find()->groupBy('supplier_id')->all();
        foreach ($po as $item) {
            $supplierList[] = $item->supplier_id;
        }

        /*$purchaseOrderList = [];
        $activeUser = $this::getActiveUser();
        if($activeUser->roleAs('section')){
            $sectionId = $activeUser->section_id;

        } else if($activeUser->roleAs('manager')){

        } else if($activeUser->roleAs('dfm')){

        } else{
            $data = Supplier::find()->where(['IN', 'id', $supplierList])->all();
        }*/


        $data = Supplier::find()->where(['IN', 'id', $supplierList])->all();

        $params = [
            'vendor' => $data
        ];
        return $this->render('index', $params);
    }

    public function actionDetail()
    {
        $id = get_data('id', 0);
        $vendor = Supplier::findOne($id);
        if (is_null($vendor)) {
            redirect(url('history/transaksi/vendor/index'));
        }

        $params = [
            'vendor' => $vendor,
        ];
        $params['params'] = $params;
        return $this->render('detail', $params);
    }
}

?>
