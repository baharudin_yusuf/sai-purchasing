<?php
namespace app\controllers\pr;

use app\helpers\Auth;

class MiscController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        if (!Auth::user()->roleAs('section')) {
            redirect(url());
        }

        return [];
    }


}

?>
