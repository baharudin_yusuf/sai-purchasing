<?php
namespace app\controllers\pr;

use app\controllers\NotificationController as Notification;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\helpers\PriceConverter;
use app\models\BudgetFinal;
use app\models\master\BudgetPeriod;
use app\models\master\Carline;
use app\models\master\Currency;
use app\models\master\GoodsService;
use app\models\master\Period;
use app\models\PurchaseRequisition;
use app\models\PurchaseRequisitionDetail;

class CreateController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        if (!Auth::user()->roleAs('section')) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $budgetFinalOption = [];
        $activeUser = Auth::user();

        $activePeriod = Period::findOne(['is_active' => 1]);
        if (!is_null($activePeriod)) {
            $budgetPeriod = BudgetPeriod::findOne([
                'period_id' => $activePeriod->id,
                'month' => date('m'),
            ]);

            if (!is_null($budgetPeriod)) {
                $budgetFinal = BudgetFinal::find()
                    ->select('budget_final.*')
                    ->leftJoin('budget_proposal', 'budget_proposal.id = budget_final.budget_proposal_id')
                    ->where(['budget_proposal.section_id' => $activeUser->section_id])
                    ->where(['budget_proposal.budget_period_id' => $budgetPeriod->id])
                    ->all();

                foreach ($budgetFinal as $item) {
                    $code = $item->code;
                    $budgetNumber = $item->budgetProposal->budget->number;
                    $budgetName = $item->budgetProposal->budget->name;
                    $periodName = $budgetPeriod->name;

                    $budgetFinalOption[$item->id] = "[$code] $budgetNumber - $budgetName ($periodName)";
                }
            }
        }

        $params = [
            'activeUser' => $activeUser,
            'budgetFinalOption' => $budgetFinalOption,
            'sessionKey' => random_string(),
        ];
        return $this->render('index', $params);
    }

    public function actionAddNew()
    {
        $finalId = intval(post_data('budget_final_id'));
        $budgetFinal = BudgetFinal::firstOrFail($finalId);

        $params = [
            'goodsServiceOption' => [
                'add-new' => 'Select New Goods/Service',
                'from-final' => 'From Budget Final',
            ],
            'budgetFinal' => $budgetFinal,
            'carlineOption' => Carline::getOption(),
            'sessionKey' => post_data('session_key')
        ];
        return $this->renderPartial('add-new', $params);
    }

    public function actionAddAllFromFinal()
    {
        $budgetFinalId = intval(post_data('budget_final_id'));

        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $addedGoodsServiceList = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $goodsService = GoodsService::findOne($gsId);
            if (!is_null($goodsService)) {
                $addedGoodsServiceList[] = $goodsService;
            }
        }

        $budgetFinal = BudgetFinal::firstOrFail($budgetFinalId);
        $budgetProposal = $budgetFinal->budgetProposal;

        foreach ($budgetProposal->getNonCancelReject() as $detail) {
            $goodsService = $detail->goodsService;
            $currCategory = $goodsService->is_po ? 'Purchase Order' : 'Direct Purchase';

            foreach ($addedGoodsServiceList as $ags) {
                $agsCategory = $ags->is_po ? 'Purchase Order' : 'Direct Purchase';
                if ($currCategory != $agsCategory) {
                    return alert_danger("Terjadi kesalahan! Item yang anda pilih memiliki kategori '$currCategory', sedangkan kategori item pertama yang anda pilih adalah '$agsCategory'.<br>Silahkan memilih item lain dengan kategori '$currCategory', atau kosongkan terlebih dahulu item yang telah anda pilih sebelumnya.");
                }
            }

            if (!isset($addedGoodsService[$goodsService->id])) {
                $addedGoodsService[$goodsService->id] = [
                    'quantity' => $detail->quantity,
                    'carline_id' => $detail->carline_id,
                    'price_estimation' => $detail->price_estimation,
                    'comment' => $detail->comment,
                    'required_date' => date('Y-m-d'),
                ];
            }
        }

        set_session($sessionKey, $addedGoodsService);
        return alert_success('Data berhasil ditambahkan') .
            "<script> loadPRDetail(); closeModal(1500); </script>";
    }

    public function actionGetGoodsServiceOption()
    {
        $budgetFinalId = intval(post_data('budget_final_id'));
        $budgetFinal = BudgetFinal::firstOrFail($budgetFinalId);
        $budgetProposal = $budgetFinal->budgetProposal;

        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $gsExceptId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsExceptId[] = $gsId;
        }

        $goodsServiceOption = [];
        $source = post_data('goods_service_source');

        if ($source == 'add-new') {
            $goodsService = GoodsService::find()
                ->where(['budget_id' => $budgetProposal->budget_id])
                ->andWhere(['NOT IN', 'id', $gsExceptId])
                ->all();

            foreach ($goodsService as $item) {
                $goodsServiceOption[$item->id] = $item->name;
            }
        } else {
            foreach ($budgetProposal->getNonCancelReject() as $detail) {
                $goodsService = $detail->goodsService;
                if (!in_array($goodsService->id, $gsExceptId)) {
                    $goodsServiceOption[$goodsService->id] = $goodsService->name;
                }
            }
        }

        if (empty($goodsServiceOption)) {
            return alert_danger('Perhatian! Tidak ada goods/service tersedia untuk digunakan!') .
                "<script> $('select[name=goods_service_id]').html('').trigger('chosen:updated'); </script>";
        }

        $option = '';
        foreach ($goodsServiceOption as $key => $value) {
            $option .= "<option value='$key'>$value</option>";
        }
        $option = base64_encode($option);

        return "
        <script>
            $('select[name=goods_service_id]').html(atob('$option')).trigger('chosen:updated');
            getGoodsServiceData();
        </script>";
    }

    public function actionGetGoodsServiceData()
    {
        $budgetFinalId = intval(post_data('budget_final_id'));
        $budgetFinal = BudgetFinal::firstOrFail($budgetFinalId);
        $budgetProposal = $budgetFinal->budgetProposal;

        $source = post_data('goods_service_source');
        $goodsServiceId = intval(post_data('goods_service_id'));

        if ($source == 'add-new') {
            $goodsService = GoodsService::findOne($goodsServiceId);
            if (is_null($goodsService)) {
                return alert_danger('Terjadi kesalahan! Data goods/service tidak tersedia!');
            }

            $priceEstimation = $goodsService->getPricelistMaxPrice();
        } else {
            $proposalDetail = $budgetProposal->getDetail()->where(['goods_service_id' => $goodsServiceId])->one();
            if (is_null($proposalDetail)) {
                return alert_danger('Terjadi kesalahan! Data goods/service tidak tersedia!');
            }

            $goodsService = $proposalDetail->goodsService;
            $priceEstimation = $proposalDetail->price_estimation;
        }

        $currency = $goodsService->currency->name;
        $category = $goodsService->is_po ? 'Purchase Order' : 'Direct Purchase';
        $displaypriceEstimation = currency_format($priceEstimation);

        if ($priceEstimation == 0) {
            $alertMsg = alert_danger('Terjadi kesalahan! Goods/Service belum terdaftar dalam pricelist');
        } else {
            $alertMsg = '';
        }

        return "
		$alertMsg
		<script>
            $('#td-currency').html('$currency');
            $('#td-category').html('$category');
            $('#td-price-estimation').html('$displaypriceEstimation');
		</script>";
    }

    public function actionSaveDetail()
    {
        $budgetFinalId = intval(post_data('budget_final_id'));
        $budgetFinal = BudgetFinal::firstOrFail($budgetFinalId);
        $budgetProposal = $budgetFinal->budgetProposal;

        $goodsServiceId = intval(post_data('goods_service_id'));
        $goodsService = GoodsService::findOne($goodsServiceId);
        if (is_null($goodsService)) {
            return alert_danger('Terjadi kesalahan! Data goods/service tidak tersedia!');
        }
        $currCategory = $goodsService->is_po ? 'Purchase Order' : 'Direct Purchase';

        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $addedGoodsServiceList = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $ags = GoodsService::findOne($gsId);
            if (!is_null($ags)) {
                $addedGoodsServiceList[] = $ags;
            }
        }

        // verifikasi jenis
        foreach ($addedGoodsServiceList as $ags) {
            $agsCategory = $ags->is_po ? 'Purchase Order' : 'Direct Purchase';
            if ($currCategory != $agsCategory) {
                return alert_danger("Terjadi kesalahan! Item yang anda pilih memiliki kategori '$currCategory', sedangkan kategori item pertama yang anda pilih adalah '$agsCategory'.<br>Silahkan memilih item lain dengan kategori '$currCategory', atau kosongkan terlebih dahulu item yang telah anda pilih sebelumnya.");
            }
        }

        // verifikasi harga
        $source = post_data('goods_service_source');
        if ($source == 'add-new') {
            $priceEstimation = $goodsService->getPricelistMaxPrice();
        } else {
            $proposalDetail = $budgetProposal->getDetail()->where(['goods_service_id' => $goodsServiceId])->one();
            if (is_null($proposalDetail)) {
                return alert_danger('Terjadi kesalahan! Data goods/service tidak tersedia!');
            }
            $priceEstimation = $proposalDetail->price_estimation;
        }

        if (!isset($addedGoodsService[$goodsService->id])) {
            $addedGoodsService[$goodsService->id] = [
                'quantity' => post_data('quantity'),
                'carline_id' => post_data('carline_id'),
                'price_estimation' => $priceEstimation,
                'comment' => post_data('comment'),
                'required_date' => post_data('required_date'),
            ];
            set_session($sessionKey, $addedGoodsService);
        }

        // validasi batas amount budget
        $budgetAvailableAmount = $budgetFinal->getAmountAvailable();
        $submissionAmount = $this->getSubmissionAmount($sessionKey);
        if ($submissionAmount > $budgetAvailableAmount) {
            $currency = Currency::getActiveCurrency();
            $budgetAvailableAmount = $currency->name . ' ' . currency_format($budgetAvailableAmount);
            $submissionAmount = $currency->name . ' ' . currency_format($submissionAmount);

            // hapus data yang baru ditambakan
            unset($addedGoodsService[$goodsService->id]);
            set_session($sessionKey, $addedGoodsService);

            return alert_danger("Terjadi kesalahan! Pengajuan purchase requisition anda melebihi batas amount budget final!<br>Batas maksimal budget final : $budgetAvailableAmount, pengajuan anda : $submissionAmount");
        }

        return alert_success('Detail purchase requisition berhasil ditambahkan!') .
            "<script> loadPRDetail(); closeModal(1000); </script>";
    }

    private function getSubmissionAmount($sessionKey)
    {
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $gsAddedId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsAddedId[] = $gsId;
        }

        $total = 0;
        $data = GoodsService::find()->where(['in', 'id', $gsAddedId])->all();
        foreach ($data as $item) {
            $priceEstimation = $addedGoodsService[$item->id]['price_estimation'];
            $cPriceEstimation = PriceConverter::convert($priceEstimation, $item->currency_id);
            $total += ($cPriceEstimation * $addedGoodsService[$item->id]['quantity']);
        }

        return $total;
    }

    public function actionLoadData()
    {
        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $gsAddedId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsAddedId[] = $gsId;
        }

        $baseCurrency = Currency::getActiveCurrency();

        $result = [];
        $total = 0;
        $data = GoodsService::find()->where(['in', 'id', $gsAddedId])->all();
        foreach ($data as $item) {
            $gsData = $addedGoodsService[$item->id];
            $currency = $item->currency;
            $priceEstimation = $gsData['price_estimation'];
            $quantity = $gsData['quantity'];
            $subtotal = $priceEstimation * $quantity;
            $cPriceEstimation = PriceConverter::convert($priceEstimation, $currency->id);
            $cSubtotal = $cPriceEstimation * $quantity;
            $total += $cSubtotal;
            $carline = Carline::findOne($gsData['carline_id']);

            $item->quantity = $gsData['quantity'];
            $item->carline = $carline->name;
            $item->comment = $gsData['comment'];
            $item->price_estimation = $currency->name . ' ' . currency_format($priceEstimation);
            $item->sub_total = $currency->name . ' ' . currency_format($subtotal);
            $item->cvt_price_estimation = $baseCurrency->name . ' ' . currency_format($cPriceEstimation);
            $item->cvt_sub_total = $baseCurrency->name . ' ' . currency_format($cSubtotal);
            $item->required_date = $gsData['required_date'];

            $result[] = $item;
        }

        $params = [
            'data' => $result,
            'displayTotal' => $baseCurrency->name . ' ' . currency_format($total),
            'sessionKey' => $sessionKey
        ];
        return $this->renderPartial('data-detail', $params);
    }

    public function actionRemoveData()
    {
        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $id = intval(post_data('id'));
        if (isset($addedGoodsService[$id])) {
            unset($addedGoodsService[$id]);
        }

        set_session($sessionKey, $addedGoodsService);
        return alert_success('Data berhasil dihapus') .
            "<script> loadPRDetail(); closeModal(1500); </script>";
    }

    public function actionClearData()
    {
        $sessionKey = post_data('session_key');
        set_session($sessionKey, []);
        return $this->actionLoadData();
    }

    public function actionSaveData()
    {
        $budgetFinalId = intval(post_data('budget_final_id'));
        $budgetFinal = BudgetFinal::firstOrFail($budgetFinalId);
        $budgetProposal = $budgetFinal->budgetProposal;
        $activeUser = Auth::user();

        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        // validasi item
        if (count($addedGoodsService) == 0) {
            return alert_danger('Error! Anda harus memilih detail item terlebih dahulu');
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $data = new PurchaseRequisition();
            $data->section_id = $activeUser->section->id;
            $data->budget_final_id = $budgetFinal->id;
            $data->date = date('Y-m-d H:i:s');
            $data->comment = post_data('comment');
            $data->is_checked = $activeUser->is_administration ? 0 : 1;

            if (!$data->save()) {
                $transaction->rollBack();
                return alert_danger('Terjadi kesalahan! Data gagal disimpan!');
            }

            $prTypeSet = false;
            foreach ($addedGoodsService as $agsId => $agsData) {
                $prDetail = new PurchaseRequisitionDetail();
                $prDetail->purchase_requisition_id = $data->id;
                $prDetail->carline_id = $agsData['carline_id'];
                $prDetail->required_date = $agsData['required_date'];
                $prDetail->goods_service_id = $agsId;
                $prDetail->quantity = $agsData['quantity'];
                $prDetail->used_quantity = 0;
                $prDetail->available_quantity = $agsData['quantity'];
                $prDetail->price_estimation = $agsData['price_estimation'];
                $prDetail->comment = $agsData['comment'];
                $prDetail->save();

                if (!$prTypeSet) {
                    $prdGoodsService = $prDetail->goodsService;
                    if ($prdGoodsService->is_po) {
                        $data->is_direct_purchase = 0;
                        $data->is_purchase_order = 1;
                        $data->save();
                    } else {
                        $data->is_direct_purchase = 1;
                        $data->is_purchase_order = 0;
                        $data->save();
                    }
                    $prTypeSet = true;
                }
            }

            // validasi batas amount budget
            $budgetAvailableAmount = $budgetFinal->getAmountAvailable();
            $submissionAmount = $this->getSubmissionAmount($sessionKey);
            if ($submissionAmount > $budgetAvailableAmount) {
                $currency = Currency::getActiveCurrency();
                $budgetAvailableAmount = $currency->name . ' ' . currency_format($budgetAvailableAmount);
                $submissionAmount = $currency->name . ' ' . currency_format($submissionAmount);

                return alert_danger("Terjadi kesalahan! Pengajuan purchase requisition anda melebihi batas amount budget final!<br>Batas maksimal budget final : $budgetAvailableAmount, pengajuan anda : $submissionAmount");
            }

            $transaction->commit();
            $data->refresh();
            $data->calculatePriceEstimation();

            // send notification
            $notifMsg = Notificatixn::render('PR_REQUEST_APPROVAL', [
                'purchase_requisition_id' => $data->code,
                'section_name' => $activeUser->section->name
            ]);

            Notification::sendNotification([
                'category' => 'pr',
                'type' => 'warning',
                'target_id' => $data->id,
                'title' => $notifMsg
            ]);

            $redirectURL = url("pr/main/detail/?id=" . $data->id);
            return alert_success('Data purchase requisition berhasil disimpan!') .
                "<script> redirectTo('$redirectURL', 1500); </script>";
        } catch (\Exception $exception) {
            log_error($exception);
            $transaction->rollBack();
            return alert_danger('Terjadi kesalahan! Data gagal disimpan!');
        }
    }
}

?>
