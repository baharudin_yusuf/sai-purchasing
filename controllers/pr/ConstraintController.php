<?php
namespace app\controllers\pr;

use app\helpers\Auth;
use app\models\master\Department;
use app\models\PrRevision;
use app\models\PurchaseRequisition;
use app\models\PurchaseRequisitionDetail;

class ConstraintController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public static function allowApprovePR(PurchaseRequisition $purchaseRequisition)
    {
        $activeUser = Auth::user();
        $approvedByManager = $purchaseRequisition->is_approved_by_manager;
        $approvedByDFM = $purchaseRequisition->is_approved_by_dfm;
        $approvedByFM = $purchaseRequisition->is_approved_by_fm;
        $approvedByPresdir = $purchaseRequisition->is_approved_by_presdir;
        $approvedByFA = $purchaseRequisition->is_approved_by_fa;
        $isClosed = $purchaseRequisition->is_closed;
        $isRejected = $purchaseRequisition->is_rejected;
        $isCanceled = $purchaseRequisition->is_canceled;

        if ($isClosed || $isRejected || $isCanceled) {
            return false;
        } else if ($activeUser->roleAs('manager') && !$approvedByManager) {
            $childSection = [];
            foreach ($activeUser->department->section as $list) {
                $childSection[] = $list->id;
            }

            if (in_array($purchaseRequisition->section_id, $childSection)) {
                return true;
            } else {
                return false;
            }
        } else if ($activeUser->roleAs('dfm') && $approvedByManager && !$approvedByDFM) {
            return true;
        } else if ($activeUser->roleAs('fm') && $approvedByManager && $approvedByDFM && !$approvedByFM) {
            return true;
        } else if ($activeUser->roleAs('presdir') && $approvedByManager && $approvedByDFM && $approvedByFM && !$approvedByPresdir) {
            return true;
        } else if ($activeUser->roleAs('manager') && $approvedByManager && $approvedByDFM && $approvedByFM && $approvedByPresdir && !$approvedByFA) {
            $department = Department::findByCode('FA');
            if ($activeUser->department_id == $department->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowApproveDetailPR(PurchaseRequisitionDetail $purchaseRequisitionDetail)
    {
        $activeUser = Auth::user();
        if ($purchaseRequisitionDetail->is_rejected) {
            $manage = false;
        } else {
            $allowApprovePR = self::allowApprovePR($purchaseRequisitionDetail->purchaseRequisition);

            if ($allowApprovePR) {
                if ($activeUser->roleAs('manager')) {
                    $department = Department::findByCode('FA');
                    if ($activeUser->department_id == $department->id) {
                        if (!$purchaseRequisitionDetail->is_approved_by_fa) {
                            $manage = true;
                        } else {
                            $manage = false;
                        }
                    } else {
                        if (!$purchaseRequisitionDetail->is_approved_by_manager) {
                            $manage = true;
                        } else {
                            $manage = false;
                        }
                    }
                } else if ($activeUser->roleAs('dfm')) {
                    if (!$purchaseRequisitionDetail->is_approved_by_dfm) {
                        $manage = true;
                    } else {
                        $manage = false;
                    }
                } else if ($activeUser->roleAs('fm')) {
                    if (!$purchaseRequisitionDetail->is_approved_by_fm) {
                        $manage = true;
                    } else {
                        $manage = false;
                    }
                } else if ($activeUser->roleAs('presdir')) {
                    if (!$purchaseRequisitionDetail->is_approved_by_presdir) {
                        $manage = true;
                    } else {
                        $manage = false;
                    }
                } else {
                    $manage = false;
                }
            } else {
                $manage = false;
            }
        }

        return $manage;
    }

    public static function allowCancel(PurchaseRequisition $purchaseRequisition)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            if ($purchaseRequisition->is_approved_by_manager) {
                return false;
            } else if ($purchaseRequisition->is_canceled) {
                return false;
            } else if ($purchaseRequisition->is_rejected) {
                return false;
            } else {
                if ($purchaseRequisition->section_id == $activeUser->section_id) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public static function allowModify(PurchaseRequisition $purchaseRequisition)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            if ($purchaseRequisition->is_closed) {
                return false;
            } else if ($purchaseRequisition->is_canceled) {
                return false;
            } else if ($purchaseRequisition->is_rejected) {
                return false;
            } else if (!$purchaseRequisition->is_approved_by_fa) {
                return false;
            } else {
                if ($purchaseRequisition->section_id == $activeUser->section_id) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public static function allowCancelRevision(PrRevision $prRevision)
    {
        $activeUser = Auth::user();
        $sectionId = $prRevision->purchaseRequisition->section_id;

        if (!$activeUser->roleAs('section')) {
            return false;
        } else if ($sectionId != $activeUser->section_id) {
            return false;
        } else if ($prRevision->is_approved_by_manager) {
            return false;
        } else if ($prRevision->is_canceled) {
            return false;
        } else if ($prRevision->is_rejected) {
            return false;
        } else {
            return true;
        }
    }

    public static function allowApproveRevision(PrRevision $prRevision)
    {
        $activeUser = Auth::user();
        $departmentId = $prRevision->purchaseRequisition->section->department_id;

        if (!$activeUser->roleAs('manager')) {
            return false;
        } else if ($departmentId != $activeUser->department_id) {
            return false;
        } else if ($prRevision->is_approved_by_manager) {
            return false;
        } else if ($prRevision->is_canceled) {
            return false;
        } else if ($prRevision->is_rejected) {
            return false;
        } else {
            return true;
        }
    }

    public static function allowCheckPR(PurchaseRequisition $purchaseRequisition)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else if ($purchaseRequisition->is_checked) {
            return false;
        } else {
            if ($activeUser->is_supervisor) {
                if ($activeUser->section_id == $purchaseRequisition->section_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}

?>
