<?php
namespace app\controllers\pr;

use app\models\PurchaseRequisitionDetail;

class ReportController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionItem()
    {
        $params = self::getItemData();
        return $this->render('list-item', $params);
    }

    private function getItemData()
    {
        $cond = [
            'is_approved_by_fa' => 1,
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (post_data('only-close')) {
            set_session('pr_only_close', true);
            $cond['is_closed'] = 1;
            $params['only_close'] = 1;
        } else {
            set_session('pr_only_close', false);
            $params['only_close'] = 0;
        }

        if (post_data('only-open')) {
            set_session('pr_only_open', true);
            $cond['is_closed'] = 0;
            $params['only_open'] = 1;
        } else {
            set_session('pr_only_open', false);
            $params['only_open'] = 0;
        }

        $params['data'] = PurchaseRequisitionDetail::find()
            ->where($cond)
            ->all();

        return $params;
    }

    public function actionDownloadItem()
    {
        $objPHPExcel = new \PHPExcel();

        $style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );

        /* header file */
        $header[] = ['PR ID', 'GOODS SERVICE', 'QUANTITY', 'USED', 'AVAILABLE'];
        for ($i = 0; $i < count($header); $i++) {
            $row = $i + 1;
            $coll = 'A';

            for ($j = 0; $j < count($header[$i]); $j++) {
                $cell = $coll . $row;
                $objPHPExcel->getActiveSheet()->SetCellValue($cell, $header[$i][$j]);
                $coll++;
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($style);

        $params = self::getItemData();
        $row = count($header) + 1;
        foreach ($params['data'] as $list) {
            $col = 'A';

            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->purchaseRequisition->code);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->goodsService->name);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->quantity);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->used_quantity);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col) . "$row", $list->available_quantity);

            $objPHPExcel->getActiveSheet()->getStyle("A$row:$col$row")->applyFromArray($style);
            $row++;
        }

        $filename = 'PR-REPORT-ITEM-' . date('dmY') . '.xlsx';
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(TEMP_DIR . $filename);

        return \Yii::$app->response->sendFile(TEMP_DIR . $filename);
    }
}

?>
