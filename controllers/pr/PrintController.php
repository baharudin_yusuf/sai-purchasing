<?php
namespace app\controllers\pr;

use app\models\PurchaseRequisition;

class PrintController extends MainController
{
    public static $textLimit = 19;
    public static $maxLine = 29;
    private static $nextExists = false;

    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public function actionFormPr()
    {
        $this->layout = 'print';
        $id = intval(get_data('id'));
        $data = PurchaseRequisition::firstOrFail($id);
        $page = intval(get_data('p', 1));
        $detail = self::getFormattedData($data);

        if (self::$nextExists) {
            $nextPage = $page + 1;
            $nextURL = url("pr/print/form-pr/?id=$id&p=$nextPage");
        } else {
            $nextURL = null;
        }

        if ($page > 1) {
            $prevPage = $page - 1;
            $prevURL = url("pr/print/form-pr/?id=$id&p=$prevPage");
        } else {
            $prevURL = null;
        }

        $params = [
            'data' => $data,
            'page' => $page,
            'detail' => $detail,
            'nextURL' => $nextURL,
            'prevURL' => $prevURL,
        ];
        return $this->render('form-pr', $params);
    }

    private static function getFormattedData(PurchaseRequisition $purchaseRequisition)
    {
        $detail = $purchaseRequisition->getNonCancelReject();
        $nData = count($detail);
        $page = intval(get_data('p', 1));
        $start = ($page - 1) * self::$maxLine;
        $end = $start + self::$maxLine;
        $end = ($end > $nData) ? $nData : $end;
        $blankText = '&nbsp;';

        $num = 0;
        $result = [];
        foreach ($detail as $item) {
            $itemLine = [''];
            $lineCount = 0;

            $goodsService = $item->goodsService;
            $gsNameSplit = explode(' ', $goodsService->name);
            foreach ($gsNameSplit as $gsWord) {
                $gsTemp = trim($itemLine[$lineCount] . ' ' . $gsWord);
                if (strlen($gsTemp) <= self::$textLimit) {
                    $itemLine[$lineCount] = $gsTemp;
                } else {
                    $lineCount++;
                    $itemLine[$lineCount] = $gsWord;
                }
            }

            $modLine = self::$maxLine - (count($result) % self::$maxLine);
            if (count($itemLine) > $modLine) {
                for ($i = 0; $i < $modLine; $i++) {
                    $result[] = [
                        'num' => $blankText,
                        'rowspan' => 1,
                        'budget_number' => $blankText,
                        'goods_service' => $blankText,
                        'quantity' => $blankText,
                        'price_estimation' => $blankText,
                        'amount' => $blankText,
                        'required_date' => $blankText,
                        'carline' => $blankText,
                    ];
                }
            }

            foreach ($itemLine as $lineNum => $line) {
                $priceEstimation = $item->display_price_estimation;
                if (strlen($item->display_converted_price_estimation) > 0) {
                    $priceEstimation = $item->display_converted_price_estimation;
                }

                $amount = $item->display_sub_total;
                if (strlen($item->display_converted_sub_total) > 0) {
                    $amount = $item->display_converted_sub_total;
                }

                $result[] = [
                    'num' => $lineNum == 0 ? $num + 1 : '',
                    'rowspan' => $lineNum == 0 ? count($itemLine) : '',
                    'budget_number' => $lineNum == 0 ? $purchaseRequisition->budget->number : '',
                    'goods_service' => $line,
                    'quantity' => $lineNum == 0 ? $item->quantity : '',
                    'price_estimation' => $lineNum == 0 ? $priceEstimation : '',
                    'amount' => $lineNum == 0 ? $amount : '',
                    'required_date' => $lineNum == 0 ? $item->required_date : '',
                    'carline' => $lineNum == 0 ? $item->carline->name : '',
                ];
            }
            $num++;
        }

        // note
        // code goes here

        if ($num > $end) {
            self::$nextExists = true;
        }

        $newResult = [];
        for ($num = $start; $num < $start + self::$maxLine; $num++) {
            if (isset($result[$num])) {
                $newResult[] = $result[$num];
            }
        }
        $result = $newResult;

        $currentLine = count($result);
        for ($i = $currentLine; $i < self::$maxLine; $i++) {
            $result[] = [
                'num' => $blankText,
                'rowspan' => 1,
                'budget_number' => $blankText,
                'goods_service' => $blankText,
                'quantity' => $blankText,
                'price_estimation' => $blankText,
                'amount' => $blankText,
                'required_date' => $blankText,
                'carline' => $blankText,
            ];
        }

        return $result;
    }
}

?>
