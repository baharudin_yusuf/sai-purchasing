<?php
namespace app\controllers\pr;

use app\controllers\NotificationController as Notification;
use app\controllers\pr\ConstraintController as PRConstraint;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\PrRevision;
use app\models\PrRevisionDetail;
use app\models\PurchaseRequisition;

class ModifyController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $prId = intval(get_data('id'));
        $data = PurchaseRequisition::firstOrFail($prId);
        if (!PRConstraint::allowModify($data)) {
            parent::throw404Error();
        }

        $params = [
            'data' => $data
        ];
        return $this->render('index', $params);
    }

    public function actionApproveRevision()
    {
        $revId = intval(post_data('rev_id'));
        $revision = PrRevision::firstOrFail($revId);

        if (PRConstraint::allowApproveRevision($revision)) {
            // UPDATE REVISION STATUS
            $revision->is_approved_by_manager = 1;
            $revision->approved_by_manager_time = date('Y-m-d H:i:s');
            $revision->save();

            // UPDATE PR STATUS
            $purchaseRequisition = $revision->purchaseRequisition;
            $purchaseRequisition->is_revision = 1;
            $purchaseRequisition->save();

            foreach ($purchaseRequisition->getNonCancelReject() as $prDetail) {
                $revDetail = PrRevisionDetail::findOne([
                    'pr_revision_id' => $revision->id,
                    'purchase_requisition_detail_id' => $prDetail->id
                ]);

                if (!is_null($revDetail)) {
                    $prDetail->quantity = $revDetail->quantity;
                    $prDetail->available_quantity = $prDetail->quantity - $prDetail->used_quantity;
                    $prDetail->save();
                }
            }

            // SEND NOTIFICATION TO SECTION
            $notifMsg = Notificatixn::render('PR_REVISION_APPROVED', [
                'pr_revision_id' => $revision->code,
                'purchase_requisition_id' => $purchaseRequisition->code,
                'department_name' => Auth::user()->department->name,
            ]);

            $notifData = [
                'category' => 'pr-rev',
                'type' => 'success',
                'target_id' => $purchaseRequisition->id,
                'title' => $notifMsg
            ];
            Notification::sendNotification($notifData);

            $purchaseRequisition->recheckClosedStatus();
            $purchaseRequisition->calculatePriceEstimation();

            return alert_success('Pengajuan revisi berhasil diterima!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menerima pengajuan revisi!');
        }
    }

    public function actionRejectRevision()
    {
        $revId = intval(post_data('rev_id'));
        $revision = PrRevision::firstOrFail($revId);
        $purchaseRequisition = $revision->purchaseRequisition;

        if (PRConstraint::allowApproveRevision($revision)) {
            $revision->is_rejected = 1;
            $revision->save();

            // SEND NOTIFICATION TO SECTION
            $notifMsg = Notificatixn::render('PR_REVISION_REJECTED', [
                'pr_revision_id' => $revision->code,
                'purchase_requisition_id' => $purchaseRequisition->code,
                'department_name' => Auth::user()->department->name,
            ]);

            $notifData = [
                'category' => 'pr-rev',
                'type' => 'danger',
                'target_id' => $purchaseRequisition->id,
                'title' => $notifMsg
            ];
            Notification::sendNotification($notifData);

            return alert_success('Pengajuan revisi berhasil ditolak!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menolak pengajuan revisi!');
        }
    }

    public function actionCancelRevision()
    {
        $revId = intval(post_data('rev_id'));
        $revision = PrRevision::firstOrFail($revId);
        $purchaseRequisition = $revision->purchaseRequisition;

        if (PRConstraint::allowCancelRevision($revision)) {
            $revision->is_canceled = 1;
            $revision->save();

            // SEND NOTIFICATION
            $notifMsg = Notificatixn::render('PR_REVISION_CANCELED', [
                'pr_revision_id' => $revision->code,
                'purchase_requisition_id' => $purchaseRequisition->code,
                'section_name' => Auth::user()->section->name,
            ]);

            $notifData = [
                'category' => 'pr-rev',
                'type' => 'warning',
                'target_id' => $purchaseRequisition->id,
                'title' => $notifMsg
            ];
            Notification::sendNotification($notifData);

            return alert_success('Pengajuan revisi berhasil dibatalkan!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan revisi!');
        }
    }

    public function actionSave()
    {
        $prId = intval(post_data('id'));
        $data = PurchaseRequisition::firstOrFail($prId);
        if (!PRConstraint::allowModify($data)) {
            parent::throw404Error();
        }

        $revList = [];
        foreach ($data->getNonCancelReject() as $item) {
            if (is_null(post_data('item-' . $item->id))) {
                continue;
            }

            if ($item->quantity != intval(post_data('item-' . $item->id))) {
                $revList[] = $item->id;
            }
        }

        if (empty($revList)) {
            return alert_danger('Error! Tidak ada perubahan data tersedia!');
        }

        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $prRevision = new PrRevision();
            $prRevision->purchase_requisition_id = $data->id;
            $prRevision->date = date('Y-m-d H:i:s');

            if ($prRevision->save()) {
                foreach ($data->getNonCancelReject() as $item) {
                    if (!in_array($item->id, $revList)) {
                        continue;
                    }

                    $revisionDetail = new PrRevisionDetail();
                    $revisionDetail->pr_revision_id = $prRevision->id;
                    $revisionDetail->purchase_requisition_detail_id = $item->id;
                    $revisionDetail->quantity = intval(post_data("item-" . $item->id));
                    $revisionDetail->save();
                }

                $transaction->commit();

                // SEND NOTIFICATION
                $notifMsg = Notificatixn::render('PR_REVISION_REQUEST_APPROVAL', [
                    'pr_revision_id' => $prRevision->code,
                    'purchase_requisition_id' => $data->code,
                    'section_name' => Auth::user()->section->name
                ]);

                $notifData = [
                    'category' => 'pr-rev',
                    'type' => 'warning',
                    'target_id' => $data->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);

                $redirectURL = url("pr/main/detail/?id=" . $data->id);
                return alert_success('Revisi PR berhasil disimpan!') .
                    "<script> redirectTo('$redirectURL', 1500); </script>";
            } else {
                return alert_danger('Error! Revisi PR gagal disimpan!');
            }
        } catch (\Exception $exception) {
            $transaction->rollBack();
            return alert_danger('Error! Revisi PR gagal disimpan!');
        }
    }

    function actionPending()
    {
        $revision = PrRevision::find()
            ->where(['is_approved_by_manager' => 0, 'is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        $params = [
            'data' => $revision
        ];
        return $this->render('pending', $params);
    }
}

?>
