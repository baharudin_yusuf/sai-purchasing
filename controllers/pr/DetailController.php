<?php
namespace app\controllers\pr;

use app\controllers\NotificationController as Notification;
use app\controllers\pr\ConstraintController as PRConstraint;
use app\helpers\Auth;
use app\models\master\Department;
use app\models\master\GoodsService;
use app\models\PurchaseRequisitionDetail;

class DetailController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionRejectSubmission($id = null)
    {
        $activeRole = Auth::user();
        if (!$activeRole->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        $PRDetail = PurchaseRequisitionDetail::find()
            ->asArray()
            ->where(['id' => $id])
            ->one();

        if (PRConstraint::allowApproveDetailPR($PRDetail)) {
            $PRDetail = PurchaseRequisitionDetail::findOne($id);
            $PRDetail->is_rejected = 1;
            $purchase_requisition_id = $PRDetail->purchase_requisition_id;
            $goodsServiceId = $PRDetail->goods_service_id;

            $goodsService = GoodsService::findOne($goodsServiceId);
            $good_service = ucwords(strtolower($goodsService->name));

            if ($PRDetail->save()) {
                /* kirim notif ke section */
                $department_name = $activeRole->department->name;
                $notifMsg = parent::getNotifMessage('PR_ITEM_REJECTED');
                $notifMsg = str_replace('{purchase_requisition_id}', 'PR' . str_pad($purchase_requisition_id, 4, '0', STR_PAD_LEFT), $notifMsg);
                $notifMsg = str_replace('{department_name}', $department_name, $notifMsg);
                $notifMsg = str_replace('{good_service}', $good_service, $notifMsg);

                $notif_data = [
                    'category' => 'pr',
                    'type' => 'danger',
                    'target_id' => $purchase_requisition_id,
                    'title' => $notifMsg
                ];
                $notif_send = Notification::sendNotification($notif_data);
                /* /kirim notif ke section */

                $msg = "alert('Detail purchase requisition berhasil ditolak!'); reload();";
                parent::calculatePriceEstimation($purchase_requisition_id);
            } else {
                $msg = "alert('Error! Detail purchase requisition gagal ditolak!');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk menolak detail purchase requisition!');";
        }

        return "<script>$msg</script>";
    }

    public function actionCancelSubmission()
    {
        $id = intval(post_data('id'));
        $data = PurchaseRequisitionDetail::firstOrFail($id);
        $purchaseRequisition = $data->purchaseRequisition;

        if (PRConstraint::allowCancel($purchaseRequisition)) {
            $data->is_canceled = 1;
            if ($data->save()) {
                $purchaseRequisition->calculatePriceEstimation();
                return alert_success('Detail purchase requisition berhasil dibatalkan!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Detail purchase requisition gagal dibatalkan!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk membatalkan detail purchase requisition!');
        }
    }

    public function actionApproveSubmission($id = null)
    {
        $activeRole = Auth::user();
        $id = is_null($id) ? intval(post_data('id')) : $id;
        $PRDetail = PurchaseRequisitionDetail::find()
            ->asArray()
            ->where(['id' => $id])
            ->one();

        if (PRConstraint::allowApproveDetailPR($PRDetail)) {
            $PRDetail = PurchaseRequisitionDetail::findOne($id);

            if ($activeRole->roleAs('manager')) {
                $faDept = Department::findByCode('FA');
                if ($activeRole->department_id == $faDept->id) {
                    $PRDetail->is_approved_by_fa = 1;
                    $PRDetail->approved_by_fa_time = date('Y-m-d H:i:s');
                } else {
                    $PRDetail->is_approved_by_manager = 1;
                    $PRDetail->approved_by_manager_time = date('Y-m-d H:i:s');
                }
            } else if ($activeRole->roleAs('FM')) {
                $PRDetail->is_approved_by_fm = 1;
                $PRDetail->approved_by_fm_time = date('Y-m-d H:i:s');
            } else if ($activeRole->roleAs('DFM')) {
                $PRDetail->is_approved_by_dfm = 1;
                $PRDetail->approved_by_dfm_time = date('Y-m-d H:i:s');
            } else {
                $PRDetail->is_approved_by_presdir = 1;
                $PRDetail->approved_by_presdir_time = date('Y-m-d H:i:s');
            }

            if ($PRDetail->save()) {
                $pr_id = $PRDetail->purchase_requisition_id;
                $this::setPRStatus($pr_id);

                $msg = "alert('Detail purchase requisition berhasil diapprove!'); reload();";
                parent::calculatePriceEstimation($pr_id);
            } else $msg = "alert('Error! Detail purchase requisition gagal diapprove!');";
        } else $msg = "alert('Error! Anda tidak memiliki hak akses untuk menerima detail purchase requisition!');";

        return "<script>$msg</script>";
    }

    public function actionBulkAction()
    {
        $this::ajaxMethod();
        $action = post_data('action');
        $msg = '';

        $list_id = explode(',', post_data('list_id'));
        foreach ($list_id as $id) {
            if ($action == 'approve') {
                $msg = self::actionApproveSubmission($id);
            } else {
                $msg = self::actionRejectSubmission($id);
            }
        }

        return $msg;
    }
}

?>
