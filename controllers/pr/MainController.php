<?php
namespace app\controllers\pr;

use app\controllers\BaseController;
use app\controllers\NotificationController as Notification;
use app\controllers\pr\ConstraintController as PRConstraint;
use app\controllers\proposal\FinalController as FinalMain;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\helpers\PriceConverter;
use app\models\DpRealizationActual;
use app\models\master\Currency;
use app\models\master\Department;
use app\models\PoRealizationActual;
use app\models\PrRevision;
use app\models\PurchaseRequisition;
use app\models\PurchaseRequisitionDetail;
use Yii;

class MainController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();
        parent::requirementCheck();

        $activeUser = Auth::user();

        $menu = ['Purchase Requisition' => url("pr/main")];
        if ($activeUser->roleAs('section')) {
            $menu['Create New'] = url("pr/create");
        }
        if ($activeUser->roleAs('manager|dfm|fm|presdir|admin')) {
            $menu['Pending Submission'] = url("pr/main/pending");
            $menu['Pending Revision'] = url("pr/modify/pending");
        }

        $menu['Report'] = [
            'Item Used' => url("pr/report/item"),
        ];

        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'purchase-requisition-panel');

        return [];
    }

    public function actionIndex()
    {
        $params = [
            'data' => self::actionLoadData(),
            'include_rejected' => get_session('include_rejected'),
            'only_rejected' => get_session('only_rejected')
        ];
        return $this->render('index', $params);
    }

    public function actionLoadData()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        if ($this->include_rejected) {
            unset($cond['is_rejected']);
        }
        if ($this->include_canceled) {
            unset($cond['is_canceled']);
        }
        if ($this->only_rejected) {
            $cond['is_rejected'] = 1;
        }
        if ($this->only_canceled) {
            $cond['is_canceled'] = 1;
        }

        $activeUser = Auth::user();
        $sectionList = [];
        if ($activeUser->roleAs('section')) {
            $sectionList[] = $activeUser->section_id;
        } else if ($activeUser->roleAs('manager')) {
            $sectionList = [];
            foreach ($activeUser->department->section as $list) {
                $sectionList[] = $list->id;
            }

            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $sectionList = [];
            }
        }

        $data['data'] = PurchaseRequisition::find()->where($cond);
        if (count($sectionList) > 0) {
            $data['data'] = $data['data']->andWhere(['section_id' => $sectionList])->all();
        } else {
            $data['data'] = $data['data']->all();
        }
        return $this->renderPartial('data', $data);
    }

    public function actionDetail()
    {
        if (Yii::$app->request->isPost) {
            $prId = intval(post_data('id'));
        } else {
            $prId = intval(get_data('id'));
        }

        $data = PurchaseRequisition::firstOrFail($prId);
        $data->calculatePriceEstimation();
        $data->refresh();

        $params = [
            'data' => $data,
            'allowCheck' => PRConstraint::allowCheckPR($data),
            'allowCancel' => PRConstraint::allowCancel($data),
            'allowModify' => PRConstraint::allowModify($data),
            'allowApprovePR' => PRConstraint::allowApprovePR($data),
            'prConstraint' => ConstraintController::class
        ];
        if (Yii::$app->request->isPost) {
            return $this->renderPartial('detail', $params);
        } else {
            return $this->render('detail', $params);
        }
    }

    public static function getPRCurrencyUsed($prId)
    {
        $PR = PurchaseRequisition::findOne($prId);
        $final_id = $PR->budget_final_id;

        return FinalMain::getFinalCurrencyUsed($final_id);
    }

    public function actionRevisionDetail()
    {
        $revId = intval(post_data('id'));
        $data['data'] = PrRevision::firstOrFail($revId);
        return $this->renderPartial('revision-detail', $data);
    }

    public function actionApprovePr($id = null)
    {
        $activeUser = Auth::user();
        $prId = is_null($id) ? intval(post_data('id')) : $id;
        $data = PurchaseRequisition::firstOrFail($prId);

        if (PRConstraint::allowApprovePR($data)) {
            $data->is_checked = 1;
            $data->save();

            // APPROVE ALL PR DETAIL
            $approvedByFA = false;
            foreach ($data->getNonCancelReject() as $detail) {
                if ($activeUser->roleAs('manager')) {
                    $faCode = Department::findByCode('FA');
                    if ($activeUser->department_id == $faCode->id) {
                        if (!$detail->is_approved_by_fa) {
                            $detail->is_approved_by_fa = 1;
                            $detail->approved_by_fa_time = date('Y-m-d H:i:s');
                            $detail->save();
                        }
                        $approvedByFA = true;
                    } else {
                        if (!$detail->is_approved_by_manager) {
                            $detail->is_approved_by_manager = 1;
                            $detail->approved_by_manager_time = date('Y-m-d H:i:s');
                            $detail->save();
                        }
                    }
                } else if ($activeUser->roleAs('dfm')) {
                    if (!$detail->is_approved_by_dfm) {
                        $detail->is_approved_by_dfm = 1;
                        $detail->approved_by_dfm_time = date('Y-m-d H:i:s');
                        $detail->save();
                    }
                } else if ($activeUser->roleAs('fm')) {
                    if (!$detail->is_approved_by_fm) {
                        $detail->is_approved_by_fm = 1;
                        $detail->approved_by_fm_time = date('Y-m-d H:i:s');
                        $detail->save();
                    }
                } else if ($activeUser->roleAs('presdir')) {
                    if (!$detail->is_approved_by_presdir) {
                        $detail->is_approved_by_presdir = 1;
                        $detail->approved_by_presdir_time = date('Y-m-d H:i:s');
                        $detail->save();
                    }
                }
            }

            /* REFINE PR STATUS */
            self::setPRStatus($data);
            $data->calculatePriceEstimation();

            if (!$approvedByFA) {
                // SEND NOTIFICATION TO FA
                $notifMsg = Notificatixn::render('PR_REQUEST_APPROVAL', [
                    'section_name' => $data->section->name,
                    'purchase_requisition_id' => $data->code
                ]);
                $notifData = [
                    'category' => 'pr',
                    'type' => 'warning',
                    'target_id' => $prId,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notifData);
            }

            // SEND NOTIFICATION TO SECTION
            $notifMsg = Notificatixn::render('PR_APPROVED', [
                'purchase_requisition_id' => $data->code,
                'department_name' => $activeUser->department->name
            ]);
            Notification::sendNotification([
                'category' => 'pr',
                'type' => 'success',
                'target_id' => $prId,
                'title' => $notifMsg
            ]);

            return alert_success('Purchase Requisition berhasil diterima!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menerima PR!');
        }
    }

    public function actionRejectPr($id = null)
    {
        $prId = is_null($id) ? intval(post_data('id')) : $id;
        $data = PurchaseRequisition::firstOrFail($prId);
        if (PRConstraint::allowApprovePR($data)) {
            $data->is_rejected = 1;

            if ($data->save()) {
                // kirim notif ke section
                $notifMsg = Notificatixn::render('', [
                    'purchase_requisition_id' => $data->code,
                    'department_name' => Auth::user()->department->name
                ]);
                Notification::sendNotification([
                    'category' => 'pr',
                    'type' => 'danger',
                    'target_id' => $prId,
                    'title' => $notifMsg
                ]);

                $data->calculatePriceEstimation();
                return alert_success('Purchase Requisition berhasil ditolak!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Purchase Requisition gagal ditolak!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menolak PR!');
        }
    }

    public function actionCancelPr()
    {
        $prId = intval(post_data('id'));
        $data = PurchaseRequisition::firstOrFail($prId);
        if (PRConstraint::allowCancel($data)) {
            $data->is_canceled = 1;
            if ($data->save()) {
                $data->calculatePriceEstimation();
                return alert_success('Pengajuan purchase requisition berhasil dibatalkan!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Purchase Requisition gagal dibatalkan!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk membatalkan PR!');
        }
    }

    public function actionCheckPr()
    {
        $prId = intval(post_data('id'));
        $data = PurchaseRequisition::firstOrFail($prId);
        if (PRConstraint::allowCheckPR($data)) {
            $data->is_checked = 1;
            if ($data->save()) {
                return alert_success('Pengajuan purchase requisition berhasil ditandai sebagai sudah dicek!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Pengajuan purchase requisition gagal ditandai sebagai sudah dicek!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk mengecek PR!');
        }
    }

    protected function setPRStatus(PurchaseRequisition $data)
    {
        // cari total detail pengajuan purchase requisition
        $prDetailCount = count($data->getNonCancelReject());

        $approval = ['manager', 'dfm', 'fm', 'presdir', 'fa'];
        foreach ($approval as $level) {
            $prApprovedCount = $data->getPurchaseRequisitionDetail()
                ->where(["is_approved_by_$level" => 1])
                ->count();

            if ($prDetailCount == $prApprovedCount) {
                if (!$data->getAttribute("is_approved_by_$level")) {
                    $data->setAttribute("is_approved_by_$level", 1);
                    $data->setAttribute("approved_by_" . $level . "_time", date('Y-m-d H:i:s'));
                    $data->save();
                }
            }
        }

        // refine approval status
        $prSubmissionTotal = $data->price_estimation;
        $currency = Currency::findOne(['name' => 'IDR']);

        $activeUser = Auth::user();
        $maxSubmission = 5000000;
        $maxSubmission = PriceConverter::convert($maxSubmission, $currency->id);

        if ($prSubmissionTotal <= $maxSubmission) {
            if ($data->is_approved_by_manager && !$data->is_approved_by_dfm) {
                $data->is_approved_by_dfm = 1;
                $data->approved_by_dfm_time = null;
                $data->is_approved_by_fm = 1;
                $data->approved_by_fm_time = null;
                $data->is_approved_by_presdir = 1;
                $data->approved_by_presdir_time = null;
                $data->save();

                foreach ($data->getNonCancelReject() as $detail) {
                    $detail->is_approved_by_dfm = 1;
                    $detail->approved_by_dfm_time = null;
                    $detail->is_approved_by_fm = 1;
                    $detail->approved_by_fm_time = null;
                    $detail->is_approved_by_presdir = 1;
                    $detail->approved_by_presdir_time = null;
                    $detail->save();
                }
            }
        } else {
            $faDept = Department::findByCode('FA');
            if ($activeUser->roleAs('manager') && $activeUser->department_id != $faDept->id) {
                $department = $activeUser->department;

                if ($department->is_under_dfm && $data->is_approved_by_manager && !$data->is_approved_by_dfm) {
                    //do nothing, normal approval
                } else if ($department->is_under_fm && $data->is_approved_by_manager && !$data->is_approved_by_dfm) {
                    $data->is_approved_by_dfm = 1;
                    $data->approved_by_dfm_time = null;
                    $data->save();

                    foreach ($data->getNonCancelReject() as $detail) {
                        $detail->is_approved_by_dfm = 1;
                        $detail->approved_by_dfm_time = null;
                        $detail->save();
                    }
                } else if ($department->is_under_presdir && $data->is_approved_by_manager && !$data->is_approved_by_dfm) {
                    $data->is_approved_by_dfm = 1;
                    $data->approved_by_dfm_time = null;
                    $data->is_approved_by_fm = 1;
                    $data->approved_by_fm_time = null;
                    $data->save();

                    foreach ($data->getNonCancelReject() as $detail) {
                        $detail->is_approved_by_dfm = 1;
                        $detail->approved_by_dfm_time = null;
                        $detail->is_approved_by_fm = 1;
                        $detail->approved_by_fm_time = null;
                        $detail->save();
                    }
                }
            } else if ($activeUser->roleAs('dfm')) {
                $maxSubmission = 30000000;
                $maxSubmission = PriceConverter::convert($maxSubmission, $currency->id);

                if ($prSubmissionTotal > $maxSubmission) {
                    //do nothing, normal approval
                } else {
                    if ($data->is_approved_by_manager && $data->is_approved_by_dfm && !$data->is_approved_by_fm) {
                        $data->is_approved_by_fm = 1;
                        $data->approved_by_fm_time = null;
                        $data->is_approved_by_presdir = 1;
                        $data->approved_by_presdir_time = null;
                        $data->save();

                        foreach ($data->getNonCancelReject() as $detail) {
                            $detail->is_approved_by_fm = 1;
                            $detail->approved_by_fm_time = null;
                            $detail->is_approved_by_presdir = 1;
                            $detail->approved_by_presdir_time = null;
                            $detail->save();
                        }
                    }
                }
            } else if ($activeUser->roleAs('fm')) {
                $maxSubmission = 50000000;
                $maxSubmission = PriceConverter::convert($maxSubmission, $currency->id);

                if ($prSubmissionTotal > $maxSubmission) {
                    //do nothing, normal approval
                } else {
                    if ($data->is_approved_by_manager && $data->is_approved_by_dfm && $data->is_approved_by_fm && !$data->is_approved_by_presdir) {
                        $data->is_approved_by_presdir = 1;
                        $data->approved_by_presdir_time = null;
                        $data->save();

                        foreach ($data->getNonCancelReject() as $detail) {
                            $detail->is_approved_by_presdir = 1;
                            $detail->approved_by_presdir_time = null;
                            $detail->save();
                        }
                    }
                }
            }
        }
    }

    public function actionPending()
    {
        $activeUser = Auth::user();
        if ($activeUser->roleAs('manager')) {
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $cond = [
                    'is_approved_by_manager' => 1,
                    'is_approved_by_dfm' => 1,
                    'is_approved_by_fm' => 1,
                    'is_approved_by_presdir' => 1,
                    'is_approved_by_fa' => 0
                ];
            } else {
                $cond = ['is_approved_by_manager' => 0];
            }
        } else if ($activeUser->roleAs('dfm')) {
            $cond = [
                'is_approved_by_manager' => 1,
                'is_approved_by_dfm' => 0
            ];
        } else if ($activeUser->roleAs('fm')) {
            $cond = [
                'is_approved_by_manager' => 1,
                'is_approved_by_dfm' => 1,
                'is_approved_by_fm' => 0
            ];
        } else if ($activeUser->roleAs('presdir')) {
            $cond = [
                'is_approved_by_manager' => 1,
                'is_approved_by_dfm' => 1,
                'is_approved_by_fm' => 1,
                'is_approved_by_presdir' => 0
            ];
        } else {
            $cond = [
                'is_approved_by_manager' => 0,
                'is_approved_by_dfm' => 0,
                'is_approved_by_fm' => 0,
                'is_approved_by_presdir' => 0,
                'is_approved_by_fa' => 0
            ];
        }

        $cond['is_rejected'] = 0;
        $cond['is_canceled'] = 0;

        $params = [
            'data' => PurchaseRequisition::find()->where($cond)->all()
        ];
        return $this->render('pending', $params);
    }

    public function actionBulkAction()
    {
        $action = post_data('action');
        $listId = explode(',', post_data('list_id'));
        foreach ($listId as $id) {
            if ($action == 'approve') {
                $this->actionApprovePr($id);
            } else {
                $this->actionRejectPr($id);
            }
        }

        return alert_success('Data berhasil diproses!') .
            "<script> reload(1500); </script>";
    }

    public function actionUsageDetail()
    {
        $id = intval(post_data('id'));
        $data = PurchaseRequisitionDetail::firstOrFail($id);
        $purchaseRequisition = $data->purchaseRequisition;

        if ($purchaseRequisition->is_direct_purchase) {
            $result = [];
            foreach ($purchaseRequisition->dpPrDetail as $item) {
                $dp = $item->directPurchase;
                if ($dp->is_rejected || $dp->is_canceled || !$dp->is_approved_by_lp_manager) {
                    continue;
                }

                $actual = DpRealizationActual::find()->where([
                    'dp_pr_detail_id' => $dp->id,
                    'goods_service_id' => $data->goods_service_id
                ])->count();

                if ($actual > 0) {
                    $result[] = $dp;
                }
            }

            return $this->renderPartial('usage-detil-dp', ['data' => $result]);
        } else {
            $result = [];
            foreach ($purchaseRequisition->poPrDetail as $item) {
                $po = $item->purchaseOrder;
                if ($po->is_rejected || $po->is_canceled || !$po->is_approved_by_lp_manager) {
                    continue;
                }

                $actual = PoRealizationActual::find()->where([
                    'po_pr_detail_id' => $item->id,
                    'goods_service_id' => $data->goods_service_id
                ])->count();

                if ($actual > 0) {
                    $result[] = $po;
                }
            }

            return $this->renderPartial('usage-detil-po', ['data' => $result]);
        }
    }
}

?>
