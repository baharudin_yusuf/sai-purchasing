<?php
namespace app\controllers;

use app\helpers\Auth;
use app\models\master\Department;
use app\models\master\Section;
use app\models\Role;
use app\models\User;
use Yii;

class UserController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Login' => url('user/login'),
        ];
        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'user');

        return [];
    }

    public function actionIndex()
    {
        redirect(url('user/login'));
    }

    public function actionRegister()
    {
        if (!Auth::user()->roleAs('admin')) {
            parent::throw404Error();
        }

        $data['roleOption'] = Role::getOption();
        $data['sectionOption'] = [];
        $data['departmentOption'] = Department::getOption();
        $data['sectionRole'] = Role::findOne(['alias' => 'section']);
        $data['managerRole'] = Role::findOne(['alias' => 'manager']);

        foreach ($data['departmentOption'] as $key => $value) {
            $data['sectionOption'] = Section::getOption($key);
            break;
        }

        return $this->renderPartial('register', $data);
    }

    public function actionGetSection()
    {
        $departmentId = intval(post_data('id'));
        $option = Section::getOption($departmentId);

        $result = '';
        foreach ($option as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        return $result;
    }

    public function actionSaveRegistration()
    {
        if (!Auth::user()->roleAs('admin')) {
            parent::throw404Error();
        }

        /* cek email */
        $email = post_data('email');
        $check = User::findOne(['email' => $email]);
        if (!is_null($check)) {
            return alert_danger('<b>Error!</b> Email sudah digunakan!');
        }

        $user = new User();
        $user->name = post_data('name');
        $user->email = post_data('email');
        $user->password = encrypt(post_data('password'));
        $user->role = post_data('role');
        $user->registration_date = date('Y-m-d H:i:s');

        if ($user->save()) {
            if ($user->roleAs('admin|dfm|fm|presdir')) {
                $user->section_id = null;
                $user->department_id = null;
                $user->is_administration = 0;
                $user->is_supervisor = 0;
            } else if ($user->roleAs('manager')) {
                $user->section_id = null;
                $user->department_id = post_data('department_id');
                $user->is_administration = 0;
                $user->is_supervisor = 0;
            } else if ($user->roleAs('section')) {
                $user->section_id = post_data('section_id');
                $user->department_id = post_data('department_id');

                $roleDetail = post_data('role_detail');
                if ($roleDetail == 'administration') {
                    $user->is_administration = 1;
                    $user->is_supervisor = 0;
                } else {
                    $user->is_administration = 0;
                    $user->is_supervisor = 1;
                }
            }

            $user->save();
            $msg = alert_success('Registrasi user baru berhasil!') .
                "<script> closeModal(1000); reload(1000); </script>";
        } else {
            $msg = alert_danger('<b>Error!</b> Data gagal diubah!');
        }

        return $msg;
    }

    public function actionLogout()
    {
        Auth::logout();
        Yii::$app->session->destroy();
        redirect(url());
    }

    public function actionModify()
    {
        if (!Auth::user()->roleAs('admin')) {
            parent::throw404Error();
        }

        $id = intval(post_data('id'));
        $user = User::findOne($id);
        if (is_null($user)) {
            parent::throw404Error();
        }

        $data['sectionRole'] = Role::findOne(['alias' => 'section']);
        $data['managerRole'] = Role::findOne(['alias' => 'manager']);
        $data['user'] = $user;
        $data['roleOption'] = Role::getOption();
        $data['departmentOption'] = Department::getOption();
        $data['sectionOption'] = Section::getOption($user->department_id);
        if (is_null($user->department_id)) {
            foreach ($data['departmentOption'] as $key => $value) {
                $data['sectionOption'] = Section::getOption($key);
                break;
            }
        }

        return $this->renderPartial('modify', $data);
    }

    public function actionUpdateData()
    {
        if (!Auth::user()->roleAs('admin')) {
            parent::throw404Error();
        }

        $id = intval(post_data('id'));
        $user = User::findOne($id);
        if (is_null($user)) {
            parent::throw404Error();
        }

        /* cek email */
        $email = post_data('email');
        $check = User::findOne(['email' => $email]);
        if (!is_null($check) && $check->id != $user->id) {
            return alert_danger('<b>Error!</b> Email sudah digunakan!');
        }

        $user->name = post_data('name');
        $user->email = post_data('email');
        $user->role = post_data('role');

        if ($user->save()) {
            if ($user->roleAs('admin|dfm|fm|presdir')) {
                $user->section_id = null;
                $user->department_id = null;
                $user->is_administration = 0;
                $user->is_supervisor = 0;
            } else if ($user->roleAs('manager')) {
                $user->section_id = null;
                $user->department_id = post_data('department_id');
                $user->is_administration = 0;
                $user->is_supervisor = 0;
            } else if ($user->roleAs('section')) {
                $user->section_id = post_data('section_id');
                $user->department_id = post_data('department_id');

                $roleDetail = post_data('role_detail');
                if ($roleDetail == 'administration') {
                    $user->is_administration = 1;
                    $user->is_supervisor = 0;
                } else {
                    $user->is_administration = 0;
                    $user->is_supervisor = 1;
                }
            }

            $user->save();
            $msg = alert_success('Data berhasil diubah!') .
                "<script> closeModal(1000); reload(1000); </script>";
        } else {
            $msg = alert_danger('<b>Error!</b> Data gagal diubah!');
        }

        return $msg;
    }

    public function actionChangePassword()
    {
        return $this->renderPartial('change-password');
    }

    public function actionUpdatePassword()
    {
        $userdata = $this::getUserData();
        $userid = $userdata['id'];
        $data = $_POST;

        $User = User::findOne($userid);
        $User->password = encrypt($data['password1']);
        $User->save();

        $msg = alert_success('Password berhasil diubah!');
        $msg .= "<script> reload(1000); </script>";

        return $msg;
    }

    public function actionLoginAsUser()
    {
        $id = intval(post_data('id'));
        $user = User::findOne($id);
        if (is_null($user)) {
            parent::throw404Error();
        }

        Auth::login($user->id);
        return alert_success('Login sebagai user berhasil') .
            "<script> reload(1500); </script>";
    }
}

?>
