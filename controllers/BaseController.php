<?php

namespace app\controllers;
date_default_timezone_set("Asia/Jakarta");

use app\controllers\po\MainController as POMain;
use app\helpers\Auth;
use app\models as Models;
use app\models\master\Currency;
use app\models\master\CurrencyDetail;
use app\models\master\Period;
use app\models\NotificationMessage;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class BaseController extends Controller
{
    public $layout;
    public $enableCsrfValidation;
    public $include_rejected, $only_rejected;
    public $include_canceled, $only_canceled;

    public function behaviors()
    {
        $this->enableCsrfValidation = false;

        if (!Auth::check()) {
            redirect(url('login'));
        }

        self::setRole();
        self::setIncludeRejectForm();
        self::initNotificationMessage();
        self::initializeMessage();

        POMain::checkDueDate();
        self::initBaseCurrency();

        return [];
    }

    public function actions()
    {
        return ['error' => ['class' => 'yii\web\ErrorAction']];
    }

    public static function getSectionCode($section)
    {
        switch ($section) {
            case 'LP':
                return 'LP';

            default:
                return null;
        }
    }

    protected static function setWarningPane($warningMsg)
    {
        Yii::$app->params['warning'][] = $warningMsg;
    }

    private function initNotificationMessage()
    {
        $NotifMsg = NotificationMessage::find()->all();

        foreach ($NotifMsg as $list) {
            if (!defined($list->key)) {
                define($list->key, $list->message);
            }
        }
    }

    private function initBaseCurrency()
    {
        $Period = Period::find()
            ->where(['is_active' => 1])
            ->one();

        if (is_null($Period)) {
            self::setWarningPane("Periode aktif belum diatur! Silahkan mengatur periode aktif saat ini!");
        } else {
            $period_id = $Period->id;
            define('ACTIVE_PERIOD_ID', $period_id);
            $currencyDetail = CurrencyDetail::find()
                ->where(['period_id' => $period_id, 'is_base_currency' => 1])
                ->one();

            if (is_null($currencyDetail)) {
                self::setWarningPane("Base currency untuk periode aktif belum diatur! Silahkan mengatur base currency pada periode aktif terlebih dahulu!");
            } else {
                $currency_id = $currencyDetail->currency_id;
                $Currency = Currency::findOne($currency_id);

                define('BASE_CURRENCY_ID', $Currency->id);
                define('BASE_CURRENCY_NAME', $Currency->name);
                define('BASE_CURRENCY_RATE', $Currency->current_rate);
            }
        }
    }

    public static function getConvertedPrice($amount, $currency_id)
    {
        $currencyDetail = CurrencyDetail::find()
            ->where(['period_id' => ACTIVE_PERIOD_ID, 'currency_id' => $currency_id])
            ->one();

        if (BASE_CURRENCY_ID == $currency_id) {
            $c_amount = $amount;
        } else {
            $current_rate = $currencyDetail->current_rate;
            if ($current_rate <= 0) {
                $c_amount = 0;
            } else {
                $c_amount = $amount / $current_rate;
            }
        }

        return $c_amount;
    }

    protected static function allowCreateNewData()
    {
        $period = Period::findOne(['is_active' => 1]);

        if (is_null($period)) {
            return false;
        } else {
            $currencyDetail = CurrencyDetail::findOne(['period_id' => $period->id, 'is_base_currency' => 1]);
            if (is_null($currencyDetail)) {
                return false;
            } else {
                return true;
            }
        }
    }

    protected function ajaxMethod()
    {
        $this->layout = 'blank';
        $this->enableCsrfValidation = false;
    }

    protected function role()
    {
        $role = Yii::$app->params['role'];
        return $role;
    }

    public static function getUserData()
    {
        $user = Auth::user();
        return $user->getDataArray();
    }

    private function setRole()
    {
        $role = self::role();

        foreach ($role as $key => $value) {
            define(strtoupper($key), $value['code']);
        }
    }

    protected function processMenu($menu = [], $menuID = '')
    {
        $sidebarMenu = "<ul class='cssmenu sidebar-menu' rel='$menuID'>";

        foreach ($menu as $title => $url) {
            if (is_array($url)) {
                $sidebarMenu .= "
				<li class='has-sub'>
				<a title='$title'><span>$title</span></a>
				" . $this->processMenu($url) . '
				</li>';
            } else $sidebarMenu .= "<li><a href='$url' title='$title'><span>$title</span></a>";
        }

        $sidebarMenu .= "</ul>";

        return $sidebarMenu;
    }

    protected function getMonthOption()
    {
        for ($i = 1; $i <= 12; $i++) {
            $month[$i] = date('F', strtotime("2000-$i-01"));
        }

        return $month;
    }

    private function setIncludeRejectForm()
    {
        if (is_null(get_session('include_rejected'))) set_session('include_rejected', false);
        if (is_null(get_session('only_rejected'))) set_session('only_rejected', false);
        if (is_null(get_session('include_canceled'))) set_session('include_canceled', false);
        if (is_null(get_session('only_canceled'))) set_session('only_canceled', false);

        $include_rejected = get_session('include_rejected');
        $only_rejected = get_session('only_rejected');
        $include_canceled = get_session('include_canceled');
        $only_canceled = get_session('only_canceled');

        $this->include_rejected = $include_rejected;
        $this->only_rejected = $only_rejected;
        $this->include_canceled = $include_canceled;
        $this->only_canceled = $only_canceled;

        $form = "
		<hr style='margin:10px 0;' class='include-exclude-rejected-panel'>
		<form class='form-inline include-exclude-rejected-panel' role='form'>
		<div class='form-group'>
			<div class='checkbox'>
				<label><input onclick='include_reject()' type='checkbox' class='include-reject-checkbox'> Include Rejected Data</label>
			</div>
		</div>
		&nbsp;|&nbsp;
		<div class='form-group'>
			<div class='checkbox'>
				<label><input onclick='only_reject()' type='checkbox' class='only-reject-checkbox'> Only Rejected Data</label>
			</div>
		</div>
		&nbsp;|&nbsp;
		<div class='form-group'>
			<div class='checkbox'>
				<label><input onclick='include_cancel()' type='checkbox' class='include-cancel-checkbox'> Include Canceled Data</label>
			</div>
		</div>
		&nbsp;|&nbsp;
		<div class='form-group'>
			<div class='checkbox'>
				<label><input onclick='only_cancel()' type='checkbox' class='only-cancel-checkbox'> Only Canceled Data</label>
			</div>
		</div>
		</form>

		<script>
		" . ($include_rejected ? "$('.include-reject-checkbox').prop('checked', true);" : '') . "
		" . ($only_rejected ? "$('.only-reject-checkbox').prop('checked', true);" : '') . "
		" . ($include_canceled ? "$('.include-cancel-checkbox').prop('checked', true);" : '') . "
		" . ($only_canceled ? "$('.only-cancel-checkbox').prop('checked', true);" : '') . "
		</script>
		";

        Yii::$app->params['includeRejectedForm'] = $form;
    }

    public function actionRemoveTemp($filename)
    {
        @unlink(TEMP_DIR . $filename);
    }

    protected function requirementCheck()
    {
        /* cek base currency */
        $currency = Models\master\Currency::find()->where(['is_base_currency' => 1])->one();
        if (is_null($currency)) {
            redirect(url('site/requirement'));
        }

        /* cek base periode */
        $activePeriod = Models\master\Period::find()->where(['is_active' => 1])->one();
        if (is_null($activePeriod)) {
            redirect(url('site/requirement'));
        }

        $futurePeriod = Models\master\Period::find()->where(['is_future' => 1])->one();
        if (is_null($futurePeriod)) {
            redirect(url('site/requirement'));
        }

        /* cek currency detail */
        $activeCurrencyValue = Models\master\CurrencyDetail::find()->where(['period_id' => $activePeriod->id])->all();
        if (count($activeCurrencyValue) == 0) {
            redirect(url('site/requirement'));
        }

        $futureCurrencyValue = Models\master\CurrencyDetail::find()->where(['period_id' => $futurePeriod->id])->all();
        if (count($futureCurrencyValue) == 0) {
            redirect(url('site/requirement'));
        }
    }

    private static function initializeMessage()
    {
        $data = [
            'PROPOSAL_REQUEST_APPROVAL' => 'Pengajuan Budget Proposal oleh section {section_name} dengan id {budget_proposal_id} memerlukan konfirmasi',
            'PROPOSAL_APPROVED' => 'Pengajuan Budget Proposal {budget_proposal_id} telah diterima oleh manager {department_name}',
            'PROPOSAL_REJECTED' => 'Pengajuan Budget Proposal {budget_proposal_id} telah ditolak oleh manager {department_name}',
            'PROPOSAL_ITEM_REJECTED' => 'Pengajuan item {good_service} pada Budget Proposal oleh dengan id {budget_proposal_id} telah ditolak oleh manager {department_name}',
            'TRANSFER_REQUEST_APPROVAL' => 'Pengajuan Budget Transfer {budget_transfer_id} oleh section {section_name} memerlukan konfirmasi',
            'TRANSFER_APPROVED' => 'Pengajuan Budget Transfer {budget_transfer_id} telah diterima oleh manager {department_name}',
            'TRANSFER_APPROVED_BY_DEST' => 'Anda menerima Budget Transfer {budget_transfer_id} dari section {section_name}',
            'TRANSFER_REJECTED' => 'Pengajuan Budget Transfer {budget_transfer_id} telah ditolak oleh manager {department_name}',
            'ADDITIONAL_REQUEST_APPROVAL' => 'Pengajuan Additional Budget {budget_additional_id} oleh section {section_name} memerlukan konfirmasi',
            'ADDITIONAL_APPROVED' => 'Pengajuan Additional Budget {budget_additional_id} telah diterima oleh manager {department_name}',
            'ADDITIONAL_REJECTED' => 'Pengajuan Additional Budget {budget_additional_id} telah ditolak oleh manager {department_name}',

            'PR_REQUEST_APPROVAL' => 'Pengajuan Purchase Requisition {purchase_requisition_id} oleh section {section_name} memerlukan konfirmasi',
            'PR_APPROVED' => 'Pengajuan Purchase Requisition {purchase_requisition_id} telah diterima oleh manager {department_name}',
            'PR_REJECTED' => 'Pengajuan Purchase Requisition {purchase_requisition_id} telah ditolak oleh manager {department_name}',
            'PR_ITEM_REJECTED' => 'Pengajuan item {good_service} pada Purchase Requisition {purchase_requisition_id} telah ditolak oleh manager {department_name}',
            'PR_REVISION_REQUEST_APPROVAL' => 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} oleh section {section_name} memerlukan konfirmasi',
            'PR_REVISION_APPROVED' => 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} telah diterima oleh departemen {department_name}',
            'PR_REVISION_REJECTED' => 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} telah ditolak oleh departemen {department_name}',
            'PR_REVISION_CANCELED' => 'Pengajuan Revisi {pr_revision_id} pada Purchase Requisition {purchase_requisition_id} telah dibatalkan oleh section {section_name}',

            'PO_REQUEST_APPROVAL' => 'Pengajuan Purchase Order {purchase_order_id} oleh section {section_name} memerlukan konfirmasi',
            'PO_APPROVED' => 'Pengajuan Purchase Order {purchase_order_id} telah diterima oleh manager {department_name}',
            'PO_REJECTED' => 'Pengajuan Purchase Order {purchase_order_id} telah ditolak oleh manager {department_name}',
            'PO_INVOICE_APPROVAL' => 'Pengajuan Invoice {invoice_id} pada Purchase Order {purchase_order_id} memerlukan konfirmasi',
            'PO_INVOICE_APPROVED' => 'Pengajuan Invoice {invoice_id} pada Purchase Order {purchase_order_id} telah diterima oleh manager {department_name}',
            'PO_INVOICE_REJECTED' => 'Pengajuan Invoice {invoice_id} pada Purchase Order {purchase_order_id} telah ditolak oleh manager {department_name}',
            'PO_REVISION_REQUEST_APPROVAL' => 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} oleh section {section_name} memerlukan konfirmasi',
            'PO_REVISION_APPROVED' => 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} telah diterima oleh departemen {department_name}',
            'PO_REVISION_REJECTED' => 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} telah ditolak oleh departemen {department_name}',
            'PO_REVISION_CANCELED' => 'Pengajuan Revisi {po_revision_id} pada Purchase Order {purchase_order_id} telah dibatalkan oleh section {section_name}',
            'PO_RECEIVING_READY' => 'Purchase Order dengan kode {purchase_order_id} telah siap untuk diterima',
            'PO_DELIVERING_READY' => 'Penerimaan item dari Purchase Order dengan kode {purchase_order_id} telah diperbarui oleh section {section_name}',

            'DP_REQUEST_APPROVAL' => 'Pengajuan Direct Purchase {direct_purchase_id} oleh section {section_name} memerlukan konfirmasi',
            'DP_APPROVED' => 'Pengajuan Direct Purchase {direct_purchase_id} telah diterima oleh manager {department_name}',
            'DP_REJECTED' => 'Pengajuan Direct Purchase {direct_purchase_id} telah ditolak oleh manager {department_name}',
            'DP_VP_APPROVAL' => 'Pengajuan Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} memerlukan konfirmasi',
            'DP_VP_APPROVED' => 'Pengajuan Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah diterima oleh manager {department_name}',
            'DP_VP_REJECTED' => 'Pengajuan Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah ditolak oleh manager {department_name}',
            'DP_VP_READY_FOR_FA' => 'Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah disetujui oleh {department_name}. Silahkan melakukan pembayaran',
            'DP_READY_FOR_ACTUAL' => 'Voucher Paying {vp_id} pada Direct Purchase {direct_purchase_id} telah dibayar oleh FA. Silahkan mengupdate harga aktual',
            'DP_VR_READY_FOR_FA' => 'Voucher Receiving {vr_id} pada Direct Purchase {direct_purchase_id} telah dikirimkan oleh section {section_name}',
            'PO_CANCELED' => 'Pengajuan Purchase Order {purchase_order_id} telah dibatalkan oleh section {section_name}',
            'DP_CANCELED' => 'Pengajuan Direct Purchase {direct_purchase_id} telah dibatalkan oleh section {section_name}',
            'DP_RECEIVING_READY' => 'Direct purchase dengan kode {direct_purchase_id} telah siap untuk diterima',
            'DP_DELIVERING_READY' => 'Penerimaan item dari Direct purchase dengan kode {direct_purchase_id} telah diperbarui oleh section {section_name}',
        ];

        $isSet = get_session('MSG_INITIALIZED');
        if (!$isSet) {
            foreach ($data as $key => $message) {
                $notif = NotificationMessage::find()->where(['key' => $key])->one();
                if (is_null($notif)) {
                    $notif = new NotificationMessage();
                    $notif->key = $key;
                }
                $notif->message = $message;
                $notif->save();
            }
            set_session('MSG_INITIALIZED', true);
        }
    }

    public static function getNotifMessage($key)
    {
        $notif = NotificationMessage::find()->where(['key' => $key])->one();
        if (is_null($notif)) {
            return '';
        } else {
            return $notif->message;
        }
    }

    protected static function throw404Error()
    {
        if (Yii::$app->request->isPost) {
            die("<div class='alert alert-danger center'>Terjadi kesalahan! Halaman tidak ditemukan!</div>");
        } else {
            throw new NotFoundHttpException();
        }
    }

    public static function getTerbilang($num)
    {
        $bil = ['', 'satu', 'dua', 'tiga', 'empat', 'lima', 'enam', 'tujuh', 'delapan', 'sembilan', 'sepuluh', 'sebelas'];

        if ($num < 12)
            return ' ' . $bil[$num];
        else if ($num < 20)
            return self::getTerbilang($num - 10) . 'belas';
        else if ($num < 100)
            return self::getTerbilang($num / 10) . ' puluh' . self::getTerbilang($num % 10);
        else if ($num < 200)
            return ' seratus' . self::getTerbilang($num - 100);
        else if ($num < 1000)
            return self::getTerbilang($num / 100) . ' ratus' . self::getTerbilang($num % 100);
        else if ($num < 2000)
            return ' seribu' . self::getTerbilang($num - 1000);
        else if ($num < 1000000)
            return self::getTerbilang($num / 1000) . ' ribu' . self::getTerbilang($num % 1000);
        else if ($num < 1000000000)
            return self::getTerbilang($num / 1000000) . ' juta' . self::getTerbilang($num % 1000000);
        else if ($num < 1000000000000)
            return self::getTerbilang($num / 1000000000) . ' miliar' . self::getTerbilang($num % 1000000000);
        else if ($num < 1000000000000000)
            return self::getTerbilang($num / 1000000000000) . ' triliun' . self::getTerbilang($num % 1000000000000);
    }
}
