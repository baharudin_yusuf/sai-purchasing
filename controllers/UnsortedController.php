<?php
namespace app\controllers;

use Yii;

class UnsortedController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Registration' => url('user/register'),
            'Login' => url('user/login'),
        ];
        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'unsorted');

        return [];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }
}

?>
