<?php
namespace app\controllers\proposal;

use app\controllers\NotificationController as Notification;
use app\controllers\proposal\ProposalConstraintController as ProposalConstraint;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\BudgetProposalDetail;
use app\models\master\Department;

class DetailController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionRejectSubmission($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        $proposalDetail = BudgetProposalDetail::firstOrFail($id);
        $proposalDetail->is_rejected = 1;

        if ($proposalDetail->save()) {
            $budgetProposal = $proposalDetail->budgetProposal;
            $goodsService = $proposalDetail->goodsService;

            $notifMsg = Notificatixn::render('PROPOSAL_ITEM_REJECTED', [
                'budget_proposal_id' => $budgetProposal->code,
                'department_name' => $activeUser->department->name,
                'good_service' => $goodsService->name,
            ]);

            $notif_data = [
                'category' => 'proposal',
                'type' => 'danger',
                'target_id' => $budgetProposal->id,
                'title' => $notifMsg
            ];
            Notification::sendNotification($notif_data);

            $msg = "alert('Item berhasil ditolak!'); reload();";
        } else {
            $msg = "alert('Error! Item gagal ditolak!');";
        }

        return "<script>$msg</script>";
    }

    public function actionApproveSubmission($id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        $proposalDetail = BudgetProposalDetail::firstOrFail($id);
        $allowManage = ProposalConstraint::allowManageProposalDetail($proposalDetail);

        if ($allowManage && $activeUser->roleAs('manager')) {
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $proposalDetail->is_approved_by_fa = 1;
                $proposalDetail->approved_by_fa_time = date('Y-m-d H:i:s');
            } else {
                $proposalDetail->is_approved_by_manager = 1;
                $proposalDetail->approved_by_manager_time = date('Y-m-d H:i:s');
            }

            if ($proposalDetail->save()) {
                $msg = "alert('Item berhasil diterima!'); reload();";
            } else {
                $msg = "alert('Error! Item gagal diterima!');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk menerima pengajuan item!');";
        }

        return "<script> $msg </script>";
    }

    public function actionCancelSubmission()
    {
        $id = intval(post_data('id'));
        $proposalDetail = BudgetProposalDetail::firstOrFail($id);

        if (ProposalConstraint::allowCancelProposal($proposalDetail->budgetProposal)) {
            $proposalDetail->is_canceled = 1;

            if ($proposalDetail->save()) {
                $msg = "
				alert('Pengajuan item dari Budget Proposal berhasil dibatalkan');
				location.reload();";
            } else {
                $msg = "alert('Error! Pengajuan item dari Budget Proposal gagal dibatalkan');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan data');";
        }

        return "<script> $msg </script>";
    }

    public function actionBulkAction()
    {
        $action = post_data('action');
        $msg = '';

        $list_id = explode(',', post_data('list_id'));
        foreach ($list_id as $id) {
            if ($action == 'approve') {
                $msg = self::actionApproveSubmission($id);
            } else {
                $msg = self::actionRejectSubmission($id);
            }
        }

        return $msg;
    }
}

?>
