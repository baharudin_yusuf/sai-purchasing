<?php
namespace app\controllers\proposal;

use app\controllers\NotificationController as Notification;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\helpers\PriceConverter;
use app\models\BudgetProposal;
use app\models\BudgetProposalDetail;
use app\models\master\Budget;
use app\models\master\BudgetPeriod;
use app\models\master\Carline;
use app\models\master\Currency;
use app\models\master\GoodsService;
use app\models\master\Period;

class CreateController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        if (!Auth::user()->roleAs('section')) {
            parent::throw404Error();
        }
        if (!parent::allowCreateNewData()) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        // budget period
        $budgetPeriodOption = [];
        $period = Period::findOne(['is_future' => 1]);
        if (!is_null($period)) {
            $budgetPeriodOption = BudgetPeriod::getOption($period->id);
        }

        // budget option
        $budgetOption = [];
        if (!is_null($period)) {
            foreach ($period->budgetLimit as $budgetLimit) {
                $budget = $budgetLimit->budget;
                $budgetOption[$budget->id] = $budget->display_name;
            }
        }

        $params = [
            'budgetPeriodOption' => $budgetPeriodOption,
            'budgetOption' => $budgetOption,
            'activeUser' => Auth::user(),
            'sessionKey' => random_string(),
            'baseCurrency' => Currency::getActiveCurrency()
        ];
        return $this->render('main', $params);
    }

    public function actionAddDetail()
    {
        $budgetId = intval(post_data('budget_id'));
        $budget = Budget::findOne($budgetId);
        if (is_null($budget)) {
            return alert_danger('Terjadi kesalahan! Data budget tidak tersedia');
        }

        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $gsExceptId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsExceptId[] = $gsId;
        }

        // GET GOODS SERVICE OPTION
        $goodsService = GoodsService::find()
            ->where(['budget_id' => $budgetId])
            ->andWhere(['NOT IN', 'id', $gsExceptId])
            ->all();

        $goodsServiceOption = [];
        foreach ($goodsService as $item) {
            $goodsServiceOption[$item->id] = $item->name;
        }

        $params = [
            'budget' => $budget,
            'goodsServiceOption' => $goodsServiceOption,
            'carlineOption' => Carline::getOption(),
            'sessionKey' => $sessionKey
        ];
        return $this->renderPartial('add-detail', $params);
    }

    public function actionClearDetailData()
    {
        $sessionKey = post_data('session_key');
        set_session($sessionKey, []);
        return "<script> reloadBudgetProposalDetail(); </script>";
    }

    public function actionReloadProposalDetail()
    {
        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $gsAddedId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsAddedId[] = $gsId;
        }

        $baseCurrency = Currency::getActiveCurrency();

        $result = [];
        $total = 0;
        $data = GoodsService::find()->where(['in', 'id', $gsAddedId])->all();
        foreach ($data as $item) {
            $gsData = $addedGoodsService[$item->id];
            $priceEstimation = $item->getPricelistMaxPrice();
            $currency = $item->currency;
            $quantity = $gsData['quantity'];
            $subtotal = $priceEstimation * $quantity;
            $cPriceEstimation = PriceConverter::convert($priceEstimation, $currency->id);
            $cSubtotal = $cPriceEstimation * $quantity;
            $total += $cSubtotal;
            $carline = Carline::findOne($gsData['carline_id']);

            $item->quantity = $gsData['quantity'];
            $item->carline = $carline->name;
            $item->comment = $gsData['comment'];
            $item->price_estimation = $currency->name . ' ' . currency_format($priceEstimation);
            $item->sub_total = $currency->name . ' ' . currency_format($subtotal);
            $item->cvt_price_estimation = $baseCurrency->name . ' ' . currency_format($cPriceEstimation);
            $item->cvt_sub_total = $baseCurrency->name . ' ' . currency_format($cSubtotal);

            $result[] = $item;
        }

        $params = [
            'data' => $result,
            'displayTotal' => $baseCurrency->name . ' ' . currency_format($total),
            'sessionKey' => $sessionKey
        ];
        return $this->renderPartial('data-detail', $params);
    }

    public function actionGetGoodsServiceData()
    {
        $goodsServiceId = intval(post_data('goods_service_id'));
        $goodsService = GoodsService::findOne($goodsServiceId);
        if (is_null($goodsService)) {
            return alert_danger('Terjadi kesalahan! Data goods/service tidak tersedia!');
        }

        $currency = $goodsService->currency->name;
        $category = $goodsService->is_po ? 'Purchase Order' : 'Direct Purchase';

        // GET MAX PRICE IN PRICELIST
        $priceEstimation = $goodsService->getPricelistMaxPrice();
        $displaypriceEstimation = currency_format($priceEstimation);

        if ($priceEstimation == 0) {
            $alertMsg = alert_danger('Terjadi kesalahan! Goods/Service belum terdaftar dalam pricelist');
            $scriptMsg = "$('#btn-save-proposal-detail').prop('disabled', true); ";
        } else {
            $alertMsg = '';
            $scriptMsg = "$('#btn-save-proposal-detail').prop('disabled', false); ";
        }

        return "
		$alertMsg
		<script>
            $scriptMsg
            $('#td-currency').html('$currency');
            $('#td-category').html('$category');
            $('#td-price-estimation').html('$displaypriceEstimation');
		</script>";
    }

    public function actionSaveDetail()
    {
        $budgetId = intval(post_data('budget_id'));
        $budget = Budget::firstOrFail($budgetId);

        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        // validasi kategori po atau dp
        $gsId = intval(post_data('goods_service_id'));
        $newGS = GoodsService::firstOrFail($gsId);
        $newGSCategory = $newGS->is_po ? 'Purchase Order' : 'Direct Purchase';

        $gsAddedId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsAddedId[] = $gsId;
        }
        $goodsServiceAdded = GoodsService::find()->where(['in', 'id', $gsAddedId])->all();
        foreach ($goodsServiceAdded as $gsa) {
            $gsaCategory = $gsa->is_po ? 'Purchase Order' : 'Direct Purchase';
            if ($gsaCategory != $newGSCategory) {
                return alert_danger("Terjadi kesalahan! Item yang anda pilih memiliki kategori '$newGSCategory', sedangkan kategori item pertama yang anda pilih adalah '$gsaCategory'.<br>
				Silahkan memilih item lain dengan kategori '$gsaCategory', atau kosongkan terlebih dahulu item yang telah anda pilih sebelumnya.");
            }
        }

        // tambahkan gs baru ke session
        $carlineId = post_data('carline_id');
        $carline = Carline::firstOrFail($carlineId);
        $addedGoodsService[$newGS->id] = [
            'quantity' => post_data('quantity'),
            'carline_id' => $carline->id,
            'comment' => post_data('comment'),
        ];
        set_session($sessionKey, $addedGoodsService);

        // validasi batas amount budget
        $budgetAvailableAmount = $budget->getAvailableAmount();
        $submissionAmount = $this->getSubmissionAmount($sessionKey);
        if ($submissionAmount > $budgetAvailableAmount) {
            $currency = Currency::getActiveCurrency();
            $budgetAvailableAmount = $currency->name . ' ' . currency_format($budgetAvailableAmount);
            $submissionAmount = $currency->name . ' ' . currency_format($submissionAmount);

            // hapus data yang baru ditambakan
            unset($addedGoodsService[$newGS->id]);
            set_session($sessionKey, $addedGoodsService);

            return alert_danger("Terjadi kesalahan! Pengajuan budget proposal anda melebihi batas amount budget!<br>Batas maksimal budget : $budgetAvailableAmount, pengajuan anda : $submissionAmount");
        }

        return alert_success('Item goods/service berhasil ditambahkan!') .
            "<script> reloadBudgetProposalDetail(); closeModal(1500); </script>";
    }

    public function actionRemoveDetail()
    {
        $id = intval(post_data('id'));
        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        if (isset($addedGoodsService[$id])) {
            unset($addedGoodsService[$id]);
            set_session($sessionKey, $addedGoodsService);
        }

        return alert_success('Data berhasil dihapus!') .
            "<script> reloadBudgetProposalDetail(); closeModal(1500); </script>";
    }

    private function getSubmissionAmount($sessionKey)
    {
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];

        $gsAddedId = [];
        foreach ($addedGoodsService as $gsId => $gsData) {
            $gsAddedId[] = $gsId;
        }

        $total = 0;
        $data = GoodsService::find()->where(['in', 'id', $gsAddedId])->all();
        foreach ($data as $item) {
            $priceEstimation = $item->getPricelistMaxPrice();
            $cPriceEstimation = PriceConverter::convert($priceEstimation, $item->currency_id);
            $total += ($cPriceEstimation * $addedGoodsService[$item->id]['quantity']);
        }

        return $total;
    }

    public function actionSaveData()
    {
        $sessionKey = post_data('session_key');
        $addedGoodsService = get_session($sessionKey, []);
        $addedGoodsService = is_array($addedGoodsService) ? $addedGoodsService : [];
        $activeUser = Auth::user();

        // validasi batas budget
        $budgetId = intval(post_data('budget_id'));
        $budget = Budget::firstOrFail($budgetId);
        $budgetAvailableAmount = $budget->getAvailableAmount();
        $submissionAmount = $this->getSubmissionAmount($sessionKey);
        $inputAmount = floatval(post_data('amount'));
        $inputAmountMax = max([$submissionAmount, $inputAmount]);
        $currency = Currency::getActiveCurrency();

        if ($submissionAmount > $inputAmount) {
            $inputAmount = $currency->name . ' ' . currency_format($inputAmount);
            $submissionAmount = $currency->name . ' ' . currency_format($submissionAmount);

            return alert_danger("Terjadi kesalahan! Pengajuan budget proposal anda melebihi batas amount yang anda masukkan!<br>Batas amount anda : $inputAmount, pengajuan anda : $submissionAmount");
        } else if ($inputAmountMax > $budgetAvailableAmount) {
            $budgetAvailableAmount = $currency->name . ' ' . currency_format($budgetAvailableAmount);
            $inputAmountMax = $currency->name . ' ' . currency_format($inputAmountMax);

            return alert_danger("Terjadi kesalahan! Pengajuan budget proposal anda melebihi batas amount budget!<br>Batas maksimal budget : $budgetAvailableAmount, pengajuan anda : $inputAmountMax");
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try {
            $data = new BudgetProposal();

            $data->date = date('Y-m-d H:i:s');
            $data->budget_period_id = post_data('budget_period_id');
            $data->budget_id = post_data('budget_id');
            $data->amount = post_data('amount');
            $data->comment = post_data('comment');
            $data->amount_used = self::getSubmissionAmount($sessionKey);
            $data->section_id = $activeUser->section_id;

            if ($data->save()) {
                $gsAddedId = [];
                foreach ($addedGoodsService as $gsId => $gsData) {
                    $gsAddedId[] = $gsId;
                }

                $dataDetail = GoodsService::find()->where(['in', 'id', $gsAddedId])->all();
                foreach ($dataDetail as $item) {
                    $gsData = $addedGoodsService[$item->id];

                    $detail = new BudgetProposalDetail();
                    $detail->carline_id = $gsData['carline_id'];
                    $detail->goods_service_id = $item->id;
                    $detail->quantity = $gsData['quantity'];
                    $detail->price_estimation = $item->getPricelistMaxPrice();
                    $detail->comment = $gsData['comment'];
                    $detail->budget_proposal_id = $data->id;
                    $detail->save();
                }

                $transaction->commit();
                remove_session($sessionKey);

                // SEND NOTIFICATION TO MANAGER
                $notifMsg = Notificatixn::render('PROPOSAL_REQUEST_APPROVAL', [
                    'budget_proposal_id' => $data->code,
                    'section_name' => $activeUser->section->name
                ]);

                $notif_data = [
                    'category' => 'proposal',
                    'type' => 'warning',
                    'target_id' => $data->id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notif_data);

                $redirectURL = url('proposal/main/detail/?id=' . $data->id);
                return alert_success('Data Budget Proposal berhasil disimpan!') .
                    "<script> redirectTo('$redirectURL', 1500); </script>";
            } else {
                $transaction->rollBack();
                return alert_danger('Terjadi kesalahan! Data gagal disimpan!');
            }
        } catch (\Exception $exception) {
            log_error($exception);
            $transaction->rollBack();
            return alert_danger('Terjadi kesalahan! Data gagal disimpan!');
        }
    }
}

?>
