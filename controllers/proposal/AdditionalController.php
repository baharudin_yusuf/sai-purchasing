<?php
namespace app\controllers\proposal;

use app\controllers\NotificationController as Notification;
use app\controllers\proposal\AdditionalConstraintController as AdditionalConstraint;
use app\controllers\proposal\ConstraintController as BPConstraint;
use app\helpers\Auth;
use app\models\BudgetAdditional;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\master\Department;
use app\models\master\Section;
use app\models\view\BudgetAdditional as BudgetAdditionalView;
use app\models\view\BudgetFinal as BudgetFinalView;

class AdditionalController extends FinalController
{
    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        $this->layout = 'main';

        return $this->render('main', $data);
    }

    public function actionList()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        if ($this->include_rejected) unset($cond['is_rejected']);
        if ($this->include_canceled) unset($cond['is_canceled']);
        if ($this->only_rejected) $cond['is_rejected'] = 1;
        if ($this->only_canceled) $cond['is_canceled'] = 1;

        $data = BudgetAdditionalView::find()
            ->asArray()
            ->where($cond)
            ->all();

        return $this->render('list', ['data' => $data]);
    }

    public function actionLoadData()
    {
        $this::ajaxMethod();
        $section_id = Auth::user()->section_id;

        $data['data'] = BudgetFinalView::find()
            ->asArray()
            ->where(['section_id' => $section_id])
            ->all();

        return $this->render('data', $data);
    }

    public function actionFormAdditional()
    {
        $this::ajaxMethod();
        $section_name = Auth::user()->section->name;
        $budget_final_id = intval(post_data('budget_final_id'));

        $data['budget_final_id'] = $budget_final_id;
        $data['section_name'] = ucwords(strtolower($section_name));

        /* budget final */
        $final = BudgetFinalView::find()
            ->where(['id' => $budget_final_id])
            ->one();

        $key = FINAL_CODE . str_pad($final->id, 4, '0', STR_PAD_LEFT);
        $section = ucwords(strtolower($final->section));
        $budget = $final->budget_name;
        $data['final'] = "[$key] $budget ($section)";

        return $this->render('form-additional', $data);
    }

    public function actionSaveData()
    {
        $this::ajaxMethod();
        $data = $_POST;

        $additional = BPConstraint::validateAdditional($data);
        if (!$additional['valid']) {
            return alert_danger($additional['msg']);
        } else {
            $budgetAdditional = new BudgetAdditional();

            $budgetAdditional->budget_final_id = $data['budget_final_id'];
            $budgetAdditional->additional_amount = $data['additional_amount'];
            $budgetAdditional->time = date('Y-m-d H:i:s');

            if ($budgetAdditional->save()) {
                $budget_additional_id = $budgetAdditional->id;
                $section_name = Auth::user()->section->name;

                /* SEND NOTIFICATION */
                $notifMsg = parent::getNotifMessage('ADDITIONAL_REQUEST_APPROVAL');
                $notifMsg = str_replace('{budget_additional_id}', ADDITIONAL_CODE . str_pad($budget_additional_id, 4, '0', STR_PAD_LEFT), $notifMsg);
                $notifMsg = str_replace('{section_name}', ucwords(strtolower($section_name)), $notifMsg);

                $notif_data = [
                    'category' => 'additional',
                    'type' => 'warning',
                    'target_id' => $budget_additional_id,
                    'title' => $notifMsg
                ];
                $notif_send = Notification::sendNotification($notif_data);

                $msg = "
				Pengajuan Additional Budget berhasil disimpan!
				<script> closeModal(1000); </script>
				";

                $msg = alert_success($msg);
            } else {
                $msg = alert_danger('<strong>Error!</strong> Pengajuan Additional Budget gagal disimpan!');
            }

            return $msg;
        }
    }

    public function actionPendingSubmission()
    {
        $data = BudgetAdditionalView::find()
            ->asArray()
            ->where(['is_approved_by_fa' => 0, 'is_rejected' => 0])
            ->andWhere(['is_approved_by_fa' => 0, 'is_canceled' => 0])
            ->all();

        return $this->render('pending', ['data' => $data]);
    }

    public function actionDetail($id = null)
    {
        if (post_data('id')) {
            $this::ajaxMethod();
            $id = intval(post_data('id'));
        } else $id = intval(get_data('id'));

        $data = BudgetAdditionalView::find()
            ->asArray()
            ->where(['id' => $id])
            ->one();
        $data['allow_approve'] = BPConstraint::allowApproveAdditional($id);
        $data['allow_cancel'] = AdditionalConstraint::allowCancel($id);

        /* GET PROPOSAL CURRENCY USED */
        $final_id = $data['budget_final_id'];
        $data['current_currency'] = parent::getFinalCurrencyUsed($final_id);

        return $this->render('detail', $data);
    }

    public function actionApproveSubmission($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        if (BPConstraint::allowApproveAdditional($id)) {
            $department_name = $activeUser->department->name;

            $budgetAdditional = BudgetAdditional::findOne($id);
            $budget_final_id = $budgetAdditional->budget_final_id;

            $budgetFinal = BudgetFinal::findOne($budget_final_id);
            $budget_proposal_id = $budgetFinal->budget_proposal_id;

            $BudgetProposal = BudgetProposal::findOne($budget_proposal_id);
            $section_id = $BudgetProposal->section_id;
            $Section = Section::findOne($section_id);
            $section_name = $Section->name;

            $approved_by_fa = false;
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $budgetAdditional->is_approved_by_fa = 1;
                $budgetAdditional->approved_by_fa_time = date('Y-m-d H:i:s');

                $approved_by_fa = true;
            } else {
                $budgetAdditional->is_approved_by_manager = 1;
                $budgetAdditional->approved_by_manager_time = date('Y-m-d H:i:s');
            }

            if ($budgetAdditional->save()) {
                if (!$approved_by_fa) {
                    /* SEND NOTIFICATION TO FA */
                    $notifMsg = parent::getNotifMessage('ADDITIONAL_REQUEST_APPROVAL');
                    $notifMsg = str_replace('{section_name}', ucwords(strtolower($section_name)), $notifMsg);
                    $notifMsg = str_replace('{budget_additional_id}', ADDITIONAL_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);

                    $notif_data = [
                        'category' => 'additional',
                        'type' => 'warning',
                        'target_id' => $id,
                        'title' => $notifMsg
                    ];
                    Notification::sendNotification($notif_data);
                }

                /* SEND NOTIFICATION TO SECTION */
                $notifMsg = parent::getNotifMessage('ADDITIONAL_APPROVED');
                $notifMsg = str_replace('{budget_additional_id}', ADDITIONAL_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);
                $notifMsg = str_replace('{department_name}', $department_name, $notifMsg);

                $notif_data = [
                    'category' => 'additional',
                    'type' => 'success',
                    'target_id' => $id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notif_data);

                $msg = "alert('Pengajuan Additional Budget berhasil diterima!'); reload();";
            } else {
                $msg = "alert('Error! Pengajuan Additional Budget gagal diterima!');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!');";
        }

        return "<script> $msg </script>";
    }

    public function actionRejectSubmission($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        $budgetAdditional = BudgetAdditional::findOne($id);
        $budgetAdditional->is_rejected = 1;

        if ($budgetAdditional->save()) {
            $department_name = $activeUser->department->name;

            /* SEND NOTIF TO SECTION */
            $notifMsg = parent::getNotifMessage('ADDITIONAL_REJECTED');
            $notifMsg = str_replace('{budget_additional_id}', ADDITIONAL_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);
            $notifMsg = str_replace('{department_name}', $department_name, $notifMsg);

            $notif_data = [
                'category' => 'additional',
                'type' => 'danger',
                'target_id' => $id,
                'title' => $notifMsg
            ];
            Notification::sendNotification($notif_data);

            $msg = "<script> alert('Pengajuan Additional Budget berhasil ditolak!'); reload(); </script>";
        } else {
            $msg = "<script> alert('Error! Pengajuan Additional Budget gagal ditolak!'); </script>";
        }

        return $msg;
    }

    public function actionCancelSubmission()
    {
        $id = intval(post_data('id'));

        if (AdditionalConstraint::allowCancel($id)) {
            $additional = BudgetAdditional::findOne($id);
            $additional->is_canceled = 1;

            if ($additional->save()) $msg = "<script> alert('Pengajuan additional budget berhasil dibatalkan!'); reload(); </script>";
            else {
                $msg = "<script> alert('Error! Pengajuan additional budget gagal dibatalkan!'); </script>";
            }
        } else {
            $msg = "<script> alert('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan additional budget!'); </script>";
        }

        return $msg;
    }

    public function actionBulkAction()
    {
        $action = post_data('action');
        $msg = '';

        $list_id = explode(',', post_data('list_id'));
        foreach ($list_id as $id) {
            if ($action == 'approve') {
                $msg = self::actionApproveSubmission($id);
            } else {
                $msg = self::actionRejectSubmission($id);
            }
        }

        return $msg;
    }
}

?>
