<?php
namespace app\controllers\proposal;

use app\controllers\NotificationController as Notification;
use app\controllers\proposal\ConstraintController as BPConstraint;
use app\controllers\proposal\TransferConstraintController as TransferConstraint;
use app\helpers\Auth;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\BudgetTransfer;
use app\models\master\Department;
use app\models\master\Section;
use app\models\view\BudgetFinal as BudgetFinalView;
use app\models\view\BudgetTransfer as BudgetTransferView;

class TransferController extends FinalController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        $this->layout = 'main';

        return $this->render('main', $data);
    }

    public function actionList()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        if ($this->include_rejected) unset($cond['is_rejected']);
        if ($this->include_canceled) unset($cond['is_canceled']);
        if ($this->only_rejected) $cond['is_rejected'] = 1;
        if ($this->only_canceled) $cond['is_canceled'] = 1;

        $data = BudgetTransferView::find()
            ->asArray()
            ->where($cond)
            ->all();

        return $this->render('list', ['data' => $data]);
    }

    public function actionLoadData()
    {
        $this::ajaxMethod();
        $section_id = Auth::user()->section_id;

        $data['data'] = BudgetFinalView::find()
            ->asArray()
            ->where(['section_id' => $section_id])
            ->all();

        return $this->render('data', $data);
    }

    public function actionFormTransfer()
    {
        $this::ajaxMethod();
        $section_name = Auth::user()->section->name;
        $budget_final_id = intval(post_data('budget_final_id'));

        $data['budget_final_id'] = $budget_final_id;
        $data['section_name'] = ucwords(strtolower($section_name));
        $data['destination'] = [];

        /* source */
        $source = BudgetFinalView::find()
            ->where(['id' => $budget_final_id])
            ->one();

        $key = FINAL_CODE . str_pad($source->id, 4, '0', STR_PAD_LEFT);
        $section = ucwords(strtolower($source->section));
        $budget = $source->budget_name;
        $data['source'] = "[$key] $budget ($section)";
        /* /source */

        $data['department_option'] = self::getDepartment();

        return $this->render('form-transfer', $data);
    }

    private function getDepartment()
    {
        $Department = Department::find()
            ->where(['is_non_department' => 0])
            ->all();

        $department_option = [];
        foreach ($Department as $list) {
            $department_option[$list->id] = $list->name;
        }

        return $department_option;
    }

    public function actionGetSection()
    {
        $dept_id = intval(post_data('department_id'));

        $Section = Section::find()
            ->where(['department_id' => $dept_id])
            ->all();

        $section_option = '';
        foreach ($Section as $list) {
            $section_option .= "<option value='" . $list->id . "'>" . $list->name . "</option>";
        }
        $section_option .= "<script> get_destination(); </script>";

        return $section_option;
    }

    public function actionGetDestination()
    {
        $section_id = intval(post_data('section_id'));
        $source_id = intval(post_data('source_id'));

        $Final = BudgetFinalView::find()
            ->asArray()
            ->where(['section_id' => $section_id])
            ->andWhere(['NOT IN', 'id', [$source_id]])
            ->all();

        $final_option = '';
        foreach ($Final as $list) {
            extract($list);
            $key = FINAL_CODE . str_pad($id, 4, '0', STR_PAD_LEFT);
            $section = ucwords(strtolower($section));

            $final_option .= "<option value='$id'>[$key] $budget_name ($section)</option>";
        }
        $final_option .= "<script>$('#transfer-destination').val('').trigger('chosen:updated');</script>";

        return $final_option;
    }

    public function actionSaveData()
    {
        $this::ajaxMethod();
        $data = $_POST;

        $transfer = BPConstraint::validateTransfer($data);
        if (!$transfer['valid']) {
            return alert_danger($transfer['msg']);
        } else {
            $BudgetTransfer = new BudgetTransfer();

            $BudgetTransfer->source = $data['source'];
            $BudgetTransfer->destination = $data['destination'];
            $BudgetTransfer->amount = $data['amount'];
            $BudgetTransfer->time = date('Y-m-d H:i:s');

            if ($BudgetTransfer->save()) {
                $budget_transfer_id = $BudgetTransfer->id;

                $BudgetFinal = BudgetFinal::findOne($data['source']);
                $budget_proposal_id = $BudgetFinal->budget_proposal_id;

                $BudgetProposal = BudgetProposal::findOne($budget_proposal_id);
                $section_id = $BudgetProposal->section_id;

                $Section = Section::findOne($section_id);
                $section_name = $Section->name;

                /* send notification */
                $notifMsg = parent::getNotifMessage('TRANSFER_REQUEST_APPROVAL');
                $notifMsg = str_replace('{budget_transfer_id}', TRANSFER_CODE . str_pad($budget_transfer_id, 4, '0', STR_PAD_LEFT), $notifMsg);
                $notifMsg = str_replace('{section_name}', ucwords(strtolower($section_name)), $notifMsg);

                $notif_data = [
                    'category' => 'transfer',
                    'type' => 'warning',
                    'target_id' => $budget_transfer_id,
                    'title' => $notifMsg
                ];
                $notif_send = Notification::sendNotification($notif_data);

                $msg = "
				Pengajuan Budget Transfer berhasil disimpan!
				<script> closeModal(1000); </script>
				";

                $msg = alert_success($msg);
            } else $msg = alert_danger('<strong>Error!</strong> Pengajuan Budget Transfer gagal disimpan!');

            return $msg;
        }
    }

    public function actionPendingSubmission()
    {
        $data = BudgetTransferView::find()
            ->asArray()
            ->where(['is_approved_by_fa' => 0, 'is_rejected' => 0, 'is_canceled' => 0])
            ->all();

        return $this->render('pending', ['data' => $data]);
    }

    public function actionDetail($id = null)
    {
        if (get_data('id')) $id = intval(get_data('id'));
        else {
            $this::ajaxMethod();
            $id = intval(post_data('id'));
        }
        $data = BudgetTransferView::find()
            ->asArray()
            ->where(['id' => $id])
            ->one();

        $data['allow_approve'] = BPConstraint::allowApproveTransfer($id);
        $data['allow_cancel'] = TransferConstraint::allowCancel($id);

        /* GET PROPOSAL CURRENCY USED */
        $final_id = $data['source_id'];
        $data['current_currency'] = parent::getFinalCurrencyUsed($final_id);

        return $this->render('detail', $data);
    }

    public function actionApproveSubmission($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        if (BPConstraint::allowApproveTransfer($id)) {
            $limit_check = BPConstraint::transferReachLimit($id);
            if ($limit_check['reach_limit']) {
                return $limit_check['msg'];
            }

            $department_name = $activeUser->department->name;

            $BudgetTransfer = BudgetTransfer::findOne($id);
            $source = $BudgetTransfer->source;

            $BudgetFinal = BudgetFinal::findOne($source);
            $budget_proposal_id = $BudgetFinal->budget_proposal_id;

            $BudgetProposal = BudgetProposal::findOne($budget_proposal_id);
            $section_id = $BudgetProposal->section_id;

            $Section = Section::findOne($section_id);
            $section_name = $Section->name;

            $approved_by_fa = false;
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $BudgetTransfer->is_approved_by_fa = 1;
                $BudgetTransfer->approved_by_fa_time = date('Y-m-d H:i:s');

                $approved_by_fa = true;
            } else {
                if (!$BudgetTransfer->is_approved_by_source_manager) {
                    $BudgetTransfer->is_approved_by_source_manager = 1;
                    $BudgetTransfer->approved_by_source_manager_time = date('Y-m-d H:i:s');
                } else {
                    $BudgetTransfer->is_approved_by_destination_manager = 1;
                    $BudgetTransfer->approved_by_destination_manager_time = date('Y-m-d H:i:s');
                }
            }

            if ($BudgetTransfer->save()) {
                if (!$approved_by_fa) {
                    /* kirim notif ke fa */
                    $notifMsg = parent::getNotifMessage('TRANSFER_REQUEST_APPROVAL');
                    $notifMsg = str_replace('{section_name}', ucwords(strtolower($section_name)), $notifMsg);
                    $notifMsg = str_replace('{budget_transfer_id}', TRANSFER_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);

                    $notif_data = [
                        'category' => 'transfer',
                        'type' => 'warning',
                        'target_id' => $id,
                        'title' => $notifMsg
                    ];
                    Notification::sendNotification($notif_data);
                    /* /kirim notif ke fa */
                } else {
                    /* kirim notif ke section */
                    $notifMsg = parent::getNotifMessage('TRANSFER_APPROVED_BY_DEST');
                    $notifMsg = str_replace('{budget_transfer_id}', TRANSFER_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);
                    $notifMsg = str_replace('{section_name}', $section_name, $notifMsg);

                    $notif_data = [
                        'category' => 'transfer',
                        'type' => 'success',
                        'target_id' => $id,
                        'title' => $notifMsg
                    ];
                    Notification::sendNotification($notif_data);
                    /* /kirim notif ke section */
                }

                /* kirim notif ke section */
                $notifMsg = parent::getNotifMessage('TRANSFER_APPROVED');
                $notifMsg = str_replace('{budget_transfer_id}', TRANSFER_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);
                $notifMsg = str_replace('{department_name}', $department_name, $notifMsg);

                $notif_data = [
                    'category' => 'transfer',
                    'type' => 'success',
                    'target_id' => $id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notif_data);
                /* /kirim notif ke section */

                $msg = "alert('Pengajuan Budget Transfer berhasil diterima!'); reload();";
            } else {
                $msg = "alert('Error! Pengajuan Budget Transfer gagal diterima!');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!');";
        }

        return "<script> $msg </script>";
    }

    public function actionRejectSubmission($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        if (BPConstraint::allowApproveTransfer($id)) {
            $BudgetTransfer = BudgetTransfer::findOne($id);
            $BudgetTransfer->is_rejected = 1;

            if ($BudgetTransfer->save()) {
                $department_name = $activeUser->department->name;

                /* kirim notif ke section */
                $notifMsg = parent::getNotifMessage('TRANSFER_REJECTED');
                $notifMsg = str_replace('{budget_transfer_id}', TRANSFER_CODE . str_pad($id, 4, '0', STR_PAD_LEFT), $notifMsg);
                $notifMsg = str_replace('{department_name}', $department_name, $notifMsg);

                $notif_data = [
                    'category' => 'transfer',
                    'type' => 'danger',
                    'target_id' => $id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notif_data);
                /* /kirim notif ke section */

                $msg = "<script> alert('Pengajuan Budget Transfer berhasil ditolak!'); reload(); </script>";
            } else {
                $msg = "<script> alert('Error! Pengajuan Budget Transfer gagal ditolak!'); </script>";
            }
        } else {
            $msg = "<script> alert('Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!'); </script>";
        }


        return $msg;
    }

    public function actionCancelSubmission()
    {
        $this::ajaxMethod();
        $id = intval(post_data('id'));

        if (TransferConstraint::allowCancel($id)) {
            $Transfer = BudgetTransfer::findOne($id);
            $Transfer->is_canceled = 1;

            if ($Transfer->save()) $msg = "<script> alert('Pengajuan transfer budget berhasil dibatalkan!'); reload(); </script>";
            else {
                $msg = "<script> alert('Error! Pengajuan transfer budget gagal dibatalkan!'); </script>";
            }
        } else {
            $msg = "<script> alert('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan transfer budget!'); </script>";
        }

        return $msg;
    }

    public function actionBulkAction()
    {
        $this::ajaxMethod();
        $action = post_data('action');
        $msg = '';

        $list_id = explode(',', post_data('list_id'));
        foreach ($list_id as $id) {
            if ($action == 'approve') $msg = self::actionApproveSubmission($id);
            else $msg = self::actionRejectSubmission($id);
        }

        return $msg;
    }
}

?>
