<?php
namespace app\controllers\proposal;

use app\helpers\Auth;
use app\models\BudgetAdditional;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\BudgetTransfer;
use app\models\master\Budget;
use app\models\master\BudgetCategoryPeriod;
use app\models\master\Department;
use app\models\master\Period;
use app\models\master\Section;

class ConstraintController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionCheckActivePeriod()
    {
        $budgetId = intval(post_data('budget_id'));
        $budget = Budget::findOne($budgetId);
        if (is_null($budget)) {
            return '';
        }

        $period = Period::findOne(['is_future' => 1]);
        if (is_null($period)) {
            return '';
        }

        $budgetCategory = $budget->subGroup->group->category;
        $budgetCategoryPeriod = BudgetCategoryPeriod::findOne([
                'budget_category_id' => $budgetCategory->id,
                'period_id' => $period->id]
        );

        if (is_null($budgetCategoryPeriod)) {
            return alert_danger("Perhatian! Masa aktif untuk budget dan periode budget yang dipilih belum diatur!") .
                "<script> $('#btn-save-proposal').prop('disabled', true); </script>";
        } else {
            return "<script> $('#btn-save-proposal').prop('disabled', false); </script>";
        }
    }

    public static function validateTransfer($data)
    {
        $budgetFinal = BudgetFinal::findOne($data['source']);
        $budgetProposalId = $budgetFinal->budget_proposal_id;

        $source = BudgetProposal::findOne($budgetProposalId);
        $available = $source->amount - $source->amount_used;
        $amount = $data['amount'];

        if ($amount > $available) {
            $amount = number_format($amount, 2, ',', '.');
            $available = number_format($available, 2, ',', '.');
            $msg = "<strong>Error!</strong> Pengajuan transfer melebihi batas alokasi yang tersedia.<br>Pengajuan : $amount, Amount tersedia : $available";

            return ['valid' => false, 'msg' => $msg];
        } else {
            return ['valid' => true];
        }
    }

    public static function transferReachLimit($id)
    {
        $budgetTransfer = BudgetTransfer::findOne($id);
        $source = $budgetTransfer->source;
        $amount = $budgetTransfer->amount;

        $budgetFinal = BudgetFinal::findOne($source);
        $budgetProposalId = $budgetFinal->budget_proposal_id;

        $budgetProposal = BudgetProposal::findOne($budgetProposalId);
        $available = $budgetProposal->amount - $budgetProposal->amount_used;

        if ($amount > $available) {
            $amount = number_format($amount, 2, ',', '.');
            $available = number_format($available, 2, ',', '.');
            $msg = "<strong>Error!</strong> Pengajuan transfer melebihi batas alokasi yang tersedia.<br>Pengajuan : $amount, Amount tersedia : $available";

            $msg = alert_danger($msg);
            return ['reach_limit' => true, 'msg' => $msg];
        } else {
            return ['reach_limit' => false];
        }
    }

    public static function allowApproveTransfer($budget_transfer_id)
    {
        $budgetTransfer = BudgetTransfer::findOne($budget_transfer_id);
        $source = $budgetTransfer->source;
        $destination = $budgetTransfer->destination;
        $is_approved_by_source_manager = $budgetTransfer->is_approved_by_source_manager;
        $is_approved_by_destination_manager = $budgetTransfer->is_approved_by_destination_manager;
        $is_approved_by_fa = $budgetTransfer->is_approved_by_fa;

        $activeUser = Auth::user();

        if (!$activeUser->roleAs('manager')) {
            return false;
        } else if ($budgetTransfer->is_rejected) {
            return false;
        } else if ($budgetTransfer->is_canceled) {
            return false;
        } else if (!$is_approved_by_source_manager) {
            $budgetFinal = BudgetFinal::findOne($source);
            $budgetProposalId = $budgetFinal->budget_proposal_id;

            $budgetProposal = BudgetProposal::findOne($budgetProposalId);
            $sectionId = $budgetProposal->section_id;

            $section = Section::findOne($sectionId);
            $departmentId = $section->department_id;

            if ($departmentId == $activeUser->department_id) {
                return true;
            } else {
                return false;
            }
        } else if ($is_approved_by_source_manager && !$is_approved_by_destination_manager) {
            $budgetFinal = BudgetFinal::findOne($destination);
            $budgetProposalId = $budgetFinal->budget_proposal_id;

            $budgetProposal = BudgetProposal::findOne($budgetProposalId);
            $sectionId = $budgetProposal->section_id;

            $section = Section::findOne($sectionId);
            $departmentId = $section->department_id;

            if ($departmentId == $activeUser->department_id) {
                return true;
            } else {
                return false;
            }
        } else if ($is_approved_by_source_manager && $is_approved_by_destination_manager && !$is_approved_by_fa) {
            $faDept = Department::findByCode('FA');
            if ($activeUser->roleAs('manager') && $activeUser->department_id == $faDept->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowTransferBudget($budgetFinalId)
    {
        $activeUser = Auth::user();
        $budgetFinal = BudgetFinal::findOne($budgetFinalId);
        $budgetProposalId = $budgetFinal->budget_proposal_id;

        $budgetProposal = BudgetProposal::findOne($budgetProposalId);

        if (!$activeUser->roleAs('section')) {
            return false;
        } else if (!$budgetProposal->is_final) {
            return false;
        } else if ($budgetProposal->section_id == $activeUser->section_id) {
            if ($budgetProposal->amount > $budgetProposal->amount_used) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function validateAdditional($data)
    {
        $budgetFinal = BudgetFinal::findOne($data['budget_final_id']);
        $budgetProposalId = $budgetFinal->budget_proposal_id;

        $budgetProposal = BudgetProposal::findOne($budgetProposalId);
        $activeUser = Auth::user();

        if (!$activeUser->roleAs('section')) {
            $msg = "<strong>Error!</strong> Anda tidak memiliki hak akses untuk mengakses fitur ini";
            return ['valid' => false, 'msg' => $msg];
        } else if ($budgetProposal->section_id != $activeUser->section_id) {
            $msg = "<strong>Error!</strong> Anda tidak memiliki hak akses untuk mengakses fitur ini";
            return ['valid' => false, 'msg' => $msg];
        } else {
            return ['valid' => true];
        }
    }

    public static function allowApproveAdditional($budget_additional_id)
    {
        $budgetAdditional = BudgetAdditional::findOne($budget_additional_id);
        $budgetFinalId = $budgetAdditional->budget_final_id;
        $is_approved_by_manager = $budgetAdditional->is_approved_by_manager;
        $is_approved_by_fa = $budgetAdditional->is_approved_by_fa;

        $activeUser = Auth::user();

        if (!$activeUser->roleAs('manager')) {
            return false;
        } else if ($budgetAdditional->is_rejected) {
            return false;
        } else if ($budgetAdditional->is_canceled) {
            return false;
        } else if (!$is_approved_by_manager) {
            $budgetFinal = BudgetFinal::findOne($budgetFinalId);
            $budgetProposalId = $budgetFinal->budget_proposal_id;
            $budgetProposal = BudgetProposal::findOne($budgetProposalId);
            $sectionId = $budgetProposal->section_id;

            $section = Section::findOne($sectionId);
            $departmentId = $section->department_id;

            if ($departmentId == $activeUser->department_id) {
                return true;
            } else {
                return false;
            }
        } else if ($is_approved_by_manager && !$is_approved_by_fa) {
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

?>
