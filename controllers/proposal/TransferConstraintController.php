<?php
namespace app\controllers\proposal;

use app\helpers\Auth;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\BudgetTransfer;

class TransferConstraintController extends ConstraintController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public static function allowCancel($transfer_id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $transfer = BudgetTransfer::findOne($transfer_id);
            if ($transfer->is_approved_by_source_manager) {
                return false;
            } else if ($transfer->is_canceled) {
                return false;
            } else if ($transfer->is_rejected) {
                return false;
            } else {
                $final = BudgetFinal::find()->where(['id' => $transfer->source])->one();
                $proposal = BudgetProposal::findOne($final->budget_proposal_id);

                if ($proposal->section_id == $activeUser->section_id) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

?>
