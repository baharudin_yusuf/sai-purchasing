<?php
namespace app\controllers\proposal;

use app\controllers\NotificationController as Notification;
use app\controllers\proposal\ConstraintController as BPConstraint;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\BudgetProposalDetail;
use app\models\BudgetReschedule;
use app\models\master\BudgetPeriod;
use app\models\view\BudgetAdditional as BudgetAdditionalView;
use app\models\view\BudgetFinal as BudgetFinalView;

class RescheduleController extends FinalController
{
    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        $this->layout = 'main';

        return $this->render('main', $data);
    }

    public function actionList()
    {
        $Reschedule = BudgetProposal::find()
            ->where(['is_reschedule' => 1, 'is_approved_by_fa' => 1])
            ->all();

        $proposal_list = [];
        foreach ($Reschedule as $list) {
            $proposal_list[] = $list->id;
        }

        $data = BudgetFinalView::find()
            ->asArray()
            ->where(['IN', 'budget_proposal_id', $proposal_list])
            ->all();

        return $this->render('list', ['data' => $data]);
    }

    public function actionLoadData()
    {
        $this::ajaxMethod();
        $userdata = $this::getUserData();
        $section_id = $userdata['section_id'];

        $data['data'] = BudgetFinalView::find()
            ->asArray()
            ->where(['is_expired' => 1, 'section_id' => $section_id])
            ->andWhere(['<', 'active_until', date('Y-m-d H:i:s')])
            ->all();

        return $this->render('data', $data);
    }

    public function actionFormReschedule()
    {
        $this::ajaxMethod();
        $userdata = $this::getUserData();
        $budget_final_id = intval(post_data('id'));

        $data = BudgetFinalView::find()
            ->asArray()
            ->where(['id' => $budget_final_id])
            ->one();

        /* GET PROPOSAL CURRENCY USED */
        $data['current_currency'] = parent::getFinalCurrencyUsed($budget_final_id);
        $data['budget_proposal_detail'] = $this::getBudgetProposalDetail($data['budget_proposal_id']);

        /* GET BUDGET PERIOD */
        $Proposal = BudgetProposal::findOne($data['budget_proposal_id']);
        $budget_period_id = $Proposal->budget_period_id;

        $BudgetPeriod = BudgetPeriod::findOne($budget_period_id);
        $period_id = $BudgetPeriod->period_id;
        $p_month = $BudgetPeriod->month;
        $p_year = $BudgetPeriod->year;

        $BudgetPeriod = BudgetPeriod::find()
            ->where(['period_id' => $period_id])
            ->andWhere(['>=', 'year', $p_year])
            ->orderBy(['year' => SORT_ASC, 'month' => SORT_ASC])
            ->all();

        $period_option = [];
        foreach ($BudgetPeriod as $list) {
            if ($list->year == $p_year && $list->month < $p_month) {
                continue;
            }

            $period_option[$list->id] = $list->name;
        }
        $data['period_option'] = $period_option;

        return $this->render('form-reschedule', $data);
    }

    public function actionSaveData()
    {
        $this::ajaxMethod();
        $data = $_POST;
        $final_id = intval($data['final_id']);
        $budget_period_id = intval($data['budget_period_id']);

        /* SAVE RESCHEDULE */
        $BudgetReschedule = new BudgetReschedule();
        $BudgetReschedule->budget_final_id = $final_id;
        $BudgetReschedule->date = date('Y-m-d H:i:s');
        $BudgetReschedule->save();

        /* SAVE PROPOSAL */
        $Final = BudgetFinal::findOne($final_id);
        $old_proposal_id = $Final->budget_proposal_id;

        $OldProposal = BudgetProposal::findOne($old_proposal_id);
        $NewProposal = new BudgetProposal();

        $NewProposal->date = date('Y-m-d H:i:s');
        $NewProposal->budget_period_id = $budget_period_id;
        $NewProposal->budget_id = $OldProposal->budget_id;
        $NewProposal->amount = $OldProposal->amount;
        $NewProposal->comment = $OldProposal->comment;
        $NewProposal->amount_used = $OldProposal->amount_used;
        $NewProposal->is_reschedule = 1;

        /* SECTION DATA */
        $userdata = parent::getUserData();
        $section_id = $userdata['section_id'];
        $section_name = $userdata['section_name'];
        $NewProposal->section_id = $section_id;

        if ($NewProposal->save()) {
            $new_proposal_id = $NewProposal->id;

            $Detail = BudgetProposalDetail::find()
                ->where(['budget_proposal_id' => $old_proposal_id])
                ->all();

            foreach ($Detail as $list) {
                $BPD = new BudgetProposalDetail();

                $BPD->carline_id = $list->carline_id;
                $BPD->goods_service_id = $list->goods_service_id;
                $BPD->quantity = $list->quantity;
                $BPD->price_estimation = $list->price_estimation;
                $BPD->comment = $list->comment;
                $BPD->budget_proposal_id = $new_proposal_id;

                $BPD->save();
            }

            /* SEND NOTIFICATION TO MANAGER */
            $proposal_code = PROPOSAL_CODE . str_pad($new_proposal_id, 4, '0', STR_PAD_LEFT);
            $notifMsg = parent::getNotifMessage('PROPOSAL_REQUEST_APPROVAL');
            $notifMsg = str_replace('{budget_proposal_id}', $proposal_code, $notifMsg);
            $notifMsg = str_replace('{section_name}', $section_name, $notifMsg);

            $notif_data = [
                'category' => 'proposal',
                'type' => 'warning',
                'target_id' => $new_proposal_id,
                'title' => $notifMsg
            ];
            $notif_send = Notification::sendNotification($notif_data);

            $msg = "alert('Reschedule budget berhasil disimpan!'); location.reload(); ";
        } else $msg = "alert('Reschedule budget gagal disimpan!');";

        return "<script> $msg </script>";
    }

    public function actionDetail($id = null)
    {
        if (post_data('id')) {
            $this::ajaxMethod();
            $id = intval(post_data('id'));
        } else $id = intval(get_data('id'));

        $data = BudgetAdditionalView::find()
            ->asArray()
            ->where(['id' => $id])
            ->one();
        $data['allow_approve'] = BPConstraint::allowApproveAdditional($id);

        return $this->render('detail', $data);
    }
}

?>
