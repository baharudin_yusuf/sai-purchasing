<?php
namespace app\controllers\proposal;

use app\controllers\BaseController;
use app\controllers\NotificationController as Notification;
use app\controllers\proposal\ProposalConstraintController as ProposalConstraint;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\BudgetProposalDetail;
use app\models\master\Budget;
use app\models\master\Department;
use Yii;

class MainController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();
        parent::requirementCheck();

        $menu = [
            'Additional' => [
                'Additional List' => url('proposal/additional/list'),
                'Create Additional' => url('proposal/additional'),
                'Pending Additional' => url('proposal/additional/pending-submission'),
            ],
            'Final' => [
                'Budget Final' => url('proposal/final'),
            ],
            'Proposal' => [
                'Proposal List' => url('proposal/main'),
                'Create Proposal' => url('proposal/create'),
                'Pending Proposal' => url('proposal/main/pending-submission'),
            ],
            'Reschedule' => [
                'Reschedule List' => url('proposal/reschedule/list'),
                'Reschedule Budget' => url('proposal/reschedule'),
            ],
            'Transfer' => [
                'Transfer List' => url('proposal/transfer/list'),
                'Transfer Budget' => url('proposal/transfer'),
                'Pending Transfer' => url('proposal/transfer/pending-submission'),
            ],
        ];

        $activeUser = Auth::user();
        if ($activeUser->roleAs('section')) {
            unset($menu['Proposal']['Pending Proposal']);
            unset($menu['Transfer']['Pending Transfer']);
            unset($menu['Additional']['Pending Additional']);
        } else if ($activeUser->roleAs('manager')) {
            unset($menu['Proposal']['Create Proposal']);
            unset($menu['Transfer']['Transfer Budget']);
            unset($menu['Additional']['Create Additional']);
            unset($menu['Reschedule']['Reschedule Budget']);
        } else if ($activeUser->roleAs('admin')) {
            unset($menu['Additional']['Create Additional']);
            unset($menu['Additional']['Pending Additional']);

            unset($menu['Proposal']['Create Proposal']);
            unset($menu['Proposal']['Pending Proposal']);

            unset($menu['Reschedule']['Reschedule Budget']);

            unset($menu['Transfer']['Transfer Budget']);
            unset($menu['Transfer']['Pending Transfer']);
        }

        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'budget-proposal');

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        $this->layout = 'main';

        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        if ($this->include_rejected) {
            unset($cond['is_rejected']);
        }
        if ($this->include_canceled) {
            unset($cond['is_canceled']);
        }
        if ($this->only_rejected) {
            $cond['is_rejected'] = 1;
        }
        if ($this->only_canceled) {
            $cond['is_canceled'] = 1;
        }

        $activeUser = Auth::user();
        $sectionList = [];
        if ($activeUser->roleAs('section')) {
            $sectionList[] = $activeUser->section_id;
        } else if ($activeUser->roleAs('manager')) {
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $sectionList = [];
            } else {
                foreach ($activeUser->department->section as $list) {
                    $section_list[] = $list->id;
                }
            }
        }

        $data = BudgetProposal::find()->where($cond);
        if (count($sectionList) > 0) {
            $data->andWhere(['section_id' => $sectionList])->all();
        }

        $params = [
            'data' => $data->all()
        ];
        return $this->renderPartial('data', $params);
    }

    public function actionBudgetInfo()
    {
        $budgetId = intval(post_data('budget_id'));
        $budget = Budget::firstOrFail($budgetId);

        $params = [
            'data' => $budget
        ];
        return $this->renderPartial('budget-info', $params);
    }

    public function actionDetail($id = null)
    {
        $id = is_null($id) ? intval(get_data('id')) : $id;
        $proposal = BudgetProposal::firstOrFail($id);
        $proposal->calculateAmountUsed();
        $proposal->refresh();

        $params = [
            'proposal' => $proposal,
            'proposalConstraint' => ProposalConstraintController::class,
            'allow_approve' => ProposalConstraint::allowApprove($proposal),
            'allowCancel' => ProposalConstraint::allowCancelProposal($proposal),
        ];
        return $this->render('detail', $params);
    }

    public static function getProposalCurrencyUsed($proposalId)
    {
        $proposal = BudgetProposal::findOne($proposalId);
        $period = $proposal->budgetPeriod->period;
        $currencyDetail = $period->getCurrencyDetail()->where(['is_base_currency' => 1])->one();

        $currentCurrency = [
            'id' => $currencyDetail->currency_id,
            'name' => $currencyDetail->currency->name,
            'rate' => $currencyDetail->current_rate
        ];

        return $currentCurrency;
    }

    public function actionPendingSubmission()
    {
        $activeUser = Auth::user();

        if (!$activeUser->roleAs('manager|admin')) {
            parent::throw404Error();
        }

        if ($activeUser->roleAs('manager')) {
            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $data = BudgetProposal::find()
                    ->where(['is_approved_by_manager' => 1, 'is_approved_by_fa' => 0, 'is_rejected' => 0])
                    ->all();
            } else {
                $section_id = [];
                foreach ($activeUser->department->section as $list) {
                    $section_id[] = $list['id'];
                }

                $data = BudgetProposal::find()
                    ->where(['IN', 'section_id', $section_id])
                    ->andWhere(['is_approved_by_manager' => 0, 'is_rejected' => 0])
                    ->all();
            }
        } else {
            $data = BudgetProposal::find()
                ->where(['is_rejected' => 0])
                ->orWhere(['is_approved_by_manager' => 0])
                ->orWhere(['is_approved_by_fa' => 0])
                ->all();
        }

        $data['data'] = $data;
        return $this->render('pending-proposal', $data);
    }

    public function actionApproveProposal($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager|admin')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        $budgetProposal = BudgetProposal::findOne($id);

        if (ProposalConstraint::allowApprove($budgetProposal)) {
            $section = $budgetProposal->section;

            $faDept = Department::findByCode('FA');
            if ($activeUser->department_id == $faDept->id) {
                $budgetProposal->is_approved_by_fa = 1;
                $budgetProposal->approved_by_fa_time = date('Y-m-d H:i:s');
            } else {
                $budgetProposal->is_approved_by_manager = 1;
                $budgetProposal->approved_by_manager_time = date('Y-m-d H:i:s');
            }

            if ($budgetProposal->save()) {
                $detail = $budgetProposal->getDetail()->where(['is_rejected' => 0, 'is_canceled' => 0])->all();
                foreach ($detail as $list) {
                    if ($activeUser->department_id == $faDept->id && !$list->is_approved_by_fa) {
                        $list->is_approved_by_fa = 1;
                        $list->approved_by_fa_time = date('Y-m-d H:i:s');
                        $list->save();
                    } else if (!$list->is_approved_by_manager) {
                        $list->is_approved_by_manager = 1;
                        $list->approved_by_manager_time = date('Y-m-d H:i:s');
                        $list->save();
                    }
                }

                // kirim notif approval ke fa
                if ($budgetProposal->is_approved_by_manager && !$budgetProposal->is_approved_by_fa) {
                    $notifMsg = Notificatixn::render('PROPOSAL_REQUEST_APPROVAL', [
                        'section_name' => $section->name,
                        'budget_proposal_id' => $budgetProposal->code
                    ]);

                    $notif_data = [
                        'category' => 'proposal',
                        'type' => 'warning',
                        'target_id' => $id,
                        'title' => $notifMsg
                    ];
                    Notification::sendNotification($notif_data);
                }

                // kirim notif ke section
                $notifMsg = Notificatixn::render('PROPOSAL_APPROVED', [
                    'budget_proposal_id' => $budgetProposal->code,
                    'department_name' => $activeUser->department->name
                ]);

                $notif_data = [
                    'category' => 'proposal',
                    'type' => 'success',
                    'target_id' => $id,
                    'title' => $notifMsg
                ];
                Notification::sendNotification($notif_data);

                // set budget final
                $final = $budgetProposal->budgetFinal;
                if ($budgetProposal->is_approved_by_fa && count($final) == 0) {
                    // set proposal expired
                    $budgetProposal->setExpiredDate();

                    // create budget final
                    $final = new BudgetFinal();
                    $final->budget_proposal_id = $budgetProposal->id;
                    $final->time = date('Y-m-d H:i:s');
                    $final->save();
                }

                $msg = "alert('Budget proposal berhasil diterima!'); reload();";
                $budgetProposal->calculateAmountUsed();
            } else {
                $msg = "alert('Error! Budget proposal gagal diterima!');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk menerima pengajuan budget proposal!');";
        }

        return "<script> $msg </script>";
    }

    public function actionRejectProposal($id = null)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('manager|admin')) {
            parent::throw404Error();
        }

        $id = is_null($id) ? intval(post_data('id')) : $id;
        $budgetProposal = BudgetProposal::findOne($id);
        $budgetProposal->is_rejected = 1;

        if ($budgetProposal->save()) {
            $userdata = $this::getUserData();
            $departmentName = $userdata['department_name'];
            $proposalCode = PROPOSAL_CODE . str_pad($id, 4, '0', STR_PAD_LEFT);

            /* kirim notif ke section */
            $notifMsg = 'Pengajuan Budget Proposal oleh dengan id {budget_proposal_id} telah ditolak oleh manager ' . $departmentName;
            $notifMsg = str_replace('{budget_proposal_id}', $proposalCode, $notifMsg);

            $notif_data = [
                'category' => 'proposal',
                'type' => 'danger',
                'target_id' => $id,
                'title' => $notifMsg
            ];
            $notif_send = Notification::sendNotification($notif_data);
            /* /kirim notif ke section */

            $msg = "<script> alert('Budget proposal berhasil ditolak!'); reload(); </script>";
        } else {
            $msg = "<script> alert('Error! Budget proposal gagal ditolak!'); </script>";
        }

        return $msg;
    }

    public function actionCancelProposal()
    {
        $proposal_id = intval(post_data('id'));

        $Proposal = BudgetProposal::findOne($proposal_id);
        if (ProposalConstraint::allowCancelProposal($Proposal)) {
            $Proposal->is_canceled = 1;

            if ($Proposal->save()) {
                $msg = "alert('Pengajuan Budget Proposal berhasil dibatalkan'); location.reload();";
            } else {
                $msg = "alert('Error! Pengajuan Budget Proposal gagal dibatalkan');";
            }
        } else {
            $msg = "alert('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan data');";
        }

        return "<script> $msg </script>";
    }

    protected function setBudgetProposalStatus($budgetId)
    {
        /* cari total detail pengajuan budget proposal */
        $BPD = BudgetProposalDetail::find()
            ->asArray()
            ->where(['budget_proposal_id' => $budgetId, 'is_rejected' => 0])
            ->all();
        $budget_proposal_detail_count = count($BPD);
        /* /cari total detail pengajuan budget proposal */

        /* approved by manager */
        $BPD = BudgetProposalDetail::find()
            ->asArray()
            ->where(['budget_proposal_id' => $budgetId, 'is_approved_by_manager' => 1])
            ->all();
        $approved_by_manager = count($BPD);

        if ($budget_proposal_detail_count == $approved_by_manager) {
            $budgetProposal = BudgetProposal::findOne($budgetId);

            if (!$budgetProposal->is_approved_by_manager) {
                $budgetProposal->is_approved_by_manager = 1;
                $budgetProposal->approved_by_manager_time = date('Y-m-d H:i:s');
                $budgetProposal->save();
            }
        }
        /* /approved by manager */

        /* approved by fa */
        $BPD = BudgetProposalDetail::find()
            ->asArray()
            ->where(['budget_proposal_id' => $budgetId, 'is_approved_by_fa' => 1])
            ->all();
        $approved_by_fa = count($BPD);

        if ($budget_proposal_detail_count == $approved_by_fa) {
            $budgetProposal = BudgetProposal::findOne($budgetId);

            if (!$budgetProposal->is_approved_by_fa) {
                $budgetProposal->is_approved_by_fa = 1;
                $budgetProposal->approved_by_fa_time = date('Y-m-d H:i:s');
                $budgetProposal->save();
            }
        }
        /* /approved by fa */
    }

    public function actionBulkAction()
    {
        $this::ajaxMethod();
        $action = post_data('action');
        $msg = '';

        $list_id = explode(',', post_data('list_id'));
        foreach ($list_id as $id) {
            if ($action == 'approve') $msg = self::actionApproveProposal($id);
            else $msg = self::actionRejectProposal($id);
        }

        return $msg;
    }
}

?>
