<?php
namespace app\controllers\proposal;

use app\helpers\Auth;
use app\models\BudgetAdditional;
use app\models\BudgetFinal;
use app\models\BudgetProposal;

class AdditionalConstraintController extends ConstraintController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public static function allowCancel($additional_id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $additional = BudgetAdditional::findOne($additional_id);
            if ($additional->is_approved_by_manager) {
                return false;
            } else if ($additional->is_canceled) {
                return false;
            } else if ($additional->is_rejected) {
                return false;
            } else {
                $final = BudgetFinal::find()->where(['id' => $additional->budget_final_id])->one();
                $proposal = BudgetProposal::findOne($final->budget_proposal_id);

                if ($proposal->section_id == $activeUser->section_id) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

?>
