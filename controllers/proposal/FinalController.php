<?php
namespace app\controllers\proposal;

use app\helpers\Auth;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\master\Department;
use app\models\master\Section;

class FinalController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $data['data'] = self::actionLoadData();
        return $this->render('main', $data);
    }

    public function actionLoadData()
    {
        $activeUser = Auth::user();
        $section_id = $activeUser->section_id;
        $department_id = $activeUser->department_id;

        $proposalId = [];
        if ($activeUser->roleAs('section')) {
            $proposalList = BudgetProposal::find()->where(['section_id' => $section_id])->all();
            foreach ($proposalList as $item) {
                $proposalId[] = $item->id;
            }
        } else if ($activeUser->roleAs('manager')) {
            $faDept = Department::findByCode('FA');
            if ($department_id != $faDept->id) {
                $section = Section::find()
                    ->where(['department_id' => $department_id])
                    ->all();

                $sectionId = [];
                foreach ($section as $list) {
                    $sectionId[] = $list->id;
                }

                $proposalList = BudgetProposal::find()->where(['in', 'section_id', $sectionId])->all();
                foreach ($proposalList as $item) {
                    $proposalId[] = $item->id;
                }
            }
        }

        $data = BudgetFinal::find();
        if (!empty($proposalId)) {
            $data->where(['in', 'budget_proposal_id', $proposalId]);
        }
        return $this->renderPartial('data', ['data' => $data->all()]);
    }

    public function actionDetail($id = null)
    {
        if (\Yii::$app->request->isPost) {
            $id = intval(post_data('id'));
            if ($id == 0) {
                $id = intval(post_data('budget_final_id'));
            }
        } else {
            $id = intval(get_data('id'));
        }

        $final = BudgetFinal::firstOrFail($id);

        $params = [
            'final' => $final,
            'proposal' => $final->budgetProposal
        ];

        if (\Yii::$app->request->isPost) {
            return $this->renderPartial('detail', $params);
        } else {
            return $this->render('detail', $params);
        }
    }

    public static function getFinalCurrencyUsed($final_id)
    {
        $Final = BudgetFinal::findOne($final_id);
        $proposal_id = $Final->budget_proposal_id;

        return parent::getProposalCurrencyUsed($proposal_id);
    }

}

?>
