<?php
namespace app\controllers\proposal;

use app\helpers\Auth;
use app\models\BudgetProposal;
use app\models\BudgetProposalDetail;
use app\models\master\Department;

class ProposalConstraintController extends ConstraintController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public static function allowManageProposalDetail(BudgetProposalDetail $budgetProposalDetail)
    {
        $budgetProposal = $budgetProposalDetail->budgetProposal;
        $approvedByManager = $budgetProposalDetail->is_approved_by_manager;
        $approvedByFA = $budgetProposalDetail->is_approved_by_fa;
        $activeUser = Auth::user();

        if ($budgetProposalDetail->is_rejected) {
            return false;
        } else if ($budgetProposalDetail->is_canceled) {
            return false;
        } else if ($approvedByManager && $approvedByFA) {
            return false;
        } else if ($budgetProposal->is_rejected) {
            return false;
        } else if ($activeUser->roleAs('manager') && !$approvedByManager && !$approvedByFA) {
            $sectionIds = [];
            foreach ($activeUser->department->section as $list) {
                $sectionIds[] = $list->id;
            }

            if (in_array($budgetProposal->section_id, $sectionIds)) {
                return true;
            } else {
                return false;
            }
        } else if ($activeUser->roleAs('manager') && $approvedByManager && !$approvedByFA) {
            $department = Department::findByCode('FA');
            if ($activeUser->department_id == $department->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowApprove(BudgetProposal $budgetProposal)
    {
        $sectionId = $budgetProposal->section_id;
        $manager = $budgetProposal->is_approved_by_manager;
        $fa = $budgetProposal->is_approved_by_fa;
        $activeUser = Auth::user();

        if ($manager && $fa) {
            return false;
        } else if ($budgetProposal->is_rejected) {
            return false;
        } else if ($budgetProposal->is_canceled) {
            return false;
        } else if ($activeUser->roleAs('manager') && !$manager && !$fa) {
            $sectionIds = [];
            $department = $activeUser->department;
            foreach ($department->section as $list) {
                $sectionIds[] = $list->id;
            }

            if (in_array($sectionId, $sectionIds)) {
                return true;
            } else {
                return false;
            }
        } else if ($activeUser->roleAs('manager') && $manager && !$fa) {
            $department = Department::findByCode('FA');
            if ($activeUser->department_id == $department->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowCancelProposal(BudgetProposal $budgetProposal)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        }

        if ($budgetProposal->is_approved_by_manager) {
            return false;
        } else if ($budgetProposal->is_canceled) {
            return false;
        } else if ($budgetProposal->is_rejected) {
            return false;
        } else {
            if ($budgetProposal->section_id == $activeUser->section_id) {
                return true;
            } else {
                return false;
            }
        }
    }
}

?>
