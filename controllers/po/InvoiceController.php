<?php
namespace app\controllers\po;

use app\controllers\NotificationController as Notification;
use app\controllers\po\ConstraintController as POConstraint;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\PoRealization;

class InvoiceController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    private function actionApproveSubmission($id)
    {
        $poRealization = PoRealization::firstOrFail($id);
        $purchaseOrder = $poRealization->purchaseOrder;

        if (POConstraint::allowApproveInvoice($poRealization)) {
            $poRealization->is_approved_by_manager = 1;
            $poRealization->approved_by_manager_time = date('Y-m-d H:i:s');
            $poRealization->approved_by_user_id = Auth::user()->id;

            if ($poRealization->save()) {
                // SEND NOTIFICATION TO SECTION
                $notifMsg = Notificatixn::render('PO_INVOICE_APPROVED', [
                    'purchase_order_id' => $purchaseOrder->code,
                    'invoice_id' => $poRealization->code,
                    'department_name' => Auth::user()->department->name
                ]);

                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'success',
                    'target_id' => $purchaseOrder->id,
                    'title' => $notifMsg
                ]);

                // SEND NOTIFICATION TO FA
                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'warning-to-fa',
                    'target_id' => $purchaseOrder->id,
                    'title' => $notifMsg
                ]);
            }
        }
    }

    private function actionRejectSubmission($id)
    {
        $poRealization = PoRealization::firstOrFail($id);
        $purchaseOrder = $poRealization->purchaseOrder;

        if (POConstraint::allowApproveInvoice($poRealization)) {
            $poRealization->is_rejected = 1;

            if ($poRealization->save()) {
                // send notification to manager
                $notifMsg = Notificatixn::render('PO_INVOICE_REJECTED', [
                    'purchase_order_id' => $purchaseOrder->code,
                    'invoice_id' => $poRealization->code,
                    'department_name' => Auth::user()->department->name
                ]);

                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'success',
                    'target_id' => $purchaseOrder->id,
                    'title' => $notifMsg
                ]);
            }
        }
    }

    public function actionCancelSubmission()
    {
        $id = intval(post_data('id'));
        $Realization = PoRealization::firstOrFail($id);

        if (POConstraint::allowCancelInvoice($Realization)) {
            $Realization->is_canceled = 1;

            if ($Realization->save()) {
                return alert_success('Pengajuan invoice berhasil dibatalkan!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Pengajuan invoice gagal dibatalkan!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan invoice!');
        }
    }

    public function actionBulkAction()
    {
        $action = post_data('action');
        $approved = 0;
        $rejected = 0;

        $listId = explode(',', post_data('list_id'));
        foreach ($listId as $id) {
            if ($action == 'approve') {
                self::actionApproveSubmission($id);
                $approved++;
            } else {
                self::actionRejectSubmission($id);
                $rejected++;
            }
        }

        if ($approved > 0) {
            $msg = "$approved pengajuan data berhasil diterima";
        } else if ($rejected > 0) {
            $msg = "$rejected pengajuan data berhasil ditolak";
        } else {
            $msg = 'Terjadi kesalahan! Data gagal diproses';
        }

        return "<script> alert('$msg'); reload(); </script>";
    }
}

?>
