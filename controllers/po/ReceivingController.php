<?php
namespace app\controllers\po;

use app\controllers\NotificationController;
use app\helpers\Auth;
use app\models\master\Section;
use app\models\PurchaseOrder;
use Yii;

class ReceivingController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $params = [
            'data' => PurchaseOrder::find()->where([
                'is_canceled' => 0,
                'is_rejected' => 0,
            ])->all(),
        ];
        return $this->render('index', $params);
    }

    public function actionDetail()
    {
        $id = get_data('id');
        $po = PurchaseOrder::findOne($id);
        if (is_null($po)) {
            parent::throw404Error();
        }

        $params = [
            'data' => $po,
            'prList' => $po->purchaseRequisitionList
        ];
        return $this->render('detail', $params);
    }

    public function actionUpdate()
    {
        $request = Yii::$app->request;
        $poId = $request->post('po_id');
        $po = PurchaseOrder::findOne($poId);
        if (is_null($po)) {
            parent::throw404Error();
        }

        $nDataUpdated = 0;
        $sectionNotified = [];

        foreach ($po->purchaseRequisitionList as $poPr) {
            foreach ($poPr->actualData as $actual) {
                $receivedKey = "received_" . $actual->id;
                $received = intval($request->post($receivedKey, 0));
                if ($received > 0 && $actual->isAllowReceive()) {
                    $actual->received = $received;
                    $actual->save();
                    $nDataUpdated++;

                    // catat notif ke pembuat PR
                    $pr = $actual->purchaseRequisition;
                    $prSectionId = $pr->section_id;
                    if (!in_array($prSectionId, $sectionNotified)) {
                        $sectionNotified[] = $prSectionId;
                    }
                }

                $deliveredKey = "delivered_" . $actual->id;
                $delivered = intval($request->post($deliveredKey, 0));
                if ($delivered > 0 && $actual->isAllowDeliver()) {
                    $actual->delivered = $delivered;
                    $actual->save();
                    $nDataUpdated++;
                }
            }
        }

        // notif ke LP apabila ada perbaruan data
        if ($nDataUpdated > 0) {
            $lpCode = parent::getSectionCode('LP');
            $lpSection = Section::findOne(['code' => $lpCode]);
            if (!in_array($lpSection->id, $sectionNotified)) {
                $sectionNotified[] = $lpSection->id;
            }
        }

        // kirim pemberitahuan
        if ($nDataUpdated > 0 && count($sectionNotified) > 0) {
            $poCode = $po->getCode();
            $userdata = Auth::user();
            $sectionName = $userdata->section->name;
            $notifMsg = parent::getNotifMessage('PO_DELIVERING_READY');
            $notifMsg = str_replace('{purchase_order_id}', $poCode, $notifMsg);
            $notifMsg = str_replace('{section_name}', $sectionName, $notifMsg);
            $notifData = [
                'category' => 'po',
                'type' => 'delivering-ready',
                'target_id' => $poId,
                'title' => $notifMsg,
                'section_list' => $sectionNotified
            ];
            NotificationController::sendNotification($notifData);
        }

        if ($nDataUpdated > 0) {
            return "
            <div class='alert alert-success center'>$nDataUpdated data berhasil diperbarui</div>
            <script> setTimeout(function() { location.reload(); }, 1500); </script>";
        } else {
            return "<div class='alert alert-warning'>Perhatian! Tidak ada data diperbarui</div>";
        }
    }

}

?>
