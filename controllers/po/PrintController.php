<?php
namespace app\controllers\po;

use app\models\PurchaseOrder;

class PrintController extends MainController
{
    public static $textLimit = 40;
    public static $maxLine = 22;
    private static $nextExists = false;

    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public function actionFormPo()
    {
        $this->layout = 'print';
        $id = intval(get_data('id'));
        $data = PurchaseOrder::firstOrFail($id);
        $page = intval(get_data('p', 1));
        $detail = self::getFormattedData($data);

        if (self::$nextExists) {
            $nextPage = $page + 1;
            $nextURL = url("po/print/form-po/?id=$id&p=$nextPage");
        } else {
            $nextURL = null;
        }

        if ($page > 1) {
            $prevPage = $page - 1;
            $prevURL = url("po/print/form-po/?id=$id&p=$prevPage");
        } else {
            $prevURL = null;
        }

        /*
        // generate note
        $note = ['&nbsp;', 'NOTE :'];
        if ($data['is_with_down_payment']) {
            $prosentase = number_format($data['down_payment_amount'] / $data['amount_realization'] * 100, 2, ',', '.');
            $note[] = "DP $prosentase % (" . number_format($data['down_payment_amount'], 2, ',', ',') . ")";
        }
        $note[] = $data['comment'];

        foreach ($note as $item) {
            $data['detail'][] = ['note' => $item];
        }
        */

        $params = [
            'data' => $data,
            'page' => $page,
            'detail' => $detail,
            'nextURL' => $nextURL,
            'prevURL' => $prevURL,
        ];
        return $this->render('form-po', $params);
    }

    private static function getFormattedData(PurchaseOrder $purchaseOrder)
    {
        $detail = [];
        foreach ($purchaseOrder->purchaseRequisitionList as $poPrList) {
            foreach ($poPrList->actualData as $item) {
                $detail[] = $item;
            }
        }

        $nData = count($detail);
        $page = intval(get_data('p', 1));
        $start = ($page - 1) * self::$maxLine;
        $end = $start + self::$maxLine;
        $end = ($end > $nData) ? $nData : $end;
        $blankText = '&nbsp;';

        $num = 0;
        $result = [];
        foreach ($detail as $item) {
            $itemLine = [''];
            $lineCount = 0;

            $goodsService = $item->goodsService;
            $gsNameSplit = explode(' ', $goodsService->name);
            foreach ($gsNameSplit as $gsWord) {
                $gsTemp = trim($itemLine[$lineCount] . ' ' . $gsWord);
                if (strlen($gsTemp) <= self::$textLimit) {
                    $itemLine[$lineCount] = $gsTemp;
                } else {
                    $lineCount++;
                    $itemLine[$lineCount] = $gsWord;
                }
            }

            $modLine = self::$maxLine - (count($result) % self::$maxLine);
            if (count($itemLine) > $modLine) {
                for ($i = 0; $i < $modLine; $i++) {
                    $result[] = [
                        'num' => $blankText,
                        'rowspan' => 1,
                        'code' => $blankText,
                        'goods_service' => $blankText,
                        'unit' => $blankText,
                        'quantity' => $blankText,
                        'price' => $blankText,
                        'amount' => $blankText,
                    ];
                }
            }

            foreach ($itemLine as $lineNum => $line) {
                $price = $item->display_price;
                if (strlen($item->display_converted_price) > 0) {
                    $price = $item->display_converted_price;
                }

                $amount = $item->display_amount;
                if (strlen($item->display_converted_amount) > 0) {
                    $amount = $item->display_converted_amount;
                }

                $result[] = [
                    'num' => $lineNum == 0 ? $num + 1 : '',
                    'rowspan' => $lineNum == 0 ? count($itemLine) : '',
                    'code' => $lineNum == 0 ? $item->purchaseRequisition->code : '',
                    'goods_service' => $line,
                    'unit' => $lineNum == 0 ? $item->goodsService->unit->name : '',
                    'quantity' => $lineNum == 0 ? $item->quantity : '',
                    'price' => $lineNum == 0 ? $price : '',
                    'amount' => $lineNum == 0 ? $amount : '',
                ];
            }
            $num++;
        }

        // note
        // code goes here

        if ($num > $end) {
            self::$nextExists = true;
        }

        $newResult = [];
        for ($num = $start; $num < $start + self::$maxLine; $num++) {
            if (isset($result[$num])) {
                $newResult[] = $result[$num];
            }
        }
        $result = $newResult;

        $currentLine = count($result);
        for ($i = $currentLine; $i < self::$maxLine; $i++) {
            $result[] = [
                'num' => $blankText,
                'rowspan' => 1,
                'code' => $blankText,
                'goods_service' => $blankText,
                'unit' => $blankText,
                'quantity' => $blankText,
                'price' => $blankText,
                'amount' => $blankText,
            ];
        }

        return $result;
    }
}

?>
