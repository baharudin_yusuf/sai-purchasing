<?php
namespace app\controllers\po;

use app\helpers\Auth;
use app\models\master\Bank;
use app\models\master\BankDetail;
use app\models\master\Department;
use app\models\master\PaymentMethod;
use app\models\PoPayment;
use app\models\PoRealization;

class PaymentController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        $activeUser = Auth::user();
        $faDept = Department::findByCode('FA');
        if ($activeUser->department_id != $faDept->id && $activeUser->roleAs('manager')) {
            //die('<span style="color:red">Error! Anda tidak memiliki hak akses untuk mengakses fitur ini!</span>');
        }

        return [];
    }

    public function actionCreateNew()
    {
        $id = intval(post_data('id'));
        $poRealization = PoRealization::firstOrFail($id);

        $bankOption = Bank::getOption();

        $bankDetailOption = [];
        foreach ($bankOption as $key => $value) {
            $bankDetailOption = BankDetail::getOption($key);
            break;
        }

        $paymentMethodOption = PaymentMethod::getOption();

        $params = [
            'data' => $poRealization,
            'bankOption' => $bankOption,
            'bankDetailOption' => $bankDetailOption,
            'paymentMethodOption' => $paymentMethodOption,
        ];
        return $this->renderPartial('create-new', $params);
    }

    public function actionGetBankDetail()
    {
        $result = '';
        $bankId = intval(post_data('bank_id'));
        foreach (BankDetail::getOption($bankId) as $key => $value) {
            $result .= "<option value='$key'>$value</option>";
        }

        return $result;
    }

    public function actionSaveData()
    {
        $id = intval(post_data('id'));
        $poRealization = PoRealization::firstOrFail($id);

        $poPayment = new PoPayment();
        $poPayment->po_realization_id = $poRealization->id;
        $poPayment->bank_detail_id = post_data('bank_detail_id');
        $poPayment->payment_method_id = post_data('payment_method_id');
        $poPayment->comment = post_data('comment');
        $poPayment->time = date('Y-m-d H:i:s');

        if ($poPayment->save()) {
            $documentName = md5('payment' . $poPayment->id);

            $upload = upload('payment_document', PO_PAYMENT_PATH, $documentName, ['pdf']);
            if ($upload['error']) {
                $poPayment->delete();

                return alert_danger('Error! Gagal mengunggah file, payment gagal disimpan!<br>Pesan kesalahan : ' . $upload['msg']);
            } else {
                $payment_document = $upload['new_name'];
                $poPayment->payment_document = $payment_document;
                $poPayment->save();

                $realization = $poPayment->realization;
                $realization->is_paid = 1;
                $realization->verified_by_user_id = Auth::user()->id;
                $realization->save();

                return alert_success('Data payment berhasil disimpan!') .
                    "<script> reload(1500); </script>";
            }
        } else {
            return alert_danger('Terjadi kesalahan! Gagal menyimpan data pembayaran');
        }
    }

    public function actionDetail()
    {
        $id = intval(post_data('id'));
        $poRealization = PoRealization::firstOrFail($id);

        $params = [
            'data' => $poRealization,
            'payment' => $poRealization->payment
        ];
        return $this->renderPartial('detail', $params);
    }

    public function actionVoucherPaying()
    {
        $this->layout = 'print';
        $invoiceId = intval(get_data('id'));
        $poRealization = PoRealization::firstOrFail($invoiceId);

        // signature
        $signature = [
            'prepared' => null,
            'approved' => null,
            'verified' => null,
        ];

        // prepared
        if (!is_null($poRealization->preparedBy)) {
            $sign = $poRealization->preparedBy->signature_url;
            $signature['prepared'] = $sign;
        }

        // approved
        if (!is_null($poRealization->approvedBy)) {
            $sign = $poRealization->approvedBy->signature_url;
            $signature['approved'] = $sign;
        }

        // verified
        if (!is_null($poRealization->verifiedBy)) {
            $sign = $poRealization->verifiedBy->signature_url;
            $signature['verified'] = $sign;
        }

        $params = [
            'data' => $poRealization,
            'purchaseOrder' => $poRealization->purchaseOrder,
            'activeUser' => Auth::user(),
            'signature' => $signature
        ];
        return $this->render('voucher-paying', $params);
    }
}

?>
