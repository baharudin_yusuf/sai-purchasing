<?php
namespace app\controllers\po;

use app\controllers\NotificationController as Notification;
use app\controllers\po\ConstraintController as POConstraint;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\PoRevision;
use app\models\PoRevisionDetail;
use app\models\PurchaseOrder;
use app\models\PurchaseRequisitionDetail;

class ModifyController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionIndex()
    {
        $poId = intval(get_data('id'));
        $data = PurchaseOrder::firstOrFail($poId);
        if (!POConstraint::allowModify($data)) {
            parent::throw404Error();
        }

        $params = [
            'data' => $data,
            'activeUser' => Auth::user(),
        ];
        return $this->render('index', $params);
    }

    public function actionRevisionDetail()
    {
        $rev_id = intval(post_data('id'));

        $poRev = PoRevision::findOne($rev_id);
        if (is_null($poRev)) {
            parent::throw404Error();
        }

        $data['data'] = $poRev;
        return $this->renderPartial('revision-detail', $data);
    }

    public function actionApproveRevision()
    {
        $rev_id = intval(post_data('rev_id'));
        $revision = PoRevision::firstOrFail($rev_id);
        $purchaseOrder = $revision->purchaseOrder;

        if (POConstraint::allowApproveRevision($revision)) {
            $revision->is_approved_by_manager = 1;
            $revision->approved_by_manager_time = date('Y-m-d H:i:s');
            $revision->save();

            // UPDATE PO STATUS
            $purchaseOrder->is_revision = 1;
            $purchaseOrder->save();

            foreach ($purchaseOrder->purchaseRequisitionList as $poPr) {
                foreach ($poPr->actualData as $actual) {
                    $revDetail = PoRevisionDetail::findOne([
                        'po_revision_id' => $revision->id,
                        'po_realization_actual_id' => $actual->id
                    ]);
                    if (is_null($revDetail)) {
                        continue;
                    }

                    $diff = $actual->quantity - $revDetail->quantity;
                    $actual->quantity = $revDetail->quantity;
                    $actual->price = $revDetail->price;
                    $actual->save();

                    // UPDATE PR DATA
                    $prDetail = PurchaseRequisitionDetail::findOne([
                        'purchase_requisition_id' => $poPr->purchaseRequisition->id,
                        'goods_service_id' => $actual->goods_service_id
                    ]);
                    $prDetail->used_quantity -= $diff;
                    $prDetail->available_quantity = $prDetail->quantity - $prDetail->used_quantity;
                    $prDetail->save();
                }
                $poPr->purchaseRequisition->recheckClosedStatus();
            }

            // RECALCULATE PO AMOUNT REALIZATION
            $purchaseOrder->calculateAmountRealization();

            // SEND NOTIFICATION TO SECTION
            $notifMsg = Notificatixn::render('PO_REVISION_APPROVED', [
                'po_revision_id' => $revision->code,
                'purchase_order_id' => $purchaseOrder->code,
                'department_name' => Auth::user()->department->code
            ]);

            Notification::sendNotification([
                'category' => 'po-rev',
                'type' => 'success',
                'target_id' => $purchaseOrder->id,
                'title' => $notifMsg
            ]);

            return alert_success('Pengajuan revisi berhasil diterima!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menerima pengajuan revisi!');
        }
    }

    public function actionRejectRevision()
    {
        $rev_id = intval(post_data('rev_id'));
        $revision = PoRevision::firstOrFail($rev_id);
        $purchaseOrder = $revision->purchaseOrder;

        if (POConstraint::allowApproveRevision($revision)) {
            $revision->is_rejected = 1;
            $revision->save();

            $notifMsg = Notificatixn::render('PO_REVISION_REJECTED', [
                'po_revision_id' => $revision->code,
                'purchase_order_id' => $purchaseOrder->id,
                'department_name' => Auth::user()->department->name
            ]);

            Notification::sendNotification([
                'category' => 'po-rev',
                'type' => 'danger',
                'target_id' => $purchaseOrder->id,
                'title' => $notifMsg
            ]);

            return alert_success('Pengajuan revisi berhasil ditolak!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menolak pengajuan revisi!');
        }
    }

    public function actionCancelRevision()
    {
        $rev_id = intval(post_data('rev_id'));
        $revision = PoRevision::firstOrFail($rev_id);
        $purchaseOrder = $revision->purchaseOrder;

        if (POConstraint::allowCancelRevision($revision)) {
            $revision->is_canceled = 1;
            $revision->save();

            // SEND NOTIFICATION
            $notifMsg = Notificatixn::render('PO_REVISION_CANCELED', [
                'po_revision_id' => $revision->code,
                'purchase_order_id' => $purchaseOrder->code,
                'section_name' => Auth::user()->section->name
            ]);

            Notification::sendNotification([
                'category' => 'po-rev',
                'type' => 'warning',
                'target_id' => $purchaseOrder->id,
                'title' => $notifMsg
            ]);

            return alert_success('Pengajuan revisi berhasil dibatalkan!') .
                "<script> reload(1500); </script>";
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan revisi!');
        }
    }

    public function actionSave()
    {
        $poId = intval(post_data('id'));
        $po = PurchaseOrder::firstOrFail($poId);

        if (!POConstraint::allowModify($po)) {
            parent::throw404Error();
        }

        if (!$po->is_approved_by_lp_manager) {
            return self::updatePO($poId);
        }

        $diffList = [];
        foreach ($po->purchaseRequisitionList as $poPr) {
            foreach ($poPr->actualData as $actual) {
                $itemId = $actual->id;
                $postQuantity = post_data("quantity-$itemId");
                $postPrice = post_data("price-$itemId");

                if ($postQuantity != $actual->quantity || $postPrice != $actual->price) {
                    $diffList[] = $itemId;
                }
            }
        }

        if (empty($diffList)) {
            return alert_danger('Error! Tidak ada perubahan data tersedia!');
        }

        $transaction = \Yii::$app->db->beginTransaction();

        $revision = new PoRevision();
        $revision->purchase_order_id = $poId;
        $revision->date = date('Y-m-d H:i:s');

        if ($revision->save()) {
            foreach ($po->purchaseRequisitionList as $poPr) {
                foreach ($poPr->actualData as $actual) {
                    $itemId = $actual->id;
                    if (!in_array($itemId, $diffList)) {
                        continue;
                    }

                    $revisionDetail = new PoRevisionDetail();
                    $revisionDetail->po_revision_id = $revision->id;
                    $revisionDetail->po_realization_actual_id = $itemId;
                    $revisionDetail->quantity = intval(post_data("quantity-$itemId"));
                    $revisionDetail->price = floatval(post_data("price-$itemId"));
                    $revisionDetail->save();
                }
            }

            $transaction->commit();

            // SEND NOTIFICATION
            $notifMsg = Notificatixn::render('PO_REVISION_REQUEST_APPROVAL', [
                'po_revision_id' => $revision->code,
                'purchase_order_id' => $po->code,
                'section_name' => Auth::user()->section->name
            ]);

            Notification::sendNotification([
                'category' => 'po-rev',
                'type' => 'warning',
                'target_id' => $po->id,
                'title' => $notifMsg
            ]);

            $redirectUrl = url("po/main/detail/?id=$poId");
            return alert_success('Revisi PO berhasil disimpan!') .
                "<script> redirectTo('$redirectUrl', 1500); </script>";
        } else {
            $transaction->rollBack();
            return alert_danger('Error! Revisi PO gagal disimpan!');
        }
    }

    private function updatePO($poId)
    {
        $po = PurchaseOrder::firstOrFail($poId);
        foreach ($po->purchaseRequisitionList as $poPr) {
            foreach ($poPr->actualData as $actual) {
                $itemId = $actual->id;
                $postQuantity = post_data("quantity-$itemId");
                $postPrice = post_data("price-$itemId");

                if ($postQuantity != $actual->quantity || $postPrice != $actual->price) {
                    $actual->quantity = $postQuantity;
                    $actual->price = $postPrice;
                    $actual->save();
                }
            }
        }

        $po->refresh();
        $po->calculateAmountRealization();

        $redirectUrl = url("po/main/detail/?id=$poId");
        return alert_success('Revisi PO berhasil disimpan!') .
            "<script> redirectTo('$redirectUrl', 2000); </script>";
    }

    function actionPending()
    {
        $revision = PoRevision::find()
            ->where(['is_approved_by_manager' => 0, 'is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        $pr_list = [];
        foreach ($revision as $list) {
            $pr_list[] = $list->purchase_order_id;
        }

        $data['data'] = PurchaseOrder::find()
            ->where(['IN', 'id', $pr_list])
            ->all();

        return $this->render('pending', $data);
    }
}

?>
