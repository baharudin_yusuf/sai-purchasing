<?php
namespace app\controllers\po;

use app\controllers\BaseController;
use app\controllers\NotificationController as Notification;
use app\controllers\po\ConstraintController as POConstraint;
use app\helpers\Auth;
use app\helpers\Notificatixn;
use app\models\master\Section;
use app\models\PoRealization;
use app\models\PurchaseOrder;
use Yii;

class MainController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();
        parent::requirementCheck();

        $activeUser = Auth::user();
        $menu = ['Purchase Order' => url('po/main')];

        $lpSection = Section::findByCode('LP');
        if ($activeUser->roleAs('section') && $activeUser->section_id == $lpSection->id) {
            $menu['Create New'] = url('po/create');
        }

        if ($activeUser->roleAs('manager|admin')) {
            $menu['Pending Submission'] = url('po/main/pending');
            $menu['Pending Revision'] = url('po/modify/pending');
        }

        $menu['Report'] = [
            'Item Receiving' => url('po/report/item-receiving'),
            'Item Price' => url('po/report/item-price'),
            'Supplier Delivery' => url('po/report/supplier-delivery'),
        ];
        $menu['Item Receiving'] = url('po/receiving');

        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'purchase-order-panel');

        return [];
    }

    public function actionLoadData()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        if ($this->include_rejected) {
            unset($cond['is_rejected']);
        }
        if ($this->include_canceled) {
            unset($cond['is_canceled']);
        }
        if ($this->only_rejected) {
            $cond['is_rejected'] = 1;
        }
        if ($this->only_canceled) {
            $cond['is_canceled'] = 1;
        }

        $data = PurchaseOrder::find()->where($cond)->all();
        return $data;
    }

    public function actionIndex()
    {
        // redirect ke receiving jika section adalah ga/gs atau exim
        $activeUser = Auth::user();
        $section = $activeUser->section;
        if ($activeUser->roleAs('section')) {
            if ($section->is_exim || $section->is_ga_gs) {
                redirect(url('po/receiving'));
            }
        }

        $data = [
            'data' => self::actionLoadData(),
            'activeUser' => Auth::user(),
            'allowCreate' => ConstraintController::allowCreatePO()
        ];
        return $this->render('index', $data);
    }

    public function actionDetail()
    {
        $poId = intval(get_data('id'));
        $data = PurchaseOrder::firstOrFail($poId);
        $data->calculateAmountRealization();

        // redirect ke detail receiving jika section adalah ga/gs atau exim
        $activeUser = Auth::user();
        $section = $activeUser->section;
        if ($activeUser->roleAs('section')) {
            if ($section->is_exim || $section->is_ga_gs) {
                redirect(url("po/receiving/detail/?id=$poId"));
            }
        }

        $isReceiving = false;
        $sectionLP = Section::findByCode('LP');
        if ($activeUser->roleAs('section') && $activeUser->section_id != $sectionLP->id) {
            $isReceiving = true;
        }

        /*if (isset($_FILES['invoice'])) {
            self::uploadInvoice();
        } else if (post_data('po_realization_id')) {
            POPayment::saveData();
        }*/

        $params = [
            'data' => $data,
            'activeUser' => Auth::user(),
            'isReceiving' => $isReceiving,
            'allowCheck' => POConstraint::allowCheckPO($data),
            'allowApprovePO' => POConstraint::allowApprovePO($data),
            'allowUploadInvoice' => POConstraint::allowUploadInvoice($data),
            'allowModifyPO' => POConstraint::allowModify($data),
            'allowCancel' => POConstraint::allowCancel($data),
            'poConstraint' => POConstraint::class
        ];
        return $this->render('detail', $params);
    }

    public function actionUploadInvoice()
    {
        // hitung deadline pembayaran
        $activeUser = Auth::user();
        $poId = intval(post_data('id'));
        $po = PurchaseOrder::firstOrFail($poId);
        $supplier = $po->supplier;
        $deadline = date("Y-m-d", strtotime("+ " . $supplier->deadline_day . " day"));

        $poRealization = new PoRealization();
        $poRealization->supplier_invoice_number = post_data('supplier_invoice_number');
        $poRealization->is_final_payment = intval(post_data('final_payment'));
        $poRealization->purchase_order_id = $poId;
        $poRealization->time = date('Y-m-d H:i:s');
        $poRealization->deadline = $deadline;
        $poRealization->prepared_by_user_id = $activeUser->id;
        $poRealization->save();

        $id = $poRealization->id;
        $invoice = md5("po-realization-$id");

        $upload = upload('invoice', PO_REALIZATION_PATH, $invoice, ['pdf']);
        if ($upload['error']) {
            $poRealization->delete();
            return alert_danger('Error! Invoice gagal disimpan!<br>Pesan kesalahan : ' . $upload['msg']);
        } else {
            $poRealization->invoice = $invoice;
            $poRealization->save();

            // send notification to manager
            $notifMsg = Notificatixn::render('PO_INVOICE_APPROVAL', [
                'purchase_order_id' => $po->code,
                'invoice_id' => $poRealization->code,
                'section_name' => $activeUser->section->name
            ]);

            Notification::sendNotification([
                'category' => 'po',
                'type' => 'warning',
                'target_id' => $poId,
                'title' => $notifMsg
            ]);

            return alert_success('Invoice berhasil disimpan!') .
                "<script> reload(1500); </script>";
        }
    }

    public function actionApprovePo($poId = null)
    {
        $poId = is_null($poId) ? intval(post_data('id')) : $poId;
        $purchaseOrder = PurchaseOrder::firstOrFail($poId);
        if (POConstraint::allowApprovePO($purchaseOrder)) {
            $purchaseOrder->is_approved_by_lp_manager = 1;
            $purchaseOrder->approved_by_lp_manager_time = date('Y-m-d H:i:s');

            if ($purchaseOrder->save()) {
                /* RECALCULATE QUANTITY IN PR */
                $purchaseOrder->recalculatePrQuantity();

                // SEND NOTIF TO SECTION
                $notifMsg = Notificatixn::render('PO_APPROVED', [
                    'purchase_order_id' => $purchaseOrder->code,
                    'department_name' => Auth::user()->department->name
                ]);
                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'success',
                    'target_id' => $poId,
                    'title' => $notifMsg
                ]);

                // SEND NOTIF TO GA/GS ATAU EXIM
                $notifMsg = Notificatixn::render('PO_RECEIVING_READY', [
                    'purchase_order_id' => $purchaseOrder->code
                ]);
                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'receiving-ready',
                    'target_id' => $poId,
                    'title' => $notifMsg
                ]);

                return alert_success('Purchase order berhasil diterima!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Purchase order gagal diterima!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menerima PO!');
        }
    }

    public function actionRejectPo($poId = null)
    {
        $poId = is_null($poId) ? intval(post_data('id')) : $poId;
        $purchaseOrder = PurchaseOrder::firstOrFail($poId);
        if (POConstraint::allowApprovePO($purchaseOrder)) {
            $purchaseOrder->is_rejected = 1;
            if ($purchaseOrder->save()) {
                foreach ($purchaseOrder->purchaseRequisitionList as $prList) {
                    $purchaseRequisition = $prList->purchaseRequisition;
                    $purchaseRequisition->is_closed = 0;
                    $purchaseRequisition->is_direct_purchase = 0;
                    $purchaseRequisition->is_purchase_order = 0;
                    $purchaseRequisition->save();
                }

                // SEND NOTIFICATION TO SECTION
                $notifMsg = Notificatixn::render('PO_REJECTED', [
                    'purchase_order_id' => $purchaseOrder->code,
                    'department_name' => Auth::user()->department->name
                ]);
                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'danger',
                    'target_id' => $poId,
                    'title' => $notifMsg
                ]);

                return alert_success('Purchase Requisition berhasil ditolak!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Purchase Requisition gagal ditolak!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk menolak PO!');
        }
    }

    public function actionCancelPo()
    {
        $poId = intval(post_data('id'));
        $purchaseOrder = PurchaseOrder::firstOrFail($poId);
        if (POConstraint::allowCancel($purchaseOrder)) {
            $purchaseOrder->is_canceled = 1;
            if ($purchaseOrder->save()) {
                // SEND NOTIFICATION TO MANAGER
                $notifMsg = Notificatixn::render('PO_CANCELED', [
                    'purchase_order_id' => $purchaseOrder->code,
                    'section_name' => Auth::user()->section->name
                ]);
                Notification::sendNotification([
                    'category' => 'po',
                    'type' => 'warning',
                    'target_id' => $poId,
                    'title' => $notifMsg
                ]);

                return alert_success('Pengajuan purchase order berhasil dibatalkan!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Pengajuan purchase order gagal dibatalkan!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk membatalkan pengajuan PO');
        }
    }

    public function actionCheckPo()
    {
        $poId = intval(post_data('id'));
        $purchaseOrder = PurchaseOrder::firstOrFail($poId);
        if (POConstraint::allowCheckPO($purchaseOrder)) {
            $purchaseOrder->is_checked = 1;
            if ($purchaseOrder->save()) {
                return alert_success('Pengajuan purchase order berhasil ditandai sebagai sudah dicek!') .
                    "<script> reload(1500); </script>";
            } else {
                return alert_danger('Error! Pengajuan purchase order gagal ditandai sebagai sudah dicek!');
            }
        } else {
            return alert_danger('Error! Anda tidak memiliki hak akses untuk mengecek PO!');
        }
    }

    public function actionPending()
    {
        $data['data'] = PurchaseOrder::find()
            ->where(['is_approved_by_lp_manager' => 0, 'is_rejected' => 0, 'is_canceled' => 0])
            ->all();

        return $this->render('pending', $data);
    }

    public function actionBulkAction()
    {
        $action = post_data('action');
        $nApproved = 0;
        $nRejected = 0;

        $listId = explode(',', post_data('list_id'));
        foreach ($listId as $id) {
            if ($action == 'approve') {
                $this->actionApprovePo($id);
                $nApproved++;
            } else {
                $this->actionRejectPo($id);
                $nRejected++;
            }
        }

        if ($nApproved > 0) {
            $msg = "$nApproved pengajuan data berhasil diterima";
        } else if ($nRejected > 0) {
            $msg = "$nRejected pengajuan data berhasil ditolak";
        } else {
            $msg = 'Terjadi kesalahan! Data gagal diproses';
        }

        return "<script> alert('$msg'); reload(); </script>";
    }

    public function actionUnfinished()
    {
        $now = date('Y-m-d H:i:s');
        $data['data'] = PurchaseOrder::find()
            ->where(['is_closed' => 0])
            ->andWhere(['<', 'estimated_time_arival', $now])
            ->all();

        return $this->render('unfinished', $data);
    }

    public static function checkDueDate()
    {
        $now = date('Y-m-d H:i:s');
        $unreceived = PurchaseOrder::find()
            ->where(['and', 'is_closed=0', "estimated_time_arival<'$now'"])
            ->all();
        $unreceived = count($unreceived);

        if ($unreceived > 0) {
            parent::setWarningPane("Terdapat $unreceived Purchase Order yang belum terselesaikan! <a href='" . url("po/main/unfinished") . "'><b>[Detail]</b></a>");
        }
    }
}

?>
