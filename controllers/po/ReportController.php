<?php
namespace app\controllers\po;

use app\models\PurchaseOrder;

class ReportController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();

        return [];
    }

    public function actionSupplierDelivery()
    {
        $data['data'] = PurchaseOrder::find()
            ->where(['is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        return $this->render('supplier-delivery', $data);
    }

    public function actionDownloadSupplierDelivery()
    {
        $objPHPExcel = new \PHPExcel();

        $style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );

        /* header file */
        $header[] = ['PO ID', 'SUPPLIER', 'EST.TIME ARRIVAL', 'ACT.TIME ARRIVAL', 'ONTIME'];
        for ($i = 0; $i < count($header); $i++) {
            $row = $i + 1;
            $coll = 'A';

            for ($j = 0; $j < count($header[$i]); $j++) {
                $cell = $coll . $row;
                $objPHPExcel->getActiveSheet()->SetCellValue($cell, $header[$i][$j]);
                $coll++;
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($style);

        $data = PurchaseOrder::find()
            ->where(['is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        $row = count($header) + 1;
        foreach ($data as $list) {
            $col = 'A';

            if (strlen($list->actual_time_arival) != 0) {
                $actual_time_arival = date('d/m/Y H:i:s', strtotime($list->actual_time_arival));
            } else {
                $list->actual_time_arival = '-';
            }

            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->code);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->supplier->name);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", date('d/m/Y H:i:s', strtotime($list->estimated_time_arival)));
            $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->actual_time_arival);
            $objPHPExcel->getActiveSheet()->SetCellValue(($col) . "$row", $list->is_ontime ? 'YES' : 'NO');

            $objPHPExcel->getActiveSheet()->getStyle("A$row:$col$row")->applyFromArray($style);

            $row++;
        }

        $filename = 'PO-SUPPLIER-DELIVERY-' . date('dmY') . '.xlsx';
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(TEMP_DIR . $filename);

        return \Yii::$app->response->sendFile(TEMP_DIR . $filename);
    }

    public function actionItemPrice()
    {
        $data['data'] = PurchaseOrder::find()
            ->where(['is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        return $this->render('item-price', $data);
    }

    public function actionDownloadItemPrice()
    {
        $objPHPExcel = new \PHPExcel();

        $style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );

        /* header file */
        $header[] = ['PO ID', 'PR ID', 'SUPPLIER', 'GOODS SERVICE', 'QUANTITY', 'CURRENCY', 'PRICE'];
        for ($i = 0; $i < count($header); $i++) {
            $row = $i + 1;
            $coll = 'A';

            for ($j = 0; $j < count($header[$i]); $j++) {
                $cell = $coll . $row;
                $objPHPExcel->getActiveSheet()->SetCellValue($cell, $header[$i][$j]);
                $coll++;
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:G1")->applyFromArray($style);

        $data = PurchaseOrder::find()
            ->where(['is_canceled' => 0, 'is_rejected' => 0])
            ->all();

        $row = count($header) + 1;
        foreach ($data as $list) {
            foreach ($list->purchaseRequisitionList as $prList) {
                foreach ($prList->actualData as $actual) {
                    $col = 'A';

                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->code);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->purchaseRequisition->code);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->supplier->name);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->goodsService->name);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->quantity);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->goodsService->currency->name);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col) . "$row", $actual->price);

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:$col$row")->applyFromArray($style);
                    $row++;
                }
            }
        }

        $filename = 'PO-REPORT-ITEM-PRICE-' . date('dmY') . '.xlsx';
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(TEMP_DIR . $filename);

        return \Yii::$app->response->sendFile(TEMP_DIR . $filename);
    }

    public function actionItemReceiving()
    {
        $data = self::getReceivingData();
        return $this->render('item-receiving', $data);
    }

    private function getReceivingData()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        $params = [];
        if (post_data('only-completely-received')) {
            set_session('po_only_completely_received', true);
            $cond['is_received'] = 1;
            $params['only_completely_received'] = 1;
        } else {
            set_session('po_only_completely_received', false);
            $params['only_completely_received'] = 0;
        }

        if (post_data('only-completely-delivered')) {
            set_session('po_only_completely_delivered', true);
            $cond['is_delivered'] = 1;
            $params['only_completely_delivered'] = 1;
        } else {
            set_session('po_only_completely_delivered', false);
            $params['only_completely_delivered'] = 0;
        }

        $params['data'] = PurchaseOrder::find()->where($cond)->all();
        return $params;
    }

    public function actionDownloadReceiving()
    {
        $objPHPExcel = new \PHPExcel();

        $style = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '000000')
                )
            )
        );

        /* header file */
        $header[] = ['PO ID', 'PR ID', 'GOODS SERVICE', 'QUANTITY', 'RECEIVED', 'DELIVERED'];
        for ($i = 0; $i < count($header); $i++) {
            $row = $i + 1;
            $coll = 'A';

            for ($j = 0; $j < count($header[$i]); $j++) {
                $cell = $coll . $row;
                $objPHPExcel->getActiveSheet()->SetCellValue($cell, $header[$i][$j]);
                $coll++;
            }
        }
        $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($style);

        $data = self::getReceivingData();
        $row = count($header) + 1;
        foreach ($data['data'] as $list) {
            foreach ($list->purchaseRequisitionList as $prList) {
                foreach ($prList->actualData as $actual) {
                    $col = 'A';

                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $list->code);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->purchaseRequisition->code);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->goodsService->name);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->quantity);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col++) . "$row", $actual->received);
                    $objPHPExcel->getActiveSheet()->SetCellValue(($col) . "$row", $actual->delivered);

                    $objPHPExcel->getActiveSheet()->getStyle("A$row:$col$row")->applyFromArray($style);
                    $row++;
                }
            }

        }

        $filename = 'PO-REPORT-RECEIVING-' . date('dmY') . '.xlsx';
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save(TEMP_DIR . $filename);

        return \Yii::$app->response->sendFile(TEMP_DIR . $filename);
    }
}

?>
