<?php
namespace app\controllers\po;

use app\helpers\Auth;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\master\Budget;
use app\models\master\Department;
use app\models\master\Section;
use app\models\PoPrDetail;
use app\models\PoRealization;
use app\models\PoRealizationActual;
use app\models\PoRevision;
use app\models\PurchaseOrder;
use app\models\PurchaseRequisition;

class ConstraintController extends MainController
{
    public function behaviors()
    {
        parent::behaviors();
        return [];
    }

    public static function allowCreatePO()
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        }

        $lpSection = Section::findByCode('LP');
        if ($activeUser->section->id != $lpSection->id) {
            return false;
        }

        return true;
    }

    public static function allowApprovePO(PurchaseOrder $purchaseOrder)
    {
        $activeUser = Auth::user();
        $isApprovedByManager = $purchaseOrder->is_approved_by_lp_manager;
        $isRejected = $purchaseOrder->is_rejected;
        $isCanceled = $purchaseOrder->is_canceled;

        if ($isApprovedByManager || $isRejected || $isCanceled) {
            return false;
        } else if ($activeUser->roleAs('manager')) {
            $deptHRGA = Department::findByCode('HRGA');
            if ($activeUser->department_id == $deptHRGA->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowApproveInvoice(PoRealization $poRealization)
    {
        if ($poRealization->is_approved_by_manager) {
            return false;
        } else if ($poRealization->is_rejected) {
            return false;
        } else if ($poRealization->is_canceled) {
            return false;
        }

        $activeUser = Auth::user();
        if ($activeUser->roleAs('manager')) {
            $deptHRGA = Department::findByCode('HRGA');
            if ($activeUser->department_id == $deptHRGA->id) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function allowUploadInvoice(PurchaseOrder $purchaseOrder)
    {
        if (!$purchaseOrder->is_approved_by_lp_manager) {
            return false;
        } else if ($purchaseOrder->is_payment_complete) {
            return false;
        }

        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        }

        $lpSection = Section::findByCode('LP');
        if ($activeUser->section_id != $lpSection->id) {
            return false;
        }

        return true;
    }

    public static function allowCancel(PurchaseOrder $purchaseOrder)
    {
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');
        if (!$activeUser->roleAs('section') || $activeUser->section_id != $lpSection->id) {
            return false;
        } else {
            if ($purchaseOrder->is_approved_by_lp_manager) {
                return false;
            } else if ($purchaseOrder->is_canceled || $purchaseOrder->is_rejected) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static function allowReceiveItem($item_id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $PR = PoRealizationActual::findOne($item_id);
            $poDetail = PoPrDetail::findOne($PR->po_pr_detail_id);
            $pr_id = $poDetail->purchase_requisition_id;
            $po_id = $poDetail->purchase_order_id;

            $po = PurchaseOrder::findOne($po_id);

            if ($po->is_rejected) {
                return false;
            } else if ($po->is_canceled) {
                return false;
            } else if ($po->is_received) {
                return false;
            } else if ($po->is_delivered) {
                return false;
            } else if ($po->is_approved_by_lp_manager) {
                $Section = $activeUser->section;

                if ($Section->is_exim || $Section->is_ga_gs) {
                    $PR = PurchaseRequisition::findOne($pr_id);
                    $final_id = $PR->budget_final_id;

                    $Final = BudgetFinal::findOne($final_id);
                    $proposal_id = $Final->budget_proposal_id;

                    $Proposal = BudgetProposal::findOne($proposal_id);
                    $budget_id = $Proposal->budget_id;

                    /* cek apakah packaging component */
                    $Budget = Budget::findOne($budget_id);
                    $packaging_component = $Budget->is_packaging_component;

                    if ($packaging_component) {
                        if ($Section->is_exim) return true;
                        else return false;
                    } else {
                        if ($Section->is_ga_gs) return true;
                        else return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static function allowDeliverItem($item_id)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $PR = PoRealizationActual::findOne($item_id);
            $poDetail = PoPrDetail::findOne($PR->po_pr_detail_id);
            $pr_id = $poDetail->purchase_requisition_id;
            $po_id = $poDetail->purchase_order_id;

            $po = PurchaseOrder::findOne($po_id);

            if ($po->is_rejected) {
                return false;
            } else if ($po->is_canceled) {
                return false;
            } else if ($po->is_delivered) {
                return false;
            } else if ($po->is_approved_by_lp_manager) {
                $PR = PurchaseRequisition::findOne($pr_id);
                $final_id = $PR->budget_final_id;

                $Final = BudgetFinal::findOne($final_id);
                $proposal_id = $Final->budget_proposal_id;

                $Proposal = BudgetProposal::findOne($proposal_id);
                $section_id = $Proposal->section_id;

                if ($activeUser->section_id == $section_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public static function allowCancelInvoice(PoRealization $poRealization)
    {
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');

        if (!$activeUser->roleAs('section')) {
            return false;
        } else if ($activeUser->section_id != $lpSection->id) {
            return false;
        } else if ($poRealization->is_approved_by_manager) {
            return false;
        } else if ($poRealization->is_rejected) {
            return false;
        } else if ($poRealization->is_canceled) {
            return false;
        } else if ($poRealization->is_paid) {
            return false;
        } else {
            return true;
        }
    }

    public static function allowPayInvoice(PoRealization $poRealization)
    {
        $activeUser = Auth::user();
        $faDept = Department::findByCode('FA');

        if (!$activeUser->roleAs('manager')) {
            return false;
        } else if ($activeUser->department_id != $faDept->id) {
            return false;
        } else if ($poRealization->is_paid) {
            return false;
        } else {
            return true;
        }
    }

    public static function allowModify(PurchaseOrder $purchaseOrder)
    {
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');
        if (!$activeUser->roleAs('section') || $activeUser->section_id != $lpSection->id) {
            return false;
        } else {
            if (is_null($purchaseOrder)) {
                return false;
            } else if ($purchaseOrder->is_closed) {
                return false;
            } else if ($purchaseOrder->is_rejected) {
                return false;
            } else if ($purchaseOrder->is_received) {
                return false;
            } else if ($purchaseOrder->is_delivered) {
                return false;
            } else if ($purchaseOrder->is_canceled) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static function allowCancelRevision(PoRevision $poRevision)
    {
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');

        if (!$activeUser->roleAs('section') || $activeUser->section_id != $lpSection->id) {
            return false;
        } else if ($poRevision->is_approved_by_manager) {
            return false;
        } else if ($poRevision->is_canceled) {
            return false;
        } else if ($poRevision->is_rejected) {
            return false;
        } else {
            return true;
        }
    }

    public static function allowApproveRevision(PoRevision $poRevision)
    {
        $activeUser = Auth::user();
        $lpSection = Section::findByCode('LP');

        if (!$activeUser->roleAs('manager')) {
            return false;
        } else if ($lpSection->department_id != $activeUser->department_id) {
            return false;
        } else if ($poRevision->is_approved_by_manager) {
            return false;
        } else if ($poRevision->is_canceled) {
            return false;
        } else if ($poRevision->is_rejected) {
            return false;
        } else {
            return true;
        }
    }

    public static function allowCheckPO(PurchaseOrder $purchaseOrder)
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else if ($purchaseOrder->is_checked) {
            return false;
        } else {
            if ($activeUser->is_supervisor) {
                $lpSection = Section::findByCode('LP');
                if ($activeUser->section_id == $lpSection->id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }
}

?>
