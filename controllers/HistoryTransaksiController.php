<?php
namespace app\controllers;

use Yii;

class HistoryTransaksiController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Vendor' => url('history/transaksi/vendor/index'),
        ];
        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'history-panel');

        return [];
    }

    public function actionIndex()
    {
        $redirectUrl = url('history/transaksi/vendor/index');
        redirect($redirectUrl);
    }
}

?>
