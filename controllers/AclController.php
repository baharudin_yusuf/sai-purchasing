<?php
namespace app\controllers;

use app\helpers\Auth;
use app\models\ACLAccess;
use app\models\ACLList;
use app\models\master\Department;
use app\models\master\Section;
use app\models\Role;

class AclController extends AdminController
{
    public function behaviors()
    {
        parent::behaviors();

        if (!Auth::user()->roleAs('admin')) {
            parent::throw404Error();
        }

        return [];
    }

    public function actionIndex()
    {
        $params = [
            'data' => Role::find()->where(['<>', 'alias', 'admin'])->all(),
            'department' => Department::find()->all(),
            'section' => Section::find()->all()
        ];
        return $this->render('index', $params);
    }

    public function actionModify()
    {
        $id = post_data('id');
        $role = Role::findOne($id);
        if (is_null($role)) {
            parent::throw404Error();
        }

        $deptId = intval(post_data('department_id'));
        if ($deptId > 0) {
            $department = Department::findOne($deptId);
            if (is_null($department)) {
                parent::throw404Error();
            }
        }

        $sectId = intval(post_data('section_id'));
        if ($sectId > 0) {
            $section = Section::findOne($sectId);
            if (is_null($section)) {
                parent::throw404Error();
            }
        }

        $aclList = [];
        $aclGroup = ACLList::find()->groupBy('module')->all();
        foreach ($aclGroup as $group) {
            $aclChild = ACLList::find()->where(['module' => $group->module])->all();
            if (count($aclChild) == 0) {
                continue;
            }

            $aclList[$group->module] = [];
            foreach ($aclChild as $child) {
                $aclList[$group->module][] = $child->module_item;
            }
        }

        $params = [
            'role' => $role,
            'aclList' => $aclList,
            'departmentId' => $deptId,
            'sectionId' => $sectId,
        ];
        return $this->renderPartial('modify', $params);
    }

    public function actionSetAccess()
    {
        $roleId = post_data('role_id');
        $moduleName = post_data('module');
        $moduleItem = post_data('item');
        $departmentId = intval(post_data('department_id'));
        $sectionId = intval(post_data('section_id'));

        $acl = ACLList::findOne(['module' => $moduleName, 'module_item' => $moduleItem]);
        if (is_null($acl)) {
            return 'off';
        }

        $condition = [
            'acl_list_id' => $acl->id,
            'role_id' => $roleId,
            'department_id' => $departmentId == 0 ? null : $departmentId,
            'section_id' => $sectionId == 0 ? null : $sectionId
        ];

        $access = ACLAccess::findOne($condition);
        if (is_null($access)) {
            $access = new ACLAccess();
            $access->updateAttributes($condition);
            $access->is_access = 0;
            $access->save();
        }

        if ($access->is_access) {
            $access->is_access = 0;
            $access->save();
            return 'off';
        } else {
            $access->is_access = 1;
            $access->save();
            return 'on';
        }
    }
}

?>
