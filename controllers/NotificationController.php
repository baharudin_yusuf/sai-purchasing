<?php
namespace app\controllers;

use app\models\BudgetAdditional;
use app\models\BudgetFinal;
use app\models\BudgetProposal;
use app\models\BudgetTransfer;
use app\models\DirectPurchase;
use app\models\master\Department;
use app\models\master\Section;
use app\models\Notification;
use app\models\PurchaseOrder;
use app\models\PurchaseRequisition;
use app\models\Role;
use app\models\User;
use Yii;

class NotificationController extends BaseController
{
    public function behaviors()
    {
        parent::behaviors();

        $menu = [
            'Registration' => url('user/register'),
            'Login' => url('user/login'),
        ];
        Yii::$app->params['sidebarMenu'] = $this->processMenu($menu, 'unsorted');

        return [];
    }

    public function actionDetail()
    {
        $id = intval(get_data('id'));
        $notification = Notification::findOne($id);
        $type = $notification->type;
        $category = $notification->category;
        $target_id = $notification->target_id;

        if ($category == 'proposal') {
            redirect(url('proposal/main/detail/?id=' . $target_id));
        } else if ($category == 'pr') {
            redirect(url('pr/main/detail/?id=' . $target_id));
        } else if ($category == 'pr-rev') {
            redirect(url('pr/main/detail/?id=' . $target_id));
        } else if ($category == 'po') {
            if (in_array($type, ['receiving-ready', 'delivering-ready'])) {
                redirect(url('po/receiving/detail/?id=' . $target_id));
            } else {
                redirect(url('po/main/detail/?id=' . $target_id));
            }
        } else if ($category == 'po-rev') {
            redirect(url('po/main/detail/?id=' . $target_id));
        } else if ($category == 'transfer') {
            redirect(url('proposal/transfer/detail/?id=' . $target_id));
        } else if ($category == 'additional') {
            redirect(url('proposal/additional/detail/?id=' . $target_id));
        } else if ($category == 'dp') {
            if (in_array($type, ['receiving-ready', 'delivering-ready'])) {
                redirect(url('dp/receiving/detail/?id=' . $target_id));
            } else {
                redirect(url('dp/main/detail/?id=' . $target_id));
            }
        }
    }

    public static function sendNotification($data)
    {
        if (!isset($data['category'])) {
            return ['error' => true, 'msg' => 'Undefined category!'];
        }

        if (!isset($data['target_id'])) {
            return ['error' => true, 'msg' => 'Undefined target id!'];
        }

        if (!isset($data['type'])) {
            return ['error' => true, 'msg' => 'Undefined type!'];
        }

        $userList = self::getUserList($data);
        if (count($userList) == 0) {
            return ['error' => true, 'msg' => 'No target user found!'];
        }

        $email_list = [];
        foreach ($userList as $userId) {
            $notification = new Notification();

            $notification->user_id = $userId;
            $notification->type = $data['type'];
            $notification->title = $data['title'];
            $notification->category = $data['category'];
            $notification->target_id = $data['target_id'];
            $notification->date = date('Y-m-d H:i:s');
            $notification->is_read = 0;

            $notification->save();

            $user = User::findOne(['id' => $userId]);
            $email_list[] = $user->email;
        }

        /* SEND NOTIFICATION TO EMAIL */
        self::emailNotification($data['title'], $email_list);
        /* /SEND NOTIFICATION TO EMAIL */

        return ['error' => false, 'msg' => 'Notification sent!'];
    }

    private static function emailNotification($message, $userList)
    {
        $mail_data = [
            'subject' => 'SAI NOTIFICATION',
            'pengirim' => ADMIN_EMAIL,
            'pesan' => Yii::$app->controller->renderPartial('/notification/notif', ['message' => $message]),
            'tujuan' => implode(',', $userList),
        ];

        MailerController::sendMail($mail_data);
    }

    private static function getUserList($data)
    {
        $category = $data['category'];
        $target_id = $data['target_id'];
        $type = $data['type'];
        extract($data);

        if ($category == 'proposal') {
            $budgetProposal = BudgetProposal::findOne($target_id);
            $sectionId = $budgetProposal->section_id;
            $isApprovedByManager = $budgetProposal->is_approved_by_manager;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */

                if (!$isApprovedByManager) {
                    /* target adalah manajernya */
                    $section = Section::findOne($sectionId);
                    $departmentId = $section->department_id;

                    $role = Role::findByAlias('manager');
                    $users = User::find()->where(['department_id' => $departmentId, 'role' => $role->id])->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                } else {
                    /* target adalah manajer FA */
                    $department = Department::findByCode('FA');
                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $department->id, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        } else if ($category == 'pr') {
            $purchaseRequisition = PurchaseRequisition::findOne($target_id);

            $sectionId = $purchaseRequisition->section_id;
            $isApprovedByManager = $purchaseRequisition->is_approved_by_manager;
            $isApprovedByDFM = $purchaseRequisition->is_approved_by_dfm;
            $isApprovedByFM = $purchaseRequisition->is_approved_by_fm;
            $isApprovedByPresdir = $purchaseRequisition->is_approved_by_presdir;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */
                if (!$isApprovedByManager) {
                    /* target adalah manajernya */
                    $section = Section::findOne($sectionId);
                    $departmentId = $section->department_id;
                    $role = Role::findByAlias('manager');

                    $users = User::find()
                        ->where(['department_id' => $departmentId, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                } else if (!$isApprovedByDFM || !$isApprovedByFM || !$isApprovedByPresdir) {
                    /* target adalah FM, DFM, atau PRESDIR */
                    if (!$isApprovedByDFM) {
                        $role = Role::findByAlias('dfm');
                    } else if (!$isApprovedByFM) {
                        $role = Role::findByAlias('fm');
                    } else {
                        $role = Role::findByAlias('presdir');
                    }

                    $users = User::find()
                        ->where(['role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                } else {
                    /* target adalah manajer FA */
                    $department = Department::findByCode('FA');
                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $department->id, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        } else if ($category == 'po') {
            $section = Section::findByCode('LP');
            $sectionId = $section->id;
            $departmentId = $section->department_id;

            $purchaseOrder = PurchaseOrder::findOne($target_id);
            $is_approved_by_lp_manager = $purchaseOrder->is_approved_by_lp_manager;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */
                $role = Role::findByAlias('manager');
                $users = User::find()
                    ->where(['department_id' => $departmentId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            } else if ($type == 'warning-to-fa') {
                /* target adalah manajer FA */
                $department = Department::findByCode('FA');
                $role = Role::findByAlias('manager');
                $users = User::find()
                    ->where(['department_id' => $department->id, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            } else if ($type == 'receiving-ready') {
                /* target adalah GA/GS atau EXIM */
                $userExim = [];
                $exim = Section::findOne(['is_exim' => 1]);
                if (!is_null($exim)) {
                    $eximList = User::find()->where(['section_id' => $exim->id])->all();
                    foreach ($eximList as $item) {
                        if (!in_array($item->id, $userExim)) {
                            $userExim[] = $item->id;
                        }
                    }
                }

                $userGags = [];
                $gags = Section::findOne(['is_ga_gs' => 1]);
                if (!is_null($gags)) {
                    $gagsList = User::find()->where(['section_id' => $gags->id])->all();
                    foreach ($gagsList as $item) {
                        if (!in_array($item->id, $userGags)) {
                            $userGags[] = $item->id;
                        }
                    }
                }

                $eximExist = false;
                $gagsExist = false;
                $po = PurchaseOrder::findOne($target_id);
                foreach ($po->purchaseRequisitionList as $prList) {
                    $pr = $prList->purchaseRequisition;
                    $budget = $pr->budget;

                    if ($budget->is_packaging_component) {
                        $eximExist = true;
                    } else {
                        $gagsExist = true;
                    }
                }

                $userList = [];
                if ($eximExist) {
                    foreach ($userExim as $userId) {
                        if (!in_array($userId, $userList)) {
                            $userList[] = $userId;
                        }
                    }
                }
                if ($gagsExist) {
                    foreach ($userGags as $userId) {
                        if (!in_array($userId, $userList)) {
                            $userList[] = $userId;
                        }
                    }
                }
            } else if ($type == 'delivering-ready') {
                $userList = [];
                $sectionList = $data['section_list'];
                $sectionUser = User::find()->where(['section_id' => $sectionList])->all();
                foreach ($sectionUser as $item) {
                    if (!in_array($item->id, $userList)) {
                        $userList[] = $item->id;
                    }
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        } else if ($category == 'pr-rev' || $category == 'po-rev') {
            $section = Section::find()->where(['code' => parent::getSectionCode('LP')])->one();
            $sectionId = $section->id;
            $departmentId = $section->department_id;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */
                $role = Role::findByAlias('manager');
                $users = User::find()
                    ->where(['department_id' => $departmentId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        } else if ($category == 'transfer') {
            $budgetTransfer = BudgetTransfer::findOne($target_id);
            $source_final_id = $budgetTransfer->source;
            $destination_final_id = $budgetTransfer->destination;
            $is_approved_by_source_manager = $budgetTransfer->is_approved_by_source_manager;
            $is_approved_by_destination_manager = $budgetTransfer->is_approved_by_destination_manager;
            $is_approved_by_fa = $budgetTransfer->is_approved_by_fa;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */

                if (!$is_approved_by_source_manager) {
                    $budgetFinal = BudgetFinal::findOne($source_final_id);
                    $budgetProposalId = $budgetFinal->budget_proposal_id;

                    $budgetProposal = BudgetProposal::findOne($budgetProposalId);
                    $sectionId = $budgetProposal->section_id;

                    $section = Section::findOne($sectionId);
                    $departmentId = $section->department_id;

                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $departmentId, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                } else if (!$is_approved_by_destination_manager) {
                    $budgetFinal = BudgetFinal::findOne($destination_final_id);
                    $budgetProposalId = $budgetFinal->budget_proposal_id;

                    $budgetProposal = BudgetProposal::findOne($budgetProposalId);
                    $sectionId = $budgetProposal->section_id;

                    $section = Section::findOne($sectionId);
                    $departmentId = $section->department_id;

                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $departmentId, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                } else {
                    /* target adalah manajer FA */
                    $department = Department::findByCode('FA');
                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $department->id, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $budgetFinal = BudgetFinal::findOne($source_final_id);
                $budgetProposalId = $budgetFinal->budget_proposal_id;

                $budgetProposal = BudgetProposal::findOne($budgetProposalId);
                $sectionId = $budgetProposal->section_id;
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        } else if ($category == 'additional') {
            $BudgetAdditional = BudgetAdditional::findOne($target_id);
            $budget_final_id = $BudgetAdditional->budget_final_id;
            $isApprovedByManager = $BudgetAdditional->is_approved_by_manager;
            $is_approved_by_fa = $BudgetAdditional->is_approved_by_fa;

            $budgetFinal = BudgetFinal::findOne($budget_final_id);
            $budgetProposalId = $budgetFinal->budget_proposal_id;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */

                if (!$isApprovedByManager) {
                    $budgetProposal = BudgetProposal::findOne($budgetProposalId);
                    $sectionId = $budgetProposal->section_id;
                    $section = Section::findOne($sectionId);
                    $departmentId = $section->department_id;

                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $departmentId, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                } else {
                    /* target adalah manajer FA */
                    $department = Department::findByCode('FA');
                    $role = Role::findByAlias('manager');
                    $users = User::find()
                        ->where(['department_id' => $department->id, 'role' => $role->id])
                        ->all();

                    $userList = [];
                    foreach ($users as $list) {
                        $userList[] = $list->id;
                    }
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $budgetProposal = BudgetProposal::findOne($budgetProposalId);
                $sectionId = $budgetProposal->section_id;
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        } else {
            $section = Section::find()->where(['code' => parent::getSectionCode('LP')])->one();
            $sectionId = $section->id;
            $departmentId = $section->department_id;

            if ($type == 'warning') {
                /* notif bahwa ada pengajuan */
                $role = Role::findByAlias('manager');
                $users = User::find()
                    ->where(['department_id' => $departmentId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            } else if ($type == 'warning-to-fa') {
                /* target adalah manajer FA */
                $department = Department::findByCode('FA');
                $role = Role::findByAlias('manager');
                $users = User::find()
                    ->where(['department_id' => $department->id, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            } else if ($type == 'receiving-ready') {
                /* target adalah GA/GS atau EXIM */
                $userExim = [];
                $exim = Section::findOne(['is_exim' => 1]);
                if (!is_null($exim)) {
                    $eximList = User::find()->where(['section_id' => $exim->id])->all();
                    foreach ($eximList as $item) {
                        if (!in_array($item->id, $userExim)) {
                            $userExim[] = $item->id;
                        }
                    }
                }

                $userGags = [];
                $gags = Section::findOne(['is_ga_gs' => 1]);
                if (!is_null($gags)) {
                    $gagsList = User::find()->where(['section_id' => $gags->id])->all();
                    foreach ($gagsList as $item) {
                        if (!in_array($item->id, $userGags)) {
                            $userGags[] = $item->id;
                        }
                    }
                }

                $eximExist = false;
                $gagsExist = false;
                $dp = DirectPurchase::findOne($target_id);
                foreach ($dp->purchaseRequisitionList as $prList) {
                    $pr = $prList->purchaseRequisition;
                    $budget = $pr->budget;

                    if ($budget->is_packaging_component) {
                        $eximExist = true;
                    } else {
                        $gagsExist = true;
                    }
                }

                $userList = [];
                if ($eximExist) {
                    foreach ($userExim as $userId) {
                        if (!in_array($userId, $userList)) {
                            $userList[] = $userId;
                        }
                    }
                }
                if ($gagsExist) {
                    foreach ($userGags as $userId) {
                        if (!in_array($userId, $userList)) {
                            $userList[] = $userId;
                        }
                    }
                }
            } else if ($type == 'delivering-ready') {
                $userList = [];
                $sectionList = $data['section_list'];
                $sectionUser = User::find()->where(['section_id' => $sectionList])->all();
                foreach ($sectionUser as $item) {
                    if (!in_array($item->id, $userList)) {
                        $userList[] = $item->id;
                    }
                }
            } else {
                /* notif bahwa pengajuan diterima atau ditolak */
                $role = Role::findByAlias('section');
                $users = User::find()
                    ->where(['section_id' => $sectionId, 'role' => $role->id])
                    ->all();

                $userList = [];
                foreach ($users as $list) {
                    $userList[] = $list->id;
                }
            }
        }

        return $userList;
    }
}

?>
