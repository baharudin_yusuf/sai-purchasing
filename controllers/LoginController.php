<?php
namespace app\controllers;

use app\helpers\Auth;
use app\models\User;
use yii\web\Controller;

class LoginController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return $behaviors;
    }

    public function actionIndex()
    {
        $this->layout = 'blank';
        return $this->render('index');
    }

    public function actionValidate()
    {
        $email = post_data('email');
        $password = encrypt(post_data('password'));

        $user = User::findOne(['email' => $email, 'password' => $password]);
        if (!is_null($user)) {
            Auth::login($user->id);

            $redirectURL = url();
            return alert_success('Login berhasil! Anda akan diarahkan dalam 3 detik') .
                "<script> redirectTo('$redirectURL', 3000); </script>";
        } else {
            return alert_danger("<b>Error!</b> Email atau password salah!");
        }
    }
}

?>
