<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>DP ID</th>
            <th>PR ID</th>
            <th>Goods Service</th>
            <th>Quantity</th>
            <th>Received</th>
            <th>Delivered</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <?php foreach ($list->purchaseRequisitionList as $prList) { ?>
                <?php foreach ($prList->realizationActual as $actual) { ?>
                    <tr class="nowrap">
                        <td><?= $list->code ?></td>
                        <td><?= $actual->directPurchase->code ?></td>
                        <td><?= $actual->goodsService->name ?></td>
                        <td class='center'><?= $actual->quantity ?></td>
                        <td class='center'><?= $actual->received ?></td>
                        <td class='center'><?= $actual->delivered ?></td>
                        <td class='center'>
                            <a href='<?= url("dp/main/detail/?id=$list->id") ?>' class='btn btn-primary btn-xs'>
                                <i class='fa fa-book'></i>Detail
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>

    <a href='<?= url('dp/report/download-receiving') ?>' class='btn btn-primary btn-sm'
       style='margin:5px 0 0 0;'><i class='fa fa-download'></i> Download Data Laporan</a>

    <hr style="margin:10px 0;" class="include-exclude-rejected-panel">
    <form id='dp-receiving-status' method='post' action='' class="form-inline include-exclude-rejected-panel"
          role="form">
        <div class="form-group">
            <div class="checkbox">
                <label><input <?= $only_completely_received ? 'checked' : '' ?> type="checkbox" name="only-completely-received" value='1'> Only Completely Received</label>
            </div>
        </div>
        &nbsp;|&nbsp;
        <div class="form-group">
            <div class="checkbox">
                <label><input <?= $only_completely_delivered ? 'checked' : '' ?> type="checkbox" name="only-completely-delivered" value='1'> Only Completely Delivered</label>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });

    $('#dp-receiving-status input').change(function () {
        $('#dp-receiving-status').submit();
    });
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-closed * {
        display: none;
    }
</style>
