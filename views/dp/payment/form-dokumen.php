<?php use yii\helpers\Html; ?>

<form onsubmit='return false' id='form-upload-dokumen' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-upload-dokumen'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>VP/VR ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $data->code ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Paid To</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $data->paid_to ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $data->time ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $data->display_amount ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Document</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('file', 'document', null, ['class' => 'form-control', 'required' => '']); ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>
    $('#form-upload-dokumen').submit(function () {
        if (confirm('Apakah anda yakin bahwa data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!')) {
            var data = getFormData('form-upload-dokumen');
            ajaxTransfer('dp/payment/upload-data', data, '#result-upload-dokumen');
        }
    });
</script>
