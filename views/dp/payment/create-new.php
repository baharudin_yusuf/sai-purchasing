<?php use yii\helpers\Html; ?>

<form action='' method='post' enctype='multipart/form-data' id='form-payment' class='form-horizontal'>
<div class='col-lg-12 col-md-12' id='result-payment-method'></div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Bank</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::dropDownList('bank', null, $bank_option, ['class'=>'form-control', 'required'=>'']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Bank Detail</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::dropDownList('bank_detail_id', null, $bank_detail_option, ['class'=>'form-control', 'required'=>'']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Payment Method</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::dropDownList('payment_method_id', null, $payment_method_option, ['class'=>'form-control']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Payment Document</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('file', 'payment_document', null, ['class'=>'form-control', 'required'=>'']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Comment</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::textarea('comment', null, ['class'=>'form-control', 'placeholder'=>'Comment']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'></label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('submit', 'submit', 'Submit', ['class'=>'btn btn-primary']) ?>
	</div>
</div>
<?= Html::input('hidden', 'po_realization_id', $po_realization_id) ?>
</form>

<script>
$('select[name="bank"]').change(function(){
	var bank_id = $(this).val();
	ajaxTransfer('po/payment/get-bank-detail', {bank_id: bank_id}, 'select[name="bank_detail_id"]');
});

$('#form-payment').submit(function(){
	var benar = confirm('Apakah anda yakin bahwa data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!');
	if(!benar) return false;
});
</script>
