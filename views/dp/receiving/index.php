<?php
$this->title = 'DP Receiving';
$data = isset($data) ? $data : [];
?>

<div class='div-datatable'>
    <table class='table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>DP ID</th>
            <th>Received</th>
            <th>Delivered</th>
            <th>Manage</th>
        </tr>
        </thead>
        <?php foreach ($data as $item){ ?>
            <tr>
                <td class='center'><?= $item->code ?></td>
                <td class='center'><i class="fa fa-lg <?= ($item->is_received ? 'fa-check-circle green' : 'fa-times-circle red') ?>"></i></td>
                <td class='center'><i class="fa fa-lg <?= ($item->is_delivered ? 'fa-check-circle green' : 'fa-times-circle red') ?>"></i></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url('dp/receiving/detail/?id='.$item->id) ?>' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
            </tr>
        <?php } ?>
    </table>
</div>

<script>
    $(document).ready(function(){
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-dp *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-received *, .filter-delivered *{
        display: none;
    }
</style>
