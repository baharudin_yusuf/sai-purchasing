<?php
use yii\helpers\Html;
$this->title = 'Create New Direct Purchase';
?>

<?php if ($prReadyCount == 0) { ?>
    Perhatian! Tidak ada data PR tersedia untuk Direct Purchase!
    <script>
        $(document).ready(function () {
            $('#form-purchase-order').find('*').prop('disabled', true);
        });
    </script>
<?php } ?>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-direct-purchase-data" data-toggle="tab">Direct Purchase Data</a>
    </li>
    <li>
        <a href="#tab-direct-purchase-detail" data-toggle="tab">Direct Purchase Detail</a>
    </li>
</ul>
<div class="tab-content bottom-margin">
    <div class="tab-pane active" id="tab-direct-purchase-data">
        <form onsubmit="return false;" id='form-direct-purchase' style='margin:10px 0;' class='form-horizontal col-lg-12 col-md-12'>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Comment</label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Comment']) ?>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'></label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?= Html::hiddenInput('session_key', $sessionKey) ?>
        </form>
    </div>
    <div class="tab-pane" id="tab-direct-purchase-detail">
        <div class="col-lg-12 col-md-12">
            <a style='margin: 10px 0;' class='btn btn-primary' onclick='addDataPR()' title='Tambah Data PR'><i
                        class='fa fa-plus'></i> Tambah Data PR</a>
            <div style='margin:10px 0' id='output-direct-purchase-detail'></div>
        </div>
    </div>
</div>

<script>
    function addDataPR() {
        var data = getFormData('form-direct-purchase');
        modalAlert('Tambah Data PR', '');
        ajaxTransfer('dp/create/add-pr', data, '#modal-output');
    }

    function loadAddedPR() {
        var data = getFormData('form-direct-purchase');
        ajaxTransfer('dp/create/load-added-pr', data, '#output-direct-purchase-detail')
    }

    $(document).ready(function () {
        $('#form-direct-purchase').submit(function () {
            modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data sudah benar? Data tidak dapat diubah setelah disubmit', function () {
                var data = getFormData('form-direct-purchase');
                ajaxTransfer('dp/create/save-data', data, '#modal-output');
            });
        });
    });
</script>

