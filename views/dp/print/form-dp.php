<div style='  color: red; border: 2px solid red; text-align: center; display: inline-block; padding: 15px 10px; font-size: 25px; position: absolute; right: 0; top: 0; font-weight: bold;'>
    CONFIDENTIAL
</div>
<div style='display: inline-block;width: 80%;float: left;'>
    <div>
        <div>PT. SURABAYA AUTOCOMP INDONESIA</div>
        <div>LOCAL PURCHASE SECTION</div>
    </div>

    <div style='font-weight: bold;font-size: 20px;text-decoration: underline;text-align: center;padding: 5px 0;'>DIRECT PURCHASE</div>

    <table>
        <tr>
            <td style='width: 20mm;'>NO</td>
            <td> :</td>
            <td><?= $data->code ?></td>
        </tr>
        <tr>
            <td>DATE</td>
            <td> :</td>
            <td><?= date('d-F-Y') ?></td>
        </tr>
        <tr>
            <td>PERIOD</td>
            <td> :</td>
            <td></td>
        </tr>
    </table>
</div>

<table style='display: inline-block;width: 20%;float: right;position: relative;top: 30px;'>
    <tr>
        <td style='border: 1px solid;text-align: center;padding: 2px 0;'>APPROVED</td>
        <td style='border: 1px solid;text-align: center;padding: 2px 0;'>PREPARED</td>
    </tr>
    <tr>
        <td style='border: 1px solid;height: 17mm;'></td>
        <td style='border: 1px solid;height: 17mm;'></td>
    </tr>
</table>

<table class='bordered' style='width:100%;display:inline-block;margin:5px 0;'>
    <thead>
    <tr>
        <th rowspan='2'>PR NO</th>
        <th rowspan='2' style='width: 110mm'>DESCRIPTION OF GOODS</th>
        <th rowspan='2'>PLACE</th>
        <th rowspan='2'>UNIT</th>
        <th colspan='3'>PLAN</th>
        <th colspan='3'>ACTUAL</th>
    </tr>
    <tr>
        <th>QTY</th>
        <th>UNIT PRICE</th>
        <th>AMOUNT+TAX</th>
        <th>QTY</th>
        <th>UNIT PRICE</th>
        <th>AMOUNT+TAX</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($detail as $item) { ?>
        <tr style="border-top: <?= strlen($item['num']) > 0 ? '1' : '' ?>px dotted;">
            <?php if ($item['rowspan'] != 0) { ?>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;white-space: nowrap;'><?= $item['code'] ?></td>
            <?php } ?>
            <td style='border-left: 1px solid;border-right: 1px solid;text-align: left;padding: 0 1.5mm;'><?= $item['goods_service'] ?></td>
            <?php if ($item['rowspan'] != 0) { ?>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;white-space: nowrap;'></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['unit'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;white-space: nowrap;'><?= $item['plan_quantity'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['plan_price'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['plan_amount'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;white-space: nowrap;'><?= $item['actual_quantity'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['actual_price'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['actual_amount'] ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
    </tbody>

    <tfoot>
    <tr>
        <td style='border: 1px solid;text-align: center;padding: 0 1.5mm;' colspan='4'><b>TOTAL</b></td>
        <td style='border: 1px solid;text-align: right;padding: 0 1.5mm;'
            colspan='3'><?= $data->display_amount_plan ?></td>
        <td style='border: 1px solid;text-align: right;padding: 0 1.5mm;'
            colspan='3'><?= $data->display_amount_actual ?></td>
    </tr>
    <tr>
        <td colspan='10' style='border:0 none; text-align:right;'>PUR-009-0</td>
    </tr>
    </tfoot>
</table>


<hr class='hide-on-print'>
<button class='hide-on-print' onclick='window.print()'>PRINT</button>
<?php if (!is_null($prevURL)) { ?>
    <a class='hide-on-print' href="<?= $prevURL ?>">
        <button>PREV</button>
    </a>
<?php } ?>
<?php if (!is_null($nextURL)) { ?>
    <a class='hide-on-print' href="<?= $nextURL ?>">
        <button>NEXT</button>
    </a>
<?php } ?>

<style>
    #page {
        width: 290mm;
        height: 200mm;
        position: relative;
    }

    table {
        border-collapse: collapse;
    }

    .bordered th, .bordered td {
        border: 1px solid;
        padding: 1px 2px;
    }

    @media print {
        .hide-on-print {
            display: none;
        }
    }
</style>
