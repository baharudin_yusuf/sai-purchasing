<?php use yii\helpers\Html; ?>

<form onsubmit='return false' id='form-voucher-paying' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-voucher-paying'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Paid To</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'paid_to', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Paid To']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= date('d-M-y') ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Additional</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= ($isAdditional ? check_times_cirle(1) : check_times_cirle(0)) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $currency->name . ' ' . currency_format($amount) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Say</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <i><?= ucwords(terbilang($amount)) ?></i>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'amount', $amount) ?>
    <?= Html::input('hidden', 'direct_purchase_id', $data->id) ?>
    <?= Html::input('hidden', 'is_additional', $isAdditional) ?>
    <?= Html::input('hidden', 'is_voucher_receiving', 0) ?>
</form>

<script>
    $('#form-voucher-paying').submit(function () {
        if (confirm('Apakah anda yakin bahwa data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!')) {
            var data = getFormData('form-voucher-paying');
            ajaxTransfer('dp/voucher-paying/save-data', data, '#result-voucher-paying');
        }
    });
</script>
