<div class='form-horizontal'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Paid To</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $data->paid_to ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= date('d-M-y', strtotime($data->time)) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Additional</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_additional) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $currency->name . ' ' . currency_format($data->amount) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Say</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <i><?= ucwords(terbilang($data->amount)) ?></i>
        </div>
    </div>
    <?php if ($data->is_paid) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'></label>
            <div class='col-lg-9 col-md-9'>
                <a target='_blank' href='<?= url("dp/voucher-paying/print-data/?id=$data->id") ?>'
                   class='btn btn-primary btn-sm'><i class='fa fa-print'></i> Cetak
                    Voucher <?= $data->is_voucher_receiving ? 'Receiving' : 'Paying' ?></a>
            </div>
        </div>
    <?php } ?>
</div>
