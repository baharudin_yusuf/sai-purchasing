<?php use yii\helpers\Html; ?>

<form onsubmit='return false' id='form-voucher-receiving' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-voucher-receiving'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Paid To</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $paymentFor ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= date('d-M-y') ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $currency->name . ' ' . currency_format($amount) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Say</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <i><?= ucwords(terbilang($amount)) ?></i>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'amount', $amount) ?>
    <?= Html::input('hidden', 'direct_purchase_id', $data->id) ?>
    <?= Html::input('hidden', 'is_additional', 0) ?>
    <?= Html::input('hidden', 'is_voucher_receiving', 1) ?>
    <?= Html::input('hidden', 'paid_to', $paymentFor) ?>
</form>

<script>
    $('#form-voucher-receiving').submit(function () {
        if (confirm('Apakah anda yakin bahwa data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!')) {
            var data = getFormData('form-voucher-receiving');
            ajaxTransfer('dp/voucher-paying/save-data', data, '#result-voucher-receiving');
        }
    });
</script>
