<?php
$this->title = 'Direct Purchase';
?>

<?php if ($allowCreate) { ?>
    <a href='<?= url("dp/create") ?>' title='Tambah Data' class="btn btn-primary"><i class='fa fa-plus'></i> Tambah Data</a>
<?php } ?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>DP ID</th>
            <th>Amount Est</th>
            <th>Amount Plan</th>
            <th>Amount Actual</th>
            <th>Comment</th>
            <th>Date</th>
            <th>Manager</th>
            <th>Received</th>
            <th>Delivered</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr class='nowrap <?= ($list->is_rejected || $list->is_canceled) ? 'rejected' : '' ?>'>
                <td class='center'><?= $list->code ?></td>
                <td class='center'><?= $list->display_amount_estimation  ?></td>
                <td class='center'><?= $list->display_amount_plan ?></td>
                <td class='center'><?= $list->display_amount_actual ?></td>
                <td><?= $list->comment ?></td>
                <td class='center'><?=  $list->submission_time  ?></td>
                <td class='center'><?= check_times_cirle($list->is_approved_by_lp_manager, $list->approved_by_lp_manager_time) ?></td>
                <td class='center'><?= check_times_cirle($list->is_received) ?></td>
                <td class='center'><?= check_times_cirle($list->is_delivered) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("dp/main/detail/?id=$list->id") ?>' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?= Yii::$app->params['includeRejectedForm'] ?>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-received *, .filter-delivered * {
        display: none;
    }
</style>
