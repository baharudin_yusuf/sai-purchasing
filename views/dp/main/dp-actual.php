<?php

use yii\helpers\Html;

?>

<div class="col-lg-12 col-md-12 col-md-12">
    <form onsubmit="return false;" style='margin:10px 0' id='form-actual-payment'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>PR ID</th>
                <th>Goods/Service</th>
                <?php if (!$isReceiving) { ?>
                    <th>Carline</th>
                <?php } ?>
                <th>Quantity</th>
                <?php if (!$isReceiving) { ?>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>Sub Total + Tax</th>
                <?php } ?>
                <?php if ($allowUpdateActualPrice) { ?>
                    <th>
                        <input type='checkbox' onchange='checkUncheckItem(this)'>
                    </th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->purchaseRequisitionList as $dpPr) { ?>
                <?php foreach ($dpPr->realizationActual as $actual) { ?>
                    <tr class="nowrap">
                        <td><?= $actual->dpPrDetail->purchaseRequisition->code ?></td>
                        <td><?= $actual->goodsService->name ?></td>
                        <?php if (!$isReceiving) { ?>
                            <td class='center'><?= $actual->carline->name ?></td>
                        <?php } ?>
                        <td class='center'><?= $actual->quantity ?></td>
                        <?php if (!$isReceiving) { ?>
                            <?php if ($allowUpdateActualPrice) { ?>
                                <?php if ($actual->is_fixed) { ?>
                                    <td class='center'>
                                        <?= $actual->display_price ?>
                                        <?php if ($actual->display_price != $actual->display_converted_price) { ?>
                                            <br>[<?= $actual->display_converted_price ?>]
                                        <?php } ?>
                                    </td>
                                <?php } else { ?>
                                    <?php $maxPrice = $actual->plan_price + (0.1 * $actual->plan_price); ?>
                                    <td class='center' style='padding: 2px;'>
                                        <div class='input-group m-bot15'>
                                            <span class='input-group-addon'><?= $actual->goodsService->currency->name ?></span>
                                            <?= Html::input('number', "price-$actual->id", $actual->plan_price, ['step' => '0.01', 'max' => $maxPrice, 'min' => 0, 'placholder' => 'Actual Price', 'class' => 'form-control', 'required' => '']) ?>
                                        </div>
                                    </td>
                                <?php } ?>
                            <?php } else { ?>
                                <td class='center'>
                                    <?= $actual->display_price ?>
                                    <?php if ($actual->display_price != $actual->display_converted_price) { ?>
                                        <br>[<?= $actual->display_converted_price ?>]
                                    <?php } ?>
                                </td>
                            <?php } ?>
                            <td class='center'>
                                <?= $actual->display_tax ?>
                            </td>
                            <td class='center'>
                                <?= $actual->display_subtotal ?>
                            </td>
                        <?php } ?>
                        <?php if ($allowUpdateActualPrice) { ?>
                            <?php if ($actual->is_fixed) { ?>
                                <td></td>
                            <?php } else { ?>
                                <td class='center'>
                                    <input type='checkbox' class='actual-item-checkbox' value='1'
                                           name='checkbox-<?= $actual->id ?>'>
                                </td>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan='6'>Total</th>
                <th><strong><?= $data->display_amount_actual ?></strong></th>
                <?php if ($allowUpdateActualPrice) { ?>
                    <th>Select</th>
                <?php } ?>
            </tr>
            </tfoot>
        </table>
        <?php if ($allowUpdateActualPrice) { ?>
            <input type='hidden' name='actual-price' value='1'>
            <button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-check'></i>Update Selected Actual Price
            </button>
        <?php } ?>
        <?= Html::hiddenInput('id', $data->id) ?>
    </form>
</div>

<script>
    function checkUncheckItem(t) {
        var is_checked = $(t).prop('checked');
        $('.actual-item-checkbox').prop('checked', is_checked);
    }

    $(document).ready(function () {
        $('#form-actual-payment').submit(function () {
            modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menyimpan data actual payment? Data tidak dapat diubah setelah disubmit', function () {
                var data = getFormData('form-actual-payment');
                ajaxTransfer('dp/main/update-actual-price', data, '#modal-output');
            });
        });
    })
</script>
