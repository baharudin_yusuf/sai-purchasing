<?php $activeUser = \app\helpers\Auth::user(); ?>

<div class="col-lg-12 col-md-12 col-md-12" style='margin:10px 0'>
    <?php if ($allowCreateVP) { ?>
        <a class='btn btn-primary btn-sm' onclick='loadModal(this)' target='dp/voucher-paying/create-new-vp'
           title='Create Voucher Paying' data='dp_id=<?= $data->id ?>'><i class='fa fa-plus'></i>Add Voucher Paying</a>
    <?php } ?>
    <?php if ($allowCreateVR) { ?>
        <a class='btn btn-primary btn-sm' onclick='loadModal(this)' target='dp/voucher-paying/create-new-vr'
           title='Create Voucher Receiving' data='dp_id=<?= $data->id ?>'><i class='fa fa-plus'></i>Add Voucher
            Receiving</a>
    <?php } ?>

    <form action='' method='post' enctype='multipart/form-data' id='form-payment'>
        <table id='table-document' style='margin:10px 0;' class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>VP/VR ID</th>
                <th>Paid To</th>
                <th>Date</th>
                <th>Amount</th>
                <th>Additional</th>
                <th>Manager</th>
                <th>Document</th>
                <th class='td-manage' style='width: 165px'>Manage</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->getDisplayRealization() as $real) { ?>
                <tr class='nowrap <?= ($real->is_rejected || $real->is_canceled) ? 'rejected' : '' ?>'>
                    <td><?= $real->code ?></td>
                    <td><?= $real->paid_to ?></td>
                    <td class='center'><?= $real->time ?></td>
                    <td class='center'><?= $real->display_amount ?></td>
                    <td class='center'><?= check_times_cirle($real->is_additional) ?></td>
                    <td class='center'><?= check_times_cirle($real->is_approved_by_manager) ?></td>
                    <td class='center'>
                        <?php if(is_null($real->document_url)) { ?>
                            -
                        <?php } else { ?>
                            <a target="_blank" href="<?= $real->document_url ?>"><?= $real->document_name ?></a>
                        <?php } ?>
                    </td>
                    <td class='center td-manage'>
                        <?php if ($real->is_approved_by_manager) { ?>
                            <?php if ($real->is_paid) { ?>
                                <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='dp/voucher-paying/detail' data='id=<?= $real->id ?>' title='Payment Detail'><i class='fa fa-book'></i> Detail</a>
                            <?php } else if($DPConstraint::allowUploadPayment($real)) { ?>
                                <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='dp/voucher-paying/detail' data='id=<?= $real->id ?>' title='Payment Detail'><i class='fa fa-book'></i> Detail</a>

                                <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='dp/payment/upload-form' data='id=<?= $real->id ?>' title='Upload Payment'><i class='fa fa-upload'></i> Upload</a>
                            <?php } else { ?>
                                <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='dp/voucher-paying/detail' data='id=<?= $real->id ?>' title='Payment Detail'><i class='fa fa-book'></i> Detail</a>
                            <?php } ?>
                        <?php } else if ($real->is_rejected || $real->is_canceled) {?>

                        <?php } else { ?>
                            <?php if ($allowApproveVP) { ?>
                                <a onclick='approveVoucherPaying(<?= $real->id ?>)' class='btn btn-primary btn-xs'><i class='fa fa-check-circle'></i>Approve</a>
                                <a onclick='rejectVoucherPaying(<?= $real->id ?>)' class='btn btn-danger btn-xs'><i class='fa fa-times-circle'></i>Reject</a>
                            <?php } ?>
                            <?php if ($allowCancelVP) { ?>
                                <a onclick='cancelVoucherPaying(<?= $real->id ?>)' class='btn btn-danger btn-xs'><i class='fa fa-times-circle'></i>Cancel</a>
                            <?php } ?>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>

    <?= Yii::$app->params['includeRejectedForm'] ?>
</div>

<script>
    function approveVoucherPaying(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menyetujui pengajuan voucher paying? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('dp/voucher-paying/approve-data', {id: id}, '#modal-output');
        });
    }

    function rejectVoucherPaying(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menolak pengajuan voucher paying? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('dp/voucher-paying/reject-data', {id: id}, '#modal-output');
        });
    }

    function cancelVoucherPaying(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan membatalkan pengajuan voucher paying? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('dp/voucher-paying/cancel-data', {id: id}, '#modal-output');
        });
    }

</script>
