<?php
$this->title = 'Pending Direct Purchase';
?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>DP ID</th>
            <th>Amount Est</th>
            <th>Amount Plan</th>
            <th>Amount Actual</th>
            <th>Comment</th>
            <th>Date</th>
            <th>Manage</th>
            <th>Select</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td class='center'><?= $list->code ?></td>
                <td class='center'><?= $list->display_amount_estimation  ?></td>
                <td class='center'><?= $list->display_amount_plan ?></td>
                <td class='center'><?= $list->display_amount_actual ?></td>
                <td><?= $list->comment ?></td>
                <td class='center'><?= $list->submission_time ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("dp/main/detail/?id=$list->id") ?>' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
                <td class='center'><input type='checkbox' value='<?= $list->id ?>' class='select-data'></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?= (count($data) > 0 ? BULK_ACTION_BTN : '') ?>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });

    function bulkActionCallback(data) {
        ajaxTransfer('dp/main/bulk-action', data, '#global-temp');
    }
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-select * {
        display: none;
    }
</style>
