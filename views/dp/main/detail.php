<?php
$this->title = 'Direct Purchase Detail';
$activeUser = \app\helpers\Auth::user();
?>

<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-dp-data" data-toggle="tab">DP Data</a></li>
    <?php if(!$isReceiving) { ?>
        <li><a href="#tab-dp-plan" data-toggle="tab">DP Plan</a></li>
        <?php if($isApproved && $showDPActual){ ?>
            <li><a href="#tab-dp-actual" data-toggle="tab">DP Actual</a>
            </li>
        <?php } ?>
    <?php } ?>
    <?php if($isApproved && $showDPActual){ ?>
        <li><a href="#tab-dp-receiving" data-toggle="tab">DP Receiving</a></li>
    <?php } ?>
    <?php if(!$isReceiving || $isApproved) { ?>
        <li><a href="#tab-dp-payment" data-toggle="tab">DP Payment</a></li>
    <?php } ?>
</ul>
<div class="tab-content bottom-margin">
    <div class="tab-pane active" id="tab-dp-data">
        <?php require 'dp-data.php'; ?>
    </div>
    <?php if(!$isReceiving) { ?>
        <div class="tab-pane" id="tab-dp-plan">
            <?php require 'dp-plan.php'; ?>
        </div>
        <?php if($isApproved && $showDPActual){ ?>
            <div class="tab-pane" id="tab-dp-actual">
                <?php require 'dp-actual.php'; ?>
            </div>
        <?php } ?>
    <?php } ?>
    <?php if($isApproved && $showDPActual){ ?>
        <div class="tab-pane" id="tab-dp-receiving">
            <?php require 'dp-receiving.php'; ?>
        </div>
    <?php } ?>
    <?php if(!$isReceiving || $isApproved) { ?>
        <div class="tab-pane" id="tab-dp-payment">
            <?php require 'dp-payment.php'; ?>
        </div>
    <?php } ?>
</div>
