<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-approve-pr' class='form-horizontal col-lg-12 col-md-12'>
<div id='result-purchase-requisition-detail'></div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Status PR</label>
	<div class='col-lg-9 col-md-9'>
		<div class='radio'>
			<label><?= Html::input('radio', 'status_pr', 'dp', ['checked'=>'']) ?> Direct Purchase</label>
		</div>
		<div class='radio'>
			<label><?= Html::input('radio', 'status_pr', 'po') ?> Purchase Order</label>
		</div>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'></label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('submit', 'simpan', "Simpan", ['class'=>'btn btn-primary']) ?>
	</div>
</div>
<?= Html::input('hidden', 'id', $id) ?>
</form>

<script>
$('#form-approve-pr').submit(function(){
	var data = getFormData('form-approve-pr');
	var approve = confirm('Apakah anda yakin akan menyetujui purchase requisition? Tindakan tidak dapat dibatalkan');
	if(approve){
		ajaxTransfer('pr/main/approve-pr', data, '#global-temp');
	}
});
</script>
