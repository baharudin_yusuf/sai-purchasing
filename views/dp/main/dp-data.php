<div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>DP ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->code ?></div>
    </div>
    <?php if(!$isReceiving) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Amount Estimation</label>
            <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_estimation ?></div>
        </div>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Amount Plan</label>
            <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_plan ?></div>
        </div>
    <?php } ?>
    <?php if($showDPActual && !$isReceiving) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Amount Actual</label>
            <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_actual ?></div>
        </div>
    <?php } ?>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->submission_time ?></div>
    </div>
    <div class='form-group' style='margin:0 0 10px 0;'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->comment ?></div>
    </div>
    <?php if(!$isReceiving) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Rejected</label>
            <div class='col-lg-9 col-md-9 form-control-static'>
                <?= check_times_cirle($data->is_rejected) ?>
            </div>
        </div>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Canceled</label>
            <div class='col-lg-9 col-md-9 form-control-static'>
                <?= check_times_cirle($data->is_canceled) ?>
            </div>
        </div>
    <?php } ?>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Received</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_received) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Delivered</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_delivered) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?php if (!$data->is_canceled && !$data->is_rejected && !$isReceiving) { ?>
                <a class='btn btn-primary btn-sm' href='<?= url("dp/print/form-dp/?id=$data->id&p=1") ?>'
                   target='_blank'><i class='fa fa-print'></i> Form DP</a>
            <?php } ?>
            <?php if ($allowApproveDP) { ?>
                <a onclick='approveDP(<?= $data->id ?>)' class='btn btn-primary btn-sm'><i
                            class='fa fa-check-circle'></i>Approve</a>
                <a onclick='rejectDP(<?= $data->id ?>)' class='btn btn-danger btn-sm'><i
                            class='fa fa-times'></i>Reject</a>
            <?php } ?>
            <?php if ($allowCancelDP) { ?>
                <a onclick='cancelDP(<?= $data->id ?>)' class='btn btn-danger btn-sm'><i
                            class='fa fa-times'></i>Cancel</a>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    function approveDP(id) {
        var approve = confirm('Apakah anda yakin akan menyetujui pengajuan direct purchase? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('dp/main/approve-dp', {id: id}, '#global-temp');
        }
    }

    function rejectDP(id) {
        var approve = confirm('Apakah anda yakin akan menolak pengajuan direct purchase? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('dp/main/reject-dp', {id: id}, '#global-temp');
        }
    }

    function cancelDP(id) {
        var cancel = confirm('Apakah anda yakin akan membatalkan pengajuan direct purchase? Tindakan tidak dapat dibatalkan');
        if (cancel) {
            ajaxTransfer('dp/main/cancel-dp', {id: id}, '#global-temp');
        }
    }
</script>
