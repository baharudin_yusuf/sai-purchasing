<div class="col-lg-12 col-md-12 col-md-12">
    <div style='margin:10px 0' id='output-budget-proposal-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>PR ID</th>
                <th>Goods/Service</th>
                <?php if(!$isReceiving){ ?>
                    <th>Carline</th>
                <?php } ?>
                <th>Quantity</th>
                <?php if(!$isReceiving){ ?>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>Sub Total + Tax</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->purchaseRequisitionList as $dpPr) { ?>
                <?php foreach ($dpPr->realizationPlan as $plan) { ?>
                    <tr class="nowrap">
                        <td><?= $plan->dpPrDetail->purchaseRequisition->code ?></td>
                        <td><?= $plan->goodsService->name ?></td>
                        <?php if(!$isReceiving){ ?>
                            <td class='center'><?= $plan->carline->name ?></td>
                        <?php } ?>
                        <td class='center'><?= $plan->quantity ?></td>
                        <?php if(!$isReceiving){ ?>
                            <td class='center'>
                                <?= $plan->display_price ?>
                                <?php if($plan->display_price != $plan->display_converted_price){ ?>
                                    <br>[<?= $plan->display_converted_price ?>]
                                <?php } ?>
                            </td>
                            <td class='center'>
                                <?= $plan->display_tax ?>
                            </td>
                            <td class='center'>
                                <?= $plan->display_subtotal ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan='6'>Total</th>
                <th><strong><?= $data->display_amount_plan ?></strong></th>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
