<?php
$this->title = 'Purchase Requisition Detail';
?>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-pr-data" data-toggle="tab">PR Data</a>
    </li>
    <li>
        <a href="#tab-pr-detail" data-toggle="tab">PR Detail</a>
    </li>

    <?php if (!$data->is_rejected && !$data->is_canceled && $data->is_approved_by_fa) { ?>
        <li>
            <a href="#tab-pr-item-availability" data-toggle="tab">PR Item Availability</a>
        </li>
        <li>
            <a href="#tab-pr-revision" data-toggle="tab">PR Revision</a>
        </li>
    <?php } ?>
</ul>
<div class="tab-content bottom-margin">
    <div class="tab-pane active" id="tab-pr-data">
        <?php require 'pr-data.php'; ?>
    </div>
    <div class="tab-pane" id="tab-pr-detail">
        <?php require 'pr-detail.php'; ?>
    </div>

    <?php if (!$data->is_rejected && !$data->is_canceled && $data->is_approved_by_fa) { ?>
        <div class="tab-pane" id="tab-pr-item-availability">
            <?php require 'pr-item-availability.php'; ?>
        </div>
        <div class="tab-pane" id="tab-pr-revision">
            <?php require 'pr-revision.php'; ?>
        </div>
    <?php } ?>
</div>

<script>
    $(document).ready(function () {
        activate_menu('<?= url('pr/main') ?>');
    })
</script>