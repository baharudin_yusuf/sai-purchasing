<table class='table table-bordered table-striped'>
    <thead>
    <tr>
        <th>DP Code</th>
        <th>Date Submit</th>
        <th>Manage</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $list) { ?>
        <tr>
            <td><?= $list->code ?></td>
            <td class='center'><?= date('d/m/Y', strtotime($list->submission_time)) ?></td>
            <td class='center'>
                <a title='DP Detail' class='btn btn-primary btn-xs' href='<?= url("dp/main/detail/?id=$list->id") ?>'><i class='fa fa-book'></i>Detail</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>