<div class="col-lg-12 col-md-12">
    <div style='margin:10px 0' id='output-budget-proposal-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>Revision Date</th>
                <th>Item Revised</th>
                <th>Approved by Manager</th>
                <th>Manage</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->getDisplayPrRevision() as $item) { ?>
                <?php $allowCancelRev = $prConstraint::allowCancelRevision($item); ?>
                <?php $allowApproveRev = $prConstraint::allowApproveRevision($item); ?>
                <tr class='<?= ($item->is_rejected || $item->is_canceled) ? 'rejected' : '' ?>'>
                    <td><?= $item->date ?></td>
                    <td class='center'><?= $item->rev_num ?></td>
                    <td class='center'><?= check_times_cirle($item->is_approved_by_manager) ?></td>
                    <td class='center'>
                        <a title='PR Revision Detail' onclick='loadModal(this)' target='pr/main/revision-detail'
                           data='id=<?= $item->id ?>' class='btn btn-primary btn-xs'><i
                                    class='fa fa-book'></i>Detail</a>
                        <?php if ($allowCancelRev) { ?>
                            <a onclick='cancelRevision(<?= $item->id ?>)' class='btn btn-danger btn-xs'><i
                                        class='fa fa-times-circle'></i>Cancel</a>
                        <?php } else if ($allowApproveRev) { ?>
                            <a onclick='approveRevision(<?= $item->id ?>)' class='btn btn-primary btn-xs'><i
                                        class='fa fa-check-circle'></i>Approve</a>
                            <a onclick='rejectRevision(<?= $item->id ?>)' class='btn btn-danger btn-xs'><i
                                        class='fa fa-times-circle'></i>Reject</a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <?= Yii::$app->params['includeRejectedForm'] ?>
</div>

<script>
    function cancelRevision(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan membatalkan pengajuan revisi?', function () {
            ajaxTransfer('pr/modify/cancel-revision/', {rev_id: id}, '#modal-output')
        });
    }

    function approveRevision(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menerima pengajuan revisi?', function () {
            ajaxTransfer('pr/modify/approve-revision/', {rev_id: id}, '#modal-output')
        });
    }

    function rejectRevision(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menolak pengajuan revisi?', function () {
            ajaxTransfer('pr/modify/reject-revision/', {rev_id: id}, '#modal-output')
        });
    }
</script>