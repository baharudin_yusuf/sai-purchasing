<table class='table table-bordered table-striped' id='table-revision-detail'>
    <thead>
    <tr>
        <th>Goods / Service</th>
        <th>Quantity</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data->revisionDetail as $item) { ?>
        <tr>
            <td><?= $item->purchaseRequisitionDetail->goodsService->name ?></td>
            <td class="center"><?= $item->quantity ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>