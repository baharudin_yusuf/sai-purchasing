<?php
$data = isset($data) ? $data : [];
?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>ID</th>
            <th>Section</th>
            <th>Budget</th>
            <th>Price Est</th>
            <th>Comment</th>
            <th>Date</th>
            <th>Manager</th>
            <th>DFM</th>
            <th>FM</th>
            <th>Presdir</th>
            <th>FA</th>
            <th>Closed</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr class='nowrap <?= ($list->is_rejected || $list->is_canceled) ? 'rejected' : '' ?>'>
                <td><?= $list->code ?></td>
                <td><?= $list->section->name ?></td>
                <td><?= $list->budget->name ?></td>
                <td class='center'><?= $list->display_price_estimation ?></td>
                <td><?= $list->comment ?></td>
                <td class='center nowrap'><?= $list->date ?></td>
                <td class='center'>
                    <?= check_times_cirle($list->is_approved_by_manager, $list->approved_by_manager_time) ?>

                <td class='center'>
                    <?= check_times_cirle($list->is_approved_by_dfm, $list->approved_by_dfm_time) ?>

                <td class='center'>
                    <?= check_times_cirle($list->is_approved_by_fm, $list->approved_by_fm_time) ?>

                <td class='center'>
                    <?= check_times_cirle($list->is_approved_by_presdir, $list->approved_by_presdir_time) ?>

                <td class='center'>
                    <?= check_times_cirle($list->is_approved_by_fa, $list->approved_by_fa_time) ?>
                </td>
                <td class='center'><?= check_times_cirle($list->is_closed) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("pr/main/detail/?id=$list->id") ?>'
                       title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-closed * {
        display: none;
    }
</style>
