<div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->code ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Section</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->section->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Budget Final</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <a title='Detail Budget Final' onclick='loadModal(this)' target='proposal/final/detail'
               data='id=<?= $data->budgetFinal->id ?>'>[<?= $data->budgetFinal->code ?>
                ] <?= $data->budget->display_name ?></a>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Price Estimation</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_price_estimation ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->date ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->comment ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Closed</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_closed) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Rejected</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_rejected) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Canceled</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_canceled) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Revised</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_revision) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?php if ($allowApprovePR || $allowCancel) { ?>
                <?php if ($allowApprovePR) { ?>
                    <a onclick='approvePurchaseRequisition(<?= $data->id ?>)' class='btn btn-primary btn-sm'><i
                                class='fa fa-check-circle'></i> Approve</a>
                    <a onclick='rejectPurchaseRequisition(<?= $data->id ?>)' class='btn btn-danger btn-sm'><i
                                class='fa fa-times'></i>Reject</a>
                <?php } ?>
                <?php if ($allowCancel) { ?>
                    <a onclick='cancelPurchaseRequisition(<?= $data->id ?>)' class='btn btn-danger btn-sm'><i
                                class='fa fa-times'></i>Cancel</a>
                <?php } ?>
            <?php } ?>
            <?php if ($allowCheck) { ?>
                <a onclick='checkPurchaseRequisition(<?= $data->id ?>)' class='btn btn-primary btn-sm'><i
                            class='fa fa-check-circle'></i>Checked</a>
            <?php } ?>
            <?php if (!$data->is_canceled && !$data->is_rejected) { ?>
                <a target='blank' href='<?= url("pr/print/form-pr/?id=$data->id&p=1") ?>'
                   class='btn btn-sm btn-primary'><i class='fa fa-print'></i>Form PR</a>
            <?php } ?>
            <?php if ($allowModify) { ?>
                <a href='<?= url("pr/modify/?id=$data->id") ?>' class='btn btn-sm btn-danger'><i class='fa fa-book'></i>Revisi
                    PR</a>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    function approvePurchaseRequisition(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menyetujui pengajuan purchase requisition? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/main/approve-pr', {id: id}, '#modal-output');
        });
    }

    function rejectPurchaseRequisition(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menolak pengajuan purchase requisition? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/main/reject-pr', {id: id}, '#modal-output');
        });
    }

    function cancelPurchaseRequisition(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan membatalkan pengajuan purchase requisition? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/main/cancel-pr', {id: id}, '#modal-output');
        });
    }

    function checkPurchaseRequisition(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menandai bahwa PR sudah dicek? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/main/check-pr', {id: id}, '#modal-output');
        });
    }
</script>
