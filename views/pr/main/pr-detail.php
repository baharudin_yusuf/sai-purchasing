<div class="col-lg-12 col-md-12">
    <div style='margin:10px 0' id='output-budget-proposal-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th rowspan='2'>Goods/Service</th>
                <th rowspan='2'>Carline</th>
                <th rowspan='2'>Comment</th>
                <th rowspan='2'>Req Date</th>
                <th rowspan='2'>Quantity</th>
                <th rowspan='2'>Price Estimation</th>
                <th rowspan='2'>Sub Total</th>
                <th colspan='5'>Approval</th>
                <th rowspan='2'>Manage</th>
            </tr>
            <tr>
                <th>Mgr</th>
                <th>DFM</th>
                <th>FM</th>
                <th>Presdir</th>
                <th>FA</th>
            </tr>
            </thead>
            <tbody>
            <?php $allowApproveDetailCount = 0; ?>
            <?php foreach ($data->getDisplayDetail() as $detail) { ?>
                <?php $allowApproveDetail = $prConstraint::allowApproveDetailPR($detail) ?>
                <?php $allowApproveDetailCount += ($allowApproveDetail ? 1 : 0); ?>
                <tr class='nowrap <?= ($detail->is_rejected || $detail->is_canceled) ? 'rejected' : '' ?>'>
                    <td><?= $detail->goodsService->name ?></td>
                    <td class='center'><?= $detail->carline->name ?></td>
                    <td class='center'><?= $detail->comment ?></td>
                    <td class='center nowrap'><?= $detail->required_date ?></td>
                    <td class='center'><?= $detail->quantity ?></td>
                    <td class='center nowrap'>
                        <?= $detail->display_price_estimation ?>
                        <?php if(strlen($detail->display_converted_price_estimation) > 0){ ?>
                            <br>[<?= $detail->display_converted_price_estimation ?>]
                        <?php } ?>
                    </td>
                    <td class='center nowrap'>
                        <?= $detail->display_sub_total ?>
                        <?php if(strlen($detail->display_converted_sub_total) > 0){ ?>
                            <br>[<?= $detail->display_converted_sub_total ?>]
                        <?php } ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_manager, $detail->approved_by_manager_time) ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_dfm, $detail->approved_by_dfm_time) ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_fm, $detail->approved_by_fm_time) ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_presdir, $detail->approved_by_presdir_time) ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_fa, $detail->approved_by_fa_time) ?>
                    </td>
                    <td class='center'>
                        <?php if ($allowApproveDetail) { ?>
                            <input type='checkbox' value='<?= $detail->id ?>' class='select-data'>
                        <?php } else if ($allowCancel && (!$detail->is_rejected && !$detail->is_canceled)) { ?>
                            <a onclick='cancelPurchaseRequisition_detail(<?= $detail->id ?>)' title='Batalkan Pengajuan' class='btn btn-danger btn-xs'><i class='fa fa-times'></i>Cancel</a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan='6'>Total</th>
                <th><b><?= count($data->getDisplayDetail()) == 0 ? '' : $data->display_price_estimation ?></b></th>
                <th colspan='6'></th>
            </tr>
            </tfoot>
        </table>
        <?= ($allowApproveDetailCount > 0 ? BULK_ACTION_BTN : '') ?>
        <?= Yii::$app->params['includeRejectedForm'] ?>
    </div>
</div>

<script>
    function approvePurchaseRequisition_detail(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menerima detail purchase requisition? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/detail/approve-submission', {id: id}, '#modal-output');
        });
    }

    function rejectPurchaseRequisition_detail(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menolak detail purchase requisition? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/detail/reject-submission', {id: id}, '#modal-output');
        });
    }

    function cancelPurchaseRequisition_detail(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan membatalkan detail purchase requisition? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('pr/detail/cancel-submission', {id: id}, '#modal-output');
        });
    }

    function bulkActionCallback(data) {
        ajaxTransfer('pr/detail/bulk-action', data, function (result) {
            modalAlert('Perhatian', result);
        });
    }
</script>
