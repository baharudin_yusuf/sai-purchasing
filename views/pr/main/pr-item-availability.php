<div class="col-lg-12 col-md-12">
    <div style='margin:10px 0' id='output-budget-proposal-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>Goods/Service</th>
                <th>Quantity</th>
                <th>Used</th>
                <th>Available</th>
                <th>Manage</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->getDisplayDetail() as $detail) { ?>
                <tr class='nowrap <?= ($detail->is_rejected || $detail->is_canceled) ? 'rejected' : '' ?>'>
                    <td><?= $detail->goodsService->name ?></td>
                    <td class='center'><?= $detail->quantity ?></td>
                    <td class='center'><?= $detail->used_quantity ?></td>
                    <td class='center'><?= $detail->available_quantity ?></td>
                    <td class='center'>
                        <a title='Usage Detail' class='btn btn-primary btn-xs' onclick='loadModal(this)' target='pr/main/usage-detail' data='id=<?= $detail->id ?>'><i class='fa fa-book'></i>Detail</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>