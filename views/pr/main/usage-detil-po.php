<table class='table table-bordered table-striped'>
    <thead>
    <tr>
        <th>PO Code</th>
        <th>Supplier</th>
        <th>Date Submit</th>
        <th>Manage</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $list) { ?>
        <tr>
            <td><?= $list->code ?></td>
            <td class='center'><?= $list->supplier->name ?></td>
            <td class='center'><?= date('d/m/Y', strtotime($list->date_submit)) ?></td>
            <td class='center'>
                <a title='PO Detail' class='btn btn-primary btn-xs' href='<?= url("po/main/detail/?id=$list->id") ?>'><i class='fa fa-book'></i>Detail</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>