<div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->code ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Section</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->section->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Budget Final</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <a title='Detail Budget Final' onclick='loadModal(this)' target='proposal/final/detail'
               data='id=<?= $data->budgetFinal->id ?>'>[<?= $data->budgetFinal->code ?>
                ] <?= $data->budget->display_name ?></a>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Price Estimation</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_price_estimation ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->date ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->comment ?></div>
    </div>
</div>