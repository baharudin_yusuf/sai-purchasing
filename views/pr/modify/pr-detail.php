<?php use yii\helpers\Html; ?>

<div class="col-lg-12 col-md-12">
    <form onsubmit='return false;' id='form-revision-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>Goods/Service</th>
                <th>Carline</th>
                <th>Quantity</th>
                <th>Price Estimation</th>
                <th>Sub Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->getNonCancelReject() as $detail) { ?>
                <tr class="nowrap">
                    <td><?= $detail->goodsService->name ?></td>
                    <td class='center'><?= $detail->carline->name ?></td>

                    <?php if ($detail->available_quantity == 0) { ?>
                        <td class='center'><?= $detail->quantity ?></td>
                    <?php } else { ?>
                        <td class='center' style='padding:2px;'>
                            <?= Html::input('number', "item-".$detail->id, $detail->quantity, ['min' => $detail->used_quantity, 'max' => $detail->quantity, 'class' => 'form-control']) ?>
                        </td>
                    <?php } ?>

                    <td class='center nowrap'>
                        <?= $detail->display_price_estimation ?>
                        <?php if(strlen($detail->display_converted_price_estimation) > 0){ ?>
                            <br>[<?= $detail->display_converted_price_estimation ?>]
                        <?php } ?>
                    </td>
                    <td class='center nowrap'>
                        <?= $detail->display_sub_total ?>
                        <?php if(strlen($detail->display_converted_sub_total) > 0){ ?>
                            <br>[<?= $detail->display_converted_sub_total ?>]
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan='4'>Total</th>
                <th><strong><?= $data->display_price_estimation ?></strong></th>
            </tr>
            </tfoot>
        </table>
        <?= Html::input('hidden', 'id', $data->id) ?>
        <?= Html::input('submit', 'submit', 'Simpan Revisi', ['class' => 'btn btn-primary mb10']) ?>
    </form>
</div>

<script>
    $('#form-revision-detail').submit(function () {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data revisi sudah benar? Data tidak dapat diubah setelah disimpan!', function () {
            var data = getFormData('form-revision-detail');
            ajaxTransfer('pr/modify/save/', data, '#modal-output');
        });
    });
</script>
