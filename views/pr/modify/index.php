<?php
$this->title = 'Purchase Requisition Revision';
?>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-pr-data" data-toggle="tab">PR Data</a>
    </li>
    <li>
        <a href="#tab-pr-detail" data-toggle="tab">PR Detail</a>
    </li>
</ul>
<div class="tab-content bottom-margin">
    <div class="tab-pane active" id="tab-pr-data">
        <?php require 'pr-data.php'; ?>
    </div>
    <div class="tab-pane" id="tab-pr-detail">
        <?php require 'pr-detail.php'; ?>
    </div>
</div>


