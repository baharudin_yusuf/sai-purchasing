<?php
$this->title = 'Pending PR Revision';
?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Section</th>
            <th>Budget</th>
            <th>Amount</th>
            <th>Comment</th>
            <th>Date</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <?php $pr = $list->purchaseRequisition; ?>
            <tr class='nowrap <?= $list->is_rejected ? 'rejected' : '' ?>'>
                <td><?= $pr->section->name ?></td>
                <td><?= $pr->budget->name ?></td>
                <td class='center'><?= $pr->display_price_estimation ?></td>
                <td><?= $pr->comment ?></td>
                <td class='center'><?= $list->date ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("pr/main/detail/?id=" . $pr->id) ?>'
                       title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-select * {
        display: none;
    }
</style>
