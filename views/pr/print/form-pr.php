<table>
    <tr>
        <td><b style='font-size: 40px;'>SAI</b></td>
        <td style='text-align: center;padding: 0 20px;'>
            <b style='display: block;font-size: 18px;'>PT. SURABAYA AUTOCOMP INDONESIA</b>
            <i>Wiring Harness Manufacturer</i>
        </td>
    </tr>
</table>

<div style='  color: red; border: 2px solid red; text-align: center; display: inline-block; padding: 15px 10px; font-size: 25px; position: absolute; right: 0px; top: 0px; font-weight: bold;'>
    CONFIDENTIAL
</div>

<table style='width: 45%;display: block;float:left;'>
    <tr>
        <td style='text-align: center; padding: 5px 0 10px 0px;' colspan='3'><b><u style='font-size: 20px;'>PURCHASE
                    REQUISITION</u></b></td>
    </tr>
    <tr>
        <td style='width: 30mm;'>DATE</td>
        <td> :</td>
        <td style='border-bottom: 1px dotted;'><?= date('d F Y') ?></td>
    </tr>
    <tr>
        <td>REGISTERED NO</td>
        <td> :</td>
        <td style='border-bottom: 1px dotted;'><?= $data->code ?></td>
    </tr>
    <tr>
        <td>DEPT/SECTION</td>
        <td> :</td>
        <td style='border-bottom: 1px dotted;'><?= $data->section->name ?></td>
    </tr>
</table>

<table style='display: block;float:right;border-collapse:collapse;margin: 26px 0 0 0;'>
    <tr>
        <td style='border: 1px solid;width: 23mm;text-align: center;'>VERIFIED</td>
        <td style='border: 1px solid;width: 23mm;text-align: center;'>APPROVED 2</td>
        <td style='border: 1px solid;width: 23mm;text-align: center;'>APPROVED 1</td>
        <td style='border: 1px solid;width: 23mm;text-align: center;'>REQUESTED</td>
    </tr>
    <tr>
        <td style='border: 1px solid;text-align: center;height: 15mm;'></td>
        <td style='border: 1px solid;text-align: center;height: 15mm;'></td>
        <td style='border: 1px solid;text-align: center;height: 15mm;'></td>
        <td style='border: 1px solid;text-align: center;height: 15mm;'></td>
    </tr>
</table>

<table style='display:block;border-collapse:collapse;top: 1mm;position: relative;width:100%;table-layout:fixed;'>
    <thead>
    <tr>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;width: 5mm;'>NO</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;width: 22mm;'>BUDGET REF.NO</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;width: 45mm;'>DETAILS OF GOODS/SERVICES</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;width: 7mm;'>QTY</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>ESTIMATED PRICE</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>AMOUNT</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;width: 18mm;'>REQUIRED DATE</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>CARLINE</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($detail as $item) { ?>
        <tr style="border-top: <?= strlen($item['num']) > 0 ? '1' : '' ?>px dotted;">
            <?php if($item['rowspan'] != 0){ ?>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['num'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['budget_number'] ?></td>
            <?php } ?>
            <td style='border-left: 1px solid;border-right: 1px solid;text-align: left;padding: 0 1.5mm;'><?= $item['goods_service'] ?></td>
            <?php if($item['rowspan'] != 0){ ?>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['quantity'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['price_estimation'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['amount'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['required_date'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['carline'] ?></td>
            <?php } ?>
        </tr>
    <?php } ?>

    <?php if (is_null($nextURL)) { ?>
        <tr>
            <td style='border: 1px solid;text-align: center;padding: 0 1.5mm;' colspan='5'><b>TOTAL</b></td>
            <td style='border: 1px solid;text-align: center;padding: 0 1.5mm;'
                colspan="3"><?= $data->display_price_estimation ?></td>
        </tr>
    <?php } ?>

    <tr>
        <td style='border: 1px solid;text-align: left;padding: 0 1.5mm;' colspan='7'>REMARKS/REASONS :</td>
        <td style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>RECEIVED</td>
    </tr>
    <tr>
        <td style='border: 1px solid;text-align: left;padding: 0 1.5mm;vertical-align:top;'
            colspan='7'><?= $data->comment ?></td>
        <td style='border: 1px solid;text-align: center;padding: 0 1.5mm;height: 15mm;'></td>
    </tr>
    <tr>
        <td style='border: 1px solid;text-align: left;padding: 0 1.5mm;' colspan='7'></td>
        <td style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>PURCHASING</td>
    </tr>
    </tbody>
</table>

<table style='display: block;width:100%;border-collapse:collapse;margin: 2mm 0 0 0;'>
    <tr>
        <td style='width: 23mm;text-align: left;'>
            <span style='border: 1px solid;width: 40mm;display: inline-block;text-align: center;'>WHITE : VOUCHER</span>
        </td>
        <td style='width: 23mm;text-align: center;'>
            <span style='border: 1px solid;width: 40mm;display: inline-block;text-align: center;'>RED : PURCHASING</span>
        </td>
        <td style='width: 23mm;text-align: right;'>
            <span style='border: 1px solid;width: 40mm;display: inline-block;text-align: center;'>BLUE : DEPT</span>
        </td>
    </tr>
    <tr>
        <td colspan='3' style='text-align:right;'>FA-005-C</td>
    </tr>
</table>

<hr class='hide-on-print'>
<button class='hide-on-print' onclick='window.print()'>PRINT</button>
<?php if (!is_null($prevURL)) { ?>
    <a class='hide-on-print' href="<?= $prevURL ?>">
        <button>PREV</button>
    </a>
<?php } ?>
<?php if (!is_null($nextURL)) { ?>
    <a class='hide-on-print' href="<?= $nextURL ?>">
        <button>NEXT</button>
    </a>
<?php } ?>

<style>
    #page {
        height: 134mm;
        width: 190mm;
        position: relative;
    }

    @media print {
        .hide-on-print {
            display: none;
        }
    }
</style>
