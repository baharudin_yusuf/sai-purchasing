<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>PR ID</th>
            <th>Goods Service</th>
            <th>Quantity</th>
            <th>Used</th>
            <th>Available</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr class='nowrap <?= ($list->is_rejected || $list->is_canceled) ? 'rejected' : '' ?>'>
                <td><?= $list->purchaseRequisition->code ?></td>
                <td><?= $list->goodsService->name ?></td>
                <td class='center'><?= $list->quantity ?></td>
                <td class='center'><?= $list->used_quantity ?></td>
                <td class='center'><?= $list->available_quantity ?></td>
                <td class='center'>
                    <a href='<?= url("pr/main/detail/?id=" . $list->purchase_requisition_id) ?>'
                       class='btn btn-primary btn-xs'>
                        <i class='fa fa-book'></i>Detail
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a href="<?= url('pr/report/download-item') ?>" class='btn btn-primary btn-sm'
       style='margin:5px 0 0 0;'><i class='fa fa-download'></i> Download Data Laporan</a>

    <hr style="margin:10px 0;" class="include-exclude-rejected-panel">
    <form id='pr-open-close' method='post' action='' class="form-inline include-exclude-rejected-panel" role="form">
        <div class="form-group">
            <div class="checkbox">
                <label><input <?= $only_close ? 'checked' : '' ?> type="checkbox" name="only-close" value='1'> Only PR
                    Close</label>
            </div>
        </div>
        &nbsp;|&nbsp;
        <div class="form-group">
            <div class="checkbox">
                <label><input <?= $only_open ? 'checked' : '' ?> type="checkbox" name="only-open" value='1'> Only PR
                    Open</label>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });

    $('#pr-open-close input').change(function () {
        if ($(this).is(':checked')) {
            $('#pr-open-close input').prop('checked', false);
            $(this).prop('checked', true);
        }

        $('#pr-open-close').submit();
    });
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-closed * {
        display: none;
    }
</style>
