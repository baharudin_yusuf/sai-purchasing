<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-budget-detail' class='form-horizontal col-lg-12 col-md-12'>
	<div id='result-purchase-requisition-detail'></div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Goods/Service</label>
		<div class='col-lg-9 col-md-9'>
			<?= Html::dropdownList('goods_service_source', null, $goodsServiceOption, ['class'=>'form-control', 'required'=>'', 'style'=>'margin:0 0 5px 0', 'onchange'=>'getGoodsServiceOption()']) ?>
			<?= Html::dropdownList('goods_service_id', null, [], ['class'=>'form-control', 'required'=>'', 'onchange'=>'getGoodsServiceData()']) ?>
		</div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Quantity</label>
		<div class='col-lg-9 col-md-9'>
			<?= Html::input('number', 'quantity', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Quantity']) ?>
		</div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Price Estimation</label>
		<div class='col-lg-9 col-md-9 form-control-static' id='td-price-estimation'></div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Currency</label>
		<div class='col-lg-9 col-md-9 form-control-static' id='td-currency'></div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Category</label>
		<div class='col-lg-9 col-md-9 form-control-static' id='td-category'></div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Carline</label>
		<div class='col-lg-9 col-md-9'>
			<?= Html::dropDownList('carline_id', null, $carlineOption, ['class'=>'form-control']) ?>
		</div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Required Date</label>
		<div class='col-lg-9 col-md-9'>
			<?= Html::input('text', 'required_date', date('Y-m-d'), ['class'=>'form-control', 'readonly'=>'', 'required'=>'']) ?>
		</div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'>Comment</label>
		<div class='col-lg-9 col-md-9'>
			<?= Html::textarea('comment', null, ['class'=>'form-control', 'placeholder'=>'Comment']) ?>
		</div>
	</div>
	<div class='form-group'>
		<label class='col-lg-3 col-md-3 control-label'></label>
		<div class='col-lg-9 col-md-9'>
			<?= Html::input('submit', 'simpan', "Simpan", ['class'=>'btn btn-primary', 'id' => 'btn-save-pr-detail']) ?>
		</div>
	</div>

	<?= Html::hiddenInput('session_key', $sessionKey) ?>
	<?= Html::hiddenInput('budget_final_id', $budgetFinal->id) ?>
</form>

<script>
    function getGoodsServiceOption() {
        var data = getFormData('form-budget-detail');
        ajaxTransfer('pr/create/get-goods-service-option', data, '#result-purchase-requisition-detail');
    }

    function getGoodsServiceData(){
        var data = getFormData('form-budget-detail');
        ajaxTransfer('pr/create/get-goods-service-data', data, '#result-purchase-requisition-detail');
    }

    $(document).ready(function () {
        getGoodsServiceOption();
        date_input('input[name="required_date"]', 'date');
        chosenConvert('select[name="goods_service_id"]');
        chosenConvert('select[name="carline_id"]');

        $('#form-budget-detail').submit(function(){
            var data = getFormData('form-budget-detail');
            ajaxTransfer('pr/create/save-detail', data, '#result-purchase-requisition-detail');
        });
    });
</script>
<style>
.tt-hint{display: none;}
</style>
