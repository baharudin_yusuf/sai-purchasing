<?php

use yii\helpers\Html;

$this->title = 'Create New Purchase Requisition';
?>

<?php if (empty($budgetFinalOption)) { ?>
    <div class="alert alert-danger">
        Perhatian! Tidak ada Budget Final tersedia!
    </div>
    <script>
        $(document).ready(function () {
            $('#form-purchase-requisition').find('*').prop('disabled', true);
        });
    </script>
<?php } ?>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-purchase-requisition-data" data-toggle="tab">Purchase Requisition Data</a>
    </li>
    <li>
        <a href="#tab-purchase-requisition-detail" data-toggle="tab">Purchase Requisition Detail</a>
    </li>
</ul>
<div class="tab-content bottom-margin">
    <div class="tab-pane active" id="tab-purchase-requisition-data">
        <form style="margin-bottom: 100px;" onsubmit="return false;" id='form-purchase-requisition'
              class='form-horizontal col-lg-12 col-md-12'>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Section</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $activeUser->section->name ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Budget Final</label>
                <div class='col-lg-9 col-md-9'>
                    <div class='input-group m-bot15'>
                        <?= Html::dropDownList('budget_final_id', null, $budgetFinalOption, ['class' => 'form-control', 'onchange' => 'clearDataPR()']) ?>
                        <span class='input-group-btn'>
						<button onclick='budgetFinalDetail()' class='btn btn-primary' type='button'><i
                                    class='fa fa-info-circle'></i>Info</button>
					</span>
                    </div>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Comment</label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Comment']) ?>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'></label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?= Html::hiddenInput('session_key', $sessionKey) ?>
        </form>
    </div>
    <div class="tab-pane" id="tab-purchase-requisition-detail">
        <div class="col-lg-12 col-md-12">
            <a onclick='addPRDetail()' style='margin:10px 0' class='btn btn-primary'><i class='fa fa-plus'></i> Add
                Purchase Requisition Detail</a>
            <a onclick='addAllFromFinal()' style='margin:10px 0' class='btn btn-primary'><i class='fa fa-plus'></i> Add All From Budget Final</a>
            <div id='output-purchase-requisition-detail'></div>
        </div>
    </div>
</div>


<script>
    function clearDataPR() {
        var data = getFormData('form-purchase-requisition');
        ajaxTransfer('pr/create/clear-data', data, '#output-purchase-requisition-detail');
    }

    function addPRDetail() {
        var data = getFormData('form-purchase-requisition');
        modalAlert('Add Purchase Requisition Detail', '');
        ajaxTransfer('pr/create/add-new', data, '#modal-output');
    }

    function budgetFinalDetail() {
        var data = getFormData('form-purchase-requisition');
        modalAlert('Budget Final Detail', '');
        ajaxTransfer('proposal/final/detail', data, '#modal-output');
    }

    function addAllFromFinal() {
        modalConfirm('Konfirmasi Tindakan', 'Tambahkan seluruh data dari budget final?', function () {
            var data = getFormData('form-purchase-requisition');
            ajaxTransfer('pr/create/add-all-from-final', data, '#modal-output');
        });
    }

    function loadPRDetail() {
        var data = getFormData('form-purchase-requisition');
        ajaxTransfer('pr/create/load-data', data, '#output-purchase-requisition-detail');
    }

    $('document').ready(function () {
        chosenConvert('select[name="budget_final_id"]');

        $('#form-purchase-requisition').submit(function () {
            modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data sudah benar? Data tidak dapat diubah setelah disubmit', function () {
                var data = getFormData('form-purchase-requisition');
                ajaxTransfer('pr/create/save-data', data, '#modal-output');
            });
        });
    });
</script>

