<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Module</th>
        <th>Module Item</th>
        <th>Access</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($aclList as $group => $child){ ?>
        <tr>
            <td style="vertical-align: middle;" rowspan="<?= count($child) ?>"><?= ucwords(dealias($group)) ?></td>
            <td><?= ucwords(dealias($child[0])) ?></td>
            <td class="center">
                <?php $status = $role->allowTo($group, $child[0], $departmentId, $sectionId); ?>
                <?php $toggle = implode(':', [$group, $child[0], $departmentId, $sectionId]); ?>
                <a onclick="toggleACL(this)" data-toggle="<?= $toggle ?>" class="on-off-switch <?= $status ? 'on' : 'off' ?>"></a>
            </td>
        </tr>
        <?php for($i=1; $i<count($child); $i++){ ?>
            <tr>
                <td><?= ucwords(dealias($child[$i])) ?></td>
                <td class="center">
                    <?php $status = $role->allowTo($group, $child[$i], $departmentId, $sectionId); ?>
                    <?php $toggle = implode(':', [$group, $child[$i], $departmentId, $sectionId]); ?>
                    <a onclick="toggleACL(this)" data-toggle="<?= $toggle ?>" class="on-off-switch <?= $status ? 'on' : 'off' ?>"></a>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>

<script>
    function toggleACL(t) {
        var toggle = $(t).attr('data-toggle').split(':');
        var data = {
            role_id: '<?= $role->id ?>',
            module: toggle[0],
            item: toggle[1],
            department_id: toggle[2],
            section_id: toggle[3]
        };

        ajaxTransfer('acl/set-access', data, function (result) {
            $(t).removeClass('on').removeClass('off').addClass(result);
        });
    }
</script>