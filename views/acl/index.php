<?php $this->title = 'Access Control List'; ?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Role</th>
            <th>Department</th>
            <th>Section</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $role) { ?>
            <?php if($role->alias == 'manager'){ ?>
                <?php foreach ($department as $dept) { ?>
                    <tr>
                        <td><?= $role->name ?></td>
                        <td class="center"><?= $dept->name ?></td>
                        <td class="center">-
                        <td class='center nowrap'>
                            <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='acl/modify' data='id=<?= $role->id ?>;department_id=<?= $dept->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                        </td></td>
                    </tr>
                <?php } ?>
            <?php } else if($role->alias == 'section'){ ?>
                <?php foreach ($section as $sect) { ?>
                    <tr>
                        <td><?= $role->name ?></td>
                        <td class="center"><?= $sect->department->name ?></td>
                        <td class="center"><?= $sect->name ?></td>
                        <td class='center nowrap'>
                            <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='acl/modify' data='id=<?= $role->id ?>;department_id=<?= $sect->department_id ?>;section_id=<?= $sect->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                        </td></td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                <tr>
                    <td><?= $role->name ?></td>
                    <td class="center">-</td>
                    <td class="center">-
                    <td class='center nowrap'>
                        <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='acl/modify' data='id=<?= $role->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    </td></td>
                </tr>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>