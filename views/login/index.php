<?php
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/fullcalendar.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/datatables/datatables.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/datatables/bootstrap.datatables.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/jquery.timepicker.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/daterangepicker-bs3.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/colpick.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/dropzone.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/jquery.handsontable.full.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/jscrollpane.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/jquery.pnotify.default.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/plugins/jquery.pnotify.default.icons.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/template/saturn/css/app.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/css/chosen.min.css') ?>'>
    <link rel='stylesheet' href='<?= url('assets/css/typeahead.css') ?>'>

    <link href="<?= url('favicon.ico') ?>" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    @javascript html5shiv respond.min
    <![endif]-->

    <script> var baseURL = '<?= url() ?>'; </script>
    <link href="<?= url('assets/css/app.css') ?>" rel="stylesheet">
    <link href="<?= url('assets/css/jquery.datetimepicker.css') ?>" rel="stylesheet">
    <script src='<?= url('assets/script/jquery-1.11.2.min.js') ?>'></script>
    <script src='<?= url('assets/script/app.js') ?>'></script>
    <script src='<?= url('assets/script/lib.js') ?>'></script>
    <script src='<?= url('assets/script/core.js') ?>'></script>
    <script src='<?= url('assets/script/sai.js') ?>'></script>
    <script src='<?= url('assets/script/jquery.datetimepicker.js') ?>'></script>

    <title>Surabaya Autocomp Indonesia</title>
</head>

<body class="glossed">
<div class="all-wrapper no-menu-wrapper light-bg">
    <div class="row" style="margin-top: 175px;">
        <div class="col-md-4 col-md-offset-4">
            <div class="widget widget-blue">
                <div class="widget-title">
                    <h3 class="text-center"><i class="fa fa-lock"></i> SAI Login</h3>
                </div>
                <div class="widget-content">
                    <div id="result-login"></div>
                    <form id="form-login" onsubmit="return false;" role="form">
                        <div class="form-group relative-w">
                            <input name="email" type="email" class="form-control" placeholder="Enter email" required>
                            <i class="fa fa-envelope input-abs-icon"></i>
                        </div>
                        <div class="form-group relative-w">
                            <input name="password" type="password" class="form-control" placeholder="Password" required>
                            <i class="fa fa-lock input-abs-icon"></i>
                        </div>
                        <input type="submit" class="btn btn-primary btn-block" value="Login" />
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div id='loading-overlay'></div>
<div id='global-temp'></div>

<script>
    $('#form-login').submit(function () {
        var data = getFormData('form-login');
        ajaxTransfer('login/validate', data, '#result-login');
    })
</script>
<script src='<?= url('assets/template/saturn/js/plugins/datatables/jquery.dataTables.min.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/datatables/jquery.dataTables.columnFilter.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/jquery.pnotify.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/tab.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/dropdown.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/tooltip.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/collapse.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/transition.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/moment.min.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/modal.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/application.js') ?>'></script>
<script src='<?= url('assets/script/chosen.jquery.min.js') ?>'></script>
<script src='<?= url('assets/script/typeahead.bundle.js') ?>'></script>
<script src='<?= url('assets/script/accounting.min.js') ?>'></script>
<?= Html::hiddenInput('base_url', url()) ?>
</body>
</html>