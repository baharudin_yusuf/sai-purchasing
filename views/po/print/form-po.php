<div style='border-bottom: 4px solid;position: relative;top: -12px;'>
    <b style='font-size: 30px;background: #fff;position: relative;top: 12px'>SAI</b>
</div>

<div style='display: inline-block;width: 100%;'>
    <div style='width: 60%;display: inline-block;float: left;position: relative;'>
        <div style='  color: red; border: 2px solid red; text-align: center; display: inline-block; padding: 15px 10px; font-size: 25px; position: absolute; right: 20px; top: 10px; font-weight: bold;'>
            CONFIDENTIAL
        </div>
        <b style='position: relative;margin: 70px 0 3px 0;display: block;font-size: 16px;text-indent: 50px;'>PURCHASE
            ORDER</b>
        <div style='border: 1px solid;margin: 0 10px 0 0;position: relative;'>
            <div style='text-align: center;'><?= $data->supplier->name ?></div>
            <div style='text-align: center;'><?= $data->supplier->address ?></div>
            <div style='text-align: center;'>
                TELP <?= $data->supplier->phone1 . ' ' . $data->supplier->phone2 . ' ' . $data->supplier->phone3 ?></div>
            <div style='text-align: center;'>
                FAX <?= (strlen($data->supplier->fax) == 0 ? '-' : $data->supplier->fax) ?></div>
            <div style='text-align: center;'><?= $data->supplier->authorized_person ?></div>
        </div>
    </div>

    <div style='width: 40%;display: inline-block;float: left;'>
        <div>
            <div><b>PT.SURABAYA AUTOCOMP INDONESIA</b></div>
            <div>NGORO INDUSTRI PERSADA KAV-T-1</div>
            <div>PO.BOX 11, NGORO - MOJOKERTO 61385</div>
            <div>EAST JAVA - INDONESIA</div>
            <div>Phone: +62-321-6817400, Fax: +62-321-6817541</div>
        </div>

        <table style='width: 100%; table-layout: fixed;'>
            <tr>
                <td style='width: 20mm;'>NO</td>
                <td style='width: 2mm;'>:</td>
                <td style='position: relative;'>
                    <?= $data->code ?> (R0) P<?= $page ?>
                </td>
            </tr>
            <tr>
                <td>DATE</td>
                <td>:</td>
                <td><?= date('d-F-Y') ?></td>
            </tr>
            <tr>
                <td>TERM</td>
                <td>:</td>
                <td><?= $data->term->name ?></td>
            </tr>
            <tr>
                <td>DELIVERY</td>
                <td>:</td>
                <td><?= date('d-F-Y', strtotime($data->estimated_time_arival)) ?></td>
            </tr>
            <tr>
                <td>FRANCO</td>
                <td></td>
                <td><?= $data->franco->name ?></td>
            </tr>
        </table>
    </div>
</div>

<table style='width:100%;margin:5px 0;border-bottom: 1px solid #000;'>
    <thead>
    <tr>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>PR.NO</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;width: 100mm;'>DESCRIPTION OF GOODS</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>UNIT</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>QTY</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>UNIT PRICE</th>
        <th style='border: 1px solid;text-align: center;padding: 0 1.5mm;'>AMOUNT</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($detail as $item) { ?>
        <tr style="border-top: <?= strlen($item['num']) > 0 ? '1' : '' ?>px dotted;">
            <?php if ($item['rowspan'] != 0) { ?>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;white-space: nowrap;'><?= $item['code'] ?></td>
            <?php } ?>
            <td style='border-left: 1px solid;border-right: 1px solid;text-align: left;padding: 0 1.5mm;'><?= $item['goods_service'] ?></td>
            <?php if ($item['rowspan'] != 0) { ?>
                <td rowspan="<?= $item['rowspan'] ?>"
                    style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;'><?= $item['unit'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: center;padding: 0 1.5mm;white-space: nowrap;'><?= $item['quantity'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['price'] ?></td>
                <td rowspan="<?= $item['rowspan'] ?>" style='border-left: 1px solid;border-right: 1px solid;text-align: right;padding: 0 1.5mm;white-space: nowrap;'><?= $item['amount'] ?></td>
            <?php } ?>
        </tr>
    <?php } ?>
    </tbody>

    <?php

    /*$totalRT = [];
    $totalVAT = [];
    $totalRaw = [];
    foreach ($detail as $item) {
        if (!$item['is_note']) {
            $totalRT[$item['id']] = $item['rev_tax_amount'];
            $totalVAT[$item['id']] = $item['vat_amount'];
            $totalRaw[$item['id']] = $item['quantity'] * $item['c_price'];
        }
    }
    $total = array_sum($totalRaw) + array_sum($totalVAT) - array_sum($totalRT);
    */
    ?>

    <?php if (is_null($nextURL)) { ?>
        <tr>
            <td style='border: 1px solid;padding: 0 1.5mm;' colspan='5'>Subtotal</td>
            <td style='white-space: nowrap;border: 1px solid;text-align: right;padding: 0 1.5mm;'>
                <?= $data->display_subtotal_amount_realization ?>
            </td>
        </tr>
        <tr>
            <td style='border: 1px solid;padding: 0 1.5mm;' colspan='5'>Value Added Tax</td>
            <td style='white-space: nowrap;border: 1px solid;text-align: right;padding: 0 1.5mm;'>
                <?= $data->display_vat_amount ?>
            </td>
        </tr>
        <tr>
            <td style='border: 1px solid;padding: 0 1.5mm;' colspan='5'>Income Tax</td>
            <td style='white-space: nowrap;border: 1px solid;text-align: right;padding: 0 1.5mm;'>
                <?= $data->display_rev_tax_amount ?>
            </td>
        </tr>
        <tr>
            <td style='border: 1px solid;padding: 0 1.5mm;' colspan='5'>Total</td>
            <td style='white-space: nowrap;border: 1px solid;text-align: right;padding: 0 1.5mm;'>
                <?= $data->display_amount_realization ?>
            </td>
        </tr>
    <?php } ?>
</table>

<table style='width: 73%;float: left;'>
    <tr>
        <td style='width: 25mm;vertical-align: top;border: 1px solid;padding: 4px 15px;'>Accepted By :</td>
        <td style='vertical-align: top;border: 1px solid;padding: 2px 5px;' rowspan='2'>
            Remarks :
            <ul style='padding: 0 0 0 15px;list-style-type: disc;margin: 0;'>
                <li>Please enclose Signed PO in Official Receipts/Invoice</li>
                <li>NPWP : 02.007.759.0-641.000</li>
                <li>Address of Faktur Pajak :</li>
                <li style='list-style-type: none;'>Ngoro Industri Persada T-1, Ngoro, Ngoro, Kab.Mojokerto, Jawa Timur
                </li>
                <li>PO number must appear on all corespondence, invoice, and shipping papers</li>
                <li>Invoice can be received after all goods and document were received</li>
                <li>Please refax PO after signed and stamped</li>
            </ul>
            <!--hr style='margin: 5px 0;border: 0 none;border-top: 1px solid #B6B6B6;'-->
            <ul style='padding: 0 0 0 15px;list-style-type: disc;margin: 5px 0;'>
                <li><b>Wajib Mematuhi Kebijakan Lingkungan dan K3</b></li>
            </ul>
        </td>
    </tr>
    <tr>
        <td style='vertical-align: top;border: 1px solid;padding: 4px 15px;'>Date :</td>
    </tr>
</table>

<div style='display: block;float: right;width: 25%;text-align: center;'>
<span style='border-bottom: 1px solid;display: block;top: 165px;position: relative;'>
    <?php if(!is_null($data->approvedBy)){ ?>
	<div style='  position: absolute; width: 190px; left: 0; height: 160px; top: -160px;'>
        <img src="<?= $data->approvedBy->signature_url ?>" style="position: absolute; left: 0; bottom: 0; width: 100%;">
	</div>
    <?= $data->approvedBy->name ?>
    <?php } ?>
</span>
</div>

<table style='width: 100%;margin: 5px 0;display: inline-block;text-align: center;'>
    <!--tr>
        <td>
            <span style='display: block;border: 1px solid;padding: 2px 0;'>WHITE : VOUCHER</span>
        </td>
        <td>
            <span style='display: block;border: 1px solid;padding: 2px 0;'>RED : FINANCE</span>
        </td>
        <td>
            <span style='display: block;border: 1px solid;padding: 2px 0;'>YELLOW : PURCHASING</span>
        </td>
        <td>
            <span style='display: block;border: 1px solid;padding: 2px 0;'>GREEN : RECEIVING</span>
        </td>
    </tr-->
    <tr>
        <td colspan='4' style='text-align: right;'>FA-006-0</td>
    </tr>
</table>
<hr class='hide-on-print'>
<button class='hide-on-print' onclick='window.print()'>PRINT</button>
<?php if (!is_null($prevURL)) { ?>
    <a class='hide-on-print' href="<?= $prevURL ?>">
        <button>PREV</button>
    </a>
<?php } ?>
<?php if (!is_null($nextURL)) { ?>
    <a class='hide-on-print' href="<?= $nextURL ?>">
        <button>NEXT</button>
    </a>
<?php } ?>

<style>
    #page {
        width: 200mm;
        height: 290mm;
    }

    table {
        border-collapse: collapse;
    }

    @media print {
        .hide-on-print {
            display: none;
        }
    }
</style>
