<?php use yii\helpers\Html; ?>

<div id='result-add-pr'></div>
<form onsubmit='return false;' id='form-add-pr' class='form-horizontal col-lg-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Supplier</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= $supplier->name ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Department</label>
        <div class='col-lg-9 col-md-9'>
            <?php if (!is_null($department)) { ?>
                <?= Html::textInput('department', $department->name, ['class' => 'form-control', 'readonly' => '']) ?>
                <?= Html::hiddenInput('department_id', $department->id) ?>
            <?php } else { ?>
                <?= Html::dropDownList('department_id', null, $departmentOption, ['required' => '', 'class' => 'form-control chosen-select', 'onchange' => 'loadSection()']) ?>
            <?php } ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Section</label>
        <div class='col-lg-9 col-md-9'>
            <?php if (!is_null($section)) { ?>
                <?= Html::textInput('section', $section->name, ['class' => 'form-control', 'readonly' => '']) ?>
                <?= Html::hiddenInput('section_id', $section->id) ?>
            <?php } else { ?>
                <?= Html::dropDownList('section_id', null, $sectionOption, ['required' => '', 'class' => 'form-control chosen-select', 'onchange' => 'loadPROpen()']) ?>
            <?php } ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Purchase Requisition</label>
        <div class='col-lg-9 col-md-9'>
            <div class='input-group m-bot15' id='budget-final-option'>
                <?= Html::dropDownList('purchase_requisition_id', null, [], ['required' => '', 'class' => 'form-control chosen-select']) ?>
                <span class='input-group-btn'>
					<button onclick='getPRInfo()' class='btn btn-primary' type='button'><i
                                class='fa fa-info-circle'></i>Info</button>
				</span>
            </div>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::hiddenInput('supplier_id', $supplier->id) ?>
    <?= Html::hiddenInput('session_key', $sessionKey) ?>
</form>

<div id='display-pr-detail' style='display:none;'>
    <div id='content'></div>
    <a class='btn btn-primary' onclick='backToAddPR()'>Kembali</a>
</div>

<script>
    function loadSection() {
        var data = getFormData('form-add-pr');
        ajaxTransfer('po/create/get-section', data, function (result) {
            $('select[name=section_id]').html(result).trigger('chosen:updated');
            loadPROpen();
        });
    }

    function loadPROpen() {
        var data = getFormData('form-add-pr');
        ajaxTransfer('po/create/get-pr-open', data, function (result) {
            $('select[name=purchase_requisition_id]').html(result).trigger('chosen:updated');
        });
    }

    function getPRInfo() {
        var data = getFormData('form-add-pr', true);
        var prId = parseInt(data['purchase_requisition_id']);

        if (isNaN(prId) || prId === 0) {
            alert('Error! Harus ada PR yang dipilih!');
        } else {
            $('#form-add-pr').css({display: 'none'});
            $('#display-pr-detail').css({display: 'block'});
            ajaxTransfer('pr/main/detail/', {id: prId}, '#display-pr-detail #content');
        }
    }

    function backToAddPR() {
        $('#form-add-pr').css({display: 'block'});
        $('#display-pr-detail').css({display: 'none'});
        $('#display-pr-detail #content').html('');
    }

    $(document).ready(function () {
        chosenConvert('#form-add-pr .chosen-select');
        loadPROpen();

        $('#form-add-pr').submit(function () {
            var data = getFormData('form-add-pr');
            if (confirm('Apakah anda yakin akan menambahkan PR pada detail Purchase Order?')) {
                ajaxTransfer('po/create/add-new-pr', data, '#result-add-pr');
            }
        });
    })

</script>
