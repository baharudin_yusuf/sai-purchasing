<?php

use yii\helpers\Html;

?>
<form id='form-detail' style='margin:0 0 10px 0;' onsubmit='return false'>
    <table style='margin: 10px 0' class='table table-bordered table-striped'>
        <thead>
        <tr>
            <th>PR Code</th>
            <th>Goods/Service</th>
            <th>Quantity</th>
            <th>Rev Tax</th>
            <th>VAT</th>
            <th>Price Estimation</th>
            <th>Sub Total + Tax</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $pr) { ?>
            <?php foreach ($pr->getNonCancelReject() as $detail) { ?>
                <?php if (isset($addedPR[$pr->id][$detail->id])) { ?>
                    <?php $inputKey = $pr->id . '_' . $detail->id; ?>
                    <?php $aprData = $addedPR[$pr->id][$detail->id]; ?>

                    <tr class='pr-detail-item nowrap'>
                        <td><?= $pr->code ?></td>
                        <td><?= $detail->goodsService->name ?></td>
                        <td style='padding:2px;width: 75px;'>
                            <?= Html::input('number', "quantity_$inputKey", $aprData['quantity'], ['class' => 'form-control quantity', 'min' => 1, 'max' => $detail->available_quantity, 'required' => '', 'style' => 'text-align:right;', 'onchange' => 'updateDataPR()', 'onkeyup' => 'updateDataPR()']) ?>
                        </td>
                        <td style='padding:2px;width: 100px;'>
                            <?= Html::dropDownList("rev_tax_$inputKey", $aprData['rev_tax_id'], $revTaxOption, ['class' => 'form-control rev-tax', 'required' => '', 'onchange' => 'updateDataPR()']) ?>
                        </td>
                        <td style='padding:2px;width: 100px;'>
                            <?= Html::dropDownList("vat_$inputKey", $aprData['vat_id'], $vatOption, ['class' => 'form-control', ($supplier->is_pkp ? 'required' : 'disabled') => '', 'onchange' => 'updateDataPR()']) ?>
                        </td>
                        <td style='padding:2px;'>
                            <div class='input-group m-bot15'>
                                <span class='input-group-addon kurs'><?= $detail->goodsService->currency->name ?></span>
                                <?php $treshold = ($threshold / 100) * $detail->price_estimation; ?>
                                <?php $maxPrice = $detail->price_estimation + $treshold; ?>
                                <?= Html::input('number', "price_estimation_$inputKey", $aprData['price_estimation'], ['class' => 'form-control price-estimation', 'step' => 0.01, 'required' => '', 'onchange' => 'updateDataPR()', 'max' => $maxPrice, 'min' => 0]) ?>
                            </div>
                        </td>
                        <td class='center display-sub-total-tax'>
                            <?= $aprData['display_converted_price'] ?>
                        </td>
                        <td class='center'>
                            <a class='btn btn-danger btn-xs' onclick='removeItem(<?= $pr->id ?>, <?= $detail->id ?>)'><i
                                        style='margin:0;' class='fa fa-times'></i></a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan='6'>Total</th>
            <th id="display-total-tax"><?= $displayTotal ?></th>
            <th></th>
        </tr>
        </tfoot>
    </table>
    <input id='update-data-detail' onclick="updateDataPR()" type='submit' class='btn btn-primary' value='Update Data'>
    <?= Html::hiddenInput('session_key', $sessionKey) ?>
    <?= Html::hiddenInput('selected_supplier_id', $supplier->id) ?>
</form>

<script>
    function updateDataPR() {
        var data = getFormData('form-detail');
        ajaxTransfer('po/create/update-added-pr', data, '#output-purchase-order-detail');
    }

    function removeItem(prId, detailId) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus item detail?', function () {
            ajaxTransfer('po/create/delete-added-pr-item', {
                session_key: '<?= $sessionKey ?>',
                purchase_requisition_id: prId,
                detail_id: detailId
            }, '#modal-output');
        });
    }

    $(document).ready(function () {
        if ($('.pr-detail-item').length > 0) {
            $('select[name=supplier_id]').attr('disabled', 'disabled').trigger('chosen:updated');
            $('#notif-supplier').removeClass('hidden');
        } else {
            $('select[name=supplier_id]').removeAttr('disabled').trigger('chosen:updated');
            $('#notif-supplier').addClass('hidden');
        }
    })
</script>
