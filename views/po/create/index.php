<?php
use yii\helpers\Html;
$this->title = 'Create New Purchase Order';
?>

<?php if($prReadyCount == 0) { ?>
    Perhatian! Tidak ada data PR tersedia untuk Purchase Order!
    <script>
        $(document).ready(function(){
            $('#form-purchase-order').find('*').prop('disabled', true);
        });
    </script>
<?php } ?>

<ul class="nav nav-tabs">
	<li class="active">
		<a href="#tab-purchase-order-data" data-toggle="tab">Purchase Order Data</a>
	</li>
	<li>
		<a href="#tab-purchase-order-detail" data-toggle="tab">Purchase Order Detail</a>
	</li>
</ul>
<div class="tab-content bottom-margin">
	<div class="tab-pane active" id="tab-purchase-order-data">
		<form onsubmit="return false;" id='form-purchase-order' style='margin:10px 0;' class='form-horizontal col-lg-12 col-md-12'>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'>Term</label>
				<div class='col-lg-9 col-md-9'>
					<?= Html::dropDownList('term_id', null, $termOption, ['class'=>'form-control']) ?>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'>Franco</label>
				<div class='col-lg-9 col-md-9'>
					<?= Html::dropDownList('franco_id', null, $francoOption, ['class'=>'form-control']) ?>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'>Supplier</label>
				<div class='col-lg-9 col-md-9'>
					<?= Html::dropDownList('supplier_id', null, $supplierOption, ['class'=>'form-control', 'onchange'=>'setSupplierPKP()']) ?>
					<small id="notif-supplier" class="red hidden">Data supplier tidak dapat diubah ketika sudah ada PR ditambahkan</small>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'>Est. Time Arrival</label>
				<div class='col-lg-9 col-md-9'>
					<?= Html::input('text', 'estimated_time_arival', null, ['class'=>'form-control date', 'required'=>'', 'placeholder'=>'Estimation Time Arrival']) ?>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'>Down Payment</label>
				<div class='col-lg-9 col-md-9'>
					<div class="input-group m-bot15">
						<span class="input-group-addon"><?= $baseCurrency->name ?></span>
						<?= Html::input('number', 'down_payment_amount', null, ['class'=>'form-control', 'placeholder'=>'Down Payment']) ?>
					</div>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'>Comment</label>
				<div class='col-lg-9 col-md-9'>
					<?= Html::textarea('comment', null, ['class'=>'form-control', 'placeholder'=>'Comment']) ?>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-lg-3 col-md-3 control-label'></label>
				<div class='col-lg-9 col-md-9'>
					<?= Html::input('submit', 'submit', 'Submit', ['class'=>'btn btn-primary']) ?>
				</div>
			</div>
			<?= Html::hiddenInput('final_supplier_id') ?>
            <?= Html::hiddenInput('session_key', $sessionKey) ?>
		</form>
	</div>
	<div class="tab-pane" id="tab-purchase-order-detail">
		<div class="col-lg-12 col-md-12">
			<a style='margin: 10px 0;' class='btn btn-primary' onclick='addDataPR()' title='Tambah Data PR'><i class='fa fa-plus'></i> Tambah Data PR</a>
			<div id='output-purchase-order-detail'></div>
		</div>
	</div>
</div>

<script>
	function loadAddedPR(){
	    var data = getFormData('form-purchase-order');
		ajaxTransfer('po/create/load-added-pr', data, '#output-purchase-order-detail');
	}
	
	function addDataPR() {
		var data = getFormData('form-purchase-order');
		modalAlert('Tambah Data PR', '');
		ajaxTransfer('po/create/add-pr', data, '#modal-output');
	}

	function setSupplierPKP() {
		var supplierId = parseInt($('select[name=supplier_id]').val());
		$('input[name=final_supplier_id]').val(supplierId);
	}

	$(document).ready(function () {
		date_input('input[name="estimated_time_arival"]', 'datetime');
		setSupplierPKP();

		chosenConvert('select[name=term_id]');
        chosenConvert('select[name=franco_id]');
        chosenConvert('select[name=supplier_id]');

		$('#form-purchase-order').submit(function(){
			modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data sudah benar? Data tidak dapat diubah setelah disubmit', function () {
                var data = getFormData('form-purchase-order');
                ajaxTransfer('po/create/save-data', data, '#modal-output');
            });
		});
	});
</script>

