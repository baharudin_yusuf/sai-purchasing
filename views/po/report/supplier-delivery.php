<?php
$this->title = 'Supplier Delivery';
?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>PO ID</th>
            <th>Supplier</th>
            <th>Est Time Arrival</th>
            <th>Actual Time Arrival</th>
            <th>Ontime</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr class="nowrap">
                <td><?= $list->code ?></td>
                <td><?= $list->supplier->name ?></td>
                <td class='center'><?= $list->estimated_time_arival ?></td>
                <td class='center'><?= strlen($list->actual_time_arival) != 0 ? $list->actual_time_arival : '-' ?></td>
                <td class='center'><?= check_times_cirle($list->is_ontime) ?></td>
                <td class='center'>
                    <a href='<?= url("po/main/detail/?id=$list->id") ?>' class='btn btn-primary btn-xs'>
                        <i class='fa fa-book'></i>Detail
                    </a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<a href="<?= url('po/report/download-supplier-delivery') ?>" class='btn btn-primary btn-sm' style='margin:5px 0 0 0;'><i class='fa fa-download'></i> Download Data Laporan</a>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-closed * {
        display: none;
    }
</style>
