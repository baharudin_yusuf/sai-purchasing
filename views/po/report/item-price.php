<?php
$this->title = 'Item Price';
?>
<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>PO ID</th>
            <th>PR ID</th>
            <th>Supplier</th>
            <th>Goods Service</th>
            <th>Quantity</th>
            <th>Currency</th>
            <th>Price</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <?php foreach ($list->purchaseRequisitionList as $prList) { ?>
                <?php foreach ($prList->actualData as $actual) { ?>
                    <tr class="nowrap">
                        <td><?= $list->code ?></td>
                        <td><?= $actual->purchaseRequisition->code ?></td>
                        <td><?= $list->supplier->name ?></td>
                        <td><?= $actual->goodsService->name ?></td>
                        <td class='center'><?= $actual->quantity ?></td>
                        <td class='center'><?= $actual->goodsService->currency->name ?></td>
                        <td class='center'><?= currency_format($actual->price) ?></td>
                        <td>
                            <a href='<?= url("po/main/detail/?id=$list->id") ?>' class='btn btn-primary btn-xs'>
                                <i class='fa fa-book'></i>Detail
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php } ?>
        </tbody>
    </table>
</div>

<a href="<?= url('po/report/download-item-price') ?>" class='btn btn-primary btn-sm'
   style='margin:5px 0 0 0;'><i class='fa fa-download'></i> Download Data Laporan</a>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-closed * {
        display: none;
    }
</style>
