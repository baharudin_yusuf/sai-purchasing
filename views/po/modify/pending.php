<?php
$this->title = 'Pending PO Revision';
$data = isset($data) ? $data : [];
?>

<div class='div-datatable'>
<table class='display table table-bordered table-striped' id='tabel-data-master'>
<thead>
<tr>
	<th>PO ID</th>
	<th>Supplier</th>
	<th>Amount Estimation</th>
	<th>Amount Realization</th>
	<th>DP</th>
	<th>Manage</th>
</tr>
</thead>
<?php
foreach($data as $list){
	if($list->is_with_down_payment){
		$down_payment = "<i class='fa fa-check-circle fa-lg green'></i>";
	}
	else{
		$down_payment = "<i class='fa fa-times-circle fa-lg red'></i>";
	}
	
	echo "
	<tr>
		<td class='center'>".$list->code."</td>
		<td class='center'>".$list->supplier->name."</td>
		<td class='center'>".number_format($list->amount_estimation, 2, ',', '.')."</td>
		<td class='center'>".number_format($list->amount_realization, 2, ',', '.')."</td>
		<td class='center'>$down_payment</td>
		<td class='center'>
			<a class='btn btn-primary btn-xs' href='".url("po/main/detail/?id=$list->id")."' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
		</td>
	</tr>
	";
}
?>
</table>
</div>

<script>
$(document).ready(function(){
	convert_datatables('tabel-data-master');
});
</script>

<style>
.filter-manager *, .filter-dp *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-select *{
display: none;
}
</style>
