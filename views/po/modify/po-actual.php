<?php use yii\helpers\Html; ?>

<div class="col-lg-12 col-md-12">
    <div id='temp-revision' style='margin:10px 0'></div>
    <form onsubmit='return false;' id='form-revision-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>PR Code</th>
                <th>Goods/Service</th>
                <th>Carline</th>
                <th>Quantity</th>
                <th>Price Estimation</th>
                <th>Sub Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data->purchaseRequisitionList as $poPr) { ?>
                <?php foreach ($poPr->actualData as $actual) { ?>
                    <?php $actualCurrency = $actual->goodsService->currency; ?>
                    <tr>
                        <td><?= $actual->purchaseRequisition->code ?></td>
                        <td><?= $actual->goodsService->name ?></td>
                        <td><?= $actual->carline->name ?></td>
                        <td class='center' style="padding:2px;">
                            <?php $min = $actual->received == 0 ? 1 : $actual->received; ?>
                            <?= Html::input('number', "quantity-" . $actual->id, $actual->quantity, ['min' => $min, 'max' => $actual->quantity, 'class' => 'form-control', 'required' => 'required']) ?>
                        </td>
                        <td class='center' style="padding:2px;">
                            <div class="input-group">
                                <span class="input-group-addon"><?= $actualCurrency->name ?></span>
                                <?php $maxPrice = $actual->price + (0.1 * $actual->price); ?>
                                <?php $minPrice = $actual->price - (0.1 * $actual->price); ?>
                                <?= Html::input('number', "price-" . $actual->id, $actual->price, ['min' => $minPrice, 'max' => $maxPrice, 'class' => 'form-control', 'required' => 'required', 'step'=>'0.01']) ?>
                            </div>
                        </td>
                        <td class='center'>
                            <?php $subTotal = $actual->price * $actual->quantity; ?>
                            <?= $actualCurrency->name . ' ' . number_format($subTotal) ?>
                        </td>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
            <tbody>
        </table>
        <?= Html::input('hidden', 'id', $data->id) ?>
        <?= Html::submitButton('Simpan Revisi', ['class' => 'btn btn-primary']) ?>
    </form>
</div>

<script>
    $('#form-revision-detail').submit(function () {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data revisi sudah benar? Data tidak dapat diubah setelah disimpan!', function () {
            var data = getFormData('form-revision-detail');
            ajaxTransfer('po/modify/save/', data, '#modal-output');
        });
    });
</script>
