<table class='table table-bordered table-striped' id='table-revision-detail'>
    <thead>
    <tr>
        <th>PR Code</th>
        <th>Goods / Service</th>
        <th>Quantity</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data->detail as $list) { ?>
        <tr class="nowrap">
            <td><?= $list->actual->poPrDetail->purchaseRequisition->code ?></td>
            <td><?= $list->actual->goodsService->name ?></td>
            <td class='center'><?= $list->quantity ?></td>
            <td class='center'><?= $list->display_price ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>