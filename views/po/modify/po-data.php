<div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->code ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount Estimation</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_estimation ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount Realization</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_realization ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Term</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->term->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Franco</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->franco->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Supplier</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->supplier->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Est. Time Arrival</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->estimated_time_arival ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Actual Time Arrival</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->is_closed ? date('d/m/Y H:i:s', strtotime($data->actual_time_arival)) : '-' ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Down Payment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_down_payment_amount ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->date_submit ?></div>
    </div>
    <div class='form-group' style='margin-bottom:10px;'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= nl2br($data->comment) ?></div>
    </div>
</div>
