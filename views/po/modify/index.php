<?php
$this->title = 'Purchase Order Revision';
?>

<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-po-data" data-toggle="tab">PO Data</a></li>
    <li><a href="#tab-po-detail" data-toggle="tab">PO Actual</a></li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-po-data">
        <?php require 'po-data.php'; ?>
    </div>
    <div class="tab-pane" id="tab-po-detail">
        <?php require 'po-actual.php'; ?>
    </div>
</div>
