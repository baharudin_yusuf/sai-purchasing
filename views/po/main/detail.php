<?php
$this->title = 'Purchase Order Detail';
?>

<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-po-data" data-toggle="tab">PO Data</a></li>
    <?php if (!$isReceiving) { ?>
        <li><a href="#tab-po-detail" data-toggle="tab">PO Actual</a></li>
    <?php } ?>
    <?php if ($data->is_approved_by_lp_manager) { ?>
        <li><a href="#tab-po-receiving" data-toggle="tab">PO Receiving</a></li>
        <?php if (!$isReceiving) { ?>
            <li><a href="#tab-po-payment" data-toggle="tab">PO Invoice</a></li>
            <li><a href="#tab-po-revision" data-toggle="tab">PO Revision</a></li>
        <?php } ?>
    <?php } ?>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-po-data">
        <?php require 'po-data.php'; ?>
    </div>
    <?php if (!$isReceiving) { ?>
        <div class="tab-pane" id="tab-po-detail">
            <?php require 'po-actual.php'; ?>
        </div>
    <?php } ?>
    <?php if ($data->is_approved_by_lp_manager) { ?>
        <div class="tab-pane" id="tab-po-receiving">
            <?php require 'po-receiving.php'; ?>
        </div>
        <?php if (!$isReceiving) { ?>
            <div class="tab-pane" id="tab-po-payment">
                <?php require 'po-payment.php'; ?>
            </div>
            <div class="tab-pane" id="tab-po-revision">
                <?php require 'po-revision.php'; ?>
            </div>
        <?php } ?>
    <?php } ?>
</div>

<script>
    function markReceived(id) {
        var approve = confirm('Apakah anda yakin akan menandai bahwa purchase order telah diterima? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('po/main/mark-received', {id: id}, '#global-temp');
        }
    }

    function markDelivered(id) {
        var approve = confirm('Apakah anda yakin akan menandai bahwa purchase order telah dikirimkan? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('po/main/mark-delivered', {id: id}, '#global-temp');
        }
    }
</script>
