<div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->code ?></div>
    </div>
    <?php if (!$isReceiving) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Amount Estimation</label>
            <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_estimation ?></div>
        </div>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Amount Realization</label>
            <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_amount_realization ?></div>
        </div>
    <?php } ?>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Term</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->term->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Franco</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->franco->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Supplier</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->supplier->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Est. Time Arrival</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->estimated_time_arival ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Actual Time Arrival</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->is_closed ? date('d/m/Y H:i:s', strtotime($data->actual_time_arival)) : '-' ?></div>
    </div>
    <?php if (!$isReceiving) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Down Payment</label>
            <div class='col-lg-9 col-md-9 form-control-static'><?= $data->display_down_payment_amount ?></div>
        </div>
    <?php } ?>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $data->date_submit ?></div>
    </div>
    <div class='form-group' style='margin-bottom:10px;'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= nl2br($data->comment) ?></div>
    </div>
    <?php if (!$isReceiving) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Rejected</label>
            <div class='col-lg-9 col-md-9 form-control-static'>
                <?= check_times_cirle($data->is_rejected) ?>
            </div>
        </div>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'>Canceled</label>
            <div class='col-lg-9 col-md-9 form-control-static'>
                <?= check_times_cirle($data->is_canceled) ?>
            </div>
        </div>
    <?php } ?>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Received</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_received) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Delivered</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($data->is_delivered) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?php if ($allowApprovePO) { ?>
                <a onclick='approvePurchaseOrder(<?= $data->id ?>)' class='btn btn-primary btn-sm'><i
                            class='fa fa-check-circle'></i>Approve</a>
                <a onclick='rejectPurchaseOrder(<?= $data->id ?>)' class='btn btn-danger btn-sm'><i
                            class='fa fa-times'></i>Reject</a>
            <?php } ?>
            <?php if ($allowCancel) { ?>
                <a onclick='cancelPurchaseOrder(<?= $data->id ?>)' class='btn btn-danger btn-sm'><i
                            class='fa fa-times'></i>Cancel</a>
            <?php } ?>
            <?php if ($allowCheck) { ?>
                <a onclick='checkPurchaseOrder(<?= $data->id ?>)' class='btn btn-primary btn-sm'><i
                            class='fa fa-check-circle'></i>Checked</a>
            <?php } ?>
            <?php if ($allowModifyPO) { ?>
                <a href='<?= url("po/modify/?id=$data->id") ?>' class='btn btn-danger btn-sm'><i class='fa fa-book'></i>Revisi
                    PO</a>
            <?php } ?>
            <?php if (!$data->is_canceled && !$data->is_rejected && !$isReceiving) { ?>
                <a class='btn btn-primary btn-sm' href='<?= url("po/print/form-po/?id=$data->id&p=1") ?>'
                   target='_blank'><i class='fa fa-print'></i> Form PO</a>
            <?php } ?>
        </div>
    </div>

</div>

<script>
    function approvePurchaseOrder(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menyetujui pengajuan purchase order? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('po/main/approve-po', {id: id}, '#modal-output');
        });
    }

    function rejectPurchaseOrder(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menolak pengajuan purchase order? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('po/main/reject-po', {id: id}, '#modal-output');
        });
    }

    function cancelPurchaseOrder(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan membatalkan pengajuan purchase order? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('po/main/cancel-po', {id: id}, '#modal-output');
        });
    }

    function checkPurchaseOrder(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menandai bahwa PO sudah dicek? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('po/main/check-po', {id: id}, '#modal-output');
        });
    }
</script>
