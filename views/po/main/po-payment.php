<?php use \yii\helpers\Html; ?>

<div class="col-lg-12 col-md-12 col-md-12">
    <div style='margin:10px 0'>
        <?php if ($allowUploadInvoice) { ?>
            <form id='form-invoice' onsubmit="return false;" class='form-inline form-component'>
                <span class='form-title'>Form Upload Invoice</span>
                <div>
                    <div class='form-group'>
                        <input type='file' class='form-control input-sm' name='invoice' required>
                        <div class='red' style='font-size: 90%;'>
                            <small>Ekstensi yang diperbolehkan : pdf</small>
                        </div>
                    </div>
                    <div class='form-group'>
                        <input type='text' class='form-control input-sm' name='supplier_invoice_number'
                               placeholder="Payment For" required>
                        <div class='red' style='font-size: 90%;'>
                            <small>Ex : KWT NO:MS-15-0033</small>
                        </div>
                    </div>
                    <div class='checkbox'>
                        <label><input type='checkbox' name='final_payment' value="1"> Final Payment</label>
                        <div>&nbsp;</div>
                    </div>
                    <div class='form-group'>
                        <button type='submit' class='btn-sm btn btn-primary'><i class='fa fa-upload'></i>Upload Invoice
                        </button>
                        <div>&nbsp;</div>
                    </div>
                </div>
                <?= Html::hiddenInput('id', $data->id) ?>
            </form>
            <script>
                $(document).ready(function () {
                    $('#form-invoice').submit(function () {
                        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data yang akan diunggah sudah benar?', function () {
                            var data = getFormData('form-invoice');
                            ajaxTransfer('po/main/upload-invoice', data, '#modal-output');
                        });
                    })
                })
            </script>
        <?php } ?>

        <table id='table-invoice' class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>ID</th>
                <th>Invoice</th>
                <th>Time</th>
                <th>Final Payment</th>
                <th>Manager</th>
                <th>Paid</th>
                <th class='td-manage'>Manage</th>
            </tr>
            </thead>
            <tbody>
            <?php $bulkOption = 0; ?>
            <?php foreach ($data->getDisplayRealization() as $num => $list) { ?>
                <tr class='<?= ($list->is_rejected || $list->is_canceled) ? 'rejected' : '' ?>'>
                    <td class='center'><?= $list->code ?></td>
                    <td><a href='<?= $list->invoice_url ?>' target='_blank'><?= $list->supplier_invoice_number ?></a></td>
                    <td class='center'><?= $list->time ?></td>
                    <td class='center'><?= check_times_cirle($list->is_final_payment) ?></td>
                    <td class='center'><?= check_times_cirle($list->is_approved_by_manager) ?></td>
                    <td class='center'><?= check_times_cirle($list->is_paid) ?></td>
                    <td class='center'>
                        <?php if ($poConstraint::allowApproveInvoice($list)) { ?>
                            <input type='checkbox' value='<?= $list->id ?>' class='select-data'>
                            <?php $bulkOption++; ?>
                        <?php } ?>

                        <?php if ($list->is_approved_by_manager) { ?>
                            <a href='<?= url("po/payment/voucher-paying/?id=".$list->id) ?>' target='_blank' class='btn btn-xs btn-primary'><i class='fa fa-book'></i>Voucher Paying</a>
                        <?php } ?>

                        <?php if ($poConstraint::allowCancelInvoice($list)) { ?>
                            <a onclick='cancelInvoice(<?= $list->id ?>)' class='btn btn-danger btn-xs'><i class='fa fa-times-circle'></i>Cancel</a>
                        <?php } ?>

                        <?php if ($poConstraint::allowPayInvoice($list)) { ?>
                            <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='po/payment/create-new' data='id=<?= $list->id ?>' title='Add Payment'><i class='fa fa-money'></i>Add Payment</a>
                        <?php } ?>

                        <?php if ($list->is_paid) { ?>
                            <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='po/payment/detail' data='id=<?= $list->id ?>' title='Payment Detail'><i class='fa fa-money'></i>Payment Detail</a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <?= ($bulkOption > 0 ? BULK_ACTION_BTN : '') ?>
        <?= Yii::$app->params['includeRejectedForm'] ?>
    </div>
</div>

<script>
    function cancelInvoice(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan membatalkan pengajuan invoice? Tindakan tidak dapat dibatalkan', function () {
            ajaxTransfer('po/invoice/cancel-submission', {id: id}, '#modal-output');
        });
    }

    function bulkActionCallback(data) {
        ajaxTransfer('po/invoice/bulk-action', data, '#global-temp');
    }
</script>
