<div class="col-lg-12 col-md-12 col-md-12">
	<table style="margin: 10px 0;" class='table table-bordered table-striped'>
		<thead>
		<tr>
			<th>PR Code</th>
			<th>Goods/Service</th>
			<th>Quantity</th>
			<th>Received</th>
			<th>Delivered</th>
			<th>Complete</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($data->purchaseRequisitionList as $prList){ ?>
			<?php foreach ($prList->actualData as $actual){ ?>
				<tr>
					<td><?= $actual->purchaseRequisition->code ?></td>
					<td><?= $actual->goodsService->name ?></td>
					<td class='center'><?= $actual->quantity ?></td>
					<td class='center'><?= $actual->received ?></td>
					<td class='center'><?= $actual->delivered ?></td>
					<td class='center'><?= check_times_cirle($actual->is_complete) ?></td>
				</tr>
			<?php } ?>
		<?php } ?>
		</tbody>
	</table>
</div>