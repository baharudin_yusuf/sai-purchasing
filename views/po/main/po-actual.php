<div class="col-lg-12 col-md-12 col-md-12">
    <div style='margin:10px 0' id='output-budget-proposal-detail'>
        <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>PR Code</th>
                <th>Goods/Service</th>
                <th>Carline</th>
                <th>Quantity</th>
                <?php if(!$isReceiving) { ?>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>Sub Total + Tax</th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach($data->purchaseRequisitionList as $prList){ ?>
                <?php foreach($prList->actualData as $actual){ ?>
                    <tr>
                        <td><?= $actual->purchaseRequisition->code ?></td>
                        <td><?= $actual->goodsService->name ?></td>
                        <td class='center'><?= $actual->carline->name ?></td>
                        <td class='center'><?= $actual->quantity ?></td>
                        <?php if(!$isReceiving) { ?>
                            <td class='center'>
                                <?= $actual->display_price ?>
                                <?php if($actual->display_price != $actual->display_converted_price){ ?>
                                    <br>[<?= $actual->display_converted_price ?>]
                                <?php } ?>
                            </td>
                            <td class='center'>
                                <?= $actual->display_tax ?>
                                <?php if($actual->display_tax != $actual->display_converted_tax){ ?>
                                    <br>[<?= $actual->display_converted_tax ?>]
                                <?php } ?>
                            </td>
                            <td class='center'>
                                <?= $actual->display_amount ?>
                                <?php if($actual->display_amount != $actual->display_converted_amount){ ?>
                                    <br>[<?= $actual->display_converted_amount ?>]
                                <?php } ?>
                            </td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
            </tbody>
            <?php if(!$isReceiving) { ?>
            <tfoot>
                <tr>
                    <th colspan='6'>Total</th>
                    <th><b><?= $data->display_amount_realization ?></b></th>
                </tr>
            </tfoot>
            <?php } ?>
        </table>
    </div>
</div>
