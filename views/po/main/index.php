<?php
$this->title = 'Purchase Order';
$data = isset($data) ? $data : [];
?>

<?php if ($allowCreate) { ?>
    <a href='<?= url("po/create") ?>' title='Tambah Data' class="btn btn-primary"><i class='fa fa-plus'></i> Tambah Data</a>
<?php } ?>

<div class='div-datatable'>
    <table class='table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>PO ID</th>
            <th>Supplier</th>
            <th>Amount Estimation</th>
            <th>Amount Realization</th>
            <th>DP</th>
            <th>Manager</th>
            <th>Received</th>
            <th>Delivered</th>
            <th>Payment</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr class='nowrap <?= ($list->is_rejected || $list->is_canceled) ? 'rejected' : '' ?>'>
                <td class='center'><?= $list->code ?></td>
                <td class='center'><?= $list->supplier->name ?></td>
                <td class='center'><?= $list->display_amount_estimation ?></td>
                <td class='center'><?= $list->display_amount_realization ?></td>
                <td class='center'><?= check_times_cirle($list->is_with_down_payment) ?></td>
                <td class='center'><?= check_times_cirle($list->is_approved_by_lp_manager, $list->approved_by_lp_manager_time) ?></td>
                <td class='center'><?= check_times_cirle($list->is_received) ?></td>
                <td class='center'><?= check_times_cirle($list->is_delivered) ?></td>
                <td class='center'><?= check_times_cirle($list->is_payment_complete) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("po/main/detail/?id=$list->id") ?>' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?= Yii::$app->params['includeRejectedForm'] ?>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-dp *, .filter-dfm *, .filter-fm *, .filter-presdir *, .filter-fa *, .filter-received *, .filter-delivered *, .filter-payment * {
        display: none;
    }
</style>
