<?php use yii\helpers\Html; ?>

<form id='form-payment' onsubmit="return false;" class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-payment-method'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Bank</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('bank_id', null, $bankOption, ['class' => 'form-control', 'required' => '', 'onchange' => 'getBankDetail()']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Bank Detail</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('bank_detail_id', null, $bankDetailOption, ['class' => 'form-control', 'required' => '']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Payment Method</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('payment_method_id', null, $paymentMethodOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Payment Document</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('file', 'payment_document', null, ['class' => 'form-control', 'required' => '']) ?>
            <div class='red' style='font-size: 90%;margin: 3px 0 0 0;'><i>Ekstensi yang diperbolehkan : pdf</i></div>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Comment']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>
    function getBankDetail() {
        var data = getFormData('form-payment');
        ajaxTransfer('po/payment/get-bank-detail', data, 'select[name="bank_detail_id"]');
    }

    $(document).ready(function () {
        $('#form-payment').submit(function () {
            if (confirm('Apakah anda yakin bahwa data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!')){
                var data = getFormData('form-payment');
                ajaxTransfer('po/payment/save-data', data, '#result-payment-method');
            }
        });
    });

</script>
