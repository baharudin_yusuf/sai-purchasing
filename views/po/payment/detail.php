<form action='' method='post' class='form-horizontal'>
<div class='col-lg-12 col-md-12' id='result-payment-method'></div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Bank</label>
	<div class='col-lg-9 col-md-9 form-control-static'>
		<?= $payment->bankDetail->bank->name ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Account Number</label>
	<div class='col-lg-9 col-md-9 form-control-static'>
        <?= $payment->bankDetail->account_number ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Holder Name</label>
	<div class='col-lg-9 col-md-9 form-control-static'>
        <?= $payment->bankDetail->holder_name ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Payment Method</label>
	<div class='col-lg-9 col-md-9 form-control-static'>
        <?= $payment->paymentMethod->name ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Payment Document</label>
	<div class='col-lg-9 col-md-9 form-control-static'>
		<a href='<?= $payment->document_url ?>' target='_blank'>PAYMENT DOCUMENT</a>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Comment</label>
	<div class='col-lg-9 col-md-9 form-control-static'>
		<?= $payment->comment ?>
	</div>
</div>
</form>
