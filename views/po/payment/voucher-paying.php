<!DOCTYPE html>
<html>
<head>
    <title>Voucher Paying</title>
    <style>
        table {
            border-collapse: collapse;
            margin: 0 0 -1px 0;
        }

        table.border td {
            border: 1px solid;
            padding: 1px 2px;
        }

        table.no-border td {
            border: 0 none;
            padding: 1px 2px;
        }

        td.bg-signature {
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
        }
    </style>

</head>
<body>
<div style='display: inline-block;width: 170mm;position:relative;'>
    <div style='  color: red; border: 2px solid red; text-align: center; display: inline-block; padding: 15px 10px; font-size: 25px; position: absolute; right: 0; top: 0; font-weight: bold;opacity: 0.6;'>
        CONFIDENTIAL
    </div>
    <!-- header -->
    <div style='display: inline-block;width: 100%;'>
        <div style='display: block;float: left;margin: 0 20px 0 0;'><b style='font-size: 45px;line-height: 45px'>SAI</b>
        </div>
        <div style='float: left;text-align: center;'>
            <b style='font-size: 15px;margin: 0 0 -2px 0;display: block;'>PT.SURABAYA AUTOCOMP INDONESIA</b>
            <i style='display: block;'>Wiring Harness Manufacturer</i>
            <b style='font-size: 19px;display: block;'><u>VOUCHER PAYING</u></b>
        </div>
        <div style='float: left;margin: 0 0 0 20px;margin: 0 0 0 20px;width: 243px;overflow: hidden;'>
            <table style='border-collapse: collapse;'>
                <tr>
                    <td style='line-height: 18px;'>No.</td>
                    <td style='line-height: 18px;'>:</td>
                    <td style='line-height: 18px;'></td>
                </tr>
                <tr>
                    <td style='line-height: 18px;'>Date</td>
                    <td style='line-height: 18px;'>:</td>
                    <td style='line-height: 18px;'><?= date('d-M-y') ?></td>
                </tr>
                <tr>
                    <td style='line-height: 18px;'>Dept.</td>
                    <td style='line-height: 18px;'>:</td>
                    <td style='float: left;'><?= $activeUser->department->name ?></td>
                </tr>
            </table>
        </div>
    </div>
    <!-- /header -->

    <!-- main table -->
    <div style='display: inline-block;width: 100%;'>
        <!-- kolom atas -->
        <table class='border' style='width:100%'>
            <tr>
                <td>Paid To: <br><b><?= $purchaseOrder->supplier->name ?></b></td>
                <td>Amount Rp. : <span
                            style='border: 1px solid;padding: 5px 15px;font-weight: bold;'><?= $purchaseOrder->display_amount_realization ?></span>
                </td>
            </tr>
            <tr>
                <td>Date : <br><b><?= date('d-M-y', strtotime($data->deadline)) ?></b></td>
                <td>Say : <i><?= ucwords(terbilang($purchaseOrder->amount_realization)) ?></i></td>
            </tr>
        </table>
        <!-- /kolom atas -->

        <!-- kolom tengah -->
        <table class='border' style='width:100%'>
            <tr>
                <td style='width:50%;vertical-align: top;' rowspan='3'>
                    Payment For : <?= $data->supplier_invoice_number ?>
                    <table class='no-border' style='margin:5px 0 0 0; width:95%;'>
                        <tr>
                            <td>Material</td>
                            <td> :</td>
                            <td style='text-align:right;'><?= $purchaseOrder->display_vp_goods_amount ?></td>
                        </tr>
                        <tr>
                            <td>Jasa</td>
                            <td> :</td>
                            <td style='text-align:right;'><?= $purchaseOrder->display_vp_services_amount ?></td>
                        </tr>
                        <tr>
                            <td style='border-top:1px solid'>PPN</td>
                            <td style='border-top:1px solid'> :</td>
                            <td style='text-align:right;border-top:1px solid;'><?= $purchaseOrder->display_vat_amount ?></td>
                        </tr>
                        <tr>
                            <td>PPH</td>
                            <td> :</td>
                            <td style='text-align:right'><?= $purchaseOrder->display_rev_tax_amount ?></td>
                        </tr>
                        <tr>
                            <td style='border-top:1px solid'>Total</td>
                            <td style='border-top:1px solid'> :</td>
                            <td style='text-align:right;border-top:1px solid'><?= $purchaseOrder->display_amount_realization ?></td>
                        </tr>
                    </table>
                </td>
                <td style='width:50%; text-align:center;' colspan='4'>General Ledger</td>
            </tr>
            <tr>
                <td style='text-align:center;font-size: 12px;'>Profit Center</td>
                <td style='text-align:center;font-size: 12px;'>Account Code</td>
                <td style='text-align:center;font-size: 12px;'>Activity Center</td>
                <td style='text-align:center;font-size: 12px;'>Amount</td>
            </tr>
            <tr>
                <td style='height: 125px;'></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <!-- /kolom tengah -->

        <table class='border' style='width:100%'>
            <tr>
                <td colspan='5'>Paid Thru :</td>
            </tr>
            <tr>
                <td style='text-align:center;font-size: 12px;'>Cashier</td>
                <td style='text-align:center;font-size: 12px;'>Verified</td>
                <td style='text-align:center;font-size: 12px;'>Approved</td>
                <td style='text-align:center;font-size: 12px;'>Prepared</td>
                <td style='text-align:center;font-size: 12px;'>Received</td>
            </tr>
            <tr>
                <td style='height: 55px;'></td>

                <?php if (!is_null($signature['verified'])) { ?>
                    <td class="bg-signature"
                        style="background-image: url('<?= $signature['verified'] ?>');"></td>
                <?php } else { ?>
                    <td></td>
                <?php } ?>

                <?php if (!is_null($signature['approved'])) { ?>
                    <td class="bg-signature"
                        style="background-image: url('<?= $signature['approved'] ?>');"></td>
                <?php } else { ?>
                    <td></td>
                <?php } ?>

                <?php if (!is_null($signature['prepared'])) { ?>
                    <td class="bg-signature"
                        style="background-image: url('<?= $signature['prepared'] ?>');"></td>
                <?php } else { ?>
                    <td></td>
                <?php } ?>

                <td></td>
            </tr>
            <tr>
                <td colspan='5' style='text-align:right;border:0 none;'>FA-004-A</td>
            </tr>
        </table>
    </div>
    <!-- /main table -->
</div>
</body>
</html>


