<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-budget-detail' class='form-horizontal col-lg-12 col-md-12'>
<div id='result-purchase-requisition-detail'></div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Goods/Service</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::dropdownList('goods_service', null, $goods_service_option, ['class'=>'form-control', 'required'=>'']) ?>
		<?= Html::dropdownList('goods_service_id', null, [], ['class'=>'form-control', 'disabled'=>'', 'required'=>'', 'style'=>'margin:5px 0 0 0']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Quantity</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('number', 'quantity', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Quantity']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Price Estimation</label>
	<div class='col-lg-9 col-md-9 form-control-static' id='td-price-estimation'></div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Currency</label>
	<div class='col-lg-9 col-md-9 form-control-static' id='td-currency'></div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Comment</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::textarea('comment', null, ['class'=>'form-control', 'placeholder'=>'Comment']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'></label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('submit', 'simpan', "Simpan", ['class'=>'btn btn-primary']) ?>
	</div>
</div>
<?= Html::input('hidden', 'price_estimation', null) ?>
<?= Html::input('hidden', 'currency_id', null) ?>
</form>

<script>
$('#form-budget-detail').submit(function(){
	var data = getFormData('form-budget-detail');
	data['budget_final_id'] = $('select[name="budget_final_id"]').val();
	
	ajaxTransfer('pr/detail/save-data', data, '#result-purchase-requisition-detail');
});

$('select[name="goods_service"]').change(function(){
	var goods_service = $(this).val();
	clear_input();
	
	if(goods_service == 'option'){
		$('select[name="goods_service_id"]').html('');
		$('select[name="goods_service_id"]').attr('disabled', 'disabled');
	}
	else if(goods_service == 'add-new'){
		$('select[name="goods_service_id"]').removeAttr('disabled');
		var budget_final_id = $('select[name="budget_final_id"]').val();
		ajaxTransfer('pr/detail/add-new-goodservice', {budget_final_id: budget_final_id}, 'select[name="goods_service_id"]');
	}
	else if(goods_service == 'update-existing'){
		$('select[name="goods_service_id"]').attr('disabled', 'disabled');
		var ubah = confirm('Ubah data budget final detail yang sudah ada?');
	}
	else if(goods_service == 'from-final'){
		$('select[name="goods_service_id"]').removeAttr('disabled');
		var budget_final_id = $('select[name="budget_final_id"]').val();
		ajaxTransfer('pr/detail/add-from-final', {budget_final_id: budget_final_id}, 'select[name="goods_service_id"]');
	}
});

$('select[name="goods_service_id"]').change(function(){
	var goods_service = $('select[name="goods_service"]').val();
	
	if(goods_service == 'from-final') load_detail_from_final();
	else getGoodsServiceData();
});

function load_detail_from_final(){
	var goods_service_id = $('select[name="goods_service_id"]').val();
	var budget_final_id = $('select[name="budget_final_id"]').val();
	
	if(goods_service_id != undefined){
		var data = {
			goods_service_id: goods_service_id,
			budget_final_id	: budget_final_id,
		};
		
		ajaxTransfer('pr/detail/load-detail-from-final', data, '#global-temp');
	}
}

function getGoodsServiceData(){
	var goods_service_id = $('select[name="goods_service_id"]').val();
	if(goods_service_id != undefined){
		ajaxTransfer('pr/detail/get-goods-service-data', {goods_service_id: goods_service_id}, '#result-purchase-requisition-detail');
	}
}

function clear_input(){
	$('input[name="quantity"]').val('');
	$('input[name="price_estimation"]').val('');
	$('textarea[name="comment"]').val('');
}
</script>
