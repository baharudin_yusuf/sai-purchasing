<table class='table table-bordered table-striped'>
<tr>
	<th>Goods/Service</th>
	<th>Quantity</th>
	<th>Price Estimation</th>
	<th>Sub Total</th>
	<th>Currency</th>
	<th>Comment</th>
	<th>Manage</th>
</tr>
<?php
$total = 0;
foreach($data as $key => $list){
	extract($list);
	
	$goods_service = $goods_service_option[$goods_service_id];
	$currency = $currency_option[$currency_id];
	$subtotal = $price_estimation * $quantity;
	$total += $subtotal;
	
	echo "
	<tr>
		<td>$goods_service</td>
		<td class='center'>$quantity</td>
		<td class='center'>".number_format($price_estimation, 2, ',', '.')."</td>
		<td class='center'>".number_format($subtotal, 2, ',', '.')."</td>
		<td class='center'>$currency</td>
		<td class='center'>$comment</td>
		<td class='center'><a onclick='remove_purchase_requisition_detail($key)' class='btn btn-danger btn-xs'><i class='fa fa-times-circle'></i> Hapus</a></td>
	</tr>
	";
}
?>
<tr>
	<th colspan='3'>Total</th>
	<th colspan='4'><?= number_format($total, 2, ',', '.') ?></th>
</tr>
</table>

<script>
function remove_purchase_requisition_detail(id){
	var hapus = confirm('Apakah anda yakin akan menghapus detail purchase requisition?');
	if(hapus) ajaxTransfer('pr/detail/remove-data', {id: id}, '#output-purchase-requisition-detail');
}
</script>
