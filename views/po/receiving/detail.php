<?php
use yii\helpers\Html;
$this->title = 'Receiving Detail : '.$data->code;
$prList = isset($prList) ? $prList : [];
?>

<div id="result-receiving"></div>
<div>Supplier : <b><?= $data->supplier->name ?></b></div>
<form onsubmit="return false;" style='margin:10px 0' id='form-po-receiving'>
    <table class='table table-bordered table-striped'>
        <thead>
        <tr>
            <th>PR Code</th>
            <th>Goods/Service</th>
            <th>Quantity</th>
            <th>Received</th>
            <th>Delivered</th>
            <th>Complete</th>
        </tr>
        </thead>
        <?php $allComplete = true; ?>
        <?php foreach($prList as $poPr){ ?>
            <?php foreach ($poPr->actualData as $item){ ?>
                <?php
                if(!$item->is_complete_received && $item->isAllowReceive()){
                    $received = Html::input('number', 'received_'.$item->id, $item->received, ['min'=>$item->received, 'max'=>$item->quantity, 'placeholder'=>'Received', 'class'=>'form-control', 'required'=>'']);
                    $styleReceived = "style='padding: 2px;'";
                } else{
                    $received = $item->received;
                    $styleReceived = '';
                }

                if(!$item->is_complete_delivered && $item->isAllowDeliver()){
                    $delivered = Html::input('number', 'delivered_'.$item->id, $item->delivered, ['min'=>$item->delivered, 'max'=>$item->received, 'placeholder'=>'Delivered', 'class'=>'form-control', 'required'=>'']);
                    $styleDelivered = "style='padding: 2px;'";
                } else{
                    $delivered = $item->delivered;
                    $styleDelivered = '';
                }
                ?>

                <tr>
                    <td><?= $item->purchaseRequisition->code ?></td>
                    <td><?= $item->goodsService->name ?></td>
                    <td class='center'><?= $item->quantity ?></td>
                    <td <?= $styleReceived ?> class='center'><?= $received ?></td>
                    <td <?= $styleDelivered ?> class='center'><?= $delivered ?></td>
                    <td class='center'>
                        <?= $item->is_complete ? APPROVED_MARK : REJECTED_MARK ?>
                    </td>
                </tr>

                <?php
                if(!$item->is_complete){
                    $allComplete = false;
                }
                ?>
            <?php } ?>
        <?php } ?>
    </table>

    <?php if(!$allComplete){ ?>
        <button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-check'></i>Update Receiving</button>
        <?= Html::hiddenInput('po_id', $data->id) ?>
    <?php } ?>
</form>

<script>
    $('#form-po-receiving').submit(function(){
        if(confirm('Apakah anda yakin akan mengubah data penerimaan? Data tidak dapat diubah setelah disimpan')){
            var data = getFormData('form-po-receiving');
            ajaxTransfer('po/receiving/update', data, '#result-receiving');
        }
    });

    activate_menu('<?= url('po/receiving') ?>');
</script>
