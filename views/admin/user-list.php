<?php $this->title = 'User List'; ?>

<a onclick='loadModal(this)' target='user/register' data='' title='Tambah Data' class="btn btn-primary"><i
            class='fa fa-plus'></i> Tambah Data</a>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Section</th>
            <th>Department</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $user) { ?>
            <tr>
                <td><?= $user->name ?></td>
                <td><?= $user->email ?></td>
                <td class='center'><?= $user->rxle->name ?></td>
                <td class='center'><?= is_null($user->section) ? '-' : $user->section->name ?></td>
                <td class='center'><?= is_null($user->department) ? '-' : $user->department->name ?></td>
                <td class='center nowrap'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='user/modify' data='id=<?= $user->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });

    function hapus_data(primary_key) {
        if (confirm('Apakah anda yakin akan menghapus data?')) {
            var data = {primary_key: primary_key};
            ajaxTransfer('master/bank/hapus-data', data, '#global-temp');
        }
    }
</script>