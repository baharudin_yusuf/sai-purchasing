<?= $msg ?>
<table class='table table-bordered table-striped'>
<thead>
	<tr>
		<th>No</th>
		<th>Pengaturan</th>
		<th>Status</th>
		<th>Keterangan</th>
	</tr>
</thead>
<?php
$no = 1;
foreach($data as $list){
	echo "
	<tr>
		<td class='center'>".($no++)."</td>
		<td>".$list['title']."</td>
		<td class='center'>".($list['status'] ? APPROVED_MARK : REJECTED_MARK)."</td>
		<td>".$list['description']."</td>
	</tr>
	";
}
?>
</table>

<style>
	.ui-pnotify{display: none !important;}
</style>