<div class='div-datatable'>
<table class='display table table-bordered table-striped' id='tabel-data-master'>
<thead>
<tr>
	<th>No</th>
	<th>Notification</th>
	<th>Date</th>
	<th>Manage</th>
</tr>
</thead>
<?php
$no = 1;
foreach($notification as $list){
	extract($list);
	echo "
	<tr>
		<td class='center'>$no</td>
		<td>$title</td>
		<td class='center nowrap'>
			".date('d/m/Y', strtotime($date))."<br>
			".date('H:i:s', strtotime($date))."
		</td>
		<td>
			<a href='".url("notification/detail/?id=$id")."' class='btn btn-primary btn-xs'><i class='fa fa-book'></i> Detail</a>
		</td>
	</tr>
	";
	$no++;
}
?>
</table>
</div>

<script>
$(document).ready(function(){
	convert_datatables('tabel-data-master');
});
</script>
