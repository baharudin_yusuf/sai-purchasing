<?php

use yii\helpers\Html;

$this->title = 'User Register';
?>

<form id='registration-form' onsubmit='return false;' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='registration-result'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Nama</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'name', null, ['required' => '', 'class' => 'form-control', 'placeholder' => 'Nama']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Email</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('email', 'email', null, ['required' => '', 'class' => 'form-control', 'placeholder' => 'Email']); ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Password</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('password', 'password', null, ['required' => '', 'class' => 'form-control', 'placeholder' => 'Password']); ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Hak Akses</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('role', null, $roleOption, ['class' => 'form-control', 'onchange'=>'section_menu()']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Departemen</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('department_id', null, $departmentOption, ['class' => 'form-control', 'disabled' => '', 'onchange' => 'load_section()']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Section</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('section_id', null, $sectionOption, ['class' => 'form-control', 'disabled' => '']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Peran</label>
        <div class='col-lg-9 col-md-9'>
            <div style='padding-left:0' class="checkbox">
                <label><input type="radio" name='role_detail' value='administration' checked>&nbsp;
                    Administration</label>
            </div>
            <div style='padding-left:0' class="checkbox">
                <label><input type="radio" name='role_detail' value='supervisor'>&nbsp; Supervisor</label>
            </div>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Simpan Registrasi', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</form>

<script>
    function load_section() {
        var id = $('select[name="department_id"]').val();
        ajaxTransfer('user/get-section', {id: id}, 'select[name="section_id"]');
    }

    function section_menu() {
        var role = parseInt($('select[name="role"]').val());

        if (role == parseInt('<?= $sectionRole->id ?>')) {
            $('select[name="department_id"]').removeAttr('disabled');
            $('select[name="section_id"]').removeAttr('disabled');
            $('input[type="radio"]').removeAttr('disabled');
        }
        else if (role == parseInt('<?= $managerRole->id ?>')) {
            $('select[name="department_id"]').removeAttr('disabled');
            $('select[name="section_id"]').attr('disabled', 'disabled');
            $('input[type="radio"]').attr('disabled', 'disabled');
        }
        else {
            $('select[name="department_id"]').attr('disabled', 'disabled');
            $('select[name="section_id"]').attr('disabled', 'disabled');
            $('input[type="radio"]').attr('disabled', 'disabled');
        }
    }

    $(document).ready(function () {
        section_menu();

        $('#registration-form').submit(function () {
            if (confirm('Apakah data sudah benar?')) {
                var data = getFormData('registration-form');
                ajaxTransfer('user/save-registration', data, '#registration-result')
            }
        });
    });
</script>
