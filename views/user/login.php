<?php
use yii\helpers\Html; 
$this->title = 'Login';
?>


<form action='' method='post' class='form-horizontal'>
<div class='col-lg-12 col-md-12'><?= $msg; ?></div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Email</label>
	<div class='col-lg-8 col-md-8'>
		<?= Html::input('email', 'email', null, ['required'=>'', 'class'=>'form-control', 'placeholder'=>'Email']); ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Password</label>
	<div class='col-lg-8 col-md-8'>
		<?= Html::input('password', 'password', null, ['required'=>'', 'class'=>'form-control', 'placeholder'=>'Password']) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'></label>
	<div class='col-lg-8 col-md-8'>
		<?= Html::input('submit', 'submit', 'Login', ['class' => 'btn btn-primary']) ?>
	</div>
</div>
</form>
