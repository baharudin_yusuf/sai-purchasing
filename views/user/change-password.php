<?php
use yii\helpers\Html; 
?>

<form id='change-password-form' onsubmit='return false;' class='form-horizontal'>
<div class='col-lg-12 col-md-12' id='change-password-result'></div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Password</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('password', 'password1', null, ['required'=>'', 'class'=>'form-control', 'placeholder'=>'Password']); ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'>Ulangi Password</label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('password', 'password2', null, ['required'=>'', 'class'=>'form-control', 'placeholder'=>'Ulangi Password']); ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-3 col-md-3 control-label'></label>
	<div class='col-lg-9 col-md-9'>
		<?= Html::input('submit', 'submit', 'Update Password', ['class' => 'btn btn-primary']) ?>
	</div>
</div>
</form>

<script>
$('#change-password-form').submit(function(){
	var save = confirm('Apakah data sudah benar?');
	if(save){
		var data = getFormData('change-password-form');
		if(data['password1'] != data['password2']){
			alert('Data password tidak sesuai!')
		}
		else{
			ajaxTransfer('user/update-password', data, '#change-password-result')
		}
	}
});
</script>
