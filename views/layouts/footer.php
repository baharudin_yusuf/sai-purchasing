<?php
use yii\helpers\Html;
?>

<div id='loading-overlay'></div>
<div id='global-temp'></div>

<script src='<?= url('assets/template/saturn/js/plugins/datatables/jquery.dataTables.min.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/datatables/jquery.dataTables.columnFilter.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/jquery.pnotify.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/tab.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/dropdown.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/tooltip.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/collapse.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/transition.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/chosen.jquery.min.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/plugins/moment.min.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/bootstrap/modal.js') ?>'></script>
<script src='<?= url('assets/template/saturn/js/application.js') ?>'></script>
<script src='<?= url('assets/script/typeahead.bundle.js') ?>'></script>
<script src='<?= url('assets/script/accounting.min.js') ?>'></script>
<?= Html::hiddenInput('base_url', url()) ?>

<script>
	var active_url = document.URL;
	$('.sidebar-wrapper li').removeClass('current');
	$('.sidebar-wrapper li:has(a[href="' + active_url + '"])').addClass('current');
	
	$('.sub-sidebar-wrapper li').removeClass('current');
	$('.sub-sidebar-wrapper li:has(a[href="' + active_url + '"])').addClass('current');
	$('.sub-sidebar-wrapper li:has(li[class="current"])').addClass('open');
	$('.sub-sidebar-wrapper li:has(li[class="current"]) > a').addClass('current');
	$('.sub-sidebar-wrapper li.open > ul').css({display: 'block'});
	
	var selectedMenu = $('.sidebar-menu').attr('rel');
	$('#'+selectedMenu).addClass('current');
</script>
