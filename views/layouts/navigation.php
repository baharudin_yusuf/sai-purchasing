<ul>
	<li id='home-panel' class="current">
		<a href="<?= url() ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Dashboard">
		<i class="fa fa-home"></i>
		</a>
	</li>
	<?php if(\app\helpers\Auth::user()->roleAs('admin')){ ?>
	<li id='admin-panel'>
		<a href="<?= url('master/bank') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Admin Panel">
		<i class="fa fa-gears"></i>
		</a>
	</li>
	<?php } ?>
	<li id='budget-proposal'>
		<a href="<?= url('proposal/main') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Budget Proposal">
		<i class="fa fa-file-text-o"></i>
		</a>
	</li>
	<li id='purchase-requisition-panel'>
		<a href="<?= url('pr/main') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Purchase Requisition">
		<i class="fa fa-file-text-o"></i>
		</a>
	</li>
	<li id='purchase-order-panel'>
		<a href="<?= url('po/main') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Purchase Order">
		<i class="fa fa-file-text-o"></i>
		</a>
	</li>
	<li id='direct-purchase-panel'>
		<a href="<?= url('dp/main') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Direct Purchase">
		<i class="fa fa-file-text-o"></i>
		</a>
	</li>
		<li id='history-panel'>
			<a href="<?= url('history-transaksi/index') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="History Transaksi">
				<i class="fa fa-briefcase"></i>
			</a>
		</li>
	<li id='profile-panel'>
		<a href="<?= url('profile') ?>" data-toggle="tooltip" data-placement="right" title="" data-original-title="Profile">
			<i class="fa fa-user"></i>
		</a>
	</li>
</ul>
