<?php
use yii\helpers\Html;

$this->title = strval($this->title);
if (strlen($this->title) == 0) $this->title = 'Surabaya Autocomp Indonesia';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php require 'header.php'; ?>
</head>
<body class='glossed'>
<div class="all-wrapper fixed-header left-menu hide-side-menu">
    <div class="page-header">
        <div class="header-links hidden-xs">

            <?php
            $userdata = \app\helpers\Auth::user();
            $profile_picture = $userdata->profile_picture;
            if (strlen($profile_picture) == 0) {
                $profile_picture = url('assets/img/user.jpg');
            } else {
                $profile_picture = url(PROFILE_PICTURE_PATH . '/' . $profile_picture);
            }
            ?>
            <div id="dropdown-peringatan" class="dropdown hidden-sm hidden-xs">
                <a href="#" data-toggle="dropdown" class="header-link">
                    <i class="fa fa-warning"></i> Peringatan <span class="badge alert-animated"><?= count(Yii::$app->params['warning']) ?></span>
                </a>
                <ul class="dropdown-menu dropdown-inbar dropdown-wide">
                    <?php foreach (Yii::$app->params['warning'] as $warning){ ?>
                        <li><?= $warning ?></li>
                    <?php } ?>
                </ul>
            </div>
            <div class="dropdown">
                <a href="#" class="header-link clearfix" data-toggle="dropdown">
                    <div class="avatar profile-img"
                         style='width: 30px;height: 30px;margin: 0 10px 0 0;background-image:url(<?= $profile_picture ?>)'></div>
                    <div class="user-name-w"><?= $userdata->name ?> <i class="fa fa-caret-down"></i></div>
                </a>
                <ul class="dropdown-menu dropdown-inbar">
                    <li><a href="<?= url('profile') ?>"><i class="fa fa-cog"></i> Account Settings</a></li>
                    <li><a href="<?= url('user/logout') ?>"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
            </div>
        </div>
        <a class="logo hidden-xs" href="<?= url() ?>"><i class="fa fa-globe"></i></a>
        <a class="menu-toggler" href="#"><i class="fa fa-bars"></i></a>
        <h1>Surabaya AutoComp Indonesia</h1>
    </div>
    <div class="side">
        <div class="sidebar-wrapper">
            <?php require 'navigation.php'; ?>
        </div>
        <div class="sub-sidebar-wrapper"><?= Yii::$app->params['sidebarMenu'] ?></div>
    </div>
    <div class="main-content">
        <div class="widget widget-blue" id="widget_calendar">
            <div class="widget-title">
                <div class="widget-controls">
                    <a class="widget-control widget-control-full-screen" data-toggle="tooltip" data-placement="top"
                       title="" data-original-title="Full Screen"><i class="fa fa-expand"></i></a>
                    <a class="widget-control widget-control-full-screen widget-control-show-when-full"
                       data-toggle="tooltip" data-placement="left" title="" data-original-title="Exit Full Screen"><i
                            class="fa fa-expand"></i></a>
                    <a class="widget-control widget-control-minimize" data-toggle="tooltip" data-placement="top"
                       title="" data-original-title="Minimize"><i class="fa fa-minus-circle"></i></a>
                </div>
                <h3><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="widget-content">
                <?= $content ?>
            </div>
        </div>
    </div>
    <div id='notif-area'></div>
    <?php require 'footer.php'; ?>
</div>
<div id="ajax-loading" class="hidden">
    <div id="ajax-loading-box">
        <img src="<?= url('assets/img/loading.gif') ?>" />
        <div>Sedang mengolah data. Mohon tunggu...</div>
    </div>
</div>
</body>
</html>
