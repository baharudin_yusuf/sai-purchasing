<?php

use yii\helpers\Html;

$this->title = 'Profile Management';
?>

<form onsubmit="return false;" id='profile-form' enctype="multipart/form-data" class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id="result-profile"></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Nama</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'name', $user->name, ['required' => '', 'class' => 'form-control', 'placeholder' => 'Nama']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Email</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('email', 'email', $user->email, ['required' => '', 'class' => 'form-control', 'placeholder' => 'Email']); ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Profile Picture</label>
        <div class='col-lg-9 col-md-9'>
            <?php
            if (!is_null($user->profile_picture_file) > 0) {
                $file = $user->profile_picture_url;
                echo "<a href='$file' target='blank' style='background-image:url($file)' class='profile-img'></a>";
            }
            ?>
            <?= Html::input('file', 'profile_picture', null, ['class' => 'form-control']); ?>
            <span class='help-block'>File : *.jpg, *.png. Pilih file untuk mengubah, kosongkan untuk tetap</span>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Signature</label>
        <div class='col-lg-9 col-md-9'>
            <?php
            if (!is_null($user->signature_file) > 0) {
                $file = $user->signature_url;
                echo "<a href='$file' target='blank' style='background-image:url($file)' class='profile-img'></a>";
            }
            ?>
            <?= Html::input('file', 'signature', null, ['class' => 'form-control']); ?>
            <span class='help-block'>File : *.jpg, *.png. Pilih file untuk mengubah, kosongkan untuk tetap</span>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'submit', 'Update Data', ['class' => 'btn btn-primary']) ?>
            <a class='btn btn-primary' onclick='loadModal(this)' target='profile/change-password' data=''>Ubah Password</a>
        </div>
    </div>
</form>

<script>
    $('#profile-form').submit(function () {
        if (confirm('Apakah anda yakin akan mengubah data profil?')) {
            var data = getFormData('profile-form');
            ajaxTransfer('profile/update', data, '#result-profile');
        }
    });
</script>
