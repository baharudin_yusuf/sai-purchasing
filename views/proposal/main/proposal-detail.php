<div class="col-lg-12 col-md-12 col-md-12">
    <div style='margin:10px 0' id='output-budget-proposal-detail'>
        <table class='table table-bordered table-striped' id='tabel-detail'>
            <thead>
            <tr>
                <th>Goods/Service</th>
                <th>Comment</th>
                <th>Carline</th>
                <th>Quantity</th>
                <th>Price Estimation</th>
                <th>Sub Total</th>
                <th>Manager</th>
                <th>FA</th>
                <th>Manage</th>
            </tr>
            </thead>
            <tbody>
            <?php $allowManageCount = 0; ?>
            <?php foreach ($proposal->getDisplayDetail() as $detail) { ?>
                <?php $allowManage = $proposalConstraint::allowManageProposalDetail($detail) ?>
                <?php $allowManageCount += ($allowManage ? 1 : 0); ?>
                <tr class="nowrap <?= ($detail->is_rejected || $detail->is_canceled) ? 'rejected' : '' ?>">
                    <td><?= $detail->goodsService->name ?></td>
                    <td class='center'><?= $detail->comment ?></td>
                    <td class='center'><?= $detail->carline->name ?></td>
                    <td class='center'><?= $detail->quantity ?></td>
                    <td class='center nowrap'>
                        <?= $detail->display_price_estimation ?>
                        <?php if(strlen($detail->display_converted_price_estimation) > 0){ ?>
                            <br>[<?= $detail->display_converted_price_estimation ?>]
                        <?php } ?>
                    </td>
                    <td class='center nowrap'>
                        <?= $detail->display_sub_total ?>
                        <?php if(strlen($detail->display_converted_sub_total) > 0){ ?>
                            <br>[<?= $detail->display_converted_sub_total ?>]
                        <?php } ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_manager) ?>
                    </td>
                    <td class='center'>
                        <?= check_times_cirle($detail->is_approved_by_fa) ?>
                    </td>
                    <td class='center'>
                        <?php if ($allowManage) { ?>
                            <input type='checkbox' value='<?= $detail->id ?>' class='select-data' />
                        <?php } else if ($allowCancel && (!$detail->is_canceled && !$detail->is_rejected)) { ?>
                            <a onclick='cancelBudgetProposalDetail(<?= $detail->id ?>)' class='btn btn-danger btn-xs'>Cancel</a>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
            <tfoot>
            <tr>
                <th colspan='5'>Total</th>
                <th colspan='1'><b><?= count($proposal->getDisplayDetail()) == 0 ? '' : $proposal->getSubmissionAmount(true) ?></b></th>
                <th colspan='3'></th>
            </tr>
            </tfoot>
        </table>
        <?= ($allowManageCount > 0 ? BULK_ACTION_BTN : '') ?>
        <?= Yii::$app->params['includeRejectedForm'] ?>
    </div>
</div>

<script>
    function approveBudgetProposalDetail(id) {
        var approve = confirm('Apakah anda yakin akan menyetujui pengajuan detail dari Budget Proposal? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('proposal/detail/approve-submission', {id: id}, '#global-temp');
        }
    }

    function rejectBudgetProposalDetail(id) {
        var approve = confirm('Apakah anda yakin akan menolak pengajuan detail dari Budget Proposal? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('proposal/detail/reject-submission', {id: id}, '#global-temp');
        }
    }

    function cancelBudgetProposalDetail(id) {
        var approve = confirm('Apakah anda yakin akan membatalkan pengajuan detail dari Budget Proposal? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('proposal/detail/cancel-submission', {id: id}, '#global-temp');
        }
    }

    function bulkActionCallback(data) {
        ajaxTransfer('proposal/detail/bulk-action', data, '#global-temp');
    }
</script>
