<?php
$this->title = 'Pending Proposal';
?>

<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>ID</th>
            <th>Section</th>
            <th>Budget Period</th>
            <th>Budget</th>
            <th>Amount</th>
            <th>Date</th>
            <th>Manager</th>
            <th>FA</th>
            <th>Manage</th>
            <th>Select</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $list->code ?></td>
                <td><?= $list->section->name ?></td>
                <td><?= $list->budgetPeriod->name ?></td>
                <td><?= $list->budget->display_name ?></td>
                <td class='center'><?= $list->display_amount ?></td>
                <td class='center nowrap'><?= $list->date ?></td>
                <td class='center'><?= check_times_cirle($list->is_approved_by_manager) ?></td>
                <td class='center'><?= check_times_cirle($list->is_approved_by_fa) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("proposal/main/detail/?id=".$list->id) ?>' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
                <td class='center'><input type='checkbox' value='<?= $list->id ?>' class='select-data'></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?= (count($data) > 0 ? BULK_ACTION_BTN : '') ?>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });

    function bulkActionCallback(data) {
        ajaxTransfer('proposal/main/bulk-action', data, '#global-temp');
    }
</script>

<style>
    .filter-manager *, .filter-fa *, .filter-select * {
        display: none;
    }
</style>
