<div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12 col-md-12'>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>ID</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->code ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Section</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->section->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Budget Period</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->budgetPeriod->name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Budget</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->budget->display_name ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->display_amount ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount Used</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->display_amount_used ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= nl2br($proposal->comment) ?></div>
    </div>
    <div class='form-group' style='margin-bottom:10px;'>
        <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
        <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->date ?></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Approved by Manager</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($proposal->is_approved_by_manager) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Approved by FA</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($proposal->is_approved_by_fa) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Proposal Rejected</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($proposal->is_rejected) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Proposal Canceled</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= check_times_cirle($proposal->is_canceled) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?php if ($allow_approve) { ?>
                <a onclick='approve_budget_proposal(<?= $proposal->id ?>)' class='btn btn-sm btn-primary'><i
                            class='fa fa-check-circle'></i> Approve</a>
                <a onclick='reject_budget_proposal(<?= $proposal->id ?>)' class='btn btn-sm btn-danger'><i
                            class='fa fa-times'></i>Reject</a>
            <?php } ?>
            <?php if ($allowCancel) { ?>
                <a onclick='cancel_budget_proposal(<?= $proposal->id ?>)' class='btn btn-sm btn-danger'><i
                            class='fa fa-times'></i>Cancel</a>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    function approve_budget_proposal(id) {
        var approve = confirm('Apakah anda yakin akan menyetujui pengajuan budget proposal? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('proposal/main/approve-proposal', {id: id}, '#global-temp');
        }
    }

    function reject_budget_proposal(id) {
        var approve = confirm('Apakah anda yakin akan menolak pengajuan budget proposal? Tindakan tidak dapat dibatalkan');
        if (approve) {
            ajaxTransfer('proposal/main/reject-proposal', {id: id}, '#global-temp');
        }
    }

    function cancel_budget_proposal(id) {
        var cancel = confirm('Apakah anda yakin akan membatalkan pengajuan budget proposal? Tindakan tidak dapat dibatalkan');
        if (cancel) {
            ajaxTransfer('proposal/main/cancel-proposal', {id: id}, '#global-temp');
        }
    }
</script>
