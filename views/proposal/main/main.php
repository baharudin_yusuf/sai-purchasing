<?php
$this->title = 'Budget Proposal';
?>

<?php if(\app\helpers\Auth::user()->roleAs('section')){ ?>
<a href='<?= url("proposal/create") ?>' title='Tambah Data' class='btn btn-primary'><i class='fa fa-plus'></i> Tambah Data</a>
<?php } ?>

<div id='list-data-master'><?= $data ?></div>

<?= Yii::$app->params['includeRejectedForm'] ?>
