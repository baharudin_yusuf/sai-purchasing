<?php
$this->title = 'Budget Final Detail';
?>

<ul class="nav nav-tabs">
    <li class="active">
        <a href="#final-data" data-toggle="tab">Data</a>
    </li>
    <li>
        <a href="#final-detail" data-toggle="tab">Detail</a>
    </li>
</ul>
<div class="tab-content" style='margin:0 0 10px 0'>
    <div class="tab-pane active" id="final-data">
        <div style='margin: 10px 0' class='form-horizontal col-lg-12 col-md-12 col-md-12'>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>ID</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $final->code ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Section</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->section->name ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Budget Period</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->budgetPeriod->name ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Budget</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->budget->display_name ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Amount</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->display_amount ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Amount Used</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $final->display_amount_used ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Amount Available</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $final->display_amount_available ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Comment</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= nl2br($proposal->comment) ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->date ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Active Until</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $proposal->active_until ?></div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="final-detail">
        <div class="col-lg-12 col-md-12 col-md-12">
            <div style='margin:10px 0' id='output-budget-proposal-detail'>
                <table class='table table-bordered table-striped'>
                    <thead>
                    <tr>
                        <th>Goods/Service</th>
                        <th>Comment</th>
                        <th>Carline</th>
                        <th>Quantity</th>
                        <th>Price Estimation</th>
                        <th>Sub Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($proposal->getDisplayDetail() as $detail) { ?>
                        <tr class="nowrap <?= ($detail->is_rejected || $detail->is_canceled) ? 'rejected' : '' ?>">
                            <td><?= $detail->goodsService->name ?></td>
                            <td class='center'><?= $detail->comment ?></td>
                            <td class='center'><?= $detail->carline->name ?></td>
                            <td class='center'><?= $detail->quantity ?></td>
                            <td class='center nowrap'>
                                <?= $detail->display_price_estimation ?>
                                <?php if(strlen($detail->display_converted_price_estimation) > 0){ ?>
                                    <br>[<?= $detail->display_converted_price_estimation ?>]
                                <?php } ?>
                            </td>
                            <td class='center nowrap'>
                                <?= $detail->display_sub_total ?>
                                <?php if(strlen($detail->display_converted_sub_total) > 0){ ?>
                                    <br>[<?= $detail->display_converted_sub_total ?>]
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan='5'>Total</th>
                        <th colspan='1'><b><?= count($proposal->getDisplayDetail()) == 0 ? '' : $proposal->getSubmissionAmount(true) ?></b></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        activate_menu('<?= url('proposal/final') ?>');
    })
</script>