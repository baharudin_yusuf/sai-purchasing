<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>ID</th>
            <th>Section</th>
            <th>Period</th>
            <th>Budget</th>
            <th>Amount</th>
            <th>Used</th>
            <th>Available</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $item) { ?>
            <?php $proposal = $item->budgetProposal; ?>
            <tr class="nowrap">
                <td><?= $item->code ?></td>
                <td><?= $proposal->section->name ?></td>
                <td class='center nowrap'><?= $proposal->budgetPeriod->name ?></td>
                <td class='center nowrap'><?= $proposal->budget->name ?></td>
                <td class='center'><?= $proposal->display_amount ?></td>
                <td class='center'><?= $item->display_amount_used ?></td>
                <td class='center'><?= $item->display_amount_available ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("proposal/final/detail/?id=".$item->id) ?>'
                       title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-manager *, .filter-fa * {
        display: none;
    }
</style>
