<?php

use yii\helpers\Html;

$this->title = 'Create New Budget Proposal';

?>
<?php if(empty($budgetOption)){ ?>
    <div class="alert alert-danger">
        Pembuatan proposal tidak dapat dilakukan dikarenakan tidak ada budget tersedia untuk periode mendatang
    </div>
    <script>
        $(document).ready(function () {
            $('#btn-save-proposal').prop('disabled', true)
        });
    </script>
<?php } ?>

<div id='budget-proposal-result'></div>
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-budget-info" data-toggle="tab">Budget Proposal Data</a>
    </li>
    <li>
        <a href="#tab-budget-detail" data-toggle="tab">Budget Proposal Detail</a>
    </li>
</ul>
<div class="tab-content bottom-margin">
    <div class="tab-pane active" id="tab-budget-info">
        <form onsubmit="return false;" id='form-budget-proposal' class='form-horizontal col-lg-12 col-md-12'>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Section</label>
                <div class='col-lg-9 col-md-9 form-control-static'><?= $activeUser->section->name ?></div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Budget Period</label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::dropdownList('budget_period_id', null, $budgetPeriodOption, ['class' => 'form-control', 'data-provide' => 'typeahead', 'required' => '']) ?>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Budget</label>
                <div class='col-lg-9 col-md-9'>
                    <div class='input-group m-bot15'>
                        <?= Html::dropdownList('budget_id', null, $budgetOption, ['class' => 'form-control', 'onchange' => 'clearProposalData()']) ?>
                        <span class='input-group-btn'>
						<button onclick='viewBudgetInfo()' class='btn btn-primary' type='button'><i
                                    class='fa fa-info-circle'></i>Info</button>
					</span>
                    </div>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Amount</label>
                <div class='col-lg-9 col-md-9'>
                    <div class="input-group m-bot15">
                        <span class="input-group-addon"><?= $baseCurrency->name ?></span>
                        <?= Html::input('number', 'amount', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Amount']) ?>
                    </div>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'>Comment</label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::textarea('comment', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Comment']) ?>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-lg-3 col-md-3 control-label'></label>
                <div class='col-lg-9 col-md-9'>
                    <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary', 'id' => 'btn-save-proposal']) ?>
                </div>
            </div>
            <?= Html::hiddenInput('session_key', $sessionKey) ?>
        </form>
    </div>
    <div class="tab-pane" id="tab-budget-detail">
        <div id="output-budget-proposal-detail" class="col-lg-12 col-md-12"></div>
    </div>
</div>

<script>
    function clearProposalData() {
        var data = getFormData('form-budget-proposal');
        ajaxTransfer('proposal/create/clear-detail-data', data, '#output-budget-proposal-detail');
        initProposalConstraint();
    }

    function initProposalConstraint() {
        var data = getFormData('form-budget-proposal');
        ajaxTransfer('proposal/constraint/check-active-period', data, '#budget-proposal-result');
    }

    function viewBudgetInfo() {
        var budget_id = $('select[name="budget_id"]').val();
        modalAlert('Budget Information', '');
        ajaxTransfer('proposal/main/budget-info', {budget_id: budget_id}, '#modal-output');
    }
    
    function reloadBudgetProposalDetail() {
        var data = getFormData('form-budget-proposal');
        ajaxTransfer('proposal/create/reload-proposal-detail', data, '#output-budget-proposal-detail');
    }

    $('document').ready(function () {
        initProposalConstraint();
        reloadBudgetProposalDetail();
        chosenConvert('select[name="budget_id"]');

        $('#form-budget-proposal').submit(function () {
            var data = getFormData('form-budget-proposal');
            modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin data sudah benar? Data tidak dapat diubah setelah disubmit', function () {
                ajaxTransfer('proposal/create/save-data', data, '#modal-output');
            });
        });
    });
</script>
