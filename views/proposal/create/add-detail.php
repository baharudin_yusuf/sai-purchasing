<?php use yii\helpers\Html; ?>

<?php if(empty($goodsServiceOption)) { ?>
    <div class="alert alert-danger">
        Terjadi kesalahan! Tidak ada goods/service tersedia untuk budget terpilih
    </div>
    <script>
        $(document).ready(function () {
            $('#form-budget-proposal-detail').find('*').prop('disabled', true);
        })
    </script>
<?php } ?>

<form action='' onsubmit='return false' id='form-budget-proposal-detail' class='form-horizontal col-lg-12 col-md-12'>
    <div id='result-add-budget-proposal-detail'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Goods/Service</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('goods_service_id', null, $goodsServiceOption, ['required' => '', 'class' => 'form-control', 'onchange' => 'getGoodsServiceData()']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Quantity</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('number', 'quantity', null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Quantity']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Price Estimation</label>
        <div class='col-lg-9 col-md-9 form-control-static' id='td-price-estimation'></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Currency</label>
        <div class='col-lg-9 col-md-9 form-control-static' id='td-currency'></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Category</label>
        <div class='col-lg-9 col-md-9 form-control-static' id='td-category'></div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Carline</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('carline_id', null, $carlineOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::textarea('comment', null, ['class' => 'form-control', 'placeholder' => 'Comment']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary', 'id' => 'btn-save-proposal-detail']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'budget_id', $budget->id) ?>
    <?= Html::input('hidden', 'session_key', $sessionKey) ?>
</form>

<script>
    function getGoodsServiceData() {
        var gsCount = $('select[name=goods_service_id]').find('option');
        if(gsCount.length > 0){
            var data = getFormData('form-budget-proposal-detail');
            ajaxTransfer('proposal/create/get-goods-service-data', data, '#result-add-budget-proposal-detail');
        }
    }

    $(document).ready(function () {
        getGoodsServiceData();
        chosenConvert('select[name="carline_id"]');
        chosenConvert('select[name="goods_service_id"]');

        $('#form-budget-proposal-detail').submit(function () {
            var data = getFormData('form-budget-proposal-detail');
            ajaxTransfer('proposal/create/save-detail', data, '#result-add-budget-proposal-detail');
        });
    });

</script>
