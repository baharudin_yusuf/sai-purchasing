<a onclick='addBudgetProposalDetail()' style='margin:10px 0' class='btn btn-primary'>
    <i class='fa fa-plus'></i> Add Budget Detail
</a>

<table class='table table-bordered table-striped'>
    <thead>
    <tr>
        <th>Goods/Service</th>
        <th>Carline</th>
        <th>Comment</th>
        <th>Quantity</th>
        <th>Price Estimation</th>
        <th>Sub Total</th>
        <th>Manage</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $list) { ?>
        <tr class="nowrap">
            <td><?= $list->name ?></td>
            <td class='center'><?= $list->carline ?></td>
            <td class='center'><?= $list->comment ?></td>
            <td class='center'><?= $list->quantity ?></td>
            <td class='center'>
                <?= $list->price_estimation ?>
                <?php if(strlen($list->converted_price_estimation) > 0) { ?>
                    <br>[<?= $list->converted_price_estimation ?>]
                <?php } ?>
            </td>
            <td class='center'>
                <?= $list->sub_total ?>
                <?php if(strlen($list->converted_sub_total) > 0) { ?>
                    <br>[<?= $list->converted_sub_total?>]
                <?php } ?>
            </td>
            <td class='center'><a onclick='removeBudgetProposalDetail(<?= $list->id ?>)' class='btn btn-danger btn-xs'><i class='fa fa-times-circle'></i> Hapus</a></td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
    <tr>
        <th colspan='5'>Total</th>
        <th><b><?= $displayTotal ?></b></th>
        <th></th>
    </tr>
    </tfoot>
</table>

<script>
    function addBudgetProposalDetail() {
        var data = getFormData('form-budget-proposal');
        modalAlert('Add Budget Detail', '');
        ajaxTransfer('proposal/create/add-detail', data, '#modal-output');
    }

    function removeBudgetProposalDetail(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus budget detail?', function () {
            var data = {id: id, session_key: '<?= $sessionKey ?>'};
            ajaxTransfer('proposal/create/remove-detail', data, '#modal-output');
        });
    }
</script>