<?php $this->title = 'Pending Additional Budget'; ?>

<div class='div-datatable'>
<table class='display table table-bordered table-striped' id='tabel-data-master'>
<thead>
<tr>
	<th>ID</th>
	<th>Budget Final</th>
	<th>Additional Amount</th>
	<th>Time</th>
	<th>Manager</th>
	<th>FA</th>
	<th>Manage</th>
	<th>Select</th>
</tr>
</thead>
<?php
$no = 1;
foreach($data as $list){
	extract($list);
	
	$source_status = $is_approved_by_manager ? "<i class='fa fa-check-circle fa-lg green'></i>" : "<i class='fa fa-times-circle fa-lg red'></i>";
	$fa_status = $is_approved_by_fa ? "<i class='fa fa-check-circle fa-lg green'></i>" : "<i class='fa fa-times-circle fa-lg red'></i>";

	$add_id = ADDITIONAL_CODE.str_pad($id, 4, '0', STR_PAD_LEFT);
	$bf_id = FINAL_CODE.str_pad($budget_final_id, 4, '0', STR_PAD_LEFT);
	
	$rejected = $is_rejected ? 'rejected' : '';
	
	echo "
	<tr class='$rejected'>
		<td class='center'>$add_id</th>
		<td>[$bf_id] $budget_name</th>
		<td class='center'>".number_format($additional_amount, 2, ',', '.')."</th>
		<td class='center'>$time</th>
		<td class='center'>$source_status</th>
		<td class='center'>$fa_status</th>
		<td class='center'>
			<a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='proposal/additional/detail' data='id=$id' title='Detail Additional Budget'><i class='fa fa-eye'></i> Detail</a>
		</td>
		<td class='center'><input type='checkbox' value='$id' class='select-data'></td>
	</tr>
	";
	$no++;
}
?>
</table>
<?= (count($data)> 0 ? BULK_ACTION_BTN : '') ?>
</div>

<script>
$(document).ready(function(){
	convert_datatables('tabel-data-master');
});

function bulkActionCallback(data){
	ajaxTransfer('proposal/additional/bulk-action', data, '#global-temp');
}
</script>

<style>
.filter-manager *, .filter-src-mgr *, .filter-dest-mgr *, .filter-fa *, .filter-select *{
display: none;
}
</style>
