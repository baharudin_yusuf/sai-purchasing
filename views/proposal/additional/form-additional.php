<?php use yii\helpers\Html; ?>

<div class="tab-content" style='border:0 none;'>
	<div class="tab-pane active" id="tab-main-form">
		<form action='' onsubmit='return false' id='form-transfer' class='form-horizontal'>
		<div id='result-form-transfer'></div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Budget Final</label>
			<div class='col-lg-9 col-md-9'>
				<div class='input-group m-bot15'>
					<?= Html::input('text', 'source_str', $final, ['class'=>'form-control', 'readonly'=>'', 'style'=>'background-color: #fff;']) ?>
					<span class='input-group-btn'>
					<button onclick='load_detail_final("source")' href='#tab-detail-data' data-toggle='tab' class='btn btn-primary' type='button'><i class='fa fa-info-circle'></i>Info</button>
					</span>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Additional Amount</label>
			<div class='col-lg-9 col-md-9'>
				<div class="input-group m-bot15">
	                <span class="input-group-addon"><?= BASE_CURRENCY_NAME ?></span>
	                <?= Html::input('number', 'additional_amount', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Additional Amount']) ?>
	            </div>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'></label>
			<div class='col-lg-9 col-md-9'>
				<?= Html::input('submit', 'simpan', "Simpan", ['class'=>'btn btn-primary']) ?>
			</div>
		</div>
		<?= Html::input('hidden', 'budget_final_id', $budget_final_id) ?>
		</form>
	</div>
	<div class="tab-pane" id="tab-detail-data">
		<div class="col-lg-12 col-md-12 col-md-12">
			<div id='result-info-final'></div>
			<a class='btn btn-primary' href="#tab-main-form" data-toggle="tab">Kembali</a>
		</div>
	</div>
</div>

<script>
var allow_save = true;
$('#form-transfer').submit(function(){
	var save = confirm('Apakah anda yakin data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!');
	if(save){
		var data = getFormData('form-transfer');
		ajaxTransfer('proposal/additional/save-data', data, '#result-form-transfer');
	}
});

function load_detail_final(opt){
	var budget_final_id = $('input[name="budget_final_id"]').val();
	ajaxTransfer('proposal/final/detail', {id: budget_final_id}, '#result-info-final');
}

</script>
