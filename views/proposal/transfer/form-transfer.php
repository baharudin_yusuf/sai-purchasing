<?php use yii\helpers\Html; ?>

<div class="tab-content" style='border:0 none;'>
	<div class="tab-pane active" id="tab-main-form">
		<form action='' onsubmit='return false' id='form-transfer' class='form-horizontal'>
		<div id='result-form-transfer'></div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Final Source</label>
			<div class='col-lg-9 col-md-9'>
				<div class='input-group m-bot15'>
					<?= Html::input('text', 'source_str', $source, ['class'=>'form-control', 'readonly'=>'', 'style'=>'background-color: #fff;']) ?>
					<span class='input-group-btn'>
					<button onclick='load_detail_final("source")' href='#tab-detail-data' data-toggle='tab' class='btn btn-primary' type='button'><i class='fa fa-info-circle'></i>Info</button>
					</span>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Department</label>
			<div class='col-lg-9 col-md-9'>
				<?= Html::dropDownList('department', 'null', $department_option, ['class'=>'form-control']) ?>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Section</label>
			<div class='col-lg-9 col-md-9'>
				<?= Html::dropDownList('section', null, [], ['class'=>'form-control', 'required'=>'']) ?>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Final Destination</label>
			<div class='col-lg-9 col-md-9'>
				<div class='input-group m-bot15'>
					<?= Html::dropDownList('destination', null, $destination, ['class'=>'form-control', 'id'=>'transfer-destination', 'required'=>'']) ?>
					<span class='input-group-btn'>
					<button onclick='load_detail_final("destination")' href='#tab-detail-data' data-toggle='tab' class='btn btn-primary' type='button'><i class='fa fa-info-circle'></i>Info</button>
					</span>
				</div>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Amount</label>
			<div class='col-lg-9 col-md-9'>
				<?= Html::input('number', 'amount', null, ['class'=>'form-control', 'required'=>'', 'placeholder'=>'Amount']) ?>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'></label>
			<div class='col-lg-9 col-md-9'>
				<?= Html::input('submit', 'simpan', "Simpan", ['class'=>'btn btn-primary']) ?>
			</div>
		</div>
		<?= Html::input('hidden', 'source', $budget_final_id) ?>
		</form>
	</div>
	<div class="tab-pane" id="tab-detail-data">
		<div class="col-lg-12 col-md-12">
			<div id='result-info-final'></div>
			<a class='btn btn-primary' href="#tab-main-form" data-toggle="tab">Kembali</a>
		</div>
	</div>
</div>

<script>
chosenConvert('#transfer-destination');

var allow_save = true;
$('#form-transfer').submit(function(){
	var save = confirm('Apakah anda yakin data sudah benar? Data tidak dapat diubah atau dihapus setelah disubmit!');
	if(save){
		var data = getFormData('form-transfer');
		ajaxTransfer('proposal/transfer/save-data', data, '#result-form-transfer');
	}
});

function load_detail_final(opt){
	var budget_final_id = 0;
	if(opt == 'source') budget_final_id = $('input[name="source"]').val();
	else budget_final_id = $('select[name="destination"]').val();
	
	ajaxTransfer('proposal/final/detail', {id: budget_final_id}, '#result-info-final');
}

function get_section(){
	var dept_id = $('select[name="department"]').val();
	ajaxTransfer('proposal/transfer/get-section', {department_id: dept_id}, 'select[name="section"]');
}
get_section();

function get_destination(){
	var section_id = $('select[name="section"]').val();
	var source_id  = $('input[name="source"]').val();
	ajaxTransfer('proposal/transfer/get-destination', {section_id: section_id, source_id: source_id}, 'select[name="destination"]');
}

$('select[name="department"]').change(function(){
	get_section();
});

$('select[name="section"]').change(function(){
	get_destination();
});

</script>

<style>
.tt-hint{display: none;}
</style>