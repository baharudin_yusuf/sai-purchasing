<?php
use yii\helpers\Html;
$this->title = 'Budget Transfer Detail';

$source_id = FINAL_CODE.str_pad($source_id, 4, '0', STR_PAD_LEFT);
$destination_id = FINAL_CODE.str_pad($destination_id, 4, '0', STR_PAD_LEFT);
?>

<div class='col-lg-12 col-md-12' id='result-transfer'></div>
<form action='' style='margin: 10px 0' id='form-budget-transfer' class='form-horizontal col-lg-12 col-md-12'>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Source Final</label>
	<div class='col-lg-8 col-md-8 form-control-static'><?= "[$source_id] $source" ?></div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Destination Final</label>
	<div class='col-lg-8 col-md-8 form-control-static'><?= "[$destination_id] $destination" ?></div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Amount</label>
	<div class='col-lg-8 col-md-8 form-control-static'><?= $current_currency['name'].' '.number_format($amount, 2, ',', '.') ?></div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Time</label>
	<div class='col-lg-8 col-md-8 form-control-static'><?= $time ?></div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Source Manager</label>
	<div class='col-lg-8 col-md-8 form-control-static'>
		<?= ($is_approved_by_source_manager ? APPROVED_MARK : REJECTED_MARK) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Destination Manager</label>
	<div class='col-lg-8 col-md-8 form-control-static'>
		<?= ($is_approved_by_destination_manager ? APPROVED_MARK : REJECTED_MARK) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Approved by FA</label>
	<div class='col-lg-8 col-md-8 form-control-static'>
		<?= ($is_approved_by_fa ? "<i class='fa fa-check-circle fa-lg green'></i>" : "<i class='fa fa-times-circle fa-lg red'></i>") ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Rejected</label>
	<div class='col-lg-8 col-md-8 form-control-static'>
		<?= ($is_rejected ? APPROVED_MARK : REJECTED_MARK) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'>Canceled</label>
	<div class='col-lg-8 col-md-8 form-control-static'>
		<?= ($is_canceled ? APPROVED_MARK : REJECTED_MARK) ?>
	</div>
</div>
<div class='form-group'>
	<label class='col-lg-4 col-md-4 control-label'></label>
	<div class='col-lg-8 col-md-8'>
		<?php if($allow_approve) { ?>
		<a onclick='approve_budget_transfer(<?= $id ?>)' class='btn btn-primary btn-sm'><i class='fa fa-check-circle'></i> Approve</a>
		<a onclick='reject_budget_transfer(<?= $id ?>)' class='btn btn-danger btn-sm'><i class='fa fa-times'></i>Reject</a>
		<?php } ?>
		<?php if($allow_cancel) { ?>
		<a onclick='cancel_budget_transfer(<?= $id ?>)' class='btn btn-danger btn-sm'><i class='fa fa-times'></i>Cancel</a>
		<?php } ?>
	</div>
</div>
</form>

<script>
function approve_budget_transfer(id){
	var approve = confirm('Apakah anda yakin akan menyetujui pengajuan budget transfer? Tindakan tidak bisa dibatalkan');
	if(approve){
		ajaxTransfer('proposal/transfer/approve-submission', {id: id}, '#result-transfer');
	}
}

function reject_budget_transfer(id){
	var approve = confirm('Apakah anda yakin akan menolak pengajuan budget transfer? Tindakan tidak bisa dibatalkan');
	if(approve){
		ajaxTransfer('proposal/transfer/reject-submission', {id: id}, '#result-transfer');
	}
}

function cancel_budget_transfer(id){
	var approve = confirm('Apakah anda yakin akan membatalkan pengajuan budget transfer? Tindakan tidak bisa dibatalkan');
	if(approve){
		ajaxTransfer('proposal/transfer/cancel-submission', {id: id}, '#result-transfer');
	}
}
</script>
