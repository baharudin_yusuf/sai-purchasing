<?php $this->title = 'Transfer List'; ?>

<div class='div-datatable'>
<table class='display table table-bordered table-striped' id='tabel-data-master'>
<thead>
<tr>
	<th>ID</th>
	<th>Src Final</th>
	<th>Dest Final</th>
	<th>Amount</th>
	<th>Time</th>
	<th>Src Mgr</th>
	<th>Dest Mgr</th>
	<th>FA</th>
	<th>Manage</th>
</tr>
</thead>
<?php
$no = 1;
foreach($data as $list){
	extract($list);
	
	$source_status = $is_approved_by_source_manager ? "<i class='fa fa-check-circle fa-lg green'></i>" : "<i class='fa fa-times-circle fa-lg red'></i>";
	$destination_status = $is_approved_by_destination_manager ? "<i class='fa fa-check-circle fa-lg green'></i>" : "<i class='fa fa-times-circle fa-lg red'></i>";
	$fa_status = $is_approved_by_fa ? "<i class='fa fa-check-circle fa-lg green'></i>" : "<i class='fa fa-times-circle fa-lg red'></i>";

	$tr_id = 'TR'.str_pad($id, 4, '0', STR_PAD_LEFT);
	$source_id = FINAL_CODE.str_pad($source_id, 4, '0', STR_PAD_LEFT);
	$destination_id = FINAL_CODE.str_pad($destination_id, 4, '0', STR_PAD_LEFT);
	
	$rejected = ($is_rejected || $is_canceled) ? 'rejected' : '';
	
	echo "
	<tr class='$rejected'>
		<td class='center'>$tr_id</th>
		<td>[$source_id] $source</th>
		<td>[$destination_id] $destination</th>
		<td class='center'>".number_format($amount, 2, ',', '.')."</th>
		<td class='center'>".date('d/m/Y H:i:s', strtotime($time))."</th>
		<td class='center'>$source_status</th>
		<td class='center'>$destination_status</th>
		<td class='center'>$fa_status</th>
		<td class='center'>
			<a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='proposal/transfer/detail' data='id=$id' title='Detail Data'><i class='fa fa-eye'></i> Detail</a>
		</td>
	</tr>
	";
	$no++;
}
?>
</table>
<?= Yii::$app->params['includeRejectedForm'] ?>
</div>

<script>
$(document).ready(function(){
	convert_datatables('tabel-data-master');
});
</script>

<style>
.filter-manager *, .filter-src-mgr *, .filter-dest-mgr *, .filter-fa *{
display: none;
}
</style>
