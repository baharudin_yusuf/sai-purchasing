<div class='div-datatable'>
<table class='display table table-bordered table-striped' id='tabel-data-master'>
<thead>
<tr>
	<th>ID</th>
	<th>Section</th>
	<th>Budget</th>
	<th>Amount</th>
	<th>Used</th>
	<th>Available</th>
	<th style='width: 155px !important;'>Manage</th>
</tr>
</thead>
<?php
$no = 1;
foreach($data as $list){
	extract($list);
	
	$date = date('d-m-Y', strtotime($date));
	$fin_id = FINAL_CODE.str_pad($id, 4, '0', STR_PAD_LEFT);
	
	echo "
	<tr>
		<td class='center'>$fin_id</td>
		<td>".ucwords(strtolower($section))."</td>
		<td>$budget_name</td>
		<td class='center'>".number_format($amount, 2)."</td>
		<td class='center'>".number_format($amount_used, 2)."</td>
		<td class='center'>".number_format($amount-$amount_used, 2)."</td>
		<td class='center'>
			<a class='btn btn-primary btn-xs' onclick='loadModal(this)' title='Budget Final Detail' target='proposal/final/detail' data='id=$id</a>'><i class='fa fa-eye'></i> Detail</a>
			<a class='btn btn-primary btn-xs' onclick='loadModal(this)' title='Transfer Budget' target='proposal/transfer/form-transfer' data='budget_final_id=$id</a>'><i class='fa fa-sign-in'></i> Transfer</a>
		</td>
	</tr>
	";
	$no++;
}
?>
</table>
</div>

<script>
$(document).ready(function(){
	convert_datatables('tabel-data-master');
});
</script>

<style>
.filter-manager *, .filter-fa *{
display: none;
}
</style>
