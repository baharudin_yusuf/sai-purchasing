<?php
use yii\helpers\Html;
?>

<ul class="nav nav-tabs">
	<li class="active">
		<a href="#final-data" data-toggle="tab">Data</a>
	</li>
	<li>
		<a href="#final-detail" data-toggle="tab">Detail</a>
	</li>
</ul>
<div class="tab-content" style='margin:0 0 10px 0'>
	<div class="tab-pane active" id="final-data">
		<form onsubmit='return false;' style='margin: 10px 0' id='form-reschedule-budget' class='form-horizontal col-lg-12 col-md-12'>
		<?= Html::input('hidden', 'final_id', $id) ?>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>ID</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= FINAL_CODE.str_pad($id, 4, '0', STR_PAD_LEFT) ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Section</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= ucwords(strtolower($section)) ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Budget Period</label>
			<div class='col-lg-9 col-md-9'>
				<?= Html::dropDownList('budget_period_id', null, $period_option, ['class'=>'form-control']) ?>
			</div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Budget</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= "[$budget_number] $budget_name" ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Amount</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= $current_currency['name'].' '.number_format($amount, 2, ',', '.') ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Amount Used</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= $current_currency['name'].' '.number_format($amount_used, 2, ',', '.') ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Comment</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= $comment ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Date Submit</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= date('d/m/Y H:i:s', strtotime($date)) ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'>Active Until</label>
			<div class='col-lg-9 col-md-9 form-control-static'><?= date('d/m/Y H:i:s', strtotime($active_until)) ?></div>
		</div>
		<div class='form-group'>
			<label class='col-lg-3 col-md-3 control-label'></label>
			<div class='col-lg-9 col-md-9 form-control-static'>
				<?= Html::input('submit', 'submit', 'Simpan Data', ['class'=>'btn btn-primary']) ?>
			</div>
		</div>
		</form>
		
		<script>
			$('#form-reschedule-budget').submit(function(){
				var data = getFormData('form-reschedule-budget');
				var save = confirm('Apakah anda yakin data sudah benar? Data tidak dapat diubah setelah disubmit!');
				if(save) ajaxTransfer('proposal/reschedule/save-data', data, '#global-temp');
			});
		</script>
	</div>
	<div class="tab-pane" id="final-detail">
		<div class="col-lg-12 col-md-12 col-md-12">
			<div style='margin:10px 0' id='output-budget-proposal-detail'>
			<table class='table table-bordered table-striped'>
			<tr>
				<th>Goods/Service</th>
				<th>Comment</th>
				<th>Carline</th>
				<th>Quantity</th>
				<th>Price Estimation</th>
				<th>Sub Total</th>
			</tr>
			<?php
			$total = 0; $c_total = 0;
			foreach($budget_proposal_detail as $list){
				extract($list);
				
				$sub_total = $price_estimation * $quantity;
				$c_sub_total = $c_price_estimation * $quantity;
				if(!$is_rejected){
					$total += $sub_total;
					$c_total += $c_sub_total;
				}
				
				$rejected = $is_rejected ? 'rejected' : '';
				
				echo "
				<tr class='$rejected'>
					<td>$goods_service</td>
					<td class='center'>$comment</td>
					<td class='center'>$carline</td>
					<td class='center'>$quantity</td>
					<td class='center nowrap'>
						$currency ".number_format($price_estimation, 2, ',', '.')."
						".($currency == $current_currency['name'] ? '' : "<br>[".$current_currency['name'].' '.number_format($c_price_estimation, 2, ',', '.')."]")."
					</td>
					<td class='center nowrap'>
						$currency ".number_format($sub_total, 2, ',', '.')."
						".($currency == $current_currency['name'] ? '' : "<br>[".$current_currency['name'].' '.number_format($c_sub_total, 2, ',', '.')."]")."
					</td>
				</tr>
				";
			}
			?>
			<tr>
				<th colspan='5'>Total</th>
				<th colspan='1'><strong><?= $current_currency['name'].' '.number_format($c_total, 2, ',', '.') ?></strong></th>
			</tr>
			</table>
			</div>
		</div>
	</div>
</div>
