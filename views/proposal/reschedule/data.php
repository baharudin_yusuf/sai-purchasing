<div class='div-datatable'>
<table class='display table table-bordered table-striped' id='tabel-data-master'>
<thead>
<tr>
	<th>ID</th>
	<th>Section</th>
	<th>Budget</th>
	<th>Amount</th>
	<th>Used</th>
	<th>Available</th>
	<th>Date</th>
	<th>Manage</th>
</tr>
</thead>
<?php
$no = 1;
foreach($data as $list){
	extract($list);
	
	$date = date('d-m-Y', strtotime($date));
	$fin_id = FINAL_CODE.str_pad($id, 4, '0', STR_PAD_LEFT);
	
	echo "
	<tr>
		<td>$fin_id</td>
		<td>$section</td>
		<td>$budget_name</td>
		<td class='center'>".number_format($amount, 2)."</td>
		<td class='center'>".number_format($amount_used, 2)."</td>
		<td class='center'>".number_format($amount-$amount_used, 2)."</td>
		<td class='center nowrap'>".date('d/m/Y', strtotime($date))."</td>
		<td class='center'>
			<a onclick='loadModal(this)' target='proposal/reschedule/form-reschedule/' data='id=$id' class='btn btn-primary btn-xs' title='Reschedule Final'><i class='fa fa-book'></i> Reschedule</a>
		</td>
	</tr>
	";
	$no++;
}
?>
</table>
</div>

<script>
$(document).ready(function(){
	convert_datatables('tabel-data-master');
});
</script>

<style>
.filter-manager *, .filter-fa *{
display: none;
}
</style>
