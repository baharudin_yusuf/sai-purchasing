<?= alert_success("$imported data telah berhasil diimport") ?>
<?= alert_danger(count($data) . " data gagal diimport, silahkan periksa ulang data anda") ?>

<div class="ov-x">
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>NO</th>
            <?php foreach ($data[0] as $key => $value) { ?>
                <th><?= strtoupper($key) ?></th>
            <?php } ?>
        </tr>
        </thead>
        <?php $no = 1; ?>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td class="center"><?= $no++ ?></td>
                <?php foreach ($list as $value) { ?>
                    <td><?= $value ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>