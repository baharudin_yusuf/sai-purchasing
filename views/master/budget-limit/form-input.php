<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-budget-limit' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-budget-limit'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Budget</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('budget_id', $data->budget_id, $budgetOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Monthly Period</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('period_id', $data->period_id, $periodOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Currency</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('currency_id', $data->currency_id, $currencyOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Amount</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('number', 'amount', $data->amount, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Amount']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Comment</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::textarea('comment', $data->comment, ['class' => 'form-control', 'placeholder' => 'Comment']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>
    chosenConvert('select[name=budget_id]');

    $('#form-budget-limit').submit(function () {
        var data = getFormData('form-budget-limit');
        ajaxTransfer('master/budget-limit/simpan-data', data, '#result-budget-limit');
    });
</script>
