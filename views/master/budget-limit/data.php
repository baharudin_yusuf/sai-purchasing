<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Budget</th>
            <th>Period</th>
            <th>Amount</th>
            <th>Comment</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $num => $list) { ?>
            <tr>
                <td><?= $list->display_budget ?></td>
                <td class='center'><?= $list->period->name ?></td>
                <td class='center'><?= $list->display_amount ?></td>
                <td><?= $list->comment ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/budget-limit/entri-data'
                       data='id=<?= $list->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i
                                class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/budget-limit/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/budget-limit/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>
