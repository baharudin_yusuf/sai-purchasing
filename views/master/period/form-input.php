<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-currency' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-currency'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Name</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'name', $data->name, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Name']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date Start</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'date_start', $data->date_start, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Date Start']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Date End</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'date_end', $data->date_end, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Date End']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Active</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= Html::checkbox('is_active', $data->is_active, ['value' => 1]) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Future</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= Html::checkbox('is_future', $data->is_future, ['value' => 1]) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>
    date_input('input[name="date_start"]', 'date');
    date_input('input[name="date_end"]', 'date');

    $('#form-currency').submit(function () {
        var data = getFormData('form-currency');
        ajaxTransfer('master/period/simpan-data', data, '#result-currency');
    });
</script>
