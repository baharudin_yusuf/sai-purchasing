<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Budget</th>
            <th>Name</th>
            <th>Unit</th>
            <th>Currency</th>
            <th>Spesification</th>
            <th>PO</th>
            <th>DP</th>
            <th style='width: 150px;'>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $list->budget->display_name ?></td>
                <td><?= $list->name ?></td>
                <td class='center'><?= $list->unit->name ?></td>
                <td class='center'><?= $list->currency->name ?></td>
                <td><?= $list->specification ?></td>
                <td class='center'><?= check_times_cirle($list->is_po) ?></td>
                <td class='center'><?= check_times_cirle($list->is_dp) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/goods-service/entri-data'
                       data='id=<?= $list->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i
                                class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/goods-service/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/goods-service/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    .filter-po *, .filter-dp * {
        display: none;
    }
</style>
