<div id="result-do-import"></div>
<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>NO</th>
            <?php foreach ($data[0] as $key => $value) { ?>
                <th><?= strtoupper($key) ?></th>
            <?php } ?>
        </tr>
        </thead>
        <?php $no = 1; ?>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $no++ ?></td>
                <?php foreach ($list as $value) { ?>
                    <td><?= $value ?></td>
                <?php } ?>
            </tr>
        <?php } ?>
    </table>
</div>
<a onclick="doImport()" class='btn btn-primary'><i class='fa fa-save'></i> Save Data</a>

<script>
    function doImport() {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan mengimport data?', function () {
            var data = {import_key: '<?= $importKey ?>'};
            ajaxTransfer('master/<?= $type ?>/save-import', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>