<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Period</th>
            <th>Base Currency</th>
            <?php foreach ($currencyOption as $curr) { ?>
                <th><?= $curr ?></th>
            <?php } ?>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($periodAll as $period) { ?>
            <?php $currBase = $period->getCurrencyDetailBase() ?>
            <tr>
                <td><?= $period->name ?></td>
                <td class='center'><?= $currBase->name ?></td>
                <?php foreach ($currencyOption as $currId => $curr) { ?>
                    <td class="center"><?= number_format($period->getCurrencyDetailRate($currId), 6, ',', '.') ?></td>
                <?php } ?>
                <td class='center nowrap'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/currency-detail/entri-data' data='period_id=<?= $period->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $period->id ?>)'><i class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/currency-detail/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {period_id: id};
            ajaxTransfer('master/currency-detail/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>

<style>
    <?php
    foreach($currencyOption as $list){
        echo "
        .filter-".strtolower($list)." *{display: none;}";
    }
    ?>
</style>