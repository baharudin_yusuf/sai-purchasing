<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-currency-detail' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-currency'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Period</label>
        <div class='col-lg-9 col-md-9'>
            <?php if (intval($period->id) > 0) { ?>
                <?= Html::input('text', 'period_name', $period->display_name, ['class' => 'form-control', 'readonly' => '']) ?>
                <?= Html::input('hidden', 'period_id', $period->id) ?>
            <?php } else { ?>
                <?= Html::dropDownList('period_id', null, $periodOption, ['class' => 'form-control']) ?>
            <?php } ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Base Currency</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('base_currency_id', $currencyBase->id, $currencyOption, ['class' => 'form-control', 'onchange' => 'setBaseCurrency()']) ?>
        </div>
    </div>

    <?php foreach ($currencyOption as $currId => $curr) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'><?= $curr ?> Rate</label>
            <div class='col-lg-9 col-md-9'>
                <?= Html::input('number', "rate_$currId", $period->getCurrencyDetailRate($currId), ['class' => 'form-control rate-input', 'required' => '', 'placeholder' => "$curr Rate", 'step' => '0.000001']) ?>
            </div>
        </div>
    <?php } ?>
    
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
</form>

<script>
    function setBaseCurrency() {
        var currencyId = $('select[name="base_currency_id"]').val();
        $('.rate-input').prop('readonly', false);
        $('input[name="rate_' + currencyId + '"]').prop('readonly', true).val('1');
    }

    $(document).ready(function () {
        setBaseCurrency();

        $('#form-currency-detail').submit(function () {
            var data = getFormData('form-currency-detail');
            ajaxTransfer('master/currency-detail/simpan-data', data, '#result-currency');
        });
    })
</script>
