<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Name</th>
            <th>Manage</th>
        </tr>
        </thead>
        <?php
        $no = 1;
        foreach ($data as $list) {
            extract($list);
            echo "
	<tr>
		<td><?= $list->name ?></td>
		<td class='center'>
			<a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/budget-proposal-status/entri-data' data='id=<?= $list->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
			<a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i class='fa fa-trash-o'></i> Hapus</a>
		</td>
	</tr>
	";
            $no++;
        }
        ?>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/budget-proposal-status/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/budget-proposal-status/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>