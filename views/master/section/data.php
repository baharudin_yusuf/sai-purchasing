<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Code</th>
            <th>Name</th>
            <th>Department</th>
            <th>Exim</th>
            <th>GA GS</th>
            <th>LP</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $list->code ?></td>
                <td><?= $list->name ?></td>
                <td><?= $list->department->name ?></td>
                <td class='center'><?= check_times_cirle($list->is_exim) ?></td>
                <td class='center'><?= check_times_cirle($list->is_ga_gs) ?></td>
                <td class='center'><?= check_times_cirle($list->is_lp) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/section/entri-data'
                       data='id=<?= $list->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i
                                class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/section/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/section/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>