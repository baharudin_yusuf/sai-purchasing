<?php
$this->title = 'Import Data ' . ucwords(dealias($type));

use yii\helpers\Html;

?>

<form onsubmit="return false;" class='form-inline form-import' id="form-import-master">
    <input type='file' name='file' required class='form-control input-sm'>
    <button type='submit' class='btn btn-primary btn-sm'><i class='fa fa-upload'></i> Upload</button>
    <a class="btn btn-primary btn-sm" target="_blank" href="<?= url("data-import/$type.xlsx") ?>">
        <i class='fa fa-download'></i> <span>Format Import</span>
    </a>

    <div class="import-warning">
        Jumlah maksimal data yang dapat diproses dalam 1 kali import adalah 100 data
    </div>
    <?= Html::hiddenInput('import_key', $importKey) ?>
</form>
<hr>

<div id="result-import"></div>

<script>
    $(document).ready(function () {
        activate_menu('<?= url("master/$type") ?>');

        $('#form-import-master').submit(function () {
            var data = getFormData('form-import-master');
            ajaxTransfer('master/<?= $type ?>/preview-import', data, '#result-import');
        })
    })
</script>
