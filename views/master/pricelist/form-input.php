<?php use yii\helpers\Html; ?>

<form onsubmit='return false' id='form-pricelist' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-pricelist'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Goods/Service</label>
        <div class='col-lg-9 col-md-9 <?= $data->id ? 'form-control-static' : '' ?>'>
            <?php if ($data->id) { ?>
                <?= $data->goodsService->name ?>
                <?= Html::hiddenInput('goods_service_id', $data->goods_service_id) ?>
            <?php } else { ?>
                <?= Html::dropDownList('goods_service_id', $data->goods_service_id, $goodsServiceOption, ['onchange' => 'getGSCurrency()', 'class' => 'form-control']) ?>
            <?php } ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Quantity</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('number', 'quantity', $data->quantity, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Quantity']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Currency</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::textInput('currency', null, ['class' => 'form-control', 'readonly' => 'readonly']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Market Price</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('number', 'market_price', $data->market_price, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Market Price', 'step' => '0.01']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Supplier</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('supplier_id', $data->supplier_id, $supplierOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Revenue Tax</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('revenue_tax_id', $data->revenue_tax_id, $revenueTaxOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Value Added Tax</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('value_added_tax_id', $data->value_added_tax_id, $valueAddedTaxOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Package</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= Html::checkbox('is_package', $data->is_package, ['value' => 1]) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>
    function getGSCurrency() {
        var data = getFormData('form-pricelist');
        ajaxTransfer('master/pricelist/load-currency', data, function (result) {
            $('input[name=currency]').val(result);
        });
    }

    $(document).ready(function () {
        getGSCurrency();
        chosenConvert('select[name=goods_service_id]');
        chosenConvert('select[name=supplier_id]');

        $('#form-pricelist').submit(function () {
            var data = getFormData('form-pricelist');
            ajaxTransfer('master/pricelist/simpan-data', data, '#result-pricelist');
        });
    });
</script>
