<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Goods Service</th>
            <th>Quantity</th>
            <th>Market Price</th>
            <th>Supplier</th>
            <th>Package</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $list->goodsService->name ?></td>
                <td class='center'><?= $list->quantity ?></td>
                <td class='center'><?= $list->display_market_price ?></td>
                <td class='center'><?= $list->supplier->name ?></td>
                <td class='center'><?= check_times_cirle($list->is_package) ?></td>
                <td class='center nowrap'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/pricelist/entri-data'
                       data='id=<?= $list->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i
                                class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/pricelist/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/pricelist/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>
