<?php
$this->title = 'Supplier Master';
?>

<a href='<?= url("master/supplier/entri-data") ?>' title='Tambah Data' class="btn btn-primary"><i
            class='fa fa-plus'></i> Tambah Data</a>
<a class='btn btn-primary' href='<?= current_url() ?>/import/'><i class='fa fa-upload'></i> Import Data</a>

<div id='list-data-master'><?= $data ?></div>
