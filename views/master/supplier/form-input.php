<?php

use yii\helpers\Html;

$this->title = 'Entri Supplier';
?>

<form onsubmit="return false;" id='form-supplier' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-supplier'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Supplier Category</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::dropDownList('supplier_category_id', $data->supplier_category_id, $categoryOption, ['class' => 'form-control']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Name</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'name', $data->name, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Name']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Address</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'address', $data->address, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Address']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Phone 1</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'phone1', $data->phone1, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Phone 1']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Phone 2</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'phone2', $data->phone2, ['class' => 'form-control', 'placeholder' => 'Phone 2']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Phone 3</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'phone3', $data->phone3, ['class' => 'form-control', 'placeholder' => 'Phone 3']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Fax</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'fax', $data->fax, ['class' => 'form-control', 'placeholder' => 'Fax']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Email</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('email', 'email', $data->email, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Email']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Website</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'website', $data->website, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Website']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Authorized Person</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'authorized_person', $data->authorized_person, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Authorized Person']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Authorized Person Cellphone</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'authorized_person_cellphone', $data->authorized_person_cellphone, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Authorized Person Cellphone']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Payment Due Date</label>
        <div class='col-lg-9 col-md-9'>
            <div style="width: 100%;" class="input-group">
                <?= Html::input('number', 'deadline_day', $data->deadline_day, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Payment Deadline']) ?>
                <span class="input-group-addon">Day</span>
            </div>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>SIUP</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'siup', $data->siup, ['class' => 'form-control', 'placeholder' => 'SIUP']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>TDP</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'tdp', $data->tdp, ['class' => 'form-control', 'placeholder' => 'TDP']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>NPWP</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'npwp', $data->npwp, ['class' => 'form-control', 'placeholder' => 'NPWP']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>SPPKP</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'sppkp', $data->sppkp, ['class' => 'form-control', 'placeholder' => 'SPPKP']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>PKP</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= Html::checkbox('is_pkp', $data->is_pkp, ['value' => 1]) ?>
        </div>
    </div>

    <?php foreach ($uploadField as $field => $label) { ?>
        <div class='form-group'>
            <label class='col-lg-3 col-md-3 control-label'><?= $label ?></label>
            <div class='col-lg-9 col-md-9'>
                <?= Html::input('file', $field, null, ['class' => 'form-control']) ?>
                <?php $url = str_replace('path', 'file_url', $field) ?>
                <?php if (!is_null($data->$url)) { ?>
                    <a target='_blank' href='<?= $data->$url ?>'><i class='fa fa-download'></i>
                        Download File</a>
                <?php } ?>
            </div>
        </div>
    <?php } ?>

    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>

    $(document).ready(function () {
        activate_menu('<?= url('master/supplier') ?>');

        $('#form-supplier').submit(function () {
            var data = getFormData('form-supplier');
            ajaxTransfer('master/supplier/simpan-data', data, '#result-supplier');
        })
    })
</script>
