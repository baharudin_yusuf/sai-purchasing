<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $list->name ?></td>
                <td><?= $list->address ?></td>
                <td class='center'><?= $list->phone1 ?></td>
                <td class='center'><?= $list->email ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' href='<?= url("master/supplier/entri-data/?id=".$list->id) ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/supplier/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/supplier/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>