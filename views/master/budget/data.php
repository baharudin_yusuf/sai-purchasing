<div class='div-datatable'>
    <table class='display table table-bordered table-striped' id='tabel-data-master'>
        <thead>
        <tr>
            <th>Number</th>
            <th>Name</th>
            <th>Group</th>
            <th>Sub Group</th>
            <th>Pkg. Cmpnt</th>
            <th>Manage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $list) { ?>
            <tr>
                <td><?= $list->number ?></td>
                <td><?= $list->name ?></td>
                <td class='center'><?= $list->subGroup->group->name ?></td>
                <td class='center'><?= $list->subGroup->name ?></td>
                <td class='center'><?= check_times_cirle($list->is_packaging_component) ?></td>
                <td class='center'>
                    <a class='btn btn-primary btn-xs' onclick='loadModal(this)' target='master/budget/entri-data' data='id=<?= $list->id ?>' title='Ubah Data'><i class='fa fa-pencil-square-o'></i> Ubah</a>
                    <a class='btn btn-danger btn-xs' onclick='removeData(<?= $list->id ?>)'><i class='fa fa-trash-o'></i> Hapus</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script>
    function reloadData() {
        ajaxTransfer('master/budget/load-data', {}, '#list-data-master');
    }

    function removeData(id) {
        modalConfirm('Konfirmasi Tindakan', 'Apakah anda yakin akan menghapus data?', function () {
            var data = {id: id};
            ajaxTransfer('master/budget/hapus-data', data, '#modal-output');
        });
    }

    $(document).ready(function () {
        convert_datatables('tabel-data-master');
    });
</script>
