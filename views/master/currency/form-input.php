<?php use yii\helpers\Html; ?>

<form action='' onsubmit='return false' id='form-currency' class='form-horizontal'>
    <div class='col-lg-12 col-md-12' id='result-currency'></div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Name</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('text', 'name', $data->name, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Name']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Base Currency</label>
        <div class='col-lg-9 col-md-9 form-control-static'>
            <?= Html::checkbox('is_base_currency', $data->is_base_currency, ['value' => 1]) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'>Current Rate</label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('number', 'current_rate', $data->current_rate, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Current Rate', 'step'=>'0.000001']) ?>
        </div>
    </div>
    <div class='form-group'>
        <label class='col-lg-3 col-md-3 control-label'></label>
        <div class='col-lg-9 col-md-9'>
            <?= Html::input('submit', 'simpan', "Simpan", ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?= Html::input('hidden', 'id', $data->id) ?>
</form>

<script>
    $('#form-currency').submit(function () {
        var data = getFormData('form-currency');
        ajaxTransfer('master/currency/simpan-data', data, '#result-currency');
    });
</script>
