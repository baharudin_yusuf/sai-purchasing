<?php $this->title = 'Currency Master'; ?>

<a onclick='loadModal(this)' target='master/currency/entri-data' data='' title='Tambah Data' class="btn btn-primary"><i
            class='fa fa-plus'></i> Tambah Data</a>
<a class='btn btn-primary' href='<?= current_url() ?>/import/'><i class='fa fa-upload'></i> Import Data</a>

<div id='list-data-master'><?= $data ?></div>
