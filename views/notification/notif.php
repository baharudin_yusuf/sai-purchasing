<table border="0" cellpadding="0" cellspacing="0" width="580" style="border-collapse:collapse;border-radius:8px;border-spacing:0;padding:0;table-layout:auto;border: 1px solid #cacaca;" bgcolor="#fff">

    <tbody>
    <tr style="padding:0">
        <td align="center" valign="middle" style="border-collapse:collapse!important;height:55px;padding:0;word-break:break-word" bgcolor="#313945">
            <a href="<?= url() ?>" style="color:#fff;text-decoration:none;font-size: 25px;" target="_blank">
                Surabaya Autocomp Indonesia
            </a>
        </td>
    </tr>


    <tr style="padding:0">
        <td align="left" valign="top" style="border-collapse:collapse!important;padding:50px 50px 40px;word-break:break-word">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0;padding:0;table-layout:auto">
                <tbody><tr style="padding:0">
                    <td align="center" valign="middle" style="border-collapse:collapse!important;padding:0;word-break:break-word">
                        <h1 style="font-size:24px;font-weight:bold;line-height:30px;margin:0;padding-bottom:25px;word-break:normal">SAI NOTIFICATION</h1>

                        <p style="font-size:16px;font-weight:normal;line-height:22px;margin:0;padding-bottom:10px">
                            <?= $message ?>
                        </p>

                        <table border="0" cellpadding="0" cellspacing="0" width="335" style="border-collapse:separate;border-spacing:0;padding:0;table-layout:auto;width:335px">
                            <tbody>
                            <tr style="padding:0">
                                <td align="center" valign="middle" style="border-collapse:collapse!important;border-radius:5px;padding:10px 15px;word-break:break-word" bgcolor="#00c9c9">
                                    <a href="<?= url() ?>" style="color:#fff!important;display:block;font-size:20px;text-decoration:none" target="_blank">GO TO SAI</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>


    <tr style="padding:0">
        <td align="center" valign="middle" style="border-collapse:collapse!important;padding:0 15px 35px;word-break:break-word">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0;padding:0;table-layout:auto">
                <tbody>
                <tr style="padding:0">
                    <td align="center" valign="middle" style="border-collapse:collapse!important;border-top-color:#ccc;border-top-style:solid;border-top-width:1px;font-size:14px;padding:30px 0 0;word-break:break-word">
                        <strong>SAI SYSTEM</strong>
                        <br>
                        <a href="mailto:<?= ADMIN_EMAIL ?>" style="color:#00b2b2;text-decoration:none" target="_blank"><?= ADMIN_EMAIL ?></a>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>