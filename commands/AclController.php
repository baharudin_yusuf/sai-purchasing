<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\controllers\master as master;
use app\models\ACLAccess;
use app\models\ACLList;
use app\models\Role;
use yii\console\Controller;

class AclController extends Controller
{
    public function actionRegister()
    {
        $data = self::getData();
        $itemUsed = [];

        foreach ($data as $class) {
            if (!property_exists($class, 'moduleName') || !property_exists($class, 'subModule')) {
                continue;
            }

            $moduleName = $class::$moduleName;
            $subModule = $class::$subModule;
            $role = Role::findByAlias('admin');

            foreach ($subModule as $item) {
                $this->stdout("register : $moduleName - $item\n");
                $acl = ACLList::findOne([
                    'module' => $moduleName,
                    'module_item' => $item
                ]);

                if (is_null($acl)) {
                    $acl = new ACLList();
                    $acl->module = $moduleName;
                    $acl->module_item = $item;
                    $acl->save();
                }

                // admin acl
                $condition = [
                    'acl_list_id' => $acl->id,
                    'role_id' => $role->id,
                    'department_id' => null,
                    'section_id' => null
                ];
                $access = ACLAccess::findOne($condition);
                if(is_null($access)){
                    $access = new ACLAccess();
                    $access->updateAttributes($condition);
                }
                $access->is_access = 1;
                $access->save();

                $itemUsed[] = $acl->id;
            }
        }

        ACLList::deleteAll(['not in', 'id', $itemUsed]);
    }

    private function getData()
    {
        $module = [
            master\BankController::class,
            master\BankDetailController::class,
            master\BudgetCategoryController::class,
            master\BudgetCategoryPeriodController::class,
            master\BudgetController::class,
            master\BudgetGroupController::class,
            master\BudgetLimitController::class,
            master\BudgetPeriodController::class,
            master\BudgetSubGroupController::class,
            master\CarlineController::class,
            master\CurrencyController::class,
            master\CurrencyDetailController::class,
            master\DepartmentController::class,
            master\FrancoController::class,
            master\GoodsServiceController::class,
            master\PaymentMethodController::class,
            master\PeriodController::class,
            master\PricelistController::class,
            master\RevenueTaxController::class,
            master\SectionController::class,
            master\SupplierCategoryController::class,
            master\SupplierController::class,
            master\TermController::class,
            master\UnitCategoryController::class,
            master\UnitController::class,
            master\ValueAddedTaxController::class,
        ];
        return $module;
    }
}
