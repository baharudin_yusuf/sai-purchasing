<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\User;
use yii\console\Controller;

class SaiController extends Controller
{
    public function actionMigrate()
    {
        foreach (User::find()->all() as $user){
            $meta = $user->meta;
            if(is_null($meta)){
                continue;
            }

            $name = $user->name;
            $this->stdout("update user : $name\n");

            $user->updateAttributes([
                'section_id' => $meta->section_id,
                'department_id' => $meta->department_id,
                'is_administration' => $meta->is_administration,
                'is_supervisor' => $meta->is_supervisor,
                'profile_picture' => $meta->profile_picture,
                'signature' => $meta->signature,
            ]);
        }
    }
}
