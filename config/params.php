<?php
define('SESSION_EXPIRED',	40);

/* FILE PATH */
define('PATH_FOTO', 	'uploads/foto/');
define('PATH_DOKUMEN',	'uploads/dokumen/');
define('UPLOAD_PATH', 	'uploads/');
define('NPWP_PATH', 	'uploads/npwp');
define('SIUP_PATH', 	'uploads/siup');
define('SPPKP_PATH', 	'uploads/sppkp');
define('TDP_PATH', 		'uploads/tdp');
define('SKT_BADAN_PATH', 		'uploads/skt_badan');
define('SKT_PRIBADI_PATH', 		'uploads/skt_pribadi');
define('NON_PKP_PATH', 			'uploads/non_pkp');
define('PO_PAYMENT_PATH', 		'uploads/po_payment');
define('PO_REALIZATION_PATH', 	'uploads/po_realization');
define('DP_REALIZATION_PATH', 	'uploads/dp_realization');
define('DP_DOCUMENT_PATH', 		'uploads/dp_document');
define('PROFILE_PICTURE_PATH', 	'uploads/profile_picture');
define('SIGNATURE_PATH', 		'uploads/signature');
/* /FILE PATH */

/* SMTP */
/*define('ADMIN_EMAIL',	'test.codemaster@gmail.com');
define('SMTP_HOST',		'smtp.gmail.com');
define('SMTP_PORT',		'587');
define('SMTP_USERNAME',	'test.codemaster@gmail.com');
define('SMTP_PASSWORD',	'cmV0c2FtZWRvYw');
define('SMTP_ENCRYPTION',	'tls');*/

define('ADMIN_EMAIL',	null);
define('SMTP_HOST',		null);
define('SMTP_PORT',		null);
define('SMTP_USERNAME',	null);
define('SMTP_PASSWORD',	null);
define('SMTP_ENCRYPTION',	null);
/* /SMTP */

/* do not change this section */
define('TEMP_DIR',		'temp/');
/* / do not change this section */

/* error message */
define('ERR_001', 'Error! Goods/Service belum terdaftar dalam pricelist');
define('ERR_002', 'Error! Anda tidak memiliki akses untuk menerima budget proposal detail!');
define('ERR_003', 'Error! Detail budget proposal gagal berhasil diapprove!');
define('ERR_004', 'Error! Anda tidak memiliki hak akses untuk menerima pengajuan budget proposal!');
define('ERR_005', 'Error! Budget proposal gagal diterima!');
define('ERR_006', '<strong>Error!</strong> Masa aktif untuk budget dan periode budget yang dipilih belum diset!');
/* /error message */

/* departmen code */
define('FA_NAME', 'FIN & ACC');
define('FA_CODE', 'FA');
define('HRGA_CODE', 'HRGA');
define('GAGS_CODE', 'GAGS');
/* /departmen code */

/* section code */
define('LP_CODE', 'LP');
/* /section code */

/* DATA CODE */
define('ADDITIONAL_CODE', 'ADD');
define('TRANSFER_CODE', 'TRF');
define('PROPOSAL_CODE', 'BP');
define('FINAL_CODE', 'FIN');
define('DP_CODE', 'DP');
define('PR_CODE', 'PR');
define('PR_REV_CODE', 'PR_REV');
define('PO_CODE', 'PO');
define('PO_REV_CODE', 'PO_REV');
define('DOCUMENT_CODE', 'DOC');
define('INVOICE_CODE', 'INV');
define('VP_CODE', 'VP');
define('VR_CODE', 'VR');
/* /DATA CODE */

/* ICONS */
define('APPROVED_MARK', "<i class='icon icon-approved'>&#10004;</i>");
define('REJECTED_MARK', "<i class='icon icon-rejected'>&#10006;</i>");

$bulk_action_btn = "<hr style='margin-bottom:10px;'>
<a onclick='select_unselect_all(this)' class='btn btn-primary btn-xs'><i class='fa fa-check-square'></i> <span>Select All</span></a> | 
<a onclick='bulk_action(\"approve\")' class='btn btn-primary btn-xs'><i class='fa fa-check-circle'></i> Approve Checked</a> 
<a onclick='bulk_action(\"reject\")' class='btn btn-danger btn-xs'><i class='fa fa-times-circle'></i> Reject Checked</a>
";
define('BULK_ACTION_BTN', $bulk_action_btn);

return [
    'logo' 			=> '',
	'navMenu'		=> '',
	'sidebarMenu'	=> '',
	'warning'		=> [],
	'includeRejectedForm'	=> '',
	'role'	=> [
		'admin'		=> ['code' => 849, 'name' => 'Administrator'],
		'guest'		=> ['code' => 586, 'name' => 'Guest'],
		'section'	=> ['code' => 435, 'name' => 'Section'],
		'manager'	=> ['code' => 234, 'name' => 'Manager'],
		'fm'		=> ['code' => 784, 'name' => 'FM'],
		'dfm'		=> ['code' => 182, 'name' => 'DFM'],
		'presdir'	=> ['code' => 364, 'name' => 'Presiden Direktur'],
	],
];
