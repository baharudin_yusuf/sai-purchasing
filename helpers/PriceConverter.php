<?php

namespace app\helpers;

use app\models\master\CurrencyDetail;
use app\models\master\Period;

class PriceConverter
{
    public static function convert($amount, $sourceCurrencyId)
    {
        $period = Period::findOne(['is_active' => 1]);
        if (is_null($period)) {
            return 0;
        }

        $baseCurrency = self::getActiveBaseCurrency();
        if ($baseCurrency->id == $sourceCurrencyId) {
            return $amount;
        } else {
            $currencyDetail = CurrencyDetail::findOne([
                'period_id' => $period->id,
                'currency_id' => $sourceCurrencyId
            ]);
            return $amount * $currencyDetail->current_rate;
        }
    }

    public static function getActiveBaseCurrency()
    {
        $period = Period::findOne(['is_active' => 1]);
        if (is_null($period)) {
            return null;
        }

        return $period->getCurrencyDetailBase();
    }
}

?>