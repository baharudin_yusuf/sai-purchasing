<?php
namespace app\helpers;

use Yii;
use app\helpers\Input;
use app\helpers\Tools;

class AccessControl{
    public static function allow($roleList){
		$roleData = Yii::$app->params['role'];
		$loginData = Tools::getSession('loginData');
		
		$activeUserRole = isset($loginData['role']) ? $loginData['role'] : $roleData['guest']['code'];
		
		$allowedRole = '';
		$roleList = explode('|', $roleList);
		foreach($roleList as $roleName){
			$roleValue = $roleData[$roleName]['code'];
			$allowedRole[$roleValue] = true;
		}
		
		return isset($allowedRole[$activeUserRole]);
	}
	
	public static function deny($roleList){
		$roleData = Yii::$app->params['role'];
		$loginData = Tools::getSession('loginData');
		$activeUserRole = isset($loginData['role']) ? $loginData['role'] : $roleData['guest']['code'];

		$allowedRole = '';
		$roleList = explode('|', $roleList);
		foreach($roleList as $roleName){
			$roleValue = $roleData[$roleName]['code'];
			$allowedRole[$roleValue] = true;
		}
		
		return isset($allowedRole[$activeUserRole]);
	}
	
	public static function restrict($roleList){
		$roleData = Yii::$app->params['role'];
		$loginData = Tools::getSession('loginData');
		
		$activeUserRole = isset($loginData['role']) ? $loginData['role'] : $roleData['guest']['code'];
		
		$allowedRole = '';
		$roleList = explode('|', $roleList);
		foreach($roleList as $roleName){
			$roleValue = $roleData[$roleName]['code'];
			$allowedRole[$roleValue] = true;
		}

		$allow = isset($allowedRole[$activeUserRole]);
		if(!$allow){
			$redirectURL = Yii::$app->params['backendBaseURL'].'error/forbidden';
			Tools::redirect($redirectURL);
		}
	}
}