<?php
namespace app\helpers;

use app\models\User;

class Auth{
    private static $sessionName = 'auth_user_id';

    public static function check()
    {
        $user = self::user();
        return is_null($user) ? false : true;
    }

    public static function login($userId)
    {
        \Yii::$app->session->set(self::$sessionName, $userId);
    }

    public static function logout()
    {
        \Yii::$app->session->remove(self::$sessionName);
    }

    /** @return User|null */
    public static function user()
    {
        $userId = intval(\Yii::$app->session->get(self::$sessionName));
        return User::findOne($userId);
    }

}
?>