<?php
namespace app\helpers;

class Tools
{
    private static $uploadPath = '';
    private static $sessionPrefix = 'saipr';

    public static function objectToArray($object)
    {
        if (!is_object($object) && !is_array($object)) {
            return $object;
        }
        if (is_object($object)) {
            $object = get_object_vars($object);
        }
        return array_map('objectToArray', $object);
    }

    public static function getSession($session_name)
    {
        if (session_status() == PHP_SESSION_NONE) session_start();
        $session_name = self::$sessionPrefix . $session_name;
        if (isset($_SESSION[$session_name])) return $_SESSION[$session_name];
        else return null;
    }

    public static function setSession($session_name, $session_value)
    {
        if (session_status() == PHP_SESSION_NONE) session_start();
        $session_name = self::$sessionPrefix . $session_name;
        $_SESSION[$session_name] = $session_value;

        self::sessionRegister($session_name);
    }

    public static function sessionRegister($session_name)
    {
        if (self::getSession('sessionRegister') == null) {
            self::setSession('sessionRegister', $session_name);
        } else {
            $sessionRegister = self::getSession('sessionRegister');
            $sessionRegister = explode(',', $sessionRegister);

            if (!in_array($session_name, $sessionRegister)) {
                $sessionRegister = implode(',', $sessionRegister) . ',' . $session_name;
                self::setSession('sessionRegister', $sessionRegister);
            }
        }
    }

    public static function sessionClear()
    {
        $sessionRegister = explode(',', self::getSession('sessionRegister'));
        foreach ($sessionRegister as $session) {
            self::unsetSession(self::removePrefix($session));
        }
    }

    public static function removePrefix($string)
    {
        $prefix_length = strlen(self::$sessionPrefix);
        return substr($string, $prefix_length, strlen($string) - $prefix_length);
    }

    public static function unsetSession($session_name)
    {
        if (is_array($session_name)) {
            for ($i = 0; $i < count($session_name); $i++) {
                $session_name[$i] = self::$sessionPrefix . $session_name[$i];
                if (isset($_SESSION[$session_name[$i]])) unset($_SESSION[$session_name[$i]]);
            }
        } else {
            $session_name = self::$sessionPrefix . $session_name;
            if (isset($_SESSION[$session_name])) unset($_SESSION[$session_name]);
        }
    }

    public static function encrypt($string)
    {
        $vokal = array('a', 'i', 'u', 'e', 'o');
        return md5(strrev(str_replace($vokal, '', md5($string))));
    }

    public static function upload($name, $uploadPath = null, $filename = '', $filter = [])
    {
        $blocked = ['php', 'html', 'exe', 'py', 'pl', 'rb'];
        if (is_null($uploadPath)) $uploadPath = self::$uploadPath;

        if (isset($_FILES[$name])) {
            if (strlen($_FILES[$name]['name']) == 0) return ['error' => false, 'msg' => 'no file choosen'];

            $ext = self::getExtension($_FILES[$name]['name']);
            if (isset($blocked[$ext])) return ['error' => true, 'msg' => 'extension not allowed'];
            if (count($filter) > 0) {
                $allowedExt = [];
                foreach ($filter as $fileExt) {
                    $allowedExt[$fileExt] = $fileExt;
                }
                if (!isset($allowedExt[$ext])) {
                    return ['error' => true, 'msg' => 'Extension not allowed. Allowed extension : ' . implode(', ', $allowedExt)];
                }
            }

            if (strlen($filename) == 0) $newName = self::generateFileName() . '.' . $ext;
            else $newName = $filename . '.' . $ext;

            if (@move_uploaded_file($_FILES[$name]['tmp_name'], $uploadPath . '/' . $newName)) {
                $data = array(
                    'error' => false,
                    'name' => $_FILES[$name]['name'],
                    'size' => $_FILES[$name]['size'],
                    'new_name' => $newName,
                    'tmp_name' => $_FILES[$name]['tmp_name'],
                    'ext' => $ext
                );
                return $data;
            } else return array('error' => true, 'msg' => 'error moving files');
        } else return array('error' => true, 'msg' => 'error initializing files');
    }

    private static function generateFileName()
    {
        $waktu = time();
        $random = rand(100, 999);
        $filename = "files_$waktu$random";

        return $filename;
    }

    public static function getExtension($filename)
    {
        $ext = explode('.', strtolower($filename));
        return strtolower(end($ext));
    }

    public static function CSVToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) return null;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) $header = $row;
                else $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }

    public static function loadCSV($input_name)
    {
        $upload = self::upload($input_name, UPLOAD_PATH, '', ['csv']);

        if (!$upload['error']) {
            $filename = $upload['new_name'];
            $filepath = UPLOAD_PATH . $filename;

            $data = self::CSVToArray($filepath);
            if (!is_null($data)) {
                @unlink($filepath);
                return $data;
            } else return null;
        } else return null;
    }

    public static function loadExcel($input_name, $limit = null)
    {
        if (!isset($_FILES[$input_name])) {
            return null;
        }

        $upload = self::upload($input_name, UPLOAD_PATH, '', ['xls', 'xlsx']);
        if (!$upload['error']) {
            $filename = $upload['new_name'];
            $filepath = UPLOAD_PATH . $filename;

            try {
                $objPHPExcel = \PHPExcel_IOFactory::load($filepath);
                $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

                $loopLimit = count($sheetData);
                if(!is_null($limit) && intval($limit) > 0){
                    $loopLimit = $limit;
                }

                $resultData = [];
                $header = [];
                $firstLoop = true;
                foreach ($sheetData as $item) {
                    if ($firstLoop) {
                        foreach ($item as $key) {
                            $header[] = $key;
                        }
                        $firstLoop = false;
                    } else {
                        $index = 0;
                        $temp = [];
                        foreach ($item as $key) {
                            $temp[$header[$index]] = $key;
                            $index++;
                        }
                        $resultData[] = $temp;

                        if(count($resultData) == $loopLimit){
                            break;
                        }
                    }
                }

                @unlink($filepath);
                return $resultData;
            } catch (\Exception $ex) {
                self::setSession('error_exists', true);
                self::setSession('msg', "<div class='alert alert-danger'>Terjadi kesalahan! Format data tidak sesuai template import. Periksa ulang data anda</div>");
                @unlink($filepath);
                return null;
            }
        } else {
            self::setSession('error_exists', true);
            self::setSession('msg', "<div class='alert alert-danger'>" . $upload['msg'] . "</div>");
            return null;
        }
    }

    public static function getLine($file_path, $line_number)
    {
        if (file_exists($file_path)) {
            $file = file_get_contents($file_path);
            $file = nl2br($file);
            $file = str_replace("\t", '', $file);
            $file = explode('<br />', $file);

            $line_number--;
            $line = $file[$line_number];
        } else $line = '';

        return $line;
    }

    public static function slug($string)
    {
        $string = str_replace(' ', '-', strtolower($string)); /* Replaces all spaces with hyphens. */
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); /* Removes special chars. */
        for ($i = 0; $i < 3; $i++) $string = str_replace('--', '-', $string);

        return $string;
    }

    public static function get_parent_name($controller_name)
    {
        $is_base = in_array($controller_name, array('core', 'controller', 'model', 'helper'));
        if ($is_base) $file_path = 'resource/system/base/' . strtolower($controller_name) . '.php';
        else $file_path = 'resource/system/controller/' . strtolower($controller_name) . '.php';

        if (file_exists($file_path)) {
            $str_name = self::getLine($file_path, 2);
            $temp = explode('extends', $str_name);

            if (count($temp) < 2) return null;
            else {
                $str_name = preg_replace('/[^A-Za-z0-9\_]/', '', $temp[1]);
                return strtolower($str_name);
            }
        } else return null;
    }

    public static function redirect($redirectTarget)
    {
        header("Location: $redirectTarget");
        exit();
    }

    public static function romanicNumber($integer)
    {
        $table = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
        $return = '';
        while ($integer > 0) {
            foreach ($table as $rom => $arb) {
                if ($integer >= $arb) {
                    $integer -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }

        return $return;
    }
}

?>
