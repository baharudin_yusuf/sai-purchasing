<?php

namespace app\helpers;

use app\models\master\Period;
use app\models\NotificationMessage;

class Notificatixn
{
    public static function render($key, $params)
    {
        $notif = NotificationMessage::findOne(['key' => $key]);
        if (is_null($notif)) {
            return '';
        } else {
            $message = $notif->message;
            foreach ($params as $tag => $replace) {
                $message = str_replace('{' . $tag . '}', $replace, $message);
            }
            return $message;
        }
    }

    public static function getActiveBaseCurrency()
    {
        $period = Period::findOne(['is_active' => 1]);
        if (is_null($period)) {
            return null;
        }

        return $period->getCurrencyDetailBase();
    }
}

?>