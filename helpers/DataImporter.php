<?php
namespace app\helpers;

class DataImporter{

    public static function loadExcel($inputName, $limit = null)
    {
        $upload = upload($inputName, UPLOAD_PATH, '', ['xls', 'xlsx']);
        if (!$upload['error']) {
            $filename = $upload['new_name'];
            $filepath = UPLOAD_PATH . $filename;

            try {
                $objPHPExcel = \PHPExcel_IOFactory::load($filepath);
                $sheetData = $objPHPExcel
                    ->getActiveSheet()
                    ->toArray(null, true, true, true);

                $loopLimit = count($sheetData);
                if(!is_null($limit) && intval($limit) > 0){
                    $loopLimit = $limit;
                }

                $resultData = [];
                $header = [];
                $firstLoop = true;
                foreach ($sheetData as $item) {
                    if ($firstLoop) {
                        foreach ($item as $key) {
                            $header[] = $key;
                        }
                        $firstLoop = false;
                    } else {
                        $index = 0;
                        $temp = [];
                        foreach ($item as $key) {
                            $temp[$header[$index]] = $key;
                            $index++;
                        }
                        $resultData[] = $temp;

                        if(count($resultData) == $loopLimit){
                            break;
                        }
                    }
                }

                @unlink($filepath);
                return $resultData;
            } catch (\Exception $ex) {
                @unlink($filepath);
                return 'Terjadi kesalahan! Format data tidak sesuai template import. Periksa ulang data anda';
            }
        } else {
            return $upload['msg'];
        }
    }

}
?>