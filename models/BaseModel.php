<?php

namespace app\models;

use yii\web\NotFoundHttpException;

class BaseModel extends \yii\db\ActiveRecord
{
    public $tableName, $primaryKey, $attributeLabels, $newEmpty, $schema;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function __get($name)
    {
        try {
            return parent::__get($name);
        } catch (\Exception $ex) {
            return null;
        }
    }

    public function __set($name, $value)
    {
        try {
            parent::__set($name, $value);
        } catch (\Exception $ex) {
            $this->$name = $value;
        }
    }

    public static function firstOrFail($params)
    {
        $data = self::findOne($params);
        if (is_null($data)) {
            if (\Yii::$app->request->isPost) {
                die("<div class='alert alert-danger center'>Terjadi kesalahan! Halaman tidak ditemukan!</div>");
            } else {
                throw new NotFoundHttpException();
            }
        }
        return $data;
    }

    protected function convertArray($objData)
    {
        $data = array();

        foreach ($objData as $key => $value) {
            $data[$key] = $value;
        }

        return $data;
    }

    protected function initChild($childObject)
    {
        $this->tableName = $childObject->tableName();

        $primaryKey = $childObject->primaryKey();
        $this->primaryKey = $primaryKey[0];

        $schema = $childObject->attributeLabels();
        foreach ($schema as $key => $value) {
            $this->newEmpty[$key] = null;
            $this->schema[] = $key;
        }

        $this->attributeLabels = $childObject->attributeLabels();
    }

    public function getData($primaryKey)
    {
        return $this->find()
            ->asArray()
            ->where([$this->primaryKey => $primaryKey])
            ->one();
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        try {
            parent::save($runValidation, $attributeNames);
            $save = true;
        } catch (\Exception  $e) {
            $message = base64_encode(strip_tags($e->getMessage()));
            echo "<script> console.log(atob('$message')); </script>";
            $save = false;
        }

        return $save;
    }

    public static function newEmpty()
    {
        $class_name = get_called_class();
        $classObject = new $class_name();
        return $classObject->newEmpty;
    }
}
