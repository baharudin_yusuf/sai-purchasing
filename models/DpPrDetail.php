<?php

namespace app\models;

class DpPrDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'dp_pr_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'direct_purchase_id', 'purchase_requisition_id');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getDirectPurchase()
    {
        return $this->hasOne(DirectPurchase::class, ['id' => 'direct_purchase_id']);
    }

    public function getPurchaseRequisition()
    {
        return $this->hasOne(PurchaseRequisition::class, ['id' => 'purchase_requisition_id']);
    }

    public function getPurchaseRequisition_id()
    {
        return $this->hasOne(PurchaseRequisition::class, ['id' => 'purchase_requisition_id']);
    }

    public function getRealizationActual()
    {
        return $this->hasMany(DpRealizationActual::class, ['dp_pr_detail_id' => 'id']);
    }

    public function getRealizationPlan()
    {
        return $this->hasMany(DpRealizationPlan::class, ['dp_pr_detail_id' => 'id']);
    }
}
