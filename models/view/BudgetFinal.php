<?php

namespace app\models\view;

use app\models\BaseModel;

class BudgetFinal extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'v_budget_final';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'budget_proposal_id', 'section_id', 'section', 'budget_period', 'budget_name', 'budget_number', 'carline', 'amount', 'amount_used', 'comment', 'is_approved_by_manager', 'approved_by_manager_time', 'is_approved_by_fa', 'approved_by_fa_time', 'is_additional', 'is_rejected', 'is_final', 'budget_proposal_status', 'date');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function getOption()
    {
        $data = BudgetFinal::find()->asArray()->all();
        $option = [];
        foreach ($data as $list) {
            extract($list);

            $option[$id] = "[$budget_number] $budget_name";
        }

        return $option;
    }
}
