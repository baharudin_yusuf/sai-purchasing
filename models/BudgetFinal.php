<?php

namespace app\models;

class BudgetFinal extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_final';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'budget_proposal_id', 'time');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'code':
                return FINAL_CODE . str_pad($this->id, 4, '0', STR_PAD_LEFT);

            case 'display_amount_used':
                $currencyUsed = $this->budgetProposal->getCurrencyUsed();
                return $currencyUsed->name . ' ' . currency_format($this->getAmountUsed());

            case 'display_amount_available':
                $currencyUsed = $this->budgetProposal->getCurrencyUsed();
                return $currencyUsed->name . ' ' . currency_format($this->getAmountAvailable());
        }

        return parent::__get($name);
    }

    public function getBudgetProposal()
    {
        return $this->hasOne(BudgetProposal::class, ['id' => 'budget_proposal_id']);
    }

    public function getPurchaseRequisition()
    {
        return $this->hasMany(PurchaseRequisition::class, ['budget_final_id' => 'id']);
    }

    public function getAmountUsed()
    {
        $amountUsed = 0;
        $prApproved = $this->getPurchaseRequisition()->where([
            'is_rejected' => 0,
            'is_canceled' => 0,
        ])->all();
        foreach ($prApproved as $pr) {
            $amountUsed += $pr->price_estimation;
        }

        return $amountUsed;
    }

    public function getAmountAvailable()
    {
        return $this->budgetProposal->amount - $this->getAmountUsed();
    }
}
