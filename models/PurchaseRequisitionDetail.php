<?php

namespace app\models;

use app\helpers\PriceConverter;
use app\models\master\Carline;
use app\models\master\GoodsService;

class PurchaseRequisitionDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'purchase_requisition_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'purchase_requisition_id', 'goods_service_id', 'quantity', 'used_quantity', 'available_quantity', 'price_estimation', 'carline_id', 'required_date', 'comment', 'is_approved_by_manager', 'approved_by_manager_time', 'is_approved_by_dfm', 'approved_by_dfm_time', 'is_approved_by_fm', 'approved_by_fm_time', 'is_approved_by_presdir', 'approved_by_presdir_time', 'is_approved_by_fa', 'approved_by_fa_time', 'is_rejected', 'is_canceled'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_price_estimation':
                $currency = $this->goodsService->currency;
                return $currency->name . ' ' . currency_format($this->price_estimation);

            case 'display_converted_price_estimation':
                $currency = PriceConverter::getActiveBaseCurrency();
                if ($currency->id == $this->goodsService->currency->id) {
                    return '';
                }

                $gsCurrency = $this->goodsService->currency;
                $converted = PriceConverter::convert($this->price_estimation, $gsCurrency->id);
                return $currency->name . ' ' . currency_format($converted);

            case 'sub_total':
                return $this->price_estimation * $this->quantity;

            case 'converted_sub_total':
                $currency = PriceConverter::getActiveBaseCurrency();
                return PriceConverter::convert($this->sub_total, $currency->id);

            case 'display_sub_total':
                $currency = $this->goodsService->currency;
                $subtotal = $this->price_estimation * $this->quantity;
                return $currency->name . ' ' . currency_format($subtotal);

            case 'display_converted_sub_total':
                $currency = PriceConverter::getActiveBaseCurrency();
                if ($currency->id == $this->goodsService->currency->id) {
                    return '';
                }

                $gsCurrency = $this->goodsService->currency;
                $subtotal = $this->price_estimation * $this->quantity;
                $converted = PriceConverter::convert($subtotal, $gsCurrency->id);
                return $currency->name . ' ' . currency_format($converted);
        }

        return parent::__get($name);
    }

    public function getPurchaseRequisition()
    {
        return $this->hasOne(PurchaseRequisition::class, ['id' => 'purchase_requisition_id']);
    }

    public function getGoodsService()
    {
        return $this->hasOne(GoodsService::class, ['id' => 'goods_service_id']);
    }

    public function getCarline()
    {
        return $this->hasOne(Carline::class, ['id' => 'carline_id']);
    }
}
