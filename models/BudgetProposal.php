<?php

namespace app\models;

use app\helpers\PriceConverter;
use app\models\master\Budget;
use app\models\master\BudgetCategory;
use app\models\master\BudgetPeriod;
use app\models\master\Period;
use app\models\master\Section;

class BudgetProposal extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_proposal';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'section_id', 'budget_period_id', 'budget_id', 'amount', 'amount_used', 'prev_amount_actual', 'comment', 'is_approved_by_manager', 'approved_by_manager_time', 'is_approved_by_fa', 'approved_by_fa_time', 'is_reschedule', 'is_additional', 'is_rejected', 'is_canceled', 'is_final', 'is_transfer', 'is_expired', 'active_until', 'date'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'code':
                return 'BP' . str_pad($this->id, 4, '0', STR_PAD_LEFT);

            case 'display_amount':
                $currencyUsed = $this->getCurrencyUsed();
                return $currencyUsed->name . ' ' . currency_format($this->amount);

            case 'display_amount_used':
                $currencyUsed = $this->getCurrencyUsed();
                return $currencyUsed->name . ' ' . currency_format($this->getAmountUsed());

        }
        return parent::__get($name);
    }

    public function getBudget()
    {
        return $this->hasOne(Budget::class, ['id' => 'budget_id']);
    }

    public function getSection()
    {
        return $this->hasOne(Section::class, ['id' => 'section_id']);
    }

    public function getBudgetPeriod()
    {
        return $this->hasOne(BudgetPeriod::class, ['id' => 'budget_period_id']);
    }

    public function getBudgetFinal()
    {
        return $this->hasMany(BudgetFinal::class, ['budget_proposal_id' => 'id']);
    }

    public function getDetail()
    {
        return $this->hasMany(BudgetProposalDetail::class, ['budget_proposal_id' => 'id']);
    }

    public function getDisplayDetail()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (get_session('include_rejected') === true) {
            unset($cond['is_rejected']);
        }

        if (get_session('only_rejected') === true) {
            $cond['is_rejected'] = 1;
        }

        if (get_session('include_canceled') === true) {
            unset($cond['is_canceled']);
        }

        if (get_session('only_canceled') === true) {
            $cond['is_canceled'] = 1;
        }

        return $this->getDetail()->where($cond)->all();
    }

    public function getNonCancelReject()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        return $this->getDetail()->where($cond)->all();
    }

    public function getSubmissionAmount($formatted = false)
    {
        $submissionAmount = 0;
        foreach ($this->getDisplayDetail() as $detail) {
            $goodsService = $detail->goodsService;
            $cPrice = PriceConverter::convert($detail->price_estimation, $goodsService->currency_id);
            $submissionAmount += ($cPrice * $detail->quantity);
        }

        if (!$formatted) {
            return $submissionAmount;
        } else {
            $currency = PriceConverter::getActiveBaseCurrency();
            return $currency->name . ' ' . currency_format($submissionAmount);
        }
    }

    public function getAmountUsed()
    {
        $finalList = $this->budgetFinal;
        if (count($finalList) == 0) {
            return 0;
        }

        $amountUsed = 0;
        foreach ($finalList as $final) {
            $prApproved = $final->getPurchaseRequisition()->where([
                'is_rejected' => 0,
                'is_canceled' => 0,
            ])->all();
            foreach ($prApproved as $pr) {
                $amountUsed += $pr->price_estimation;
            }
        }

        return $amountUsed;
    }

    public function getCurrencyUsed()
    {
        /** @var $period Period */
        $period = $this->budgetPeriod->period;
        $currencyDetail = $period->getCurrencyDetail()->where(['is_base_currency' => 1])->one();
        return $currencyDetail->currency;
    }

    public function calculateAmountUsed()
    {
        $amountUsed = 0;
        foreach ($this->detail as $detail) {
            $goodsService = $detail->goodsService;
            $cPrice = PriceConverter::convert($detail->price_estimation, $goodsService->currency_id);
            $amountUsed += ($cPrice * $detail->quantity);
        }
        $this->amount_used = $amountUsed;
        $this->save();
    }

    public function setExpiredDate()
    {
        $budgetPeriod = $this->budgetPeriod;
        $period_id = $budgetPeriod->period_id;
        $proposalYear = $budgetPeriod->year;
        $proposalMonth = $budgetPeriod->month;
        $proposalDate = "$proposalYear-$proposalMonth-01 00:00:00";
        $proposalTime = strtotime($proposalDate);

        /** @var $budgetCategory BudgetCategory */
        $budgetCategory = $this->budget->subGroup->group->category;
        $budgetCategoryPeriod = $budgetCategory->getBudgetCategoryPeriod()
            ->where(['period_id' => $period_id])
            ->one();

        $activePeriod = $budgetCategoryPeriod->active_period;

        $actualExpired = date("Y-m-d H:i:s", strtotime("+$activePeriod month", $proposalTime));
        $actualTime = strtotime($actualExpired);

        /* GET LAST MONTH IN PERIOD */
        /** @var $period Period */
        $period = $budgetPeriod->period;
        $lastPeriod = $period->getBudgetPeriod()->orderBy(['month' => SORT_DESC])->one();
        $lastYear = $lastPeriod->year;
        $lastMonth = $lastPeriod->month;
        $lastDate = "$lastYear-$lastMonth-01 00:00:00";
        $lastTime = strtotime($lastDate);

        $periodExpired = date("Y-m-d H:i:s", strtotime("+1 month", $lastTime));
        $lastTime = strtotime($periodExpired);
        $periodExpired = date("Y-m-d H:i:s", strtotime("-1 day", $lastTime));
        $lastTime = strtotime($periodExpired);

        if ($actualTime > $lastTime) {
            $actualTime = $lastTime;
        }

        $this->active_until = date('Y-m-d H:i:s', $actualTime);
        $this->save();
    }
}
