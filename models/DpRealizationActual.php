<?php

namespace app\models;

use app\controllers\BaseController;
use app\helpers\Auth;
use app\helpers\PriceConverter;
use app\models\master\Carline;
use app\models\master\GoodsService;

class DpRealizationActual extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'dp_realization_actual';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'dp_pr_detail_id', 'goods_service_id', 'quantity', 'received', 'delivered', 'price', 'is_fixed', 'carline_id', 'rev_tax_name', 'rev_tax_rate', 'rev_tax_amount', 'vat_name', 'vat_rate', 'vat_amount');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'is_complete':
                return ($this->received == $this->delivered) && ($this->delivered == $this->quantity);

            case 'is_complete_received':
                return $this->received == $this->quantity;

            case 'is_complete_delivered':
                return $this->delivered == $this->quantity;

            case 'tax':
                return $this->rev_tax_amount + $this->vat_amount;

            case 'c_tax':
                $goodsService = $this->goodsService;
                return BaseController::getConvertedPrice($this->tax, $goodsService->currency_id);

            case 'c_price':
                $goodsService = $this->goodsService;
                return BaseController::getConvertedPrice($this->price, $goodsService->currency_id);

            case 'plan_price':
                $pr = $this->dpPrDetail;
                $plan = $pr->getRealizationPlan()->where(['goods_service_id' => $this->goods_service_id])->one();
                return $plan->price;

            case 'display_price':
                $currency = $this->goodsService->currency;
                return $currency->name . ' ' . currency_format($this->price);

            case 'display_converted_price':
                $gsCurrency = $this->goodsService->currency;
                $cPrice = PriceConverter::convert($this->price, $gsCurrency->id);
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($cPrice);

            case 'display_tax':
                $currency = PriceConverter::getActiveBaseCurrency();
                $tax = $this->rev_tax_amount + $this->vat_amount;
                return $currency->name . ' ' . currency_format($tax);

            case 'display_subtotal':
                $gsCurrency = $this->goodsService->currency;
                $subtotal = $this->quantity * $this->price;
                $cSubtotal = PriceConverter::convert($subtotal, $gsCurrency->id);
                $cSubtotal += ($this->rev_tax_amount + $this->vat_amount);

                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($cSubtotal);
        }

        return parent::__get($name);
    }

    public function isAllowReceive()
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $dp = $this->dpPrDetail->directPurchase;
            $pr = $this->dpPrDetail->purchaseRequisition;

            if ($dp->is_rejected) {
                return false;
            } else if ($dp->is_canceled) {
                return false;
            } else if ($dp->is_received) {
                return false;
            } else if ($dp->is_delivered) {
                return false;
            } else if ($dp->is_approved_by_lp_manager) {
                $section = $activeUser->section;
                if ($section->is_exim || $section->is_ga_gs) {
                    /* cek apakah packaging component */
                    $budget = $pr->budgetFinal->budgetProposal->budget;
                    $packagingComponent = $budget->is_packaging_component;
                    if ($packagingComponent) {
                        return $section->is_exim ? true : false;
                    } else {
                        return $section->is_ga_gs ? true : false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function isAllowDeliver()
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $dp = $this->dpPrDetail->directPurchase;
            $pr = $this->dpPrDetail->purchaseRequisition;

            if ($dp->is_rejected) {
                return false;
            } else if ($dp->is_canceled) {
                return false;
            } else if ($dp->is_delivered) {
                return false;
            } else if ($dp->is_approved_by_lp_manager) {
                $proposal = $pr->budgetFinal->budgetProposal;
                $section_id = $proposal->section_id;
                if ($activeUser->section_id == $section_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function getPurchaseRequisition()
    {
        $dpPr = $this->dpPrDetail;
        return $dpPr->purchaseRequisition;
    }

    public function getDirectPurchase()
    {
        $dpPr = $this->dpPrDetail;
        return $dpPr->directPurchase;
    }

    public function getDpPrDetail()
    {
        return $this->hasOne(DpPrDetail::class, ['id' => 'dp_pr_detail_id']);
    }

    public function getGoodsService()
    {
        return $this->hasOne(GoodsService::class, ['id' => 'goods_service_id']);
    }

    public function getCarline()
    {
        return $this->hasOne(Carline::class, ['id' => 'carline_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $revTaxAmount = $this->price * $this->quantity * ($this->rev_tax_rate / 100);
        $vatAmount = $this->price * $this->quantity * ($this->vat_rate / 100);

        DpRealizationActual::updateAll([
            'rev_tax_amount' => $revTaxAmount,
            'vat_amount' => $vatAmount,
        ], ['id' => $this->id]);

        $dp = $this->dpPrDetail->directPurchase;

        $nItem = 0;
        $nReceived = 0;
        $nDelivered = 0;
        foreach ($dp->purchaseRequisitionList as $dpPr) {
            foreach ($dpPr->realizationActual as $actual) {
                if ($actual->is_complete_received) {
                    $nReceived++;
                }
                if ($actual->is_complete_delivered) {
                    $nDelivered++;
                }
                $nItem++;
            }
        }

        // update received
        if ($nItem == $nReceived) {
            $dp->is_received = 1;
            $dp->save();
        }

        // update delivered
        if ($nItem == $nDelivered) {
            $dp->is_delivered = 1;
            $dp->save();
        }

        // update closed
        if ($dp->is_delivered) {
            $dp->is_closed = 1;
            $dp->actual_time_arival = date('Y-m-d H:i:s');

            // calculate ontime
            $estimation = strtotime($dp->estimated_time_arival);
            $actual = strtotime($dp->actual_time_arival);
            $dp->is_ontime = ($actual < $estimation) ? 1 : 0;
            $dp->save();
        }

        // update amount actual
        $this->dpPrDetail->directPurchase->calculateAmountActual();

        parent::afterSave($insert, $changedAttributes);
    }
}
