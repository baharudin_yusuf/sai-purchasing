<?php

namespace app\models;

class BudgetTransfer extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_transfer';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'source', 'destination', 'amount', 'time', 'is_approved_by_source_manager', 'approved_by_source_manager_time', 'is_approved_by_destination_manager', 'approved_by_destination_manager_time', 'is_approved_by_fa', 'approved_by_fa_time', 'is_rejected', 'is_canceled');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }
}
