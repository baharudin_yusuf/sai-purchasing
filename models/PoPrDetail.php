<?php

namespace app\models;

class PoPrDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'po_pr_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'purchase_order_id', 'purchase_requisition_id');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getPurchaseOrder()
    {
        return $this->hasOne(PurchaseOrder::class, ['id' => 'purchase_order_id']);
    }

    public function getPurchaseRequisition()
    {
        return $this->hasOne(PurchaseRequisition::class, ['id' => 'purchase_requisition_id']);
    }

    public function getPurchaseRequisition_id()
    {
        return $this->hasOne(PurchaseRequisition::class, ['id' => 'purchase_requisition_id']);
    }

    public function getActualData()
    {
        return $this->hasMany(PoRealizationActual::class, ['po_pr_detail_id' => 'id']);
    }
}
