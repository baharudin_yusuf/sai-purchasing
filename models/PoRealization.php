<?php

namespace app\models;

class PoRealization extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'po_realization';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'purchase_order_id', 'supplier_invoice_number', 'invoice', 'is_final_payment', 'is_approved_by_manager', 'approved_by_manager_time', 'time', 'deadline', 'is_rejected', 'is_canceled', 'is_paid', 'prepared_by_user_id', 'approved_by_user_id', 'verified_by_user_id');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'code':
                return INVOICE_CODE . str_pad($this->id, 3, '0', STR_PAD_LEFT);

            case 'invoice_url':
                return url(PO_REALIZATION_PATH . "/" . $this->invoice . ".pdf");
        }

        return parent::__get($name);
    }

    public function getPurchaseOrder()
    {
        return $this->hasOne(PurchaseOrder::class, ['id' => 'purchase_order_id']);
    }

    public function getPreparedBy()
    {
        return $this->hasOne(User::class, ['id' => 'prepared_by_user_id']);
    }

    public function getApprovedBy()
    {
        return $this->hasOne(User::class, ['id' => 'approved_by_user_id']);
    }

    public function getVerifiedBy()
    {
        return $this->hasOne(User::class, ['id' => 'verified_by_user_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (get_called_class() == self::className()) {
            $this->purchaseOrder->paymentCompleteCheck();
        }
    }

    public function getPayment()
    {
        return $this->hasOne(PoPayment::class, ['po_realization_id' => 'id']);
    }
}
