<?php

namespace app\models;

class PrRevisionDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'pr_revision_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'pr_revision_id', 'purchase_requisition_detail_id', 'quantity'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getPurchaseRequisitionDetail()
    {
        return $this->hasOne(PurchaseRequisitionDetail::class, ['id' => 'purchase_requisition_detail_id']);
    }

    public function getPrRevision()
    {
        return $this->hasOne(PrRevision::class, ['id' => 'pr_revision_id']);
    }
}
