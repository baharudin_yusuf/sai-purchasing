<?php

namespace app\models;

use app\controllers\BaseController;
use app\helpers\PriceConverter;
use app\helpers\Tools;

class DirectPurchase extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'direct_purchase';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'is_approved_by_lp_manager', 'approved_by_lp_manager_time', 'comment', 'submission_time', 'is_rejected', 'is_received', 'is_delivered', 'is_canceled', 'amount_plan', 'amount_actual', 'amount_estimation');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_amount_estimation':
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($this->amount_estimation);

            case 'display_amount_plan':
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($this->amount_plan);

            case 'display_amount_actual':
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($this->amount_actual);

            case 'vat':
                $vat = 0;
                foreach ($this->purchaseRequisitionList as $pr) {
                    foreach ($pr->realizationActual as $actual) {
                        $vat += $actual->vat_amount;
                    }
                }
                return $vat;

            case 'rev_tax':
                $rev_tax = 0;
                foreach ($this->purchaseRequisitionList as $pr) {
                    foreach ($pr->realizationActual as $actual) {
                        $rev_tax += $actual->rev_tax_amount;
                    }
                }
                return $rev_tax;

            case 'vp_goods':
                return $this->getVpGoods();

            case 'vp_services':
                return $this->getVpServices();

            case 'display_rev_tax_amount':
                return $this->getRevTax(true, true);

            case 'display_vat_amount':
                return $this->getVAT(true, true);

            case 'display_vp_goods_amount':
                return $this->getVpGoods(true, true);

            case 'display_vp_services_amount':
                return $this->getVpServices(true, true);
        }

        return parent::__get($name);
    }

    public function getCode()
    {
        $no = str_pad($this->no, 3, '0', STR_PAD_LEFT);
        $section = 'PUR';
        $bulan = Tools::romanicNumber(date('n', strtotime($this->submission_time)));
        $tahun = date('y', strtotime($this->submission_time));
        return "DP-$no/$section-SAI/$bulan/$tahun";
    }

    public function getRealization()
    {
        return $this->hasMany(DpRealization::class, ['direct_purchase_id' => 'id']);
    }

    public function getDisplayRealization()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (get_session('include_rejected') === true) {
            unset($cond['is_rejected']);
        }

        if (get_session('only_rejected') === true) {
            $cond['is_rejected'] = 1;
        }

        if (get_session('include_canceled') === true) {
            unset($cond['is_canceled']);
        }

        if (get_session('only_canceled') === true) {
            $cond['is_canceled'] = 1;
        }

        return $this->getRealization()->where($cond)->all();
    }

    public function getPurchaseRequisitionList()
    {
        return $this->hasMany(DpPrDetail::class, ['direct_purchase_id' => 'id']);
    }

    private function getRevTax($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->realizationActual as $actual) {
                $total += $actual->rev_tax_amount;
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getVAT($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->realizationActual as $actual) {
                $total += $actual->vat_amount;
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getVpGoods($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $pr) {
            foreach ($pr->realizationActual as $actual) {
                $gs = $actual->goodsService;
                if ($gs->type == 'goods') {
                    $total += ($actual->quantity * $actual->price);
                }
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getVpServices($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $pr) {
            foreach ($pr->realizationActual as $actual) {
                $gs = $actual->goodsService;
                if ($gs->type == 'service') {
                    $total += ($actual->quantity * $actual->price);
                }
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    public function calculateAmountEstimation()
    {
        $this->refresh();
        $amount_estimation = 0;
        foreach ($this->purchaseRequisitionList as $pr) {
            foreach ($pr->realizationPlan as $plan) {
                $gs = $plan->goodsService;
                $currency_id = $gs->currency_id;

                $prDetail = PurchaseRequisitionDetail::findOne([
                    'purchase_requisition_id' => $pr->purchase_requisition_id,
                    'goods_service_id' => $gs->id
                ]);

                $price_estimation = BaseController::getConvertedPrice($prDetail->price_estimation, $currency_id);
                $amount_estimation += ($price_estimation * $plan->quantity);
            }
        }

        self::updateAll(['amount_estimation' => $amount_estimation], ['id' => $this->id]);
    }

    public function calculateAmountPlan()
    {
        $this->refresh();
        $amount_plan = 0;
        foreach ($this->purchaseRequisitionList as $pr) {
            foreach ($pr->realizationPlan as $plan) {
                $gs = $plan->goodsService;
                $currency_id = $gs->currency_id;

                $subtotal = ($plan->quantity * $plan->price) + $plan->tax;
                $amount_plan += BaseController::getConvertedPrice($subtotal, $currency_id);
            }
        }

        self::updateAll(['amount_plan' => $amount_plan], ['id' => $this->id]);
    }

    public function calculateAmountActual()
    {
        $this->refresh();
        $amount_actual = 0;
        foreach ($this->purchaseRequisitionList as $dpPr) {
            foreach ($dpPr->realizationActual as $actual) {
                $gs = $actual->goodsService;
                $currency_id = $gs->currency_id;

                $subtotal = ($actual->quantity * $actual->price) + $actual->tax;
                $amount_actual += BaseController::getConvertedPrice($subtotal, $currency_id);
            }
        }

        self::updateAll(['amount_actual' => $amount_actual], ['id' => $this->id]);
    }

    public function recalculatePrQuantity()
    {
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->realizationActual as $actual) {
                $prDetail = PurchaseRequisitionDetail::findOne([
                    'purchase_requisition_id' => $prList->purchase_requisition_id,
                    'goods_service_id' => $actual->goods_service_id
                ]);

                $prDetail->used_quantity += $actual->quantity;
                $prDetail->available_quantity -= $actual->quantity;
                $prDetail->save();
            }
            $prList->purchaseRequisition->recheckClosedStatus();
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $date = date('Y-m', strtotime($this->submission_time));
        $dateStart = "$date-01";
        $dateEnd = "$date-31";

        $num = DirectPurchase::find()
            ->where(['between', 'submission_time', $dateStart, $dateEnd])
            ->andWhere(['<', 'id', $this->id])
            ->count();
        DirectPurchase::updateAll(['no' => $num + 1], ['id' => $this->id]);

        $this->calculateAmountEstimation();
        $this->calculateAmountPlan();
        $this->calculateAmountActual();
    }
}
