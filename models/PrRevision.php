<?php

namespace app\models;

class PrRevision extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'pr_revision';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'purchase_requisition_id', 'date', 'is_approved_by_manager', 'approved_by_manager_time', 'is_canceled', 'is_rejected');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'code':
                return PR_REV_CODE . str_pad($this->id, 4, '0', STR_PAD_LEFT);;

            case 'rev_num':
                return $this->getRevisionDetail()->count();
        }
        return parent::__get($name);
    }

    public function getPurchaseRequisition()
    {
        return $this->hasOne(PurchaseRequisition::class, ['id' => 'purchase_requisition_id']);
    }

    public function getRevisionDetail()
    {
        return $this->hasMany(PrRevisionDetail::class, ['pr_revision_id' => 'id']);
    }
}
