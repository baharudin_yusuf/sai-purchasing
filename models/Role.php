<?php

namespace app\models;

use yii\db\ActiveRecord;

class Role extends ActiveRecord
{

    public static function tableName()
    {
        return 'role';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'alias', 'name'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function findByAlias($alias)
    {
        return self::findOne(['alias' => $alias]);
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $item) {
            $option[$item->id] = $item->name;
        }
        return $option;
    }

    public static function getIds($roleList)
    {
        $ids = [];
        $roleList = strtolower($roleList);
        $roleList = explode('|', $roleList);
        foreach ($roleList as $alias) {
            $role = self::findOne(['alias' => $alias]);
            if (!is_null($role)) {
                $ids[] = $role->id;
            }
        }
        return $ids;
    }

    public function allowTo($moduleName, $moduleItem, $departmentId, $sectionId)
    {
        $acl = ACLList::findOne(['module' => $moduleName, 'module_item' => $moduleItem]);
        if (is_null($acl)) {
            return false;
        }

        $departmentId = intval($departmentId);
        $sectionId = intval($sectionId);

        $condition = [
            'acl_list_id' => $acl->id,
            'role_id' => $this->id,
            'department_id' => $departmentId == 0 ? null : $departmentId,
            'section_id' => $sectionId == 0 ? null : $sectionId
        ];

        $access = ACLAccess::findOne($condition);
        if (is_null($access)) {
            return false;
        }

        return $access->is_access ? true : false;
    }
}
