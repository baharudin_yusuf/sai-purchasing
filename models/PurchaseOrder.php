<?php

namespace app\models;

use app\helpers\PriceConverter;
use app\helpers\Tools;
use app\models\master\Franco;
use app\models\master\Supplier;
use app\models\master\Term;

class PurchaseOrder extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'purchase_order';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'no', 'date_submit', 'is_approved_by_lp_manager', 'approved_by_lp_manager_time', 'refax_path', 'term_id', 'franco_id', 'supplier_id', 'estimated_time_arival', 'actual_time_arival', 'is_with_down_payment', 'down_payment_amount', 'amount_estimation', 'amount_realization', 'comment', 'is_closed', 'is_rejected', 'is_confirmed', 'is_received', 'is_delivered', 'is_canceled', 'is_ontime', 'is_revision', 'is_checked', 'is_payment_complete'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'vat':
                $vat = 0;
                foreach ($this->purchaseRequisitionList as $pr) {
                    foreach ($pr->actualData as $actual) {
                        $vat += $actual->vat_amount;
                    }
                }
                return $vat;

            case 'rev_tax':
                $rev_tax = 0;
                foreach ($this->purchaseRequisitionList as $pr) {
                    foreach ($pr->actualData as $actual) {
                        $rev_tax += $actual->rev_tax_amount;
                    }
                }
                return $rev_tax;

            case 'vp_goods':
                return $this->getVpGoods();

            case 'vp_services':
                return $this->getVpServices();

            case 'display_amount_estimation':
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($this->amount_estimation);

            case 'display_subtotal_amount_realization':
                return $this->getSubtotalAmountRealization(true, true);

            case 'display_amount_realization':
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($this->amount_realization);

            case 'display_down_payment_amount':
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($this->down_payment_amount);

            case 'display_rev_tax_amount':
                return $this->getRevTax(true, true);

            case 'display_vat_amount':
                return $this->getVAT(true, true);

            case 'display_vp_goods_amount':
                return $this->getVpGoods(true, true);

            case 'display_vp_services_amount':
                return $this->getVpServices(true, true);
        }

        return parent::__get($name);
    }

    private function getSubtotalAmountRealization($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->actualData as $actual) {
                $total += ($actual->quantity * $actual->price);
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getRevTax($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->actualData as $actual) {
                $total += $actual->rev_tax_amount;
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getVAT($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->actualData as $actual) {
                $total += $actual->vat_amount;
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getVpGoods($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $pr) {
            foreach ($pr->actualData as $actual) {
                $gs = $actual->goodsService;
                if ($gs->type == 'goods') {
                    $total += ($actual->quantity * $actual->price);
                }
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    private function getVpServices($formatted = false, $converted = false)
    {
        $total = 0;
        foreach ($this->purchaseRequisitionList as $pr) {
            foreach ($pr->actualData as $actual) {
                $gs = $actual->goodsService;
                if ($gs->type == 'service') {
                    $total += ($actual->quantity * $actual->price);
                }
            }
        }

        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }

        if ($formatted) {
            return $currency->name . ' ' . currency_format($total);
        } else {
            return $total;
        }
    }

    public function getTerm()
    {
        return $this->hasOne(Term::class, ['id' => 'term_id']);
    }

    public function getFranco()
    {
        return $this->hasOne(Franco::class, ['id' => 'franco_id']);
    }

    public function getCode()
    {
        $no = str_pad($this->no, 3, '0', STR_PAD_LEFT);
        $section = 'PUR';
        $bulan = Tools::romanicNumber(date('n', strtotime($this->date_submit)));
        $tahun = date('y', strtotime($this->date_submit));
        return "PO-$no/$section-SAI/$bulan/$tahun";
    }

    public function getPurchaseRequisitionList()
    {
        return $this->hasMany(PoPrDetail::class, ['purchase_order_id' => 'id']);
    }

    public function getSupplier()
    {
        return $this->hasOne(Supplier::class, ['id' => 'supplier_id']);
    }

    public function getRealization()
    {
        return $this->hasMany(PoRealization::class, ['purchase_order_id' => 'id']);
    }

    public function getDisplayRealization()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (get_session('include_rejected') === true) {
            unset($cond['is_rejected']);
        }

        if (get_session('only_rejected') === true) {
            $cond['is_rejected'] = 1;
        }

        if (get_session('include_canceled') === true) {
            unset($cond['is_canceled']);
        }

        if (get_session('only_canceled') === true) {
            $cond['is_canceled'] = 1;
        }

        return $this->getRealization()->where($cond)->all();
    }

    public function getRevision()
    {
        return $this->hasMany(PoRevision::class, ['purchase_order_id' => 'id']);
    }

    public function getDisplayRevision()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (get_session('include_rejected') === true) {
            unset($cond['is_rejected']);
        }

        if (get_session('only_rejected') === true) {
            $cond['is_rejected'] = 1;
        }

        if (get_session('include_canceled') === true) {
            unset($cond['is_canceled']);
        }

        if (get_session('only_canceled') === true) {
            $cond['is_canceled'] = 1;
        }

        return $this->getRevision()->where($cond)->all();
    }

    public function paymentCompleteCheck()
    {
        $this->refresh();
        $data = $this->getRealization()->where([
            'is_approved_by_manager' => 1,
            'is_rejected' => 0,
            'is_canceled' => 0,
            'is_paid' => 0,
        ])->count();

        if ($data == 0) {
            self::updateAll(['is_payment_complete' => 1], ['id' => $this->id]);
        } else {
            self::updateAll(['is_payment_complete' => 0], ['id' => $this->id]);
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->refresh();
        if ($this->is_approved_by_lp_manager) {
            PurchaseOrder::updateAll(['is_checked' => 1], ['id' => $this->id]);
        }

        $date = date('Y-m', strtotime($this->date_submit));
        $num = PurchaseOrder::find()
            ->where(['between', 'date_submit', "$date-01", "$date-31"])
            ->andWhere(['<', 'id', $this->id])
            ->count();
        PurchaseOrder::updateAll(['no' => $num + 1], ['id' => $this->id]);

        $this->paymentCompleteCheck();
    }

    public function getStatus()
    {
        if ($this->is_closed) {
            return 'Closed';
        } else if ($this->is_rejected) {
            return 'Rejected';
        } else if ($this->is_canceled) {
            return 'Canceled';
        } else if ($this->is_delivered) {
            return 'Delivered';
        } else if ($this->is_received) {
            return 'Received';
        } else if ($this->is_confirmed) {
            return 'Confirmed';
        } else {
            return 'Open';
        }
    }

    public function calculateAmountEstimation()
    {
        $this->refresh();
        $amountEstimation = 0;
        foreach ($this->purchaseRequisitionList as $poPr) {
            foreach ($poPr->actualData as $actual) {
                $gs = $actual->goodsService;
                $prDetail = PurchaseRequisitionDetail::findOne([
                    'purchase_requisition_id' => $poPr->purchase_requisition_id,
                    'goods_service_id' => $gs->id
                ]);

                $priceEstimation = PriceConverter::convert($prDetail->price_estimation, $gs->currency_id);
                $amountEstimation += ($priceEstimation * $actual->quantity);
            }
        }

        $this->amount_estimation = $amountEstimation;
        $this->save();
    }

    public function calculateAmountRealization()
    {
        $this->refresh();
        $amountRealization = 0;
        foreach ($this->purchaseRequisitionList as $poPr) {
            foreach ($poPr->actualData as $actual) {
                $gs = $actual->goodsService;
                $subtotal = ($actual->quantity * $actual->price) + $actual->rev_tax_amount + $actual->vat_amount;
                $amountRealization += PriceConverter::convert($subtotal, $gs->currency_id);
            }
        }

        $this->amount_realization = $amountRealization;
        $this->save();
    }

    public function recalculatePrQuantity()
    {
        foreach ($this->purchaseRequisitionList as $prList) {
            foreach ($prList->actualData as $actual) {
                $prDetail = PurchaseRequisitionDetail::findOne([
                    'purchase_requisition_id' => $prList->purchase_requisition_id,
                    'goods_service_id' => $actual->goods_service_id
                ]);

                $prDetail->used_quantity += $actual->quantity;
                $prDetail->available_quantity -= $actual->quantity;
                $prDetail->save();
            }
            $prList->purchaseRequisition->recheckClosedStatus();
        }
    }
}
