<?php

namespace app\models;

use app\helpers\PriceConverter;
use app\models\master\Carline;
use app\models\master\GoodsService;

class BudgetProposalDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_proposal_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'goods_service_id', 'quantity', 'price_estimation', 'carline_id', 'comment', 'is_approved_by_manager', 'approved_by_manager_time', 'is_approved_by_fa', 'approved_by_fa_time', 'budget_proposal_id', 'is_rejected', 'is_canceled'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_price_estimation':
                $currency = $this->goodsService->currency;
                return $currency->name . ' ' . currency_format($this->price_estimation);

            case 'display_converted_price_estimation':
                $currency = PriceConverter::getActiveBaseCurrency();
                if ($currency->id == $this->goodsService->currency->id) {
                    return '';
                }

                $converted = PriceConverter::convert($this->price_estimation, $currency->id);
                return $currency->name . ' ' . currency_format($converted);

            case 'display_sub_total':
                $currency = $this->goodsService->currency;
                $subtotal = $this->price_estimation * $this->quantity;
                return $currency->name . ' ' . currency_format($subtotal);

            case 'display_converted_sub_total':
                $currency = PriceConverter::getActiveBaseCurrency();
                if ($currency->id == $this->goodsService->currency->id) {
                    return '';
                }

                $subtotal = $this->price_estimation * $this->quantity;
                $converted = PriceConverter::convert($subtotal, $currency->id);
                return $currency->name . ' ' . currency_format($converted);
        }

        return parent::__get($name);
    }

    public function getGoodsService()
    {
        return $this->hasOne(GoodsService::class, ['id' => 'goods_service_id']);
    }

    public function getCarline()
    {
        return $this->hasOne(Carline::class, ['id' => 'carline_id']);
    }

    public function getBudgetProposal()
    {
        return $this->hasOne(BudgetProposal::class, ['id' => 'budget_proposal_id']);
    }
}
