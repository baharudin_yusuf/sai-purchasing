<?php

namespace app\models;

use app\controllers\BaseController;
use app\helpers\PriceConverter;
use app\models\master\Carline;
use app\models\master\GoodsService;

class DpRealizationPlan extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'dp_realization_plan';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'dp_pr_detail_id', 'goods_service_id', 'quantity', 'price', 'carline_id', 'rev_tax_name', 'rev_tax_rate', 'rev_tax_amount', 'vat_name', 'vat_rate', 'vat_amount');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'tax':
                return $this->rev_tax_amount + $this->vat_amount;

            case 'c_tax':
                $goodsService = $this->goodsService;
                return BaseController::getConvertedPrice($this->tax, $goodsService->currency_id);

            case 'c_price':
                $goodsService = $this->goodsService;
                return BaseController::getConvertedPrice($this->price, $goodsService->currency_id);

            case 'display_price':
                $currency = $this->goodsService->currency;
                return $currency->name . ' ' . currency_format($this->price);

            case 'display_converted_price':
                $gsCurrency = $this->goodsService->currency;
                $cPrice = PriceConverter::convert($this->price, $gsCurrency->id);
                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($cPrice);

            case 'display_tax':
                $currency = PriceConverter::getActiveBaseCurrency();
                $tax = $this->rev_tax_amount + $this->vat_amount;
                return $currency->name . ' ' . currency_format($tax);

            case 'display_subtotal':
                $gsCurrency = $this->goodsService->currency;
                $subtotal = $this->quantity * $this->price;
                $cSubtotal = PriceConverter::convert($subtotal, $gsCurrency->id);
                $cSubtotal += ($this->rev_tax_amount + $this->vat_amount);

                $currency = PriceConverter::getActiveBaseCurrency();
                return $currency->name . ' ' . currency_format($cSubtotal);
        }

        return parent::__get($name);
    }

    public function getDpPrDetail()
    {
        return $this->hasOne(DpPrDetail::class, ['id' => 'dp_pr_detail_id']);
    }

    public function getGoodsService()
    {
        return $this->hasOne(GoodsService::class, ['id' => 'goods_service_id']);
    }

    public function getCarline()
    {
        return $this->hasOne(Carline::class, ['id' => 'carline_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $revTaxAmount = $this->price * $this->quantity * ($this->rev_tax_rate / 100);
        $vatAmount = $this->price * $this->quantity * ($this->vat_rate / 100);

        DpRealizationPlan::updateAll([
            'rev_tax_amount' => $revTaxAmount,
            'vat_amount' => $vatAmount,
        ], ['id' => $this->id]);

        // update amount plan
        $this->dpPrDetail->directPurchase->calculateAmountPlan();

        parent::afterSave($insert, $changedAttributes);
    }
}
