<?php

namespace app\models;

use app\helpers\PriceConverter;
use app\helpers\Tools;
use app\models\master\Section;

class PurchaseRequisition extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'purchase_requisition';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'no', 'section_id', 'budget_final_id', 'price_estimation', 'date', 'is_approved_by_manager', 'approved_by_manager_time', 'is_approved_by_dfm', 'approved_by_dfm_time', 'is_approved_by_fm', 'approved_by_fm_time', 'is_approved_by_presdir', 'approved_by_presdir_time', 'is_approved_by_fa', 'approved_by_fa_time', 'comment', 'is_closed', 'is_direct_purchase', 'is_purchase_order', 'is_rejected', 'is_canceled', 'is_revision', 'is_checked'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'code':
                $no = str_pad($this->no, 3, '0', STR_PAD_LEFT);
                $section = $this->section->code;
                $bulan = Tools::romanicNumber(date('n', strtotime($this->date)));
                $tahun = date('y', strtotime($this->date));
                return "$no/$section-SAI/$bulan/$tahun";

            case 'display_price_estimation':
                $currencyUsed = $this->budgetFinal->budgetProposal->getCurrencyUsed();
                return $currencyUsed->name . ' ' . currency_format($this->price_estimation);

            case 'converted_price_estimation':
                $currencyUsed = $this->budgetFinal->budgetProposal->getCurrencyUsed();
                return PriceConverter::convert($this->price_estimation, $currencyUsed->id);
        }

        return parent::__get($name);
    }

    public function getSection()
    {
        return $this->hasOne(Section::class, ['id' => 'section_id']);
    }

    public function getBudget()
    {
        return $this->budgetFinal->budgetProposal->budget;
    }

    public function getBudgetFinal()
    {
        return $this->hasOne(BudgetFinal::class, ['id' => 'budget_final_id']);
    }

    public function getPurchaseRequisitionDetail()
    {
        return $this->hasMany(PurchaseRequisitionDetail::class, ['purchase_requisition_id' => 'id']);
    }

    public function getPrRevision()
    {
        return $this->hasMany(PrRevision::class, ['purchase_requisition_id' => 'id']);
    }

    public function getDisplayPrRevision()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (get_session('include_rejected') === true) {
            unset($cond['is_rejected']);
        }

        if (get_session('only_rejected') === true) {
            $cond['is_rejected'] = 1;
        }

        if (get_session('include_canceled') === true) {
            unset($cond['is_canceled']);
        }

        if (get_session('only_canceled') === true) {
            $cond['is_canceled'] = 1;
        }

        return $this->getPrRevision()->where($cond)->all();
    }

    public function getDisplayDetail()
    {
        $cond = [
            'is_rejected' => 0,
            'is_canceled' => 0
        ];

        if (get_session('include_rejected') === true) {
            unset($cond['is_rejected']);
        }

        if (get_session('only_rejected') === true) {
            $cond['is_rejected'] = 1;
        }

        if (get_session('include_canceled') === true) {
            unset($cond['is_canceled']);
        }

        if (get_session('only_canceled') === true) {
            $cond['is_canceled'] = 1;
        }

        return $this->getPurchaseRequisitionDetail()->where($cond)->all();
    }

    public function getNonCancelReject()
    {
        $cond = ['is_rejected' => 0, 'is_canceled' => 0];
        return $this->getPurchaseRequisitionDetail()->where($cond)->all();
    }

    public function getDpPrDetail()
    {
        return $this->hasMany(DpPrDetail::class, ['purchase_requisition_id' => 'id']);
    }

    public function getPoPrDetail()
    {
        return $this->hasMany(PoPrDetail::class, ['purchase_requisition_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($this->is_approved_by_manager || $this->is_approved_by_fa) {
            PurchaseRequisition::updateAll(['is_checked' => 1], ['id' => $this->id]);
        }

        $this->setPRNumber();
    }

    private function setPRNumber()
    {
        $date = date('Y-m', strtotime($this->date));
        $num = PurchaseRequisition::find()
            ->where(['between', 'date', "$date-01", "$date-31"])
            ->andWhere(['<', 'id', $this->id])
            ->count();
        PurchaseRequisition::updateAll(['no' => $num + 1], ['id' => $this->id]);
    }

    public function calculatePriceEstimation()
    {
        $total = 0;
        foreach ($this->purchaseRequisitionDetail as $prDetail) {
            $goodsService = $prDetail->goodsService;
            $cPrice = PriceConverter::convert($prDetail->price_estimation, $goodsService->currency_id);
            $total += ($cPrice * $prDetail->quantity);
        }

        $this->price_estimation = $total;
        $this->save();
    }

    public function recheckClosedStatus()
    {
        $isClosed = true;
        foreach ($this->purchaseRequisitionDetail as $list) {
            if ($list->available_quantity > 0) {
                $isClosed = false;
            }
        }
        $this->is_closed = $isClosed ? 1 : 0;
        $this->save();
    }
}
