<?php

namespace app\models;

use app\helpers\Auth;
use app\helpers\PriceConverter;
use app\models\master\Carline;
use app\models\master\GoodsService;

class PoRealizationActual extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'po_realization_actual';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'po_pr_detail_id', 'goods_service_id', 'quantity', 'received', 'delivered', 'price', 'carline_id', 'rev_tax_name', 'rev_tax_rate', 'rev_tax_amount', 'vat_name', 'vat_rate', 'vat_amount');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'is_complete':
                return ($this->received == $this->delivered) && ($this->delivered == $this->quantity);

            case 'is_complete_received':
                return $this->received == $this->quantity;

            case 'is_complete_delivered':
                return $this->delivered == $this->quantity;

            case 'display_price':
                return $this->calculatePrice(true, false);

            case 'display_converted_price':
                return $this->calculatePrice(true, true);

            case 'display_amount':
                return $this->calculateAmount(true, false);

            case 'display_converted_amount':
                return $this->calculateAmount(true, true);

            case 'display_tax':
                return $this->calculateTax(true, false);

            case 'display_converted_tax':
                return $this->calculateTax(true, true);
        }

        return parent::__get($name);
    }

    private function calculateTax($formatted = false, $converted = false)
    {
        $total = $this->rev_tax_amount + $this->vat_amount;
        $currency = PriceConverter::getActiveBaseCurrency();
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }
        return $formatted ? $currency->name . ' ' . currency_format($total) : $total;
    }

    private function calculatePrice($formatted = false, $converted = false)
    {
        $total = $this->price;
        $currency = $this->goodsService->currency;
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }
        return $formatted ? $currency->name . ' ' . currency_format($total) : $total;
    }

    private function calculateAmount($formatted = false, $converted = false)
    {
        $total = $this->price * $this->quantity;
        $currency = $this->goodsService->currency;
        if ($converted) {
            $total = PriceConverter::convert($total, $currency->id);
        }
        return $formatted ? $currency->name . ' ' . currency_format($total) : $total;
    }

    public function isAllowReceive()
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $po = $this->poPrDetail->purchaseOrder;
            $pr = $this->poPrDetail->purchaseRequisition;

            if ($po->is_rejected) {
                return false;
            } else if ($po->is_canceled) {
                return false;
            } else if ($po->is_received) {
                return false;
            } else if ($po->is_delivered) {
                return false;
            } else if ($po->is_approved_by_lp_manager) {
                $section = $activeUser->section;
                if ($section->is_exim || $section->is_ga_gs) {
                    /* cek apakah packaging component */
                    $budget = $pr->budgetFinal->budgetProposal->budget;
                    $packagingComponent = $budget->is_packaging_component;
                    if ($packagingComponent) {
                        return $section->is_exim ? true : false;
                    } else {
                        return $section->is_ga_gs ? true : false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function isAllowDeliver()
    {
        $activeUser = Auth::user();
        if (!$activeUser->roleAs('section')) {
            return false;
        } else {
            $po = $this->poPrDetail->purchaseOrder;
            $pr = $this->poPrDetail->purchaseRequisition;

            if ($po->is_rejected) {
                return false;
            } else if ($po->is_canceled) {
                return false;
            } else if ($po->is_delivered) {
                return false;
            } else if ($po->is_approved_by_lp_manager) {
                $proposal = $pr->budgetFinal->budgetProposal;
                $section_id = $proposal->section_id;
                if ($activeUser->section_id == $section_id) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    public function getPurchaseRequisition()
    {
        return $this->poPrDetail->purchaseRequisition;
    }

    public function getPurchaseOrder()
    {
        $poPr = $this->poPrDetail;
        return $poPr->purchaseOrder;
    }

    public function getPoPrDetail()
    {
        return $this->hasOne(PoPrDetail::class, ['id' => 'po_pr_detail_id']);
    }

    public function getGoodsService()
    {
        return $this->hasOne(GoodsService::class, ['id' => 'goods_service_id']);
    }

    public function getCarline()
    {
        return $this->hasOne(Carline::class, ['id' => 'carline_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        // update tax
        $revTaxAmount = $this->price * $this->quantity * ($this->rev_tax_rate / 100);
        $vatAmount = $this->price * $this->quantity * ($this->vat_rate / 100);

        self::updateAll([
            'rev_tax_amount' => $revTaxAmount,
            'vat_amount' => $vatAmount,
        ], ['id' => $this->id]);

        $po = $this->poPrDetail->purchaseOrder;

        $nItem = 0;
        $nReceived = 0;
        $nDelivered = 0;
        foreach ($po->purchaseRequisitionList as $poPr) {
            foreach ($poPr->actualData as $actual) {
                if ($actual->is_complete_received) {
                    $nReceived++;
                }
                if ($actual->is_complete_delivered) {
                    $nDelivered++;
                }
                $nItem++;
            }
        }

        // update received
        if ($nItem == $nReceived) {
            $po->is_received = 1;
            $po->save();
        }

        // update delivered
        if ($nItem == $nDelivered) {
            $po->is_delivered = 1;
            $po->save();
        }

        // update closed
        if ($po->is_delivered) {
            $po->is_closed = 1;
            $po->actual_time_arival = date('Y-m-d H:i:s');

            // calculate ontime
            $estimation = strtotime($po->estimated_time_arival);
            $actual = strtotime($po->actual_time_arival);
            $po->is_ontime = ($actual < $estimation) ? 1 : 0;
            $po->save();
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
