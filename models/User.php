<?php

namespace app\models;

use app\models\master\Department;
use app\models\master\Section;

class User extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'user';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'name', 'email', 'password', 'role', 'registration_date', 'section_id', 'department_id', 'is_administration', 'is_supervisor', 'profile_picture', 'signature');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'profile_picture_file':
                $path = PROFILE_PICTURE_PATH . '/' . $this->profile_picture;
                if (is_file($path)) {
                    return $path;
                } else {
                    return null;
                }

            case 'profile_picture_url':
                if (is_null($this->profile_picture_file)) {
                    return url('assets/img/user.jpg');
                }

                $path = PROFILE_PICTURE_PATH . '/' . $this->profile_picture;
                return url($path);

            case 'signature_file':
                $path = SIGNATURE_PATH . '/' . $this->signature;
                if (is_file($path)) {
                    return $path;
                } else {
                    return null;
                }

            case 'signature_url':
                if (is_null($this->signature_file)) {
                    return url('assets/image/blank.png');
                }

                $path = SIGNATURE_PATH . '/' . $this->signature;
                return url($path);

        }

        return parent::__get($name);
    }

    public function getRxle()
    {
        return $this->hasOne(Role::class, ['id' => 'role']);
    }

    public function getSection()
    {
        return $this->hasOne(Section::class, ['id' => 'section_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    public function roleAs($roleAliases)
    {
        $roleAliases = strtolower($roleAliases);
        $roleAliases = explode('|', $roleAliases);
        if (in_array($this->rxle->alias, $roleAliases)) {
            return true;
        } else {
            return false;
        }
    }

    public function allowTo($moduleName, $subModule)
    {
        return $this->rxle->allowTo($moduleName, $subModule, $this->department_id, $this->section_id);
    }

    public function getMeta()
    {
        return $this->hasOne(UserMeta::class, ['user_id' => 'id']);
    }

    public function getDataArray()
    {
        $data = $this->getAttributes();

        $section = $this->section;
        $data['section_code'] = is_null($section) ? null : $section->code;
        $data['section_name'] = is_null($section) ? null : $section->name;

        $department = $this->department;
        $data['department_code'] = is_null($department) ? null : $department->code;
        $data['department_name'] = is_null($department) ? null : $department->name;

        return $data;
    }

}
