<?php

namespace app\models;

class BudgetAdditional extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_additional';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'budget_final_id', 'additional_amount', 'is_approved_by_manager', 'approved_by_manager_time', 'is_approved_by_fa', 'approved_by_fa_time', 'time', 'is_rejected', 'is_canceled'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }
}
