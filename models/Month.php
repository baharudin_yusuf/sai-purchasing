<?php

namespace app\models;

class Month
{
    public static function getOption()
    {
        $month = [];
        for ($i = 1; $i <= 12; $i++) {
            $month[$i] = date('F', strtotime("2000-$i-01"));
        }
        return $month;
    }
}
