<?php

namespace app\models\master;

use app\models\BaseModel;
use app\models\Month;

class BudgetPeriod extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_period';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'name', 'year', 'month', 'period_id');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_month':
                $option = Month::getOption();
                return isset($option[$this->month]) ? $option[$this->month] : null;

        }

        return parent::__get($name);
    }

    public static function getOption($periodId)
    {
        $option = [];
        $data = self::find()->where(['period_id' => $periodId])->all();
        foreach ($data as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }

    public function getPeriod()
    {
        return $this->hasOne(Period::class, ['id' => 'period_id']);
    }
}
