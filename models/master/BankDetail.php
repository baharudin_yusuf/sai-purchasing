<?php

namespace app\models\master;

use app\models\BaseModel;

class BankDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'bank_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'bank_id', 'account_number', 'holder_name');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getBank()
    {
        return $this->hasOne(Bank::class, ['id' => 'bank_id']);
    }

    public static function getOption($bankId)
    {
        $option = [];
        $data = self::find()->where(['bank_id' => $bankId])->all();
        foreach ($data as $item) {
            $option[$item->id] = $item->account_number . ' - ' . $item->holder_name;
        }
        return $option;
    }
}
