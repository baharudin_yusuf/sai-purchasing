<?php

namespace app\models\master;

use app\models\BaseModel;

class Period extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'period';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('name', 'date_start', 'date_end', 'is_active', 'is_future');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_name':
                return $this->name . ' [' . $this->date_start . ' - ' . $this->date_end . ']';
        }
        return parent::__get($name); // TODO: Change the autogenerated stub
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $list) {
            $option[$list->id] = $list->name . ' [' . $list->date_start . ' - ' . $list->date_end . ']';
        }

        return $option;
    }

    public function getCurrencyDetail()
    {
        return $this->hasMany(CurrencyDetail::class, ['period_id' => 'id']);
    }

    public function getBudgetPeriod()
    {
        return $this->hasMany(BudgetPeriod::class, ['period_id' => 'id']);
    }

    public function getBudgetCategoryPeriod()
    {
        return $this->hasMany(BudgetCategoryPeriod::class, ['period_id' => 'id']);
    }

    public function getBudgetLimit()
    {
        return $this->hasMany(BudgetLimit::class, ['period_id' => 'id']);
    }

    public function getCurrencyDetailRate($currencyId)
    {
        $data = $this->getCurrencyDetail()->where(['currency_id' => $currencyId])->one();
        if (!is_null($data)) {
            return $data->current_rate;
        }

        $currency = Currency::findOne($currencyId);
        return is_null($currency) ? new Currency() : $currency->current_rate;
    }

    public function getCurrencyDetailBase()
    {
        $data = $this->getCurrencyDetail()->where(['is_base_currency' => 1])->one();
        if (!is_null($data)) {
            return $data->currency;
        }

        $currency = Currency::findOne(['is_base_currency' => 1]);
        return is_null($currency) ? new Currency() : $currency;
    }
}
