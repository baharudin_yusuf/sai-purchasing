<?php

namespace app\models\master;

use app\models\BaseModel;

class BudgetLimit extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_limit';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'budget_id', 'period_id', 'amount', 'comment', 'currency_id'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getBudget()
    {
        return $this->hasOne(Budget::class, ['id' => 'budget_id']);
    }

    public function getPeriod()
    {
        return $this->hasOne(Period::class, ['id' => 'period_id']);
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_amount':
                $curr = $this->currency->name;
                return $curr . ' ' . currency_format($this->amount);

            case 'display_budget':
                $budget = $this->budget;
                return '[' . $budget->number . '] ' . $budget->name;
        }

        return parent::__get($name);
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $list) {
            $option[$list->id] = $list->name;
        }
        return $option;
    }
}
