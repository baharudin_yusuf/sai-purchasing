<?php

namespace app\models\master;

use app\models\BaseModel;

class PaymentMethod extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'payment_method';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'name', 'is_non_bank');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $item) {
            $option[$item->id] = $item->name;
        }
        return $option;
    }
}
