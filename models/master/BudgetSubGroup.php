<?php

namespace app\models\master;

use app\models\BaseModel;

class BudgetSubGroup extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_sub_group';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'budget_group_id', 'name');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getGroup()
    {
        return $this->hasOne(BudgetGroup::class, ['id' => 'budget_group_id']);
    }

    public function getBudget()
    {
        return $this->hasMany(Budget::class, ['budget_sub_group_id' => 'id']);
    }

    public static function getOption($groupId)
    {
        $option = [];
        $data = self::find()->where(['budget_group_id' => $groupId])->all();
        foreach ($data as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }
}
