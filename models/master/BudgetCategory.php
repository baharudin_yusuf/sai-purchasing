<?php

namespace app\models\master;

use app\models\BaseModel;

class BudgetCategory extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_category';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'name');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }

    public function getBudgetCategoryPeriod()
    {
        return $this->hasMany(BudgetCategoryPeriod::class, ['budget_category_id' => 'id']);
    }

    public function getBudgetGroup()
    {
        return $this->hasMany(BudgetGroup::class, ['budget_category_id' => 'id']);
    }
}
