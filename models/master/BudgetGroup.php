<?php

namespace app\models\master;

use app\models\BaseModel;

class BudgetGroup extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget_group';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'budget_category_id', 'name');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getCategory()
    {
        return $this->hasOne(BudgetCategory::class, ['id' => 'budget_category_id']);
    }

    public function getBudgetSubGroup()
    {
        return $this->hasMany(BudgetSubGroup::class, ['budget_group_id' => 'id']);
    }

    public static function getOption($categoryId)
    {
        $option = [];
        $data = self::find()->where(['budget_category_id' => $categoryId])->all();
        foreach ($data as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }
}
