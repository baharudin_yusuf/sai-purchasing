<?php

namespace app\models\master;

use app\helpers\PriceConverter;
use app\models\BaseModel;
use app\models\BudgetProposal;

class Budget extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'budget';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'budget_sub_group_id', 'number', 'name', 'amount', 'is_packaging_component', 'comment');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getSubGroup()
    {
        return $this->hasOne(BudgetSubGroup::class, ['id' => 'budget_sub_group_id']);
    }

    public function getBudgetLimit()
    {
        return $this->hasMany(BudgetLimit::class, ['budget_id' => 'id']);
    }

    public static function getOption()
    {
        $option = [];
        foreach (Budget::find()->all() as $list) {
            $option[$list->id] = "[" . $list->number . "] " . "$list->name";
        }

        return $option;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_name':
                return '[' . $this->number . '] ' . $this->name;

            case 'display_amount':
                $futurePeriod = Period::findOne(['is_future' => 1]);
                if (is_null($futurePeriod)) {
                    return '-';
                }

                $budgetLimit = $this->getBudgetLimit()->where(['period_id' => $futurePeriod->id])->one();
                if (is_null($budgetLimit)) {
                    return '-';
                }

                return $budgetLimit->currency->name . ' ' . currency_format($budgetLimit->amount);
        }

        return parent::__get($name);
    }

    public function getAvailableAmount()
    {
        // kalkulasi budget yang digunakan pada periode kedepan
        $period = Period::findOne(['is_future' => 1]);
        if (is_null($period)) {
            return 0;
        }

        $usedAmount = 0;
        foreach ($period->budgetPeriod as $budgetPeriod) {
            $budgetProposal = BudgetProposal::find()->where([
                'is_rejected' => 0,
                'is_expired' => 0,
                'budget_id' => $this->id,
                'budget_period_id' => $budgetPeriod->id
            ])->all();

            foreach ($budgetProposal as $bPropItem) {
                $bpDetails = $bPropItem->getDetail()->where(['is_rejected' => 0, 'is_canceled' => 0])->all();
                foreach ($bpDetails as $bpDetail) {
                    // convert price to base currency
                    $goodsService = $bpDetail->goodsService;
                    $priceEstimation = $bpDetail->price_estimation;
                    $cPriceEstimation = PriceConverter::convert($priceEstimation, $goodsService->currency_id);
                    $usedAmount += ($cPriceEstimation * $bpDetail->quantity);
                }
            }
        }

        // ambil nilai maksimal budget
        $budgetLimit = BudgetLimit::findOne(['budget_id' => $this->id, 'period_id' => $period->id]);
        if (is_null($budgetLimit)) {
            return 0;
        }

        $cAmount = PriceConverter::convert($budgetLimit->amount, $budgetLimit->currency_id);
        $available = $cAmount - $usedAmount;

        return $available;
    }
}
