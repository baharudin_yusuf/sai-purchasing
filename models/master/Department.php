<?php

namespace app\models\master;

use app\models\BaseModel;

class Department extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'department';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'code', 'name', 'is_fa', 'is_ga_gs', 'is_under_dfm', 'is_under_fm', 'is_under_presdir');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function getSection()
    {
        return $this->hasMany(Section::class, ['department_id' => 'id']);
    }

    public static function findByCode($code)
    {
        return self::findOne(['code' => $code]);
    }

    public static function getOption()
    {
        $data = Department::find()->asArray()->orderBy(['name' => SORT_ASC])->all();
        $option = [];
        foreach ($data as $list) {
            extract($list);

            $option[$id] = $name;
        }

        return $option;
    }
}
