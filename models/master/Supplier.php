<?php

namespace app\models\master;

use app\models\BaseModel;
use app\models\PurchaseOrder;

class Supplier extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'supplier';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'supplier_category_id', 'name', 'address', 'phone1', 'phone2', 'phone3', 'fax', 'email', 'authorized_person', 'authorized_person_cellphone', 'siup', 'tdp', 'npwp', 'sppkp', 'is_pkp', 'siup_path', 'tdp_path', 'npwp_path', 'sppkp_path', 'skt_badan_path', 'skt_pribadi_path', 'non_pkp_path', 'deadline_day', 'website');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'siup_file_url':
            case 'tdp_file_url':
            case 'npwp_file_url':
            case 'sppkp_file_url':
            case 'skt_badan_file_url':
            case 'skt_pribadi_file_url':
            case 'non_pkp_file_url':
                $field = str_replace('file_url', 'path', $name);
                $constant = strtoupper($field);
                if (defined($constant)) {
                    $uploadPath = constant($constant);
                    $filePath = $uploadPath . '/' . $this->getAttribute($field);
                    if (is_file($filePath)) {
                        return url($filePath);
                    }
                }
                return null;
        }

        return parent::__get($name);
    }

    public function getSupplierCategory()
    {
        return $this->hasOne(SupplierCategory::class, ['id' => 'supplier_category_id']);
    }

    public function getPurchaseOrder()
    {
        return $this->hasMany(PurchaseOrder::class, ['supplier_id' => 'id']);
    }

    public function getTotalAmountTransaction()
    {
        $po = PurchaseOrder::find()->where(['is_closed' => 1, 'supplier_id' => $this->id])->all();
        $total = 0;
        foreach ($po as $item) {
            $total += $item->amount_realization;
        }

        return $total;
    }

    public static function getOption()
    {
        $option = [];
        foreach (Supplier::find()->all() as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }
}
