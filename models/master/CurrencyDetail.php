<?php

namespace app\models\master;

use app\models\BaseModel;

class CurrencyDetail extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'currency_detail';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'currency_id', 'period_id', 'current_rate', 'is_base_currency'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    public function getPeriod()
    {
        return $this->hasOne(Period::class, ['id' => 'period_id']);
    }
}
