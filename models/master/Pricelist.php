<?php

namespace app\models\master;

use app\models\BaseModel;

class Pricelist extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'pricelist';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = ['id', 'goods_service_id', 'quantity', 'market_price', 'supplier_id', 'is_package', 'add_time', 'update_time', 'revenue_tax_id', 'value_added_tax_id'];

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_market_price':
                $currency = $this->goodsService->currency->name;
                return $currency . ' ' . currency_format($this->market_price);
        }

        return parent::__get($name);
    }

    public function getGoodsService()
    {
        return $this->hasOne(GoodsService::class, ['id' => 'goods_service_id']);
    }

    public function getSupplier()
    {
        return $this->hasOne(Supplier::class, ['id' => 'supplier_id']);
    }

    public function getRevenueTax()
    {
        return $this->hasOne(RevenueTax::class, ['id' => 'revenue_tax_id']);
    }

    public function getValueAddedTax()
    {
        return $this->hasOne(ValueAddedTax::class, ['id' => 'value_added_tax_id']);
    }
}
