<?php

namespace app\models\master;

use app\models\BaseModel;

class GoodsService extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'goods_service';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'budget_id', 'name', 'unit_id', 'specification', 'currency_id', 'is_po', 'is_dp', 'type');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public function __get($name)
    {
        switch ($name) {
            case 'display_budget':
                $budget = $this->budget;
                return '[' . $budget->number . '] ' . $budget->name;
        }

        return parent::__get($name);
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $list) {
            $option[$list->id] = $list->name;
        }

        return $option;
    }

    public function getCurrency()
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    public function getBudget()
    {
        return $this->hasOne(Budget::class, ['id' => 'budget_id']);
    }

    public function getUnit()
    {
        return $this->hasOne(Unit::class, ['id' => 'unit_id']);
    }

    public function getPricelist()
    {
        return $this->hasMany(Pricelist::class, ['goods_service_id' => 'id']);
    }

    public function getPricelistMaxPrice()
    {
        $data = $this->getPricelist()->select('max(market_price) as market_price')->one();
        return is_null($data) ? 0 : $data->market_price;
    }

    public static function generateTypeOption()
    {
        return [
            'goods' => 'Goods',
            'service' => 'Service'
        ];
    }
}
