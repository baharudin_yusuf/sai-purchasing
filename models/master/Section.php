<?php

namespace app\models\master;

use app\models\BaseModel;

class Section extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'section';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'department_id', 'code', 'name', 'is_exim', 'is_ga_gs');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function findByCode($code)
    {
        return self::findOne(['code' => $code]);
    }

    public static function getOption($departmentId)
    {
        $option = [];
        foreach (self::find()->where(['department_id' => $departmentId])->all() as $item) {
            $option[$item->id] = $item->name;
        }
        return $option;
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }
}
