<?php

namespace app\models\master;

use app\models\BaseModel;

class Currency extends BaseModel
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        parent::initChild($this);
        return [];
    }

    public static function tableName()
    {
        return 'currency';
    }

    public static function primaryKey()
    {
        $primary_key = 'id';
        return array($primary_key);
    }

    public function attributeLabels()
    {
        $field = array('id', 'name', 'is_base_currency', 'current_rate', 'last_updated_rate');

        $attributeLabels = array();
        foreach ($field as $key) {
            $attributeLabels[$key] = $key;
        }

        return $attributeLabels;
    }

    public static function getOption()
    {
        $option = [];
        foreach (self::find()->all() as $list) {
            $option[$list->id] = $list->name;
        }
        return $option;
    }

    public static function getActiveCurrency()
    {
        $period = Period::findOne(['is_active' => 1]);
        if (is_null($period)) {
            return null;
        }

        $currencyDetail = CurrencyDetail::findOne(['period_id' => $period->id, 'is_base_currency' => 1]);
        if (is_null($currencyDetail)) {
            return null;
        }

        return $currencyDetail->currency;
    }
}
